#ifndef TW_NRF_EVENT_STRING_H
#define TW_NRF_EVENT_STRING_H

extern void nrf_events_strings_log(uint16_t evt_id);
extern void nrf_sys_events_strings_log(uint32_t evt_id);
#endif

