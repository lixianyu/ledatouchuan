
#ifndef __LEDA_AVR_H__
#define __LEDA_AVR_H__

#include "stdint.h"
#include "mimas_ble_nus.h"

/******************* Macro defination *************************************/

/*****************************************/

/******************* Function defination **********************************/
extern uint32_t avr_uart_init(uint32_t baudrate);
extern void avr_uart_uninit(void);
#endif //__LEDA_AVR_H__

