#ifndef __TETHYS_LED_H__
#define __TETHYS_LED_H__

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

typedef void (*LED_DH_callback)(void);
/******************************************************************************/
/************************ Functions Declarations ******************************/
/******************************************************************************/
extern void init_LED(bool flag);
extern void open_LEDs(void);
extern void open_LED(uint8_t led);
extern void close_LED(uint8_t led);
extern void invert_LED(uint8_t led);
extern void close_LEDS(void);

extern void led_blink(uint8_t lednumber);
extern void led_blink_stop(void);
extern void led_blink_error(uint32_t ledmask);
extern void led_blink_alert(uint16_t hang);
extern void led_blink_kaiji(void);
extern void led_blink_connected(void);
extern void led_Softblink_start(void);
extern void led_Softblink_stop(void);
extern void led_Softblink_charge_start(uint32_t ledmask);
extern void led_Softblink_charge_stop(void);
extern void led_pwm_init(void);
extern void led_pwm_start(void);
extern void led_pwm_stop(void);
extern void led_pwm_init_1(void);
extern void led_pwm_start_1(void);
extern void led_pwm_stop_1(void);
#endif
