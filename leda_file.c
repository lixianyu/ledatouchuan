#include <stdlib.h>
#include "pstorage.h"
#include "leda_file.h"
#include "app_timer.h"
#include "mimas_config.h"
#include "mimas_log.h"
#include "tethys_led.h"
#include "leda_communicate_protocol.h"
#include "leda_queue.h"

extern uint32_t gBaudrate;

typedef enum
{
    TETHYS_PSTORAGE_NOTHING,
    TETHYS_PSTORAGE_UPDATE_TEMP_PRE,
    TETHYS_PSTORAGE_,
} TETHYS_PSTORAGE_STATE_t;

TETHYS_PSTORAGE_STATE_t g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;

uint32_t gTemperatureOffsetCount = 0;

static void pstorage_callback_handler_lxy1(pstorage_handle_t *p_handle,
                                      uint8_t             op_code,
                                      uint32_t            result,
                                      uint8_t            *p_data,
                                      uint32_t            data_len);
static void pstorage_callback_handler_temp(pstorage_handle_t *p_handle,
                                      uint8_t             op_code,
                                      uint32_t            result,
                                      uint8_t            *p_data,
                                      uint32_t            data_len);
static pstorage_handle_t g_storage_handle_lxy1;
static pstorage_handle_t g_storage_handle_temperature;
static int32_t g_adcT, g_curTOffset;

int32_t leda_storage_get_temp_offset(int32_t adcTemp)
{
    pstorage_handle_t block_handle;
    bool flagIfGot = false;
    int32_t adcT;
    uint32_t i,iEnd;
    int32_t subValue = 0;
    int32_t subValueMin = 0;
    bool ifFirst = true;
    int32_t offsetClosest = 0;
    pstorage_block_identifier_get(&g_storage_handle_temperature, 0, &block_handle);
    iEnd = gTemperatureOffsetCount * 8;
    for (i = 0; i < iEnd; i += 8)
    {
        pstorage_load((uint8_t*)&adcT, &block_handle, 4, i);
        if (adcTemp == adcT)
        {
            flagIfGot = true;
            break;
        }
        if (ifFirst)
        {
            ifFirst = false;
            subValueMin = abs(adcT - adcTemp);
            pstorage_load((uint8_t*)&offsetClosest, &block_handle, 4, i + 4);
        }
        else
        {
            subValue = abs(adcT - adcTemp);
            if (subValue < subValueMin)
            {
                subValueMin = subValue;
                pstorage_load((uint8_t*)&offsetClosest, &block_handle, 4, i + 4);
            }
        }
    }
    if (flagIfGot)
    {
        pstorage_load((uint8_t*)&offsetClosest, &block_handle, 4, i + 4);
    }
    return offsetClosest;
}

/*
 * adcTemp: 由ADC得到的温度
 * curTempOffset : 当前环境实际温度
 */
void leda_storage_update_temp_offset(int32_t adcTemp, int32_t curTemp)
{
    pstorage_handle_t block_handle;
    bool flagIfUpdate = false;
    int32_t adcT;
    uint32_t i,iEnd;
    int32_t curTempOffset = curTemp - adcTemp;
        
    pstorage_block_identifier_get(&g_storage_handle_temperature, 0, &block_handle);
    iEnd = gTemperatureOffsetCount * 8;
    for (i = 0; i < iEnd; i += 8)
    {
        pstorage_load((uint8_t*)&adcT, &block_handle, 4, i);
        //pstorage_load((uint8_t*)&curT, &block_handle, 4, i + 4);
        if (adcT == adcTemp)
        {
            flagIfUpdate = true;
            g_adcT = adcTemp;
            g_curTOffset = curTempOffset;
            break;
        }
    }
    if (flagIfUpdate)
    {
        pstorage_update(&block_handle, (uint8_t*)&g_adcT, 4, i);
        pstorage_update(&block_handle, (uint8_t*)&g_curTOffset, 4, i + 4);
    }
    else
    {
        g_adcT = adcTemp;
        g_curTOffset = curTempOffset;
        pstorage_store(&block_handle, (uint8_t*)&g_adcT, 4, iEnd);
        pstorage_store(&block_handle, (uint8_t*)&g_curTOffset, 4, iEnd + 4);
        pstorage_block_identifier_get(&g_storage_handle_lxy1, BLOCK_NUM_TEMP_OFFSET_COUNT, &block_handle);
        gTemperatureOffsetCount++;
        pstorage_update(&block_handle, (uint8_t*)&gTemperatureOffsetCount, 4, 0);
    }
}

static void leda_init_storage_temperature(void)
{
    uint32_t err_code;
    pstorage_module_param_t param;
    param.block_size  = 1024;//4
    param.block_count = 1;
    param.cb          = pstorage_callback_handler_temp;
    err_code = pstorage_register(&param, &g_storage_handle_temperature);
    APP_ERROR_CHECK(err_code);
}

static void leda_init_storage_lxy1(void)
{
    uint32_t err_code;
    pstorage_module_param_t param;
    param.block_size  = BLOCK_SIZE;//4
    param.block_count = BLOCK_COUNT;
    param.cb          = pstorage_callback_handler_lxy1;
    err_code = pstorage_register(&param, &g_storage_handle_lxy1);
    APP_ERROR_CHECK(err_code);

    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_handle_lxy1, BLOCK_NUM_BAUDRATE, &block_handle);
    uint32_t baudrate;
    pstorage_load((uint8_t*)&baudrate, &block_handle, 4, 0);
    if (baudrate != 0xFFFFFFFF)
    {
        gBaudrate = baudrate;
    }
    ///////////////////////////////////////////////////////////////////////////////////////
    
    pstorage_block_identifier_get(&g_storage_handle_lxy1, BLOCK_NUM_TEMP_OFFSET_COUNT, &block_handle);
    pstorage_load((uint8_t*)&gTemperatureOffsetCount, &block_handle, 4, 0);
    if (gTemperatureOffsetCount == 0xFFFFFFFF)
    {
        gTemperatureOffsetCount = 0;
    }
}

void leda_storage_update_baudrate(void)
{
    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_handle_lxy1, BLOCK_NUM_BAUDRATE, &block_handle);
    pstorage_update(&block_handle, (uint8_t*)&gBaudrate, 4, 0);
}

void leda_pstorage_init(void)
{
    leda_init_storage_temperature();
    leda_init_storage_lxy1();
}

static void pstorage_callback_handler_lxy1(pstorage_handle_t *p_handle,
                                      uint8_t             op_code,
                                      uint32_t            result,
                                      uint8_t            *p_data,
                                      uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:

        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            //app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void pstorage_callback_handler_temp(pstorage_handle_t *p_handle,
                                      uint8_t             op_code,
                                      uint32_t            result,
                                      uint8_t            *p_data,
                                      uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:

        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            //app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static int gErrorCode = 0;
void leda_save_error(int errcode)
{
    static uint16_t blocknum = BLOCK_NUM_ERROR;
    static uint16_t offset = 0;
    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_handle_lxy1, blocknum, &block_handle);
    gErrorCode = errcode;
    pstorage_update(&block_handle, (uint8_t*)&gErrorCode, 4, offset);
    offset += 4;
    if (offset >= BLOCK_SIZE)
    {
        blocknum++;
        if (blocknum >= BLOCK_NUM_SYS_RUN_TIME)
        {
            blocknum = BLOCK_NUM_ERROR;
        }
        offset = 0;
    }
}

#ifdef LEDA_MONITOR_SYS_RUN_TIME
void leda_save_sys_run_time(void)
{
    static uint32_t sys_run_minutes = 0;
    static uint16_t blocknum = BLOCK_NUM_SYS_RUN_TIME;
    static uint16_t offset = 0;
    pstorage_handle_t block_handle;

    sys_run_minutes++;
    pstorage_block_identifier_get(&g_storage_handle_lxy1, blocknum, &block_handle);
    pstorage_update(&block_handle, (uint8_t*)&sys_run_minutes, 4, offset);
    offset += 4;
    if (offset >= BLOCK_SIZE)
    {
        blocknum++;
        if (blocknum >= BLOCK_COUNT)
        {
            blocknum = BLOCK_NUM_SYS_RUN_TIME;
        }
        offset = 0;
    }
}
#endif

