#ifndef __TETHYS_BATTERY_H__
#define __TETHYS_BATTERY_H__

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "compiler_abstraction.h"

#define BATTERY_FULL 4200
//#define BATTERY_FULL 4180
#define BATTERY_LOW  3700

/******************************************************************************/
/************************ Functions Declarations ******************************/
/******************************************************************************/
extern void init_battery(void);
extern void tethys_battery_calculate(void);
extern bool tethys_if_low_battery(void);
extern __INLINE uint8_t batteryLevelInPercent(const uint16_t mvolts);
#endif
