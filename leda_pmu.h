#ifndef __LEDA_PMU_H__
#define __LEDA_PMU_H__

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/

/******************************************************************************/
/************************ Functions Declarations ******************************/
/******************************************************************************/
extern void init_leda_pmu(void);
extern void handle_IO_LOWBAT_event(void);
extern bool pmu_is_charging(void);
extern void IO_VBUS_pin_handler_adc(void);
extern void handle_usb_state_change(void);
#endif // __LEDA_PMU_H__
