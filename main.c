/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 *
 * @defgroup ble_sdk_app_proximity_main main.c
 * @{
 * @ingroup ble_sdk_app_proximity_eval
 * @brief Proximity Application main file.
 *
 * This file contains is the source code for a sample proximity application using the
 * Immediate Alert, Link Loss and Tx Power services.
 *
 * This application would accept pairing requests from any peer device.
 *
 * It demonstrates the use of fast and slow advertising intervals.
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_soc.h"
#include "nrf_adc.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_radio_notification.h"
#include "ble_dis.h"
#include "ble_tps.h"
#ifdef BLE_DFU_APP_SUPPORT
#include "ble_dfu.h"
#include "dfu_app_handler.h"
#endif // BLE_DFU_APP_SUPPORT
#include "tw_ble_ias.h"
#include "ble_lls.h"
#include "ble_bas.h"
#include "mimas_ble_nus.h"
#include "tethys_ble_nus.h"
#include "leda_ble_nus.h"
#include "ble_conn_params.h"
#include "mimas_ble_debug_assert_handler.h"
#include "sensorsim.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "device_manager.h"
#include "ble_ias_c.h"
#include "app_util.h"
#include "pstorage.h"
#include "app_trace.h"
#include "mimas_bsp.h"
#include "mimas_bsp_btn_ble.h"
#include "app_uart.h"
#include "app_pwm.h"
#ifndef NRF51
#include "nrf_drv_saadc.h"
#endif //NRF51
#include "nrf_delay.h"
#include "leda_communicate_protocol.h"
#include "app_timer_appsh.h"
#include "app_scheduler.h"
#include "nrf_drv_wdt.h"
#include "leda_nrf_drv_uart.h"
#include "tw_nrf_event_string.h"
#include "mimas_log.h"
#include "config.h"
#include "mimas_config.h"
#include "nrf_drv_gpiote.h"
#include "mimas_run_in_ram.h"
#include "tethys_led.h"
#include "tethys_battery.h"
#include "leda_avr.h"
#include "leda_file.h"
#include "leda_pmu.h"

//static void leda_wdt_test(void);
extern bool leda_if_connected(void);
extern uint8_t tethys_get_OTA(void);

#define IS_SRVC_CHANGED_CHARACT_PRESENT     1                                            /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/

#define SIGNAL_ALERT_BUTTON_ID              0                                            /**< Button used for send or cancel High Alert to the peer. */
#define STOP_ALERTING_BUTTON_ID             1                                            /**< Button used for clearing the Alert LED that may be blinking or turned ON because of alerts from the central. */

#if (LEDA_HARDWARE_REVISION == 2)
#define HW_REV_STR                      "0.3"
#else
#define HW_REV_STR                      "0.1"
#endif
#define FW_REV_STR                      "110"
#define MANUFACTURER_NAME               "HUOWEIYI"                      /**< Manufacturer. Will be passed to Device Information Service. */
#define MODEL_NUM                       "AA"                           /**< Model number. Will be passed to Device Information Service. */
#define MANUFACTURER_ID                 0x1122334455                               /**< Manufacturer ID, part of System ID. Will be passed to Device Information Service. */
#define ORG_UNIQUE_ID                   0x667788                                   /**< Organizational Unique ID, part of System ID. Will be passed to Device Information Service. */
#if 0
#define DEVICE_NAME                         "Tethys"  /**< Name of device. Will be included in the advertising data. */
#else
//static uint8_t gDeviceName[21] = "mCenter";
static uint8_t gDeviceName[21] = "mCookie";
#endif
#define APP_TIMER_PRESCALER                 0                                            /**< Value of the RTC1 PRESCALER register. */

#define ADXL362_INTERVAL          APP_TIMER_TICKS(999, APP_TIMER_PRESCALER)

#define ADV_LED_ON_TIME                     APP_TIMER_TICKS(100, APP_TIMER_PRESCALER)    /**< Advertisement LED ON period when in blinking state. */
#define ADV_LED_OFF_TIME                    APP_TIMER_TICKS(900, APP_TIMER_PRESCALER)    /**< Advertisement LED OFF period when in blinking state. */

#define MILD_ALERT_LED_ON_TIME              APP_TIMER_TICKS(100, APP_TIMER_PRESCALER)    /**< Alert LED ON period when in blinking state. */
#define MILD_ALERT_LED_OFF_TIME             APP_TIMER_TICKS(900, APP_TIMER_PRESCALER)    /**< Alert LED OFF period when in blinking state. */


#define SEC_PARAM_BOND                      1                                            /**< Perform bonding. */
#ifdef TETHYS_PAIR
#define SEC_PARAM_MITM                      1                                            /**< Man In The Middle protection required. */
#define SEC_PARAM_IO_CAPABILITIES           BLE_GAP_IO_CAPS_DISPLAY_ONLY
#else
#define SEC_PARAM_MITM                      0                                            /**< Man In The Middle protection not required. */
#define SEC_PARAM_IO_CAPABILITIES           BLE_GAP_IO_CAPS_NONE                         /**< No I/O capabilities. */

#endif
#define SEC_PARAM_OOB                       0                                            /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE              7                                            /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE              16                                           /**< Maximum encryption key size. */

#define ADC_REF_VOLTAGE_IN_MILLIVOLTS       600                                          /**< Reference voltage (in milli volts) used by ADC while doing conversion. */
#define ADC_PRE_SCALING_COMPENSATION        6                                            /**< The ADC is configured to use VDD with 1/3 prescaling as input. And hence the result of conversion is to be multiplied by 3 to get the actual value of the battery voltage.*/
#define DIODE_FWD_VOLT_DROP_MILLIVOLTS      270                                          /**< Typical forward voltage drop of the diode (Part no: SD103ATW-7-F) that is connected in series with the voltage supply. This is the voltage drop when the forward current is 1mA. Source: Data sheet of 'SURFACE MOUNT SCHOTTKY BARRIER DIODE ARRAY' available at www.diodes.com. */

#define DEAD_BEEF                           0xDEADBEEF                                   /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define ADC_REF_VBG_VOLTAGE_IN_MILLIVOLTS   1200                                         /**< Value in millivolts for voltage used as reference in ADC conversion on NRF51. */
#define ADC_INPUT_PRESCALER                 3                                            /**< Input prescaler for ADC convestion on NRF51. */
#define ADC_RES_10BIT                       1024                                         /**< Maximum digital value for 10-bit ADC conversion. */

/**@brief Macro to convert the result of ADC conversion in millivolts.
 *
 * @param[in]  ADC_VALUE   ADC result.
 *
 * @retval     Result converted to millivolts.
 */
#ifdef TETHYS_ADC
#define ADC_RESULT_IN_MILLI_VOLTS(ADC_VALUE)\
        ((((ADC_VALUE) * ADC_REF_VBG_VOLTAGE_IN_MILLIVOLTS) / ADC_RES_10BIT) * ADC_INPUT_PRESCALER)
#else
#define ADC_RESULT_IN_MILLI_VOLTS(ADC_VALUE)\
        ((((ADC_VALUE) * ADC_REF_VBG_VOLTAGE_IN_MILLIVOLTS) / ADC_RES_10BIT) * ADC_INPUT_PRESCALER)
#endif // NRF51

#ifdef BLE_DFU_APP_SUPPORT
#define DFU_REV_MAJOR                    0x00                                       /** DFU Major revision number to be exposed. */
#define DFU_REV_MINOR                    0x01                                       /** DFU Minor revision number to be exposed. */
#define DFU_REVISION                     ((DFU_REV_MAJOR << 8) | DFU_REV_MINOR)     /** DFU Revision number to be exposed. Combined of major and minor versions. */
#define APP_SERVICE_HANDLE_START         0x000C                                     /**< Handle of first application specific service when when service changed characteristic is present. */
#define BLE_HANDLE_MAX                   0xFFFF                                     /**< Max handle value in BLE. */

STATIC_ASSERT(IS_SRVC_CHANGED_CHARACT_PRESENT);                                     /** When having DFU Service support in application the Service Changed Characteristic should always be present. */
#endif // BLE_DFU_APP_SUPPORT


/**@brief Advertisement states. */
typedef enum
{
    BLE_NO_ADV,                                                                      /**< No advertising running. */
    BLE_FAST_ADV_WHITELIST,                                                          /**< Advertising with whitelist. */
    BLE_FAST_ADV,                                                                    /**< Fast advertising running. */
    BLE_SLOW_ADV,                                                                    /**< Slow advertising running. */
    BLE_SLEEP,                                                                       /**< Go to system-off. */
    BLE_FOREVER_LED,
    BLE_FOREVER
} ble_advertising_mode_t;

#ifdef TETHYS_PAIR
static ble_opt_t   m_static_pin_option;
#endif
#if 0
typedef enum
{
    ADXL362_WORK_MODE_1,
    ADXL362_WORK_MODE_2,
    ADXL362_WORK_MODE_3,
} adxl362_work_mode_t;
adxl362_work_mode_t g_adxl362_work_mode = ADXL362_WORK_MODE_1;
#else
uint8_t g_adxl362_work_mode = 0;
#endif
ble_tps_t                        m_tps;                                       /**< Structure used to identify the TX Power service. */
static ble_ias_t                        m_ias;                                       /**< Structure used to identify the Immediate Alert service. */
static ble_lls_t                        m_lls;                                       /**< Structure used to identify the Link Loss service. */

static ble_bas_t                        m_bas;                                       /**< Structure used to identify the battery service. */
//static ble_ias_c_t                      m_ias_c;                                     /**< Structure used to identify the client to the Immediate Alert Service at peer. */

static uint8_t m_advertising_mode = BLE_FOREVER;

static bool m_adv_in_mic = false;
uint16_t                         global_connect_handle = BLE_CONN_HANDLE_INVALID;     /**< Handle of the current connection. */

static volatile bool                    m_is_high_alert_signalled;                   /**< Variable to indicate whether a high alert has been signalled to the peer. */
static volatile bool                    m_is_ias_present = false;                    /**< Variable to indicate whether the immediate alert service has been discovered at the connected peer. */
tethys_ble_nus_t m_tethys_nus;
leda_ble_nus_t m_leda_nus;
APP_TIMER_DEF(m_battery_timer_id);                                                   /**< Battery measurement timer. */
APP_TIMER_DEF(m_test_task_timer_id); // To test something.
APP_TIMER_DEF(m_temperature_timer_id);
APP_TIMER_DEF(m_adv_in_mic_timer_id);
static uint8_t g_timer_task;
static dm_application_instance_t        m_app_handle;                                /**< Application identifier allocated by device manager */

static bool                             m_memory_access_in_progress = false;         /**< Flag to keep track of ongoing operations on persistent memory. */
pstorage_handle_t       g_storage_handle;
int8_t g_device_tx_power = TX_POWER_LEVEL;
static uint8_t g_last_temp = 0;
static uint8_t g_last_batt = 1;

static void on_ias_evt(ble_ias_t *p_ias, ble_ias_evt_t *p_evt);
static void on_lls_evt(ble_lls_t *p_lls, ble_lls_evt_t *p_evt);
//static void on_ias_c_evt(ble_ias_c_t *p_lls, ble_ias_c_evt_t *p_evt);
static void on_bas_evt(ble_bas_t *p_bas, ble_bas_evt_t *p_evt);
//static void advertising_init(uint8_t adv_flags);
static void show_battery_level(void *p_event_data, uint16_t event_size);
static void alert_signal(uint8_t alert_level);
#ifdef BLE_DFU_APP_SUPPORT
static ble_dfu_t                         m_dfus;                                    /**< Structure used to identify the DFU service. */
#endif // BLE_DFU_APP_SUPPORT   

#ifdef NRF52
static nrf_saadc_value_t adc_buf[2];
#endif //NRF52

//extern uint8_t should_update_prv_bond_info;
uint8_t g_pstorage_state = PSTORAGE_STATE_UPDATE_IDLE;
uint8_t g_pstorage_buffer[32];
uint16_t g_interval = 1280;// 1280 x 0.625 = 800ms
#ifdef TETHYS_PAIR
uint8_t gPasskey[6] = "123456";
#endif
extern bool gEndByApp;
extern bool gLedaIsTransmittingBin;
//static ble_gap_adv_params_t m_adv_params;                                 /**< Parameters to be passed to the stack when starting advertising. */
uint8_t m_beacon_info[APP_BEACON_INFO_LENGTH] =                    /**< Information advertised by the Beacon. */
{
    APP_DEVICE_TYPE,     // Manufacturer specific information. Specifies the device type in this
    // implementation.
    APP_ADV_DATA_LENGTH, // Manufacturer specific information. Specifies the length of the
    // manufacturer specific data in this implementation.
    APP_BEACON_UUID,     // 128 bit UUID value.
    APP_MAJOR_VALUE,     // Major arbitrary value that can be used to distinguish between Beacons.
    APP_MINOR_VALUE,     // Minor arbitrary value that can be used to distinguish between Beacons.
    APP_MEASURED_RSSI,    // Used as battery level.(Manufacturer specific information. The Beacon's measured TX power in)
    // this implementation.
    0x64// Used as state.
};

#if 1
void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t *p_file_name)
{
#ifndef DEBUG
    LOG("line_num = %u, filename=%s,error_code=0x%x", line_num, p_file_name, error_code);
    //LEDS_ON(LEDS_MASK);

    //for (;;)
    {
        LOG("line_num = %u, filename=%s,error_code=0x%x", line_num, p_file_name, error_code);
        #if 0
        nrf_delay_ms(100);
        switch (error_code)
        {
            case 1:
                LEDS_INVERT(BSP_LED_0_MASK); // blue
                break;
            case 2:
                LEDS_INVERT(BSP_LED_1_MASK); // red
                break;
            case 3:
                LEDS_INVERT(BSP_LED_2_MASK); // green
                break;
            case 4:
                LEDS_INVERT(BSP_LED_3_MASK); // red
                break;
            default:
                LEDS_INVERT(BSP_LED_1_MASK|BSP_LED_0_MASK);
                break;
        }
        #endif
    }
#else
    //LOG("line_num = %u, filename=%s,error_code=0x%x", line_num,p_file_name,error_code);
    ble_debug_assert_handler(error_code, line_num, p_file_name);
#endif
}
#endif
/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t *p_file_name)
{
    LOG_ENTER;
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/**@brief Function for handling Service errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in] nrf_error   Error code containing information about what went wrong.
 */
static void service_error_handler(uint32_t nrf_error)
{
    LOG_ENTER;
    APP_ERROR_HANDLER(nrf_error);
}

static int32_t map(int32_t x, int32_t in_min, int32_t in_max, int32_t out_min, int32_t out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

extern uint8_t gIfUSBIn;
extern bool gIfCheckVBUS;
#if 0
extern bool gPmuVbusCheck;
extern bool gPmuBatteryCheck;
extern void handle_vbus_check(uint16_t vol);
static void adc_handle_vbus(void)
{
    uint16_t adc_value = nrf_adc_result_get();
    uint16_t volts = ADC_RESULT_IN_MILLI_VOLTS(adc_value);
    if (volts <= 2300)
    {
        gIfUSBIn = false;
        IO_VBUS_pin_handler_adc();
    }
}
static void adc_handle_vbus_ttl(void)
{
    uint16_t adc_value = nrf_adc_result_get();
    uint16_t volts = ADC_RESULT_IN_MILLI_VOLTS(adc_value);
    handle_vbus_check(volts);
}

#endif
static bool gIfFirstStart = true;
static void adc_handle_vbus_check(void)
{
//    static bool flag = true;
    static uint8_t lastIfUSBIn = 1;
    uint16_t adc_value = nrf_adc_result_get();
    uint16_t volts = ADC_RESULT_IN_MILLI_VOLTS(adc_value);
    if (volts <= 1000)
    {
        gIfUSBIn = 1;
        //open_LED(1);
    }
    else
    {
        gIfUSBIn = 2;
        //close_LED(1);
    }
#if 0
    if (flag)
    {
        flag = false;
        open_LED(2);
    }
    else
    {
        flag = true;
        close_LED(2);
    }
#endif
    if (gIfFirstStart)
    {
        gIfFirstStart = false;
        return;
    }
    if (lastIfUSBIn != gIfUSBIn)
    {
        lastIfUSBIn = gIfUSBIn;
        handle_usb_state_change();
    }
}
uint16_t g_battery_voltage_mv = BATTERY_FULL;
uint8_t g_percentage_batt_lvl = 0x64;
/**@brief Function for handling the ADC interrupt.
 *
 * @details  This function will fetch the conversion result from the ADC, convert the value into
 *           percentage and send it to peer.
 */
#ifdef TETHYS_ADC
void ADC_IRQHandler(void)
{
//    static uint8_t flags = 0;
//    uint32_t err_code;
    uint16_t adc_value;
    uint16_t batt_lvl_in_milli_volts;
    LOG_ENTER;
    nrf_adc_conversion_event_clean();
    nrf_adc_stop();
#if 0
    if (gPmuBatteryCheck)
    {
        goto BATTERY_CHECK;
    }
    if (gPmuVbusCheck)
    {
        //gPmuVbusCheck = false;
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)adc_handle_vbus_ttl);
        return;
    }
    if (gIfUSBIn)
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)adc_handle_vbus);
        return;
    }
#endif
#if 1
    if (gIfCheckVBUS)
    {
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)adc_handle_vbus_check);
        return;
    }
#endif
BATTERY_CHECK:
    adc_value = nrf_adc_result_get();

    //Please see 'nRF51_Series_Reference_manual v3.0.pdf'
    batt_lvl_in_milli_volts = ADC_RESULT_IN_MILLI_VOLTS(adc_value);// 0 ~ 1650mv
#if (LEDA_HARDWARE_REVISION == 2)
    g_battery_voltage_mv = map(batt_lvl_in_milli_volts, 1, 2100, 1, BATTERY_FULL);//0 ~ 4200mv
#else
    g_battery_voltage_mv = map(batt_lvl_in_milli_volts, 1, 1650, 1, BATTERY_FULL);//0 ~ 4200mv
#endif

    g_percentage_batt_lvl     = batteryLevelInPercent(g_battery_voltage_mv);

    //if ((++flags % 3 == 0) || leda_if_connected())
    {
#if 0
        err_code = ble_bas_battery_level_update(&m_bas, g_percentage_batt_lvl);

        if (
            (err_code != NRF_SUCCESS)
            &&
            (err_code != NRF_ERROR_INVALID_STATE)
            &&
            (err_code != BLE_ERROR_NO_TX_BUFFERS)
            &&
            (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
        )
        {
            APP_ERROR_HANDLER(err_code);
        }
#else
        ble_bas_battery_level_update(&m_bas, g_percentage_batt_lvl);
#endif
    }
    app_sched_event_put(&g_percentage_batt_lvl, 1, show_battery_level);
}
#else
void ADC_IRQHandler(void)
{
    uint32_t err_code;
    uint16_t adc_value;
    uint8_t  percentage_batt_lvl;
    uint16_t batt_lvl_in_milli_volts;
    LOG_ENTER;
    nrf_adc_conversion_event_clean();
    nrf_adc_stop();

    adc_value = nrf_adc_result_get();


    batt_lvl_in_milli_volts = ADC_RESULT_IN_MILLI_VOLTS(adc_value);
    g_battery_voltage_mv = batt_lvl_in_milli_volts;

    percentage_batt_lvl     = batteryLevelInPercent(batt_lvl_in_milli_volts);

    err_code = ble_bas_battery_level_update(&m_bas, percentage_batt_lvl);
    if (
        (err_code != NRF_SUCCESS)
        &&
        (err_code != NRF_ERROR_INVALID_STATE)
        &&
        (err_code != BLE_ERROR_NO_TX_BUFFERS)
        &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
    )
    {
        APP_ERROR_HANDLER(err_code);
    }
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)show_battery_level);
}
#endif

/**@brief Function for configuring ADC to do battery level conversion.
 */
#ifdef TETHYS_ADC
static void adc_configure(void)
{
    //LOG_ENTER;
    nrf_adc_config_t adc_config;
    adc_config.resolution = NRF_ADC_CONFIG_RES_10BIT;
    adc_config.scaling = NRF_ADC_CONFIG_SCALING_INPUT_ONE_THIRD;
    adc_config.reference = NRF_ADC_CONFIG_REF_VBG;
    nrf_adc_configure(&adc_config);
    nrf_adc_int_enable(ADC_INTENSET_END_Msk);
    NVIC_EnableIRQ(ADC_IRQn);
    NVIC_SetPriority(ADC_IRQn, 3);
#if (LEDA_HARDWARE_REVISION == 2)
    nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_5); // For VBUS voltage check.
#else
    nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_5);
#endif
    nrf_adc_start();
}
#else
static void adc_configure(void)
{
    //LOG_ENTER;
#ifdef NRF51
    nrf_adc_config_t adc_config = NRF_ADC_CONFIG_DEFAULT;
    adc_config.scaling = NRF_ADC_CONFIG_SCALING_SUPPLY_ONE_THIRD;
    nrf_adc_configure(&adc_config);
    nrf_adc_int_enable(ADC_INTENSET_END_Msk);
    NVIC_EnableIRQ(ADC_IRQn);
    NVIC_SetPriority(ADC_IRQn, 3);
    nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_DISABLED);
#else //  NRF52
    ret_code_t err_code = nrf_drv_saadc_init(NULL, saadc_event_handler);
    APP_ERROR_CHECK(err_code);

    nrf_saadc_channel_config_t config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_VDD);
    err_code = nrf_drv_saadc_channel_init(0, &config);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(&adc_buf[0], 1);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(&adc_buf[1], 1);
    APP_ERROR_CHECK(err_code);
#endif //NRF51
}
#endif

/**@brief Function for starting advertising.
 */
void advertising_start(uint8_t type)
{
    uint32_t             err_code;
    ble_gap_adv_params_t adv_params;
    ble_gap_whitelist_t  whitelist;
    uint32_t             count;

    //LOG_ENTER;
    // Verify if there is any flash access pending, if yes delay starting advertising until
    // it's complete.
    err_code = pstorage_access_status_get(&count);
    APP_ERROR_CHECK(err_code);

    if (count != 0)
    {
        m_memory_access_in_progress = true;
        return;
    }

    // Initialize advertising parameters with defaults values
    memset(&adv_params, 0, sizeof(adv_params));

#ifdef BUTTON_SWITCH_ADV_STATE
    adv_params.type        = type;
#else
    //adv_params.type        = BLE_GAP_ADV_TYPE_ADV_NONCONN_IND;
    adv_params.type        = BLE_GAP_ADV_TYPE_ADV_IND;
#endif
    adv_params.p_peer_addr = NULL;
    adv_params.fp          = BLE_GAP_ADV_FP_ANY;
    adv_params.p_whitelist = NULL;

    // Configure advertisement according to current advertising state
    switch (m_advertising_mode)
    {
    case BLE_NO_ADV:
        m_advertising_mode = BLE_FAST_ADV_WHITELIST;
    // fall through.

    case BLE_FAST_ADV_WHITELIST:
    {
        ble_gap_addr_t        *p_whitelist_addr[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
        ble_gap_irk_t         *p_whitelist_irk[BLE_GAP_WHITELIST_IRK_MAX_COUNT];

        whitelist.addr_count = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;
        whitelist.irk_count  = BLE_GAP_WHITELIST_IRK_MAX_COUNT;
        whitelist.pp_addrs   = p_whitelist_addr;
        whitelist.pp_irks    = p_whitelist_irk;

        err_code = dm_whitelist_create(&m_app_handle, &whitelist);
        APP_ERROR_CHECK(err_code);

        if ((whitelist.addr_count != 0) || (whitelist.irk_count != 0))
        {
            adv_params.fp          = BLE_GAP_ADV_FP_FILTER_CONNREQ;
            adv_params.p_whitelist = &whitelist;

            //advertising_init(BLE_GAP_ADV_FLAG_BR_EDR_NOT_SUPPORTED);
            advertising_init_beacon();
            m_advertising_mode = BLE_FAST_ADV;
        }
        else
        {
            m_advertising_mode = BLE_SLOW_ADV;
        }

        adv_params.interval = APP_ADV_INTERVAL_FAST;
        adv_params.timeout  = APP_FAST_ADV_TIMEOUT;

        //err_code            = bsp_indication_set(BSP_INDICATE_ADVERTISING_WHITELIST);
        //APP_ERROR_CHECK(err_code);
        break;
    }

    case BLE_FAST_ADV:
        //advertising_init(BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE);
        //advertising_init_beacon();
        advertising_init_microduino();

        adv_params.interval = APP_ADV_INTERVAL_FAST;
        adv_params.timeout  = 6;
        m_advertising_mode  = BLE_FOREVER;
        //err_code            = bsp_indication_set(BSP_INDICATE_ADVERTISING);
        //APP_ERROR_CHECK(err_code);
        break;

    case BLE_SLOW_ADV:
        //advertising_init(BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE);
        advertising_init_beacon();

        adv_params.interval = g_interval;
        adv_params.timeout  = APP_SLOW_ADV_TIMEOUT;
        m_advertising_mode  = BLE_SLEEP;

        //err_code            = bsp_indication_set(BSP_INDICATE_ADVERTISING_SLOW);
        //APP_ERROR_CHECK(err_code);
        break;

    case BLE_FOREVER_LED:
        //advertising_init_beacon();
        advertising_init_microduino();
        adv_params.interval = g_interval;
        adv_params.timeout  = 0;
        m_advertising_mode  = BLE_FOREVER_LED;
        //err_code            = bsp_indication_set(BSP_INDICATE_ADVERTISING_SLOW);
        break;
    case BLE_FOREVER:
        //advertising_init(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
        advertising_init_beacon();

        //adv_params.interval = APP_ADV_INTERVAL_SLOW;
        adv_params.interval = g_interval;
        adv_params.timeout  = 0;
        m_advertising_mode  = BLE_FOREVER;
        //err_code            = bsp_indication_set(BSP_INDICATE_IDLE);
        //APP_ERROR_CHECK(err_code);
        //close_LEDS();
        break;
    default:
        // No implementation needed.
        break;
    }

    // Start advertising.
    err_code = sd_ble_gap_adv_start(&adv_params);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling the Battery measurement timer timeout.
 *
 * @details This function will be called each time the battery level measurement timer expires.
 *          This function will start the ADC.
 *
 * @param[in] p_context   Pointer used for passing some arbitrary information (context) from the
 *                        app_start_timer() call to the timeout handler.
 */
static void battery_level_meas_timeout_handler(void *p_context)
{
    UNUSED_PARAMETER(p_context);
#if 0
    if(global_connect_handle != BLE_CONN_HANDLE_INVALID)//if in connected state.
    {
        LOG("temperature:Connectted state!return!");
        return;
    }
#endif
    LOG_ENTER;
#ifdef NRF51
    //nrf_adc_start();
#else // NRF52
    uint32_t err_code;
    err_code = nrf_drv_saadc_sample();
    APP_ERROR_CHECK(err_code);
#endif // NRF51
}

static void show_battery_level(void *p_event_data, uint16_t event_size)
{
    //uint8_t  percentage_batt_lvl = battery_level_in_percent(g_battery_voltage_mv);
    //uint8_t  percentage_batt_lvl = batteryLevelInPercent(g_battery_voltage_mv);
    uint8_t  percentage_batt_lvl = *((uint8_t *)p_event_data);
    LOG("batt level:%d\r\n", percentage_batt_lvl);
#if 1
    gIfCheckVBUS = true;
    nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_5); // For USB voltage check
#else
    gPmuBatteryCheck = false;
    nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_5); // For USB voltage check
#endif
    //m_beacon_info[BEACON_BATTERY_OFFSET] = percentage_batt_lvl;
    m_beacon_info[BEACON_BATTERY_OFFSET] = (m_beacon_info[BEACON_BATTERY_OFFSET] & ~BATTERY_Msk) |
                                           percentage_batt_lvl;
    m_beacon_info[BEACON_BATTERY_MV_OFFSET] = g_battery_voltage_mv >> 8;
    m_beacon_info[BEACON_BATTERY_MV_OFFSET + 1] = g_battery_voltage_mv & 0x00FF;
    if (g_last_batt != percentage_batt_lvl)
    {
        advertising_init_beacon();
        if (percentage_batt_lvl == 0)
        {
            handle_IO_LOWBAT_event();
        }
        g_last_batt = percentage_batt_lvl;
    }
    //open_LED(1);
}

extern void get_nrf51822_temperature(void *p_event_data, uint16_t event_size);
static void test_task_timeout_handler(void *p_context)
{
    LOG("test_task_timeout_handler: p_context = 0x%x\r\n", p_context);
#ifdef LEDA_MONITOR_SYS_RUN_TIME
    if (g_timer_task == 10)
    {
        app_timer_start(m_test_task_timer_id, APP_TIMER_TICKS(60000, APP_TIMER_PRESCALER), NULL);
        leda_save_sys_run_time();
    }
#endif
}

static void adv_in_mic_timeout_handler(void *p_context)
{
    //sd_nvic_SystemReset();
    m_adv_in_mic = false;
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
}

void stop_login_timer(void)
{
    //    app_timer_stop(m_tethys_login_timer_id);
}

static void temperature_timeout_handler(void *p_context)
{
    int32_t temp;
    if(global_connect_handle != BLE_CONN_HANDLE_INVALID)//If in connected state.
    {
        LOG("temperature:Connectted state!return!");
        return;
    }
    sd_temp_get(&temp);
    LOG("temp = %d\r\n", temp);

    float cTemp = (float)temp / 4;
    cTemp -= 3.0;

    m_beacon_info[BEACON_TEMP_OFFSET] = 20 + cTemp;
    if (g_last_temp != m_beacon_info[BEACON_TEMP_OFFSET])
    {
        g_last_temp = m_beacon_info[BEACON_TEMP_OFFSET];
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    }
    else
    {
        //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_beacon);
    }
    LOG("temperature:%.2f;\r\n", cTemp);
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{
    //LOG_ENTER;
    uint32_t err_code;

    // Initialize timer module.
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

    // Create battery timer.
    err_code = app_timer_create(&m_battery_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                battery_level_meas_timeout_handler);
    APP_ERROR_CHECK(err_code);
    err_code = app_timer_create(&m_test_task_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                test_task_timeout_handler);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_temperature_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                temperature_timeout_handler);
    APP_ERROR_CHECK(err_code);
    err_code = app_timer_create(&m_adv_in_mic_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                adv_in_mic_timeout_handler);
    APP_ERROR_CHECK(err_code);
#ifdef LEDA_MONITOR_SYS_RUN_TIME
    g_timer_task = 10;
    app_timer_start(m_test_task_timer_id, APP_TIMER_TICKS(60000, APP_TIMER_PRESCALER), NULL);
#endif
}

static bool gMacAuthorized = false;
// length of 'serialNumber' must >= 12
void getMacASCII(uint8_t *serialNumber)
{
    ble_gap_addr_t mac_addr_t;
    sd_ble_gap_address_get(&mac_addr_t);
    //uint8_t serialNumber[14] = "\0";
    uint8_t aNumber;
    uint8_t j = 0;
    for (int i = BLE_GAP_ADDR_LEN - 1; i >= 0; i--)
    {
        aNumber = mac_addr_t.addr[i];
        sprintf((char *)serialNumber + j * 2, "%02X", aNumber);
        j++;
    }
    //uint8_t addr[BLE_GAP_ADDR_LEN] = {0xDB, 0x96, 0x42, 0x79, 0xCC, 0x78};
    //char addr[14] = "DB964279CC78";
    //char addr[14] = "D8E507551E04";
    char addr[14] = "EDE9C872E210";
    if (memcmp(addr, serialNumber, 12) == 0)
    {
        gMacAuthorized = true;
    }
}

bool isMacAuthorized(void)
{
    return gMacAuthorized;
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    //LOG_ENTER;
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
    //BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&sec_mode);
    //BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&sec_mode);

    uint8_t buf[14] = {0};
    getMacASCII(buf);
    memcpy(gDeviceName + 7, buf + 8, 4);
#if 0
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
#else
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          gDeviceName,
                                          11);
#endif
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_GENERIC_KEYRING);
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL_DEFAULT;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL_DEFAULT;
    gap_conn_params.slave_latency     = SLAVE_LATENCY_DEFAULT;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT_DEFAULT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_tx_power_set(g_device_tx_power);
    APP_ERROR_CHECK(err_code);
#ifdef TETHYS_PAIR
    m_static_pin_option.gap_opt.passkey.p_passkey = gPasskey;
    err_code = sd_ble_opt_set(BLE_GAP_OPT_PASSKEY, &m_static_pin_option);
    APP_ERROR_CHECK(err_code);
#endif
#if 1
    ble_opt_t ble_options;
    ble_options.gap_opt.scan_req_report.enable = 1;
    err_code = sd_ble_opt_set(BLE_GAP_OPT_SCAN_REQ_REPORT, &ble_options);
    APP_ERROR_CHECK(err_code);
#endif
}

/**@brief Function for initializing the Event Scheduler.
 */
static void scheduler_init(void)
{
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}

/**@brief Function for initializing the TX Power Service.
 */
static void tps_init(void)
{
    LOG_ENTER;
    uint32_t       err_code;
    ble_tps_init_t tps_init_obj;

    memset(&tps_init_obj, 0, sizeof(tps_init_obj));
    tps_init_obj.initial_tx_power_level = g_device_tx_power;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&tps_init_obj.tps_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&tps_init_obj.tps_attr_md.write_perm);

    err_code = ble_tps_init(&m_tps, &tps_init_obj);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Device Information Service.
 */
static void Device_Information_init(void)
{
    //LOG_ENTER;
    uint32_t         err_code;
    ble_dis_init_t   dis_init;
    ble_dis_sys_id_t sys_id;
    // Initialize Device Information Service.
    memset(&dis_init, 0, sizeof(dis_init));

    ble_srv_ascii_to_utf8(&dis_init.manufact_name_str, MANUFACTURER_NAME);
    ble_srv_ascii_to_utf8(&dis_init.model_num_str,     MODEL_NUM);
    ble_srv_ascii_to_utf8(&dis_init.hw_rev_str,     HW_REV_STR);
    ble_srv_ascii_to_utf8(&dis_init.fw_rev_str,     FW_REV_STR);
    ble_srv_ascii_to_utf8(&dis_init.sw_rev_str,     SW_REV_STR);

    sys_id.manufacturer_id            = MANUFACTURER_ID;
    sys_id.organizationally_unique_id = ORG_UNIQUE_ID;
    dis_init.p_sys_id                 = &sys_id;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_init.dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init.dis_attr_md.write_perm);

    err_code = ble_dis_init(&dis_init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Immediate Alert Service.
 */
static void ias_init(void)
{
    LOG_ENTER;
    uint32_t       err_code;
    ble_ias_init_t ias_init_obj;

    memset(&ias_init_obj, 0, sizeof(ias_init_obj));
    ias_init_obj.evt_handler = on_ias_evt;

    err_code = tw_ble_ias_init(&m_ias, &ias_init_obj);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Link Loss Service.
 */
static void lls_init(void)
{
    LOG_ENTER;
    uint32_t       err_code;
    ble_lls_init_t lls_init_obj;

    // Initialize Link Loss Service
    memset(&lls_init_obj, 0, sizeof(lls_init_obj));

    lls_init_obj.evt_handler         = on_lls_evt;
    lls_init_obj.error_handler       = service_error_handler;
    lls_init_obj.initial_alert_level = INITIAL_LLS_ALERT_LEVEL;
#if 0
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&lls_init_obj.lls_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&lls_init_obj.lls_attr_md.write_perm);
#else
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&lls_init_obj.lls_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&lls_init_obj.lls_attr_md.write_perm);
#endif
    err_code = ble_lls_init(&m_lls, &lls_init_obj);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Battery Service.
 */
static void bas_init(void)
{
    LOG_ENTER;
    uint32_t       err_code;
    ble_bas_init_t bas_init_obj;

    memset(&bas_init_obj, 0, sizeof(bas_init_obj));

    bas_init_obj.evt_handler          = on_bas_evt;
    bas_init_obj.support_notification = true;
    bas_init_obj.p_report_ref         = NULL;
    bas_init_obj.initial_batt_level   = 100;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init_obj.battery_level_char_attr_md.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init_obj.battery_level_char_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&bas_init_obj.battery_level_char_attr_md.write_perm);

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init_obj.battery_level_report_read_perm);

    err_code = ble_bas_init(&m_bas, &bas_init_obj);
    APP_ERROR_CHECK(err_code);
}

#ifdef BLE_DFU_APP_SUPPORT
/**@brief Function for stopping advertising.
 */
static void advertising_stop(void)
{
    LOG_ENTER;
    uint32_t err_code;

    err_code = sd_ble_gap_adv_stop();
    APP_ERROR_CHECK(err_code);

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for loading application-specific context after establishing a secure connection.
 *
 * @details This function will load the application context and check if the ATT table is marked as
 *          changed. If the ATT table is marked as changed, a Service Changed Indication
 *          is sent to the peer if the Service Changed CCCD is set to indicate.
 *
 * @param[in] p_handle The Device Manager handle that identifies the connection for which the context
 *                     should be loaded.
 */
static void app_context_load(dm_handle_t const *p_handle)
{
    LOG_ENTER;
    uint32_t                 err_code;
    static uint32_t          context_data;
    dm_application_context_t context;

    context.len    = sizeof(context_data);
    context.p_data = (uint8_t *)&context_data;

    err_code = dm_application_context_get(p_handle, &context);
    if (err_code == NRF_SUCCESS)
    {
        // Send Service Changed Indication if ATT table has changed.
        if ((context_data & (DFU_APP_ATT_TABLE_CHANGED << DFU_APP_ATT_TABLE_POS)) != 0)
        {
            err_code = sd_ble_gatts_service_changed(global_connect_handle, APP_SERVICE_HANDLE_START, BLE_HANDLE_MAX);
            if ((err_code != NRF_SUCCESS) &&
                    (err_code != BLE_ERROR_INVALID_CONN_HANDLE) &&
                    (err_code != NRF_ERROR_INVALID_STATE) &&
                    (err_code != BLE_ERROR_NO_TX_BUFFERS) &&
                    (err_code != NRF_ERROR_BUSY) &&
                    (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING))
            {
                APP_ERROR_HANDLER(err_code);
            }
        }

        err_code = dm_application_context_delete(p_handle);
        APP_ERROR_CHECK(err_code);
    }
    else if (err_code == DM_NO_APP_CONTEXT)
    {
        // No context available. Ignore.
    }
    else
    {
        APP_ERROR_HANDLER(err_code);
    }
}


/** @snippet [DFU BLE Reset prepare] */
/**@brief Function for preparing for system reset.
 *
 * @details This function implements @ref dfu_app_reset_prepare_t. It will be called by
 *          @ref dfu_app_handler.c before entering the bootloader/DFU.
 *          This allows the current running application to shut down gracefully.
 */
static void reset_prepare(void)
{
    LOG_ENTER;
    uint32_t err_code;

    //NRF_UICR->CUSTOMER[30] = 0xFFFFFFF1;
    // Turn on flash write enable and wait until the NVMC is ready.
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos);
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    NRF_UICR->CUSTOMER[30] = 0xFFFFFFF1;
    // Wait flash write to finish
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }
    // Turn off flash write enable and wait until the NVMC is ready.
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing
    }
    if (global_connect_handle != BLE_CONN_HANDLE_INVALID)
    {
        // Disconnect from peer.
        err_code = sd_ble_gap_disconnect(global_connect_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        APP_ERROR_CHECK(err_code);
        err_code = bsp_indication_set(BSP_INDICATE_IDLE);
        APP_ERROR_CHECK(err_code);
    }
    else
    {
        // If not connected, the device will be advertising. Hence stop the advertising.
        advertising_stop();
    }

    err_code = ble_conn_params_stop();
    APP_ERROR_CHECK(err_code);

    nrf_delay_ms(500);
}

static void tethys_dfu_app_on_dfu_evt(ble_dfu_t *p_dfu, ble_dfu_evt_t *p_evt)
{
    if (1 == tethys_get_OTA())
    {
#if 0
        if (p_evt->ble_dfu_evt_type == BLE_DFU_START)
        {
            NRF_UICR->CUSTOMER[30] = 0xFFFFFFF1;
        }
#endif
        dfu_app_on_dfu_evt(p_dfu, p_evt);
    }
}

static void dfu_init(void)
{
    LOG_ENTER;
    uint32_t       err_code;
    /** @snippet [DFU BLE Service initialization] */
    ble_dfu_init_t   dfus_init;

    // Initialize the Device Firmware Update Service.
    memset(&dfus_init, 0, sizeof(dfus_init));

    dfus_init.evt_handler   = tethys_dfu_app_on_dfu_evt;
    dfus_init.error_handler = NULL;
    dfus_init.revision      = DFU_REVISION;

    err_code = ble_dfu_init(&m_dfus, &dfus_init);
    APP_ERROR_CHECK(err_code);

    dfu_app_reset_prepare_set(reset_prepare);
    dfu_app_dm_appl_instance_set(m_app_handle);
    /** @snippet [DFU BLE Service initialization] */
}
/** @snippet [DFU BLE Reset prepare] */
#endif // BLE_DFU_APP_SUPPORT

// 0xFFF0
static void tethys_nus_init(void)
{
    tethys_ble_nus_init_t nus_init;
    memset(&nus_init, 0, sizeof(nus_init));
    nus_init.data_handler = L1_receive_data_uart;
    tethys_ble_nus_init(&m_tethys_nus, &nus_init);
}

static void leda_nus_init(void)
{
    leda_ble_nus_init_t nus_init;
    memset(&nus_init, 0, sizeof(nus_init));
    nus_init.data_handler = L1_leda_receive_data;
    leda_ble_nus_init(&m_leda_nus, &nus_init);
}

/**@snippet [Handling the data received over BLE] */

/**@brief Function for initializing the services that will be used by the application.
 */
static void leda_services_init(void)
{
    //LOG_ENTER;
    tethys_nus_init();
    leda_nus_init();

    ias_init();
    tps_init();
    lls_init();
    bas_init();
#ifdef BLE_DFU_APP_SUPPORT
    dfu_init();
#endif
    Device_Information_init();
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    LOG_ENTER;
    APP_ERROR_HANDLER(nrf_error);
}

static void on_conn_params_evt(ble_conn_params_evt_t *p_evt)
{
    //uint32_t err_code;

    if (global_connect_handle == BLE_CONN_HANDLE_INVALID)
    {
        return;
    }
    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        LOG("BLE_CONN_PARAMS_EVT_FAILED\r\n");
    }
    else
    {
        LOG("BLE_CONN_PARAMS_EVT_SUCCEEDED\r\n");
    }
}

/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    //LOG_ENTER;
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;
    ble_gap_conn_params_t  gap_conn_params;

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));
    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL_DEFAULT;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL_DEFAULT;
    gap_conn_params.slave_latency     = SLAVE_LATENCY_DEFAULT;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT_DEFAULT;
    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = &gap_conn_params;//NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for the Signals alert event from Immediate Alert or Link Loss services.
 *
 * @param[in] alert_level  Requested alert level.
 */
static void alert_signal(uint8_t alert_level)
{
    LOG_ENTER;
    //uint32_t err_code;
    switch (alert_level)
    {
    case BLE_CHAR_ALERT_LEVEL_NO_ALERT:
#if 0
        err_code = bsp_indication_set(BSP_INDICATE_ALERT_OFF);
        APP_ERROR_CHECK(err_code);
#else
        close_LED(1);
        close_LED(2);
#endif
        break;

    case BLE_CHAR_ALERT_LEVEL_MILD_ALERT:
#if 0
        err_code = bsp_indication_set(BSP_INDICATE_ALERT_0);
        APP_ERROR_CHECK(err_code);
        app_timer_start(m_alert_off_timer_id, APP_TIMER_TICKS(10000, APP_TIMER_PRESCALER), NULL);
#else
        led_blink_alert(1000);
#endif
        break;

    case BLE_CHAR_ALERT_LEVEL_HIGH_ALERT:
#if 0
        err_code = bsp_indication_set(BSP_INDICATE_ALERT_2);
        APP_ERROR_CHECK(err_code);
        app_timer_start(m_alert_off_timer_id, APP_TIMER_TICKS(10000, APP_TIMER_PRESCALER), NULL);
#else
        led_blink_alert(300);
#endif
        break;

    default:
        // No implementation needed.
        break;
    }
}


/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
void mimas_sleep_mode_enter(void)
{
    LOG_ENTER;
    uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling Immediate Alert events.
 *
 * @details This function will be called for all Immediate Alert events which are passed to the
 *          application.
 *
 * @param[in] p_ias  Immediate Alert structure.
 * @param[in] p_evt  Event received from the Immediate Alert service.
 */
static void on_ias_evt(ble_ias_t *p_ias, ble_ias_evt_t *p_evt)
{
    LOG_ENTER;
    switch (p_evt->evt_type)
    {
    case BLE_IAS_EVT_ALERT_LEVEL_UPDATED:
        alert_signal(p_evt->params.alert_level);
        break;

    default:
        // No implementation needed.
        break;
    }
}


/**@brief Function for handling Link Loss events.
 *
 * @details This function will be called for all Link Loss events which are passed to the
 *          application.
 *
 * @param[in] p_lls  Link Loss structure.
 * @param[in] p_evt  Event received from the Link Loss service.
 */
static void on_lls_evt(ble_lls_t *p_lls, ble_lls_evt_t *p_evt)
{
    LOG_ENTER;
    switch (p_evt->evt_type)
    {
    case BLE_LLS_EVT_LINK_LOSS_ALERT:
        alert_signal(p_evt->params.alert_level);
        break;

    default:
        // No implementation needed.
        break;
    }
}

/**@brief Function for handling the Battery Service events.
 *
 * @details This function will be called for all Battery Service events which are passed to the
 |          application.
 *
 * @param[in] p_bas  Battery Service structure.
 * @param[in] p_evt  Event received from the Battery Service.
 */
static void on_bas_evt(ble_bas_t *p_bas, ble_bas_evt_t *p_evt)
{
    LOG_ENTER;
    uint32_t err_code;

    switch (p_evt->evt_type)
    {
    case BLE_BAS_EVT_NOTIFICATION_ENABLED:
        // Start battery timer
        err_code = app_timer_start(m_battery_timer_id, BATTERY_LEVEL_MEAS_INTERVAL, NULL);
        APP_ERROR_CHECK(err_code);
        break;

    case BLE_BAS_EVT_NOTIFICATION_DISABLED:
        err_code = app_timer_stop(m_battery_timer_id);
        APP_ERROR_CHECK(err_code);
        break;

    default:
        // No implementation needed.
        break;
    }
}

void leda_disconnect_now(void)
{
    if(global_connect_handle != BLE_CONN_HANDLE_INVALID)//If in connected state.
    {
        sd_ble_gap_disconnect(global_connect_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    }
}

static void leda_leave_connect_to_debug_do(void)
{
    if(global_connect_handle != BLE_CONN_HANDLE_INVALID)//If in connected state.
    {
        sd_ble_gap_disconnect(global_connect_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    }
    else
    {
        advertising_start(BLE_GAP_ADV_TYPE_ADV_IND);
    }
}
void leda_leave_connect_to_debug(uint32_t error_id)
{
    m_advertising_mode = BLE_FOREVER;
    memcpy(m_beacon_info + BEACON_UUID_OFFSET, &error_id, 4);
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)leda_leave_connect_to_debug_do);
}

/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t *p_ble_evt)
{
    //LOG_ENTER;
    uint32_t        err_code      = NRF_SUCCESS;
    uint8_t disconnectedReason;

    switch (p_ble_evt->header.evt_id)
    {
    case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
        //led_blink(1);
        break;
    case BLE_GAP_EVT_CONNECTED:
        global_connect_handle      = p_ble_evt->evt.gap_evt.conn_handle;
        m_advertising_mode = BLE_NO_ADV;
        close_LED(1);
        close_LED(2);
        //led_blink_connected();
        //leda_start_pmu_vbus_check();
        break;

    case BLE_GAP_EVT_DISCONNECTED:
        //led_blink(3);
        global_connect_handle = BLE_CONN_HANDLE_INVALID;

        disconnectedReason = p_ble_evt->evt.gap_evt.params.disconnected.reason;
        memcpy(m_beacon_info + BEACON_UUID_OFFSET + 4, &disconnectedReason, 1);

        m_advertising_mode = BLE_FOREVER;
        leda_uart_uninit();
        advertising_start(BLE_GAP_ADV_TYPE_ADV_IND);
        break;

    case BLE_GAP_EVT_TIMEOUT:
        //led_blink(4);
        if (p_ble_evt->evt.gap_evt.params.timeout.src == BLE_GAP_TIMEOUT_SRC_ADVERTISING)
        {
            if (m_advertising_mode == BLE_SLEEP)
            {
                mimas_sleep_mode_enter();
            }
            else
            {
                advertising_start(BLE_GAP_ADV_TYPE_ADV_NONCONN_IND);
            }
        }
        break;

    case BLE_GATTC_EVT_TIMEOUT:
    case BLE_GATTS_EVT_TIMEOUT:
        //led_blink(3);
        // Disconnect on GATT Server and Client timeout events.
        err_code = sd_ble_gap_disconnect(global_connect_handle,
                                         BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        APP_ERROR_CHECK(err_code);
        break;
#if 0
    case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
        err_code = sd_ble_gap_sec_params_reply(global_connect_handle,
                                               BLE_GAP_SEC_STATUS_SUCCESS, &m_sec_params, NULL);
        APP_ERROR_CHECK(err_code);
        break;
#endif
    case BLE_GAP_EVT_SCAN_REQ_REPORT:
        //led_blink(1);
        if (m_adv_in_mic == false)
        {
            m_adv_in_mic = true;
            app_timer_start(m_adv_in_mic_timer_id, APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER), NULL);
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)advertising_init_microduino);
        }
        break;
    default:
        // No implementation needed.
        break;
    }
}

bool leda_if_connected(void)
{
    if (global_connect_handle == BLE_CONN_HANDLE_INVALID)
    {
        return false;
    }
    return true;
}

/**@brief Function for handling the Application's system events.
 *
 * @param[in] sys_evt  system event.
 */
static void on_sys_evt(uint32_t sys_evt)
{
    //LOG_ENTER;
    switch(sys_evt)
    {
    case NRF_EVT_FLASH_OPERATION_SUCCESS:
    case NRF_EVT_FLASH_OPERATION_ERROR:

        if (m_memory_access_in_progress)
        {
            m_memory_access_in_progress = false;
            advertising_start(BLE_GAP_ADV_TYPE_ADV_NONCONN_IND);
        }
        break;

    default:
        // No implementation needed.
        break;
    }
}

/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t *p_ble_evt)
{
    //LOG_ENTER;
    //nrf_events_strings_log(p_ble_evt->header.evt_id);
    //LOG("evt_len = %d\r\n", p_ble_evt->header.evt_len);

    tethys_ble_nus_on_ble_evt(&m_tethys_nus, p_ble_evt);
    leda_ble_nus_on_ble_evt(&m_leda_nus, p_ble_evt);
    dm_ble_evt_handler(p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    tw_ble_ias_on_ble_evt(&m_ias, p_ble_evt);
    ble_lls_on_ble_evt(&m_lls, p_ble_evt);
    ble_bas_on_ble_evt(&m_bas, p_ble_evt);
    ble_tps_on_ble_evt(&m_tps, p_ble_evt);
    bsp_btn_ble_on_ble_evt(p_ble_evt);
#ifdef BLE_DFU_APP_SUPPORT
    /** @snippet [Propagating BLE Stack events to DFU Service] */
    ble_dfu_on_ble_evt(&m_dfus, p_ble_evt);
    /** @snippet [Propagating BLE Stack events to DFU Service] */
#endif // BLE_DFU_APP_SUPPORT
    on_ble_evt(p_ble_evt);
}

/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in] sys_evt  System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
    //LOG_ENTER;
    //nrf_sys_events_strings_log(sys_evt);
    pstorage_sys_event_handler(sys_evt);
    on_sys_evt(sys_evt);
}

/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    //LOG_ENTER;
    uint32_t err_code;

    // Initialize the SoftDevice handler module.
    SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_XTAL_20_PPM, NULL);

    // Enable BLE stack.
    ble_enable_params_t ble_enable_params;
    memset(&ble_enable_params, 0, sizeof(ble_enable_params));
#if (defined(S130) || defined(S132))
    ble_enable_params.gatts_enable_params.attr_tab_size   = BLE_GATTS_ATTR_TAB_SIZE_DEFAULT;
#endif
    ble_enable_params.gatts_enable_params.service_changed = IS_SRVC_CHANGED_CHARACT_PRESENT;
    err_code = sd_ble_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the Device Manager events.
 *
 * @param[in] p_evt  Data associated to the device manager event.
 */
static uint32_t device_manager_evt_handler(dm_handle_t const *p_handle,
        dm_event_t const   *p_event,
        ret_code_t        event_result)
{
    LOG_ENTER;
    APP_ERROR_CHECK(event_result);
#ifdef BLE_DFU_APP_SUPPORT
    if (p_event->event_id == DM_EVT_LINK_SECURED)
    {
        app_context_load(p_handle);
    }
#endif // BLE_DFU_APP_SUPPORT
    return NRF_SUCCESS;
}


/**@brief Function for the Device Manager initialization.
 *
 * @param[in] erase_bonds  Indicates whether bonding information should be cleared from
 *                         persistent storage during initialization of the Device Manager.
 */
static void device_manager_init(bool erase_bonds)
{
    LOG("Enter device_manager_init(), erase_bonds = %d\r\n", erase_bonds);
    uint32_t               err_code;
    dm_init_param_t        init_param = {.clear_persistent_data = erase_bonds};
    dm_application_param_t register_param;

    // Initialize persistent storage module.
    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);

    err_code = dm_init(&init_param);
    APP_ERROR_CHECK(err_code);

    memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));

    register_param.sec_param.bond         = SEC_PARAM_BOND;
    register_param.sec_param.mitm         = SEC_PARAM_MITM;
    register_param.sec_param.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    register_param.sec_param.oob          = SEC_PARAM_OOB;
    register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    register_param.evt_handler            = device_manager_evt_handler;
    register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

    err_code = dm_register(&m_app_handle, &register_param);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated when button is pressed.
 */
static void bsp_event_handler(bsp_event_t event)
{
    //LOG_ENTER;
    static uint8_t flag = 1;
    //uint32_t err_code;
    //bsp_indication_t ind;
    LOG(LEVEL_INFO, "event = %d", event);
    switch (event)
    {
    case BSP_EVENT_SLEEP:
        //sleep_mode_enter();
        break;

    case BSP_EVENT_DISCONNECT:
        break;

    case BSP_EVENT_KEY_0:
        break;

    case BSP_EVENT_KEY_1:
        break;

    case BSP_EVENT_INTO_CONNECTABLE:
        LOG("BSP_EVENT_INTO_CONNECTABLE\r\n");
        if (flag)
        {
            flag = 0;
            //open_LEDs();
        }
        else
        {
            flag = 1;
            //close_LEDS();
        }
        //tethys_factory_reset();
        break;

    case BSP_EVENT_INTO_NON_CONNECTABLE:
        LOG("BSP_EVENT_INTO_NON_CONNECTABLE\r\n");
        break;

    case BSP_EVENT_ALERT:
        LOG("BSP_EVENT_ALERT\r\n");
        //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)alert_phone);
        break;
    default:
        break;
    }
}

/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool *p_erase_bonds)
{
    //LOG_ENTER;
    //bsp_event_t startup_event;
#if 0
    uint32_t err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS,
                                 APP_TIMER_TICKS(100, APP_TIMER_PRESCALER),
                                 bsp_event_handler);
#else
    uint32_t err_code = bsp_init(BSP_INIT_LED,
                                 APP_TIMER_TICKS(100, APP_TIMER_PRESCALER),
                                 bsp_event_handler);
#endif
    APP_ERROR_CHECK(err_code);

#if 0
    err_code = bsp_btn_ble_init(NULL, NULL);
    APP_ERROR_CHECK(err_code);
#endif
    *p_erase_bonds = false;
}

#ifdef LEDA_WDT
nrf_drv_wdt_channel_id m_channel_id;
/**
 * @brief WDT events handler.
 */
static void leda_wdt_event_handler(void)
{
    //LEDS_OFF(LEDS_MASK);
    LOG_ENTER;
    //NOTE: The max amount of time we can spend in WDT interrupt is two cycles of 32768[Hz] clock - after that, reset occurs
}

static void leda_wdt_config(void)
{
    uint32_t err_code = NRF_SUCCESS;
    //Configure WDT.
    nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
    err_code = nrf_drv_wdt_init(&config, leda_wdt_event_handler);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_wdt_channel_alloc(&m_channel_id);
    APP_ERROR_CHECK(err_code);
    nrf_drv_wdt_enable();
}

void leda_wdt_feed(void)
{
    nrf_drv_wdt_channel_feed(m_channel_id);
}
#endif

void get_nrf51822_temperature(void *p_event_data, uint16_t event_size)
{
    int32_t temp;
    sd_temp_get(&temp);
    LOG("temp = %d\r\n", temp);
#if 0
    float cTemp = (temp - 32) * 5;
    cTemp /= 9.0;
    cTemp -= 7.0;
#else
    float cTemp = (float)temp / 4;
    cTemp -= 3.0;
#endif
    LOG("temperature:%.2f;\r\n", cTemp);
}

static void gpio_init(void)
{
    uint32_t i = 0;
    for(i = 0; i < 32 ; ++i )
    {
        NRF_GPIO->PIN_CNF[i] = (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos)
                               | (GPIO_PIN_CNF_DRIVE_S0S1 << GPIO_PIN_CNF_DRIVE_Pos)
                               | (GPIO_PIN_CNF_PULL_Disabled << GPIO_PIN_CNF_PULL_Pos)
                               | (GPIO_PIN_CNF_INPUT_Disconnect << GPIO_PIN_CNF_INPUT_Pos)
                               | (GPIO_PIN_CNF_DIR_Input << GPIO_PIN_CNF_DIR_Pos);
    }
}


static void bootloader_protect(void)
{
    //NRF_MPU->DISABLEINDEBUG = MPU_DISABLEINDEBUG_DISABLEINDEBUG_Disabled << MPU_DISABLEINDEBUG_DISABLEINDEBUG_Pos;
    NRF_MPU->PROTENSET1 = MPU_PROTENSET1_PROTREG63_Msk | MPU_PROTENSET1_PROTREG62_Msk | MPU_PROTENSET1_PROTREG61_Msk | MPU_PROTENSET1_PROTREG60_Msk;
}

/**@snippet [UART Initialization] */
/**@brief Function for application main entry.
 */
int main(void)
{
    bool erase_bonds;

    gpio_init();
    timers_init();
    scheduler_init();
    buttons_leds_init(&erase_bonds);
    open_LED(3);
    ble_stack_init();//协议栈初始化
    adc_configure();
    device_manager_init(erase_bonds);
    leda_pstorage_init();
    gap_params_init();//Generic Access Profile
    leda_services_init();//需要开启的服务初始化
    conn_params_init();//连接参数设置
    bootloader_protect();
    LOG("PSTORAGE_FLASH_PAGE_SIZE : %d\r\n", PSTORAGE_FLASH_PAGE_SIZE);
    LOG("CODESIZE : %u\r\n", NRF_FICR->CODESIZE);
    LOG("CODEPAGESIZE : %u\r\n", NRF_FICR->CODEPAGESIZE);
    LOG("BOOTLOADER_ADDRESS : 0x%x\r\n", BOOTLOADER_ADDRESS);

    //LOG("SCHED_MAX_EVENT_DATA_SIZE = %d\r\n", SCHED_MAX_EVENT_DATA_SIZE);
    //LOG("RESETREAS:%x; POWER_RESETREAS_RESETPIN_Msk:%x\r\n", NRF_POWER->RESETREAS, POWER_RESETREAS_RESETPIN_Msk);
    //LOG("uint64_t:%d", sizeof(uint64_t));
    //NRF_POWER->RESETREAS = 0;
    //NRF_POWER->RESETREAS &= POWER_RESETREAS_SREQ_Msk;
    //LOG("RESETREAS:%x; POWER_RESETREAS_RESETPIN_Msk:%x\r\n",NRF_POWER->RESETREAS,POWER_RESETREAS_RESETPIN_Msk);
    //app_sched_event_put(NULL, 0, get_nrf51822_temperature);

    if (NRF_POWER->RESETREAS & POWER_RESETREAS_OFF_Msk)
    {
        init_LED(false);
    }
    else
    {
        init_LED(true);
    }
    NRF_POWER->RESETREAS = 0;
    init_battery();
    init_leda_pmu();
#ifdef LEDA_WDT
    leda_wdt_config();
#endif
    leda_init_communicate();
    leda_uart_init();
    nrf_gpio_cfg_output(AVR_RESET_PIN);
    nrf_gpio_pin_clear(AVR_RESET_PIN);
    // Enter main loop.
    mimas_loop();
}

/**
 * @}
 */
