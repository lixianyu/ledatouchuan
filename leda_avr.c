#include <stdint.h>
#include "string.h"

#include "mimas_bsp.h"
#include "config.h"
#include "leda_communicate_protocol.h"
#include "nrf_error.h"
#include <app_scheduler.h>
#include "app_error.h"
#include "app_timer.h"
#include "nrf_drv_rng.h"
#include "mimas_log.h"
#include "mimas_ble_flash.h"
#include "ble_hci.h"
#include "ble_conn_params.h"
#include "app_trace.h"

#include "mimas_config.h"
#include "tethys_led.h"
#include "leda_nrf_drv_uart.h"
#include "app_uart.h"
#include "main_common.h"
#include "leda_file.h"

extern bool gEndByApp;
extern bool gLedaIsTransmittingBin;
extern bool gHaveUpdateCoonParamToMin;
extern uint8_t g_pstorage_buffer[32];

//static uint8_t rx_buffer[1];
uint32_t avr_uart_init(uint32_t baudrate)
{
    uint32_t err_code = NRF_SUCCESS;
    app_uart_comm_params_t comm_params =
    {
        LEDA_RX_PIN,
        LEDA_TX_PIN,
        0xFF,
        0xFF,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        baudrate//UART_BAUDRATE_BAUDRATE_Baud115200
    };
#if 0
    APP_UART_FIFO_INIT( &comm_params,
                        FINGERPRINT_UART_RX_BUF_SIZE,
                        FINGERPRINT_UART_TX_BUF_SIZE,
                        fingerprint_uart_handle,
                        APP_IRQ_PRIORITY_LOW,
                        err_code);
#endif
    app_uart_comm_params_t * p_comm_params = &comm_params;
    nrf_drv_uart_config_t config;
    config.baudrate = (nrf_uart_baudrate_t)p_comm_params->baud_rate;
    config.hwfc = (p_comm_params->flow_control == APP_UART_FLOW_CONTROL_DISABLED) ?
            NRF_UART_HWFC_DISABLED : NRF_UART_HWFC_ENABLED;
    config.interrupt_priority = APP_IRQ_PRIORITY_LOW;
    config.parity = p_comm_params->use_parity ? NRF_UART_PARITY_INCLUDED : NRF_UART_PARITY_EXCLUDED;
    config.pselcts = p_comm_params->cts_pin_no;
    config.pselrts = p_comm_params->rts_pin_no;
    config.pselrxd = p_comm_params->rx_pin_no;
    config.pseltxd = p_comm_params->tx_pin_no;

    err_code = nrf_drv_uart_init(&config, NULL);

    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    nrf_drv_uart_rx_enable();
    //return nrf_drv_uart_rx_avr(rx_buffer, 1);
    return 0;
}

void avr_uart_uninit(void)
{
    nrf_drv_uart_uninit();
#if 1
    NRF_UART0->TASKS_STOPTX = 1;
    NRF_UART0->TASKS_STOPRX = 1;
    NRF_UART0->ENABLE = 0;
#endif
}

