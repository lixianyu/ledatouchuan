/* Copyright (c) [2014 Baidu]. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File Name          :
 * Author             :
 * Version            : $Revision:$
 * Date               : $Date:$
 * Description        :
 *
 * HISTORY:
 * Date               | Modification                    | Author
 * 28/03/2014         | Initial Revision                |

 */
#ifndef _CONFIG_H__
#define _CONFIG_H__
#include <stddef.h>
#include <stdbool.h>
#include <nordic_common.h>

#define SW_REV_STR                      "0.0.0805.04"


#define APP_ADV_INTERVAL_FAST               1601        /**< Fast advertising interval (in units of 0.625 ms. This value corresponds to 1000 ms). */
#define APP_ADV_INTERVAL_SLOW               5000      /**< Slow advertising interval (in units of 0.625 ms. This value corresponds to 2 seconds). */
#define APP_SLOW_ADV_TIMEOUT                180       /**< The duration of the slow advertising period (in seconds). */
#define APP_FAST_ADV_TIMEOUT                30        /**< The duration of the fast advertising period (in seconds). */

#define APP_TIMER_OP_QUEUE_SIZE           10                                                 /**< Size of timer operation queues. */

#define BATTERY_LEVEL_MEAS_INTERVAL       APP_TIMER_TICKS(10000, APP_TIMER_PRESCALER)        /**< Battery level measurement interval (ticks). */

/* Apple Connnection Params Limits
 1. Interval Max * (Slave Latency + 1) �� 2 seconds��
 2. Interval Min �� 20 ms��
 3. Interval Min + 20 ms �� Interval Max��
 4. Slave Latency �� 4��
 5. connSupervisionTimeout �� 6 seconds
 6. Interval Max * (Slave Latency + 1) * 3 < connSupervisionTimeout
 */
#define MIN_CONN_INTERVAL_DEFAULT                   MSEC_TO_UNITS(31, UNIT_1_25_MS)            /**< Minimum acceptable connection interval (0.5 seconds).  */
#define MAX_CONN_INTERVAL_DEFAULT                   MSEC_TO_UNITS(51, UNIT_1_25_MS)            /**< Maximum acceptable connection interval (1 second). */
#define SLAVE_LATENCY_DEFAULT                       0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT_DEFAULT                    MSEC_TO_UNITS(6000, UNIT_10_MS)             /**< Connection supervisory timeout (6 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY      APP_TIMER_TICKS(100, APP_TIMER_PRESCALER)   /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY       APP_TIMER_TICKS(500, APP_TIMER_PRESCALER)   /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT        4                                            /**< Number of attempts before giving up the connection parameter negotiation. */

#define INITIAL_LLS_ALERT_LEVEL           BLE_CHAR_ALERT_LEVEL_NO_ALERT                     /**< Initial value for the Alert Level characteristic in the Link Loss service. */
#define TX_POWER_LEVEL (RADIO_TXPOWER_TXPOWER_Pos4dBm)/**< TX Power Level value. This will be set both in the TX Power service, in the advertising data, and also used to set the radio transmit power. */

/*****************************************************************************
* add codes for enalbe app schedule
******************************************************************************/
#define SCHED_MAX_EVENT_DATA_SIZE       16  /**< Maximum size of scheduler events. Note that scheduler BLE stack events do not contain any data, as the events are being pulled from the stack in the event handler. */
#define SCHED_QUEUE_SIZE                20  /**< Maximum number of events in the scheduler queue. */

#endif  //_CONFIG_H__
