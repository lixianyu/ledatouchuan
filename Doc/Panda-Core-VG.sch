<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.4">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="11" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="13" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="7" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="14" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="7" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="14" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="14" fill="6" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="11" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="13" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="no" active="no"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="190" name="testFixtureBottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="191" name="testFixtureUpper" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="#_Microduino">
<packages>
<package name="0402-RES_L">
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1" roundness="50"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1" roundness="50"/>
<text x="0" y="0.873125" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-0.873125" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1778" y1="-0.3556" x2="0.1778" y2="0.3556" layer="21"/>
</package>
<package name="0805-RES">
<wire x1="-1.016" y1="0.555625" x2="1.016" y2="0.555625" width="0.1016" layer="21"/>
<wire x1="-1.016" y1="-0.555625" x2="1.016" y2="-0.555625" width="0.1016" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1" roundness="40"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1" roundness="40"/>
<text x="0" y="1.11125" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-1.11125" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
<rectangle x1="-0.3048" y1="-0.508" x2="0.3048" y2="0.508" layer="21"/>
<rectangle x1="-1.076325" y1="-0.508" x2="-0.576225" y2="0.508" layer="51"/>
<rectangle x1="0.568325" y1="-0.508" x2="1.068425" y2="0.508" layer="51"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.4984" y="0" dx="0.508" dy="0.7112" layer="1" roundness="50"/>
<smd name="2" x="0.4984" y="0" dx="0.508" dy="0.7112" layer="1" roundness="50"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1778" y1="-0.3556" x2="0.1778" y2="0.3556" layer="21"/>
<text x="0" y="0.873125" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-0.873125" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
</package>
<package name="0402-CAP_L">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1" roundness="50"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1" roundness="50"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<circle x="0" y="0" radius="0.0254" width="0.4064" layer="21"/>
<text x="0" y="1.031875" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-1.031875" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
</package>
<package name="0603-CAP">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.5588" layer="21"/>
<smd name="1" x="-0.7" y="0" dx="0.75" dy="1" layer="1" roundness="50"/>
<smd name="2" x="0.7" y="0" dx="0.75" dy="1" layer="1" roundness="50"/>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<text x="0" y="1.11125" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-1.11125" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="0" y1="0.0254" x2="0" y2="-0.0254" width="0.4064" layer="21"/>
<smd name="1" x="-0.4984" y="0" dx="0.508" dy="0.7112" layer="1" roundness="50"/>
<smd name="2" x="0.4984" y="0" dx="0.508" dy="0.7112" layer="1" roundness="50"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<text x="0" y="0.873125" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-0.873125" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
</package>
<package name="0805-CAP">
<wire x1="-1.016" y1="0.555625" x2="1.016" y2="0.555625" width="0.1016" layer="21"/>
<wire x1="-1.016" y1="-0.555625" x2="1.016" y2="-0.555625" width="0.1016" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1" roundness="40"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1" roundness="40"/>
<text x="0" y="1.11125" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-1.11125" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
<rectangle x1="-1.076325" y1="-0.508" x2="-0.576225" y2="0.508" layer="51"/>
<rectangle x1="0.568325" y1="-0.508" x2="1.068425" y2="0.508" layer="51"/>
<wire x1="0" y1="0.079375" x2="0" y2="-0.079375" width="0.6096" layer="21"/>
</package>
<package name="MICRO-USB-NJ">
<smd name="P1" x="-1.3" y="2.575" dx="0.4" dy="1.35" layer="1" roundness="10"/>
<smd name="P2" x="-0.65" y="2.575" dx="0.4" dy="1.35" layer="1" roundness="10"/>
<smd name="P3" x="0" y="2.575" dx="0.4" dy="1.35" layer="1" roundness="10"/>
<smd name="P4" x="0.65" y="2.575" dx="0.4" dy="1.35" layer="1" roundness="10"/>
<smd name="P5" x="1.3" y="2.575" dx="0.4" dy="1.35" layer="1" roundness="10"/>
<smd name="G9" x="-0.781" y="0" dx="1.27" dy="1.27" layer="1" roundness="10"/>
<smd name="G10" x="0.8065" y="0" dx="1.27" dy="1.27" layer="1" roundness="10" rot="R180"/>
<text x="5.3975" y="-0.889" size="0.6096" layer="25" font="vector" rot="R90" align="center">&gt;NAME</text>
<pad name="G3" x="-3.625" y="-0.2" drill="0.7" shape="octagon"/>
<pad name="G6" x="3.625" y="-0.2" drill="0.7" shape="octagon"/>
<pad name="G2" x="-3.625" y="0" drill="0.7" shape="octagon"/>
<pad name="G1" x="-3.625" y="0.2" drill="0.7" shape="octagon"/>
<pad name="G5" x="3.625" y="0" drill="0.7" shape="octagon"/>
<pad name="G4" x="3.625" y="0.2" drill="0.7" shape="octagon"/>
<pad name="G7" x="-2.425" y="2.65" drill="0.7" shape="octagon"/>
<pad name="G8" x="2.425" y="2.65" drill="0.7" shape="octagon" rot="R90"/>
<wire x1="-3.65125" y1="-2.06375" x2="3.65125" y2="-2.06375" width="0.127" layer="21"/>
<wire x1="3.65125" y1="-2.06375" x2="3.65125" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-3.65125" y1="-3.175" x2="-3.65125" y2="-2.06375" width="0.127" layer="21"/>
<wire x1="-3.65125" y1="-3.175" x2="-4.1275" y2="-3.175" width="0.127" layer="21"/>
<wire x1="3.65125" y1="-3.175" x2="4.1275" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="2.8575" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.8575" x2="3.81" y2="2.8575" width="0.127" layer="51"/>
<wire x1="3.81" y1="2.8575" x2="3.81" y2="-1.905" width="0.127" layer="51"/>
<wire x1="-3.65125" y1="-3.175" x2="-3.65125" y2="-2.06375" width="0.127" layer="20"/>
<wire x1="-3.65125" y1="-3.175" x2="-4.1275" y2="-3.175" width="0.127" layer="20"/>
<wire x1="-3.65125" y1="-2.06375" x2="3.65125" y2="-2.06375" width="0.127" layer="20"/>
<wire x1="3.65125" y1="-2.06375" x2="3.65125" y2="-3.175" width="0.127" layer="20"/>
<wire x1="3.65125" y1="-3.175" x2="4.1275" y2="-3.175" width="0.127" layer="20"/>
</package>
<package name="SSOP20">
<wire x1="-3.1646" y1="-2.75" x2="3.1646" y2="-2.75" width="0.1524" layer="21"/>
<wire x1="3.1646" y1="2.75" x2="3.1646" y2="-2.75" width="0.1524" layer="21"/>
<wire x1="3.1646" y1="2.75" x2="-3.1646" y2="2.75" width="0.1524" layer="21"/>
<wire x1="-3.1646" y1="-2.75" x2="-3.1646" y2="2.75" width="0.1524" layer="21"/>
<circle x="-2.2756" y="-1.2192" radius="0.4572" width="0.1524" layer="21"/>
<smd name="1" x="-2.925" y="-3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-2.275" y="-3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-1.625" y="-3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="-0.975" y="-3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="-0.325" y="-3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="0.325" y="-3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="0.975" y="-3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="1.625" y="-3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="2.275" y="-3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="2.925" y="-3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="2.925" y="3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="2.275" y="3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="1.625" y="3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="0.975" y="3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="15" x="0.325" y="3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="16" x="-0.325" y="3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="17" x="-0.975" y="3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="18" x="-1.625" y="3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="19" x="-2.275" y="3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="20" x="-2.925" y="3.6178" dx="0.3048" dy="0.9906" layer="1"/>
<text x="0" y="0" size="0.6096" layer="25" font="vector" ratio="10" rot="R180" align="center">&gt;NAME</text>
<rectangle x1="-3.0266" y1="-3.821" x2="-2.8234" y2="-2.9828" layer="51"/>
<rectangle x1="-2.3766" y1="-3.821" x2="-2.1734" y2="-2.9828" layer="51"/>
<rectangle x1="-1.7266" y1="-3.821" x2="-1.5234" y2="-2.9828" layer="51"/>
<rectangle x1="-1.0766" y1="-3.821" x2="-0.8734" y2="-2.9828" layer="51"/>
<rectangle x1="-0.4266" y1="-3.821" x2="-0.2234" y2="-2.9828" layer="51"/>
<rectangle x1="0.2234" y1="-3.821" x2="0.4266" y2="-2.9828" layer="51"/>
<rectangle x1="0.8734" y1="-3.821" x2="1.0766" y2="-2.9828" layer="51"/>
<rectangle x1="1.5234" y1="-3.821" x2="1.7266" y2="-2.9828" layer="51"/>
<rectangle x1="2.1734" y1="-3.821" x2="2.3766" y2="-2.9828" layer="51"/>
<rectangle x1="2.8234" y1="-3.821" x2="3.0266" y2="-2.9828" layer="51"/>
<rectangle x1="2.8234" y1="2.9828" x2="3.0266" y2="3.821" layer="51"/>
<rectangle x1="2.1734" y1="2.9828" x2="2.3766" y2="3.821" layer="51"/>
<rectangle x1="1.5234" y1="2.9828" x2="1.7266" y2="3.821" layer="51"/>
<rectangle x1="0.8734" y1="2.9828" x2="1.0766" y2="3.821" layer="51"/>
<rectangle x1="0.2234" y1="2.9828" x2="0.4266" y2="3.821" layer="51"/>
<rectangle x1="-0.4266" y1="2.9828" x2="-0.2234" y2="3.821" layer="51"/>
<rectangle x1="-1.0766" y1="2.9828" x2="-0.8734" y2="3.821" layer="51"/>
<rectangle x1="-1.7266" y1="2.9828" x2="-1.5234" y2="3.821" layer="51"/>
<rectangle x1="-2.3766" y1="2.9828" x2="-2.1734" y2="3.821" layer="51"/>
<rectangle x1="-3.0266" y1="2.9828" x2="-2.8234" y2="3.821" layer="51"/>
</package>
<package name="SOIC16">
<description>&lt;b&gt;SMALL OUTLINE INTEGRATED CIRCUIT&lt;/b&gt;&lt;p&gt;
16-Pin (150-Mil) SOIC&lt;br&gt;
Source: http://www.cypress.com .. 38-12025_0P_V.pdf</description>
<wire x1="4.79" y1="-1.795" x2="-4.79" y2="-1.795" width="0.2032" layer="21"/>
<wire x1="-4.79" y1="-1.795" x2="-4.79" y2="1.795" width="0.2032" layer="21"/>
<wire x1="-4.79" y1="1.795" x2="4.79" y2="1.795" width="0.2032" layer="21"/>
<wire x1="4.79" y1="1.795" x2="4.79" y2="-1.795" width="0.2032" layer="21"/>
<circle x="-4.05" y="-0.995" radius="0.3256" width="0.2032" layer="21"/>
<smd name="2" x="-3.175" y="-2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="13" x="-0.635" y="2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="1" x="-4.445" y="-2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="3" x="-1.905" y="-2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="4" x="-0.635" y="-2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="14" x="-1.905" y="2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="12" x="0.635" y="2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="11" x="1.905" y="2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="6" x="1.905" y="-2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="9" x="4.445" y="2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="5" x="0.635" y="-2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="7" x="3.175" y="-2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="10" x="3.175" y="2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="8" x="4.445" y="-2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="15" x="-3.175" y="2.695" dx="0.635" dy="1.524" layer="1"/>
<smd name="16" x="-4.445" y="2.695" dx="0.635" dy="1.524" layer="1"/>
<rectangle x1="-4.6901" y1="-2.921" x2="-4.1999" y2="-1.8951" layer="51"/>
<rectangle x1="-3.4201" y1="-2.921" x2="-2.9299" y2="-1.8951" layer="51"/>
<rectangle x1="-2.1501" y1="-2.921" x2="-1.6599" y2="-1.8951" layer="51"/>
<rectangle x1="-0.8801" y1="-2.921" x2="-0.3899" y2="-1.8951" layer="51"/>
<rectangle x1="0.3899" y1="-2.921" x2="0.8801" y2="-1.8951" layer="51"/>
<rectangle x1="1.6599" y1="-2.921" x2="2.1501" y2="-1.8951" layer="51"/>
<rectangle x1="2.9299" y1="-2.921" x2="3.4201" y2="-1.8951" layer="51"/>
<rectangle x1="4.1999" y1="-2.921" x2="4.6901" y2="-1.8951" layer="51"/>
<rectangle x1="4.1999" y1="1.8951" x2="4.6901" y2="2.921" layer="51"/>
<rectangle x1="2.9299" y1="1.8951" x2="3.4201" y2="2.921" layer="51"/>
<rectangle x1="1.6599" y1="1.8951" x2="2.1501" y2="2.921" layer="51"/>
<rectangle x1="0.3899" y1="1.8951" x2="0.8801" y2="2.921" layer="51"/>
<rectangle x1="-0.8801" y1="1.8951" x2="-0.3899" y2="2.921" layer="51"/>
<rectangle x1="-2.1501" y1="1.8951" x2="-1.6599" y2="2.921" layer="51"/>
<rectangle x1="-3.4201" y1="1.8951" x2="-2.9299" y2="2.921" layer="51"/>
<rectangle x1="-4.6901" y1="1.8951" x2="-4.1999" y2="2.921" layer="51"/>
<text x="0" y="0" size="0.6096" layer="21" font="vector" align="center">&gt;NAME</text>
</package>
<package name="CRYSTAL-3225">
<smd name="4" x="-1.1" y="0.85" dx="1.273" dy="1.073" layer="1" roundness="40"/>
<smd name="3" x="1.1" y="0.85" dx="1.273" dy="1.073" layer="1" roundness="40"/>
<smd name="2" x="1.1" y="-0.85" dx="1.273" dy="1.073" layer="1" roundness="40"/>
<smd name="1" x="-1.1" y="-0.85" dx="1.273" dy="1.073" layer="1" roundness="40"/>
<wire x1="-1.5875" y1="1.27" x2="-1.5875" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-1.5875" y1="-1.27" x2="1.5875" y2="-1.27" width="0.127" layer="51"/>
<wire x1="1.5875" y1="-1.27" x2="1.5875" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.5875" y1="1.27" x2="-1.5875" y2="1.27" width="0.127" layer="51"/>
<text x="0" y="1.905" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="2.54" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
</package>
<package name="27UPIN-COOKIE-PAD-NOF">
<smd name="1" x="0" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="2" x="2.54" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="3" x="5.08" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="4" x="7.62" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="5" x="10.16" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="6" x="12.7" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="7" x="15.24" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="8" x="17.78" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="9" x="20.32" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="10" x="22.86" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="11" x="22.86" y="-7.62" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="12" x="22.86" y="-5.08" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="13" x="22.86" y="-2.54" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="14" x="22.86" y="0" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="15" x="22.86" y="2.54" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="16" x="22.86" y="5.08" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="17" x="22.86" y="7.62" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="18" x="22.86" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="19" x="20.32" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="20" x="17.78" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="21" x="15.24" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="22" x="12.7" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="23" x="10.16" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="24" x="7.62" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="25" x="5.08" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="26" x="2.54" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="27" x="0" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<wire x1="27.5" y1="-12" x2="27.5" y2="-16" width="0.127" layer="51"/>
<wire x1="27.5" y1="-16" x2="23.5" y2="-20" width="0.127" layer="51" curve="-90"/>
<wire x1="23.5" y1="-20" x2="-0.5" y2="-20" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-20" x2="-4.5" y2="-16" width="0.127" layer="51" curve="-90"/>
<wire x1="-4.5" y1="-16" x2="-4.5" y2="-12" width="0.127" layer="51"/>
<wire x1="27.5" y1="12" x2="27.5" y2="16" width="0.127" layer="51"/>
<wire x1="27.5" y1="16" x2="23.5" y2="20" width="0.127" layer="51" curve="90"/>
<wire x1="23.5" y1="20" x2="-0.5" y2="20" width="0.127" layer="51"/>
<wire x1="-0.5" y1="20" x2="-4.5" y2="16" width="0.127" layer="51" curve="90"/>
<wire x1="-4.5" y1="16" x2="-4.5" y2="12" width="0.127" layer="51"/>
<hole x="21.5" y="16" drill="5.2"/>
<hole x="21.5" y="-16" drill="5.2"/>
<hole x="1.5" y="16" drill="5.2"/>
<hole x="1.5" y="-16" drill="5.2"/>
<wire x1="3.5" y1="12" x2="-4.5" y2="12" width="0.127" layer="48"/>
<wire x1="-4.5" y1="12" x2="-4.5" y2="20" width="0.127" layer="48"/>
<wire x1="-4.5" y1="20" x2="3.5" y2="20" width="0.127" layer="48"/>
<wire x1="3.5" y1="20" x2="3.5" y2="12" width="0.127" layer="48"/>
<wire x1="11.5" y1="12" x2="3.5" y2="12" width="0.127" layer="48"/>
<wire x1="3.5" y1="12" x2="3.5" y2="20" width="0.127" layer="48"/>
<wire x1="3.5" y1="20" x2="11.5" y2="20" width="0.127" layer="48"/>
<wire x1="11.5" y1="20" x2="11.5" y2="12" width="0.127" layer="48"/>
<wire x1="19.5" y1="12" x2="11.5" y2="12" width="0.127" layer="48"/>
<wire x1="11.5" y1="12" x2="11.5" y2="20" width="0.127" layer="48"/>
<wire x1="11.5" y1="20" x2="19.5" y2="20" width="0.127" layer="48"/>
<wire x1="19.5" y1="20" x2="19.5" y2="12" width="0.127" layer="48"/>
<wire x1="27.5" y1="12" x2="19.5" y2="12" width="0.127" layer="48"/>
<wire x1="19.5" y1="12" x2="19.5" y2="20" width="0.127" layer="48"/>
<wire x1="19.5" y1="20" x2="27.5" y2="20" width="0.127" layer="48"/>
<wire x1="27.5" y1="20" x2="27.5" y2="12" width="0.127" layer="48"/>
<wire x1="3.5" y1="4" x2="-4.5" y2="4" width="0.127" layer="48"/>
<wire x1="-4.5" y1="4" x2="-4.5" y2="12" width="0.127" layer="48"/>
<wire x1="-4.5" y1="12" x2="3.5" y2="12" width="0.127" layer="48"/>
<wire x1="3.5" y1="12" x2="3.5" y2="4" width="0.127" layer="48"/>
<wire x1="3.5" y1="4" x2="3.5" y2="12" width="0.127" layer="48"/>
<wire x1="3.5" y1="12" x2="11.5" y2="12" width="0.127" layer="48"/>
<wire x1="11.5" y1="12" x2="11.5" y2="4" width="0.127" layer="48"/>
<wire x1="19.5" y1="4" x2="11.5" y2="4" width="0.127" layer="48"/>
<wire x1="11.5" y1="4" x2="11.5" y2="12" width="0.127" layer="48"/>
<wire x1="11.5" y1="12" x2="19.5" y2="12" width="0.127" layer="48"/>
<wire x1="19.5" y1="12" x2="19.5" y2="4" width="0.127" layer="48"/>
<wire x1="27.5" y1="4" x2="19.5" y2="4" width="0.127" layer="48"/>
<wire x1="19.5" y1="4" x2="19.5" y2="12" width="0.127" layer="48"/>
<wire x1="19.5" y1="12" x2="27.5" y2="12" width="0.127" layer="48"/>
<wire x1="27.5" y1="12" x2="27.5" y2="4" width="0.127" layer="48"/>
<wire x1="3.5" y1="-4" x2="-4.5" y2="-4" width="0.127" layer="48"/>
<wire x1="-4.5" y1="-4" x2="-4.5" y2="4" width="0.127" layer="48"/>
<wire x1="-4.5" y1="4" x2="3.5" y2="4" width="0.127" layer="48"/>
<wire x1="3.5" y1="4" x2="3.5" y2="-4" width="0.127" layer="48"/>
<wire x1="11.5" y1="-4" x2="3.5" y2="-4" width="0.127" layer="48"/>
<wire x1="3.5" y1="-4" x2="3.5" y2="4" width="0.127" layer="48"/>
<wire x1="3.5" y1="4" x2="11.5" y2="4" width="0.127" layer="48"/>
<wire x1="11.5" y1="4" x2="11.5" y2="-4" width="0.127" layer="48"/>
<wire x1="19.5" y1="-4" x2="11.5" y2="-4" width="0.127" layer="48"/>
<wire x1="11.5" y1="-4" x2="11.5" y2="4" width="0.127" layer="48"/>
<wire x1="11.5" y1="4" x2="19.5" y2="4" width="0.127" layer="48"/>
<wire x1="19.5" y1="4" x2="19.5" y2="-4" width="0.127" layer="48"/>
<wire x1="27.5" y1="-4" x2="19.5" y2="-4" width="0.127" layer="48"/>
<wire x1="19.5" y1="-4" x2="19.5" y2="4" width="0.127" layer="48"/>
<wire x1="19.5" y1="4" x2="27.5" y2="4" width="0.127" layer="48"/>
<wire x1="27.5" y1="4" x2="27.5" y2="-4" width="0.127" layer="48"/>
<wire x1="3.5" y1="-12" x2="-4.5" y2="-12" width="0.127" layer="48"/>
<wire x1="-4.5" y1="-12" x2="-4.5" y2="-4" width="0.127" layer="48"/>
<wire x1="-4.5" y1="-4" x2="3.5" y2="-4" width="0.127" layer="48"/>
<wire x1="3.5" y1="-4" x2="3.5" y2="-12" width="0.127" layer="48"/>
<wire x1="3.5" y1="-20" x2="-4.5" y2="-20" width="0.127" layer="48"/>
<wire x1="-4.5" y1="-20" x2="-4.5" y2="-12" width="0.127" layer="48"/>
<wire x1="-4.5" y1="-12" x2="3.5" y2="-12" width="0.127" layer="48"/>
<wire x1="3.5" y1="-12" x2="3.5" y2="-20" width="0.127" layer="48"/>
<wire x1="11.5" y1="-12" x2="3.5" y2="-12" width="0.127" layer="48"/>
<wire x1="3.5" y1="-12" x2="3.5" y2="-4" width="0.127" layer="48"/>
<wire x1="3.5" y1="-4" x2="11.5" y2="-4" width="0.127" layer="48"/>
<wire x1="11.5" y1="-4" x2="11.5" y2="-12" width="0.127" layer="48"/>
<wire x1="19.5" y1="-12" x2="11.5" y2="-12" width="0.127" layer="48"/>
<wire x1="11.5" y1="-12" x2="11.5" y2="-4" width="0.127" layer="48"/>
<wire x1="11.5" y1="-4" x2="19.5" y2="-4" width="0.127" layer="48"/>
<wire x1="19.5" y1="-4" x2="19.5" y2="-12" width="0.127" layer="48"/>
<wire x1="27.5" y1="-12" x2="19.5" y2="-12" width="0.127" layer="48"/>
<wire x1="19.5" y1="-12" x2="19.5" y2="-4" width="0.127" layer="48"/>
<wire x1="19.5" y1="-4" x2="27.5" y2="-4" width="0.127" layer="48"/>
<wire x1="27.5" y1="-4" x2="27.5" y2="-12" width="0.127" layer="48"/>
<wire x1="11.5" y1="-20" x2="3.5" y2="-20" width="0.127" layer="48"/>
<wire x1="3.5" y1="-20" x2="3.5" y2="-12" width="0.127" layer="48"/>
<wire x1="3.5" y1="-12" x2="11.5" y2="-12" width="0.127" layer="48"/>
<wire x1="11.5" y1="-12" x2="11.5" y2="-20" width="0.127" layer="48"/>
<wire x1="19.5" y1="-20" x2="11.5" y2="-20" width="0.127" layer="48"/>
<wire x1="11.5" y1="-20" x2="11.5" y2="-12" width="0.127" layer="48"/>
<wire x1="11.5" y1="-12" x2="19.5" y2="-12" width="0.127" layer="48"/>
<wire x1="19.5" y1="-12" x2="19.5" y2="-20" width="0.127" layer="48"/>
<wire x1="27.5" y1="-20" x2="19.5" y2="-20" width="0.127" layer="48"/>
<wire x1="19.5" y1="-20" x2="19.5" y2="-12" width="0.127" layer="48"/>
<wire x1="19.5" y1="-12" x2="27.5" y2="-12" width="0.127" layer="48"/>
<wire x1="27.5" y1="-12" x2="27.5" y2="-20" width="0.127" layer="48"/>
</package>
<package name="MICRO-USB-LC">
<pad name="3" x="0" y="0" drill="0.45" shape="octagon"/>
<pad name="1" x="-1.3" y="0" drill="0.45" shape="octagon"/>
<pad name="5" x="1.3" y="0" drill="0.45" shape="octagon"/>
<pad name="2" x="-0.65" y="-1" drill="0.45" shape="octagon"/>
<pad name="4" x="0.65" y="-1" drill="0.45" shape="octagon"/>
<pad name="P$1" x="-3.575" y="0" drill="0.35" shape="octagon"/>
<pad name="P$5" x="-3.575" y="-0.125" drill="0.35" shape="octagon"/>
<pad name="P$6" x="-3.575" y="-0.25" drill="0.35" shape="octagon"/>
<pad name="P$7" x="-3.575" y="-0.375" drill="0.35" shape="octagon"/>
<pad name="P$8" x="-3.575" y="-0.5" drill="0.35" shape="octagon"/>
<pad name="P$2" x="3.575" y="0" drill="0.35" shape="octagon"/>
<pad name="P$15" x="3.575" y="-0.125" drill="0.35" shape="octagon"/>
<pad name="P$16" x="3.575" y="-0.25" drill="0.35" shape="octagon"/>
<pad name="P$17" x="3.575" y="-0.375" drill="0.35" shape="octagon"/>
<pad name="P$18" x="3.575" y="-0.5" drill="0.35" shape="octagon"/>
<wire x1="3.575" y1="-1.75" x2="-3.575" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-3.575" y1="-1.75" x2="-3.575" y2="1.15" width="0.127" layer="21"/>
<wire x1="-3.575" y1="1.15" x2="3.575" y2="1.15" width="0.127" layer="21"/>
<wire x1="3.575" y1="1.15" x2="3.575" y2="-1.75" width="0.127" layer="21"/>
</package>
<package name="0603-RES">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="0.75" dy="1" layer="1" roundness="50"/>
<smd name="2" x="0.7" y="0" dx="0.75" dy="1" layer="1" roundness="50"/>
<rectangle x1="-0.8303" y1="-0.4699" x2="-0.3302" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
<text x="0" y="1.11125" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-1.11125" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
</package>
<package name="27UPIN-MICRO-NORMAL">
<pad name="2" x="2.54" y="-10.16" drill="1.4"/>
<pad name="3" x="5.08" y="-10.16" drill="1.4"/>
<pad name="4" x="7.62" y="-10.16" drill="1.4"/>
<pad name="5" x="10.16" y="-10.16" drill="1.4"/>
<pad name="6" x="12.7" y="-10.16" drill="1.4"/>
<pad name="7" x="15.24" y="-10.16" drill="1.4"/>
<pad name="8" x="17.78" y="-10.16" drill="1.4"/>
<pad name="9" x="20.32" y="-10.16" drill="1.4"/>
<pad name="10" x="22.86" y="-10.16" drill="1.4"/>
<pad name="11" x="22.86" y="-7.62" drill="1.4"/>
<pad name="12" x="22.86" y="-5.08" drill="1.4"/>
<pad name="13" x="22.86" y="-2.54" drill="1.4"/>
<pad name="14" x="22.86" y="0" drill="1.4"/>
<pad name="15" x="22.86" y="2.54" drill="1.4"/>
<pad name="16" x="22.86" y="5.08" drill="1.4"/>
<pad name="17" x="22.86" y="7.62" drill="1.4"/>
<pad name="18" x="22.86" y="10.16" drill="1.4"/>
<pad name="19" x="20.32" y="10.16" drill="1.4"/>
<pad name="20" x="17.78" y="10.16" drill="1.4"/>
<pad name="21" x="15.24" y="10.16" drill="1.4"/>
<pad name="22" x="12.7" y="10.16" drill="1.4"/>
<pad name="23" x="10.16" y="10.16" drill="1.4"/>
<pad name="24" x="7.62" y="10.16" drill="1.4"/>
<pad name="25" x="5.08" y="10.16" drill="1.4"/>
<pad name="26" x="2.54" y="10.16" drill="1.4"/>
<pad name="27" x="0" y="10.16" drill="1.4"/>
<pad name="1" x="0" y="-10.16" drill="1.4"/>
<text x="21.2725" y="-5.08" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A1</text>
<text x="21.2725" y="-2.54" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A2</text>
<text x="21.2725" y="0" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A3</text>
<text x="21.2725" y="2.54" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">SDA</text>
<text x="21.2725" y="5.08" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">SCL</text>
<text x="21.2725" y="6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A6</text>
<text x="18.415" y="6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">A7</text>
<text x="5.08" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D7</text>
<text x="7.62" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D8</text>
<text x="10.16" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D9</text>
<text x="12.7" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D10</text>
<text x="15.24" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D11</text>
<text x="17.78" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D12</text>
<text x="20.32" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D13</text>
<text x="0" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">5V</text>
<text x="0" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">GND</text>
<text x="2.54" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">RST</text>
<text x="2.54" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">3V3</text>
<text x="20.32" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D0</text>
<text x="17.78" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D1</text>
<text x="15.24" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D2</text>
<text x="12.7" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D3</text>
<text x="10.16" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D4</text>
<text x="7.62" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D5</text>
<text x="21.2725" y="-6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A0</text>
<text x="5.08" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D6</text>
<text x="18.415" y="-6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">REF</text>
<wire x1="20.32" y1="-7.62" x2="19.685" y2="-6.985" width="0.127" layer="22"/>
<wire x1="19.685" y1="-6.985" x2="17.4244" y2="-6.985" width="0.127" layer="22"/>
<wire x1="21.59" y1="-8.89" x2="22.86" y2="-10.16" width="0.127" layer="22"/>
<wire x1="22.86" y1="10.16" x2="21.59" y2="8.89" width="0.127" layer="22"/>
<wire x1="20.32" y1="7.62" x2="19.685" y2="6.985" width="0.127" layer="22"/>
<wire x1="19.685" y1="6.985" x2="17.4625" y2="6.985" width="0.127" layer="22"/>
<text x="11.43" y="-0.635" size="1.6764" layer="22" font="vector" ratio="14" rot="MR0" align="bottom-center">Microduino</text>
<text x="11.43" y="-3.81" size="1.016" layer="22" font="vector" rot="MR0" align="bottom-center">(Arduino-Compatible)</text>
<text x="11.43" y="1.905" size="1.016" layer="22" font="vector" rot="MR0" align="bottom-center">www.microduino.cc</text>
<text x="4.1275" y="0.47625" size="0.6096" layer="22" font="vector" ratio="6" rot="MR0" align="bottom-center">TM</text>
<text x="25.0825" y="0" size="0.8128" layer="21" font="vector" rot="R90" align="bottom-center">A Product Of MakerModule</text>
</package>
<package name="27UPIN-MICRO-NULL+">
<pad name="2" x="2.54" y="-10.16" drill="1.4"/>
<pad name="3" x="5.08" y="-10.16" drill="1.4"/>
<pad name="4" x="7.62" y="-10.16" drill="1.4"/>
<pad name="5" x="10.16" y="-10.16" drill="1.4"/>
<pad name="6" x="12.7" y="-10.16" drill="1.4"/>
<pad name="7" x="15.24" y="-10.16" drill="1.4"/>
<pad name="8" x="17.78" y="-10.16" drill="1.4"/>
<pad name="9" x="20.32" y="-10.16" drill="1.4"/>
<pad name="10" x="22.86" y="-10.16" drill="1.4"/>
<pad name="11" x="22.86" y="-7.62" drill="1.4"/>
<pad name="12" x="22.86" y="-5.08" drill="1.4"/>
<pad name="13" x="22.86" y="-2.54" drill="1.4"/>
<pad name="14" x="22.86" y="0" drill="1.4"/>
<pad name="15" x="22.86" y="2.54" drill="1.4"/>
<pad name="16" x="22.86" y="5.08" drill="1.4"/>
<pad name="17" x="22.86" y="7.62" drill="1.4"/>
<pad name="18" x="22.86" y="10.16" drill="1.4"/>
<pad name="19" x="20.32" y="10.16" drill="1.4"/>
<pad name="20" x="17.78" y="10.16" drill="1.4"/>
<pad name="21" x="15.24" y="10.16" drill="1.4"/>
<pad name="22" x="12.7" y="10.16" drill="1.4"/>
<pad name="23" x="10.16" y="10.16" drill="1.4"/>
<pad name="24" x="7.62" y="10.16" drill="1.4"/>
<pad name="25" x="5.08" y="10.16" drill="1.4"/>
<pad name="26" x="2.54" y="10.16" drill="1.4"/>
<pad name="27" x="0" y="10.16" drill="1.4"/>
<pad name="1" x="0" y="-10.16" drill="1.4"/>
<text x="21.2725" y="-5.08" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A1</text>
<text x="21.2725" y="-2.54" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A2</text>
<text x="21.2725" y="0" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A3</text>
<text x="21.2725" y="2.54" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">SDA</text>
<text x="21.2725" y="5.08" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">SCL</text>
<text x="21.2725" y="6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A6</text>
<text x="18.415" y="6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">A7</text>
<text x="5.08" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D7</text>
<text x="7.62" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D8</text>
<text x="10.16" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D9</text>
<text x="12.7" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D10</text>
<text x="15.24" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D11</text>
<text x="17.78" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D12</text>
<text x="20.32" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D13</text>
<text x="0" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">5V</text>
<text x="0" y="12.3825" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">GND</text>
<text x="2.54" y="12.3825" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">RST</text>
<text x="2.54" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">3V3</text>
<text x="20.32" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D0</text>
<text x="17.78" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D1</text>
<text x="15.24" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D2</text>
<text x="12.7" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D3</text>
<text x="10.16" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D4</text>
<text x="7.62" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D5</text>
<text x="21.2725" y="-6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A0</text>
<text x="5.08" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D6</text>
<text x="18.415" y="-6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">REF</text>
<wire x1="20.32" y1="-7.62" x2="19.685" y2="-6.985" width="0.127" layer="22"/>
<wire x1="19.685" y1="-6.985" x2="17.4244" y2="-6.985" width="0.127" layer="22"/>
<wire x1="21.59" y1="-8.89" x2="22.86" y2="-10.16" width="0.127" layer="22"/>
<wire x1="22.86" y1="10.16" x2="21.59" y2="8.89" width="0.127" layer="22"/>
<wire x1="20.32" y1="7.62" x2="19.685" y2="6.985" width="0.127" layer="22"/>
<wire x1="19.685" y1="6.985" x2="17.4625" y2="6.985" width="0.127" layer="22"/>
</package>
<package name="27UPIN-MICRO-NORMAL+">
<pad name="2" x="2.54" y="-10.16" drill="1.4"/>
<pad name="3" x="5.08" y="-10.16" drill="1.4"/>
<pad name="4" x="7.62" y="-10.16" drill="1.4"/>
<pad name="5" x="10.16" y="-10.16" drill="1.4"/>
<pad name="6" x="12.7" y="-10.16" drill="1.4"/>
<pad name="7" x="15.24" y="-10.16" drill="1.4"/>
<pad name="8" x="17.78" y="-10.16" drill="1.4"/>
<pad name="9" x="20.32" y="-10.16" drill="1.4"/>
<pad name="10" x="22.86" y="-10.16" drill="1.4"/>
<pad name="11" x="22.86" y="-7.62" drill="1.4"/>
<pad name="12" x="22.86" y="-5.08" drill="1.4"/>
<pad name="13" x="22.86" y="-2.54" drill="1.4"/>
<pad name="14" x="22.86" y="0" drill="1.4"/>
<pad name="15" x="22.86" y="2.54" drill="1.4"/>
<pad name="16" x="22.86" y="5.08" drill="1.4"/>
<pad name="17" x="22.86" y="7.62" drill="1.4"/>
<pad name="18" x="22.86" y="10.16" drill="1.4"/>
<pad name="19" x="20.32" y="10.16" drill="1.4"/>
<pad name="20" x="17.78" y="10.16" drill="1.4"/>
<pad name="21" x="15.24" y="10.16" drill="1.4"/>
<pad name="22" x="12.7" y="10.16" drill="1.4"/>
<pad name="23" x="10.16" y="10.16" drill="1.4"/>
<pad name="24" x="7.62" y="10.16" drill="1.4"/>
<pad name="25" x="5.08" y="10.16" drill="1.4"/>
<pad name="26" x="2.54" y="10.16" drill="1.4"/>
<pad name="27" x="0" y="10.16" drill="1.4"/>
<pad name="1" x="0" y="-10.16" drill="1.4"/>
<text x="21.2725" y="-5.08" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A1</text>
<text x="21.2725" y="-2.54" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A2</text>
<text x="21.2725" y="0" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A3</text>
<text x="21.2725" y="2.54" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">SDA</text>
<text x="21.2725" y="5.08" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">SCL</text>
<text x="21.2725" y="6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A6</text>
<text x="18.415" y="6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">A7</text>
<text x="5.08" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D7</text>
<text x="7.62" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D8</text>
<text x="10.16" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D9</text>
<text x="12.7" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D10</text>
<text x="15.24" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D11</text>
<text x="17.78" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D12</text>
<text x="20.32" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D13</text>
<text x="0" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">5V</text>
<text x="0" y="12.54125" size="0.8128" layer="22" font="vector" rot="SMR0" align="top-center">GND</text>
<text x="2.54" y="12.54125" size="0.8128" layer="22" font="vector" rot="SMR0" align="top-center">RST</text>
<text x="2.54" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">3V3</text>
<text x="20.32" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D0</text>
<text x="17.78" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D1</text>
<text x="15.24" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D2</text>
<text x="12.7" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D3</text>
<text x="10.16" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D4</text>
<text x="7.62" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D5</text>
<text x="21.2725" y="-6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A0</text>
<text x="5.08" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D6</text>
<text x="18.415" y="-6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">REF</text>
<wire x1="20.32" y1="-7.62" x2="19.685" y2="-6.985" width="0.127" layer="22"/>
<wire x1="19.685" y1="-6.985" x2="17.4244" y2="-6.985" width="0.127" layer="22"/>
<wire x1="21.59" y1="-8.89" x2="22.86" y2="-10.16" width="0.127" layer="22"/>
<wire x1="22.86" y1="10.16" x2="21.59" y2="8.89" width="0.127" layer="22"/>
<wire x1="20.32" y1="7.62" x2="19.685" y2="6.985" width="0.127" layer="22"/>
<wire x1="19.685" y1="6.985" x2="17.4625" y2="6.985" width="0.127" layer="22"/>
<text x="11.43" y="-0.635" size="1.6764" layer="22" font="vector" ratio="14" rot="MR0" align="bottom-center">Microduino</text>
<text x="11.43" y="-3.81" size="1.016" layer="22" font="vector" rot="MR0" align="bottom-center">(Arduino-Compatible)</text>
<text x="11.43" y="1.905" size="1.016" layer="22" font="vector" rot="MR0" align="bottom-center">www.microduino.cc</text>
<text x="4.1275" y="0.47625" size="0.6096" layer="22" font="vector" ratio="6" rot="MR0" align="bottom-center">TM</text>
<text x="25.0825" y="0" size="0.8128" layer="21" font="vector" rot="R90" align="bottom-center">A Product Of MakerModule</text>
</package>
<package name="27UPIN-MICRO-SMALL">
<pad name="2" x="2.54" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="3" x="5.08" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="4" x="7.62" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="5" x="10.16" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="6" x="12.7" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="7" x="15.24" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="8" x="17.78" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="9" x="20.32" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="10" x="22.86" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="11" x="22.86" y="-7.62" drill="0.8" diameter="1.4224"/>
<pad name="12" x="22.86" y="-5.08" drill="0.8" diameter="1.4224"/>
<pad name="13" x="22.86" y="-2.54" drill="0.8" diameter="1.4224"/>
<pad name="14" x="22.86" y="0" drill="0.8" diameter="1.4224"/>
<pad name="15" x="22.86" y="2.54" drill="0.8" diameter="1.4224"/>
<pad name="16" x="22.86" y="5.08" drill="0.8" diameter="1.4224"/>
<pad name="17" x="22.86" y="7.62" drill="0.8" diameter="1.4224"/>
<pad name="18" x="22.86" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="19" x="20.32" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="20" x="17.78" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="21" x="15.24" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="22" x="12.7" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="23" x="10.16" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="24" x="7.62" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="25" x="5.08" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="26" x="2.54" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="27" x="0" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="1" x="0" y="-10.16" drill="0.8" diameter="1.4224"/>
<text x="21.2725" y="-5.08" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A1</text>
<text x="21.2725" y="-2.54" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A2</text>
<text x="21.2725" y="0" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A3</text>
<text x="21.2725" y="2.54" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">SDA</text>
<text x="21.2725" y="5.08" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">SCL</text>
<text x="21.2725" y="6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A6</text>
<text x="18.415" y="6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">A7</text>
<text x="5.08" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D7</text>
<text x="7.62" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D8</text>
<text x="10.16" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D9</text>
<text x="12.7" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D10</text>
<text x="15.24" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D11</text>
<text x="17.78" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D12</text>
<text x="20.32" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D13</text>
<text x="0" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">5V</text>
<text x="0" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">GND</text>
<text x="2.54" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">RST</text>
<text x="2.54" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">3V3</text>
<text x="20.32" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D0</text>
<text x="17.78" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D1</text>
<text x="15.24" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D2</text>
<text x="12.7" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D3</text>
<text x="10.16" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D4</text>
<text x="7.62" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D5</text>
<text x="21.2725" y="-6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A0</text>
<text x="5.08" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D6</text>
<text x="18.415" y="-6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">REF</text>
<wire x1="20.32" y1="-7.62" x2="19.685" y2="-6.985" width="0.127" layer="22"/>
<wire x1="19.685" y1="-6.985" x2="17.4244" y2="-6.985" width="0.127" layer="22"/>
<wire x1="21.59" y1="-8.89" x2="22.86" y2="-10.16" width="0.127" layer="22"/>
<wire x1="22.86" y1="10.16" x2="21.59" y2="8.89" width="0.127" layer="22"/>
<wire x1="20.32" y1="7.62" x2="19.685" y2="6.985" width="0.127" layer="22"/>
<wire x1="19.685" y1="6.985" x2="17.4625" y2="6.985" width="0.127" layer="22"/>
</package>
<package name="27UPIN-MICRO-NULL">
<pad name="2" x="2.54" y="-10.16" drill="1.4"/>
<pad name="3" x="5.08" y="-10.16" drill="1.4"/>
<pad name="4" x="7.62" y="-10.16" drill="1.4"/>
<pad name="5" x="10.16" y="-10.16" drill="1.4"/>
<pad name="6" x="12.7" y="-10.16" drill="1.4"/>
<pad name="7" x="15.24" y="-10.16" drill="1.4"/>
<pad name="8" x="17.78" y="-10.16" drill="1.4"/>
<pad name="9" x="20.32" y="-10.16" drill="1.4"/>
<pad name="10" x="22.86" y="-10.16" drill="1.4"/>
<pad name="11" x="22.86" y="-7.62" drill="1.4"/>
<pad name="12" x="22.86" y="-5.08" drill="1.4"/>
<pad name="13" x="22.86" y="-2.54" drill="1.4"/>
<pad name="14" x="22.86" y="0" drill="1.4"/>
<pad name="15" x="22.86" y="2.54" drill="1.4"/>
<pad name="16" x="22.86" y="5.08" drill="1.4"/>
<pad name="17" x="22.86" y="7.62" drill="1.4"/>
<pad name="18" x="22.86" y="10.16" drill="1.4"/>
<pad name="19" x="20.32" y="10.16" drill="1.4"/>
<pad name="20" x="17.78" y="10.16" drill="1.4"/>
<pad name="21" x="15.24" y="10.16" drill="1.4"/>
<pad name="22" x="12.7" y="10.16" drill="1.4"/>
<pad name="23" x="10.16" y="10.16" drill="1.4"/>
<pad name="24" x="7.62" y="10.16" drill="1.4"/>
<pad name="25" x="5.08" y="10.16" drill="1.4"/>
<pad name="26" x="2.54" y="10.16" drill="1.4"/>
<pad name="27" x="0" y="10.16" drill="1.4"/>
<pad name="1" x="0" y="-10.16" drill="1.4"/>
<text x="21.2725" y="-5.08" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A1</text>
<text x="21.2725" y="-2.54" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A2</text>
<text x="21.2725" y="0" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A3</text>
<text x="21.2725" y="2.54" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">SDA</text>
<text x="21.2725" y="5.08" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">SCL</text>
<text x="21.2725" y="6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A6</text>
<text x="18.415" y="6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">A7</text>
<text x="5.08" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D7</text>
<text x="7.62" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D8</text>
<text x="10.16" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D9</text>
<text x="12.7" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D10</text>
<text x="15.24" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D11</text>
<text x="17.78" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D12</text>
<text x="20.32" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">D13</text>
<text x="0" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">5V</text>
<text x="0" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">GND</text>
<text x="2.54" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">RST</text>
<text x="2.54" y="-8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">3V3</text>
<text x="20.32" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D0</text>
<text x="17.78" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D1</text>
<text x="15.24" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D2</text>
<text x="12.7" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D3</text>
<text x="10.16" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D4</text>
<text x="7.62" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D5</text>
<text x="21.2725" y="-6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="center-left">A0</text>
<text x="5.08" y="8.5725" size="0.8128" layer="22" font="vector" rot="MR0" align="top-center">D6</text>
<text x="18.415" y="-6.6675" size="0.8128" layer="22" font="vector" rot="MR0" align="bottom-center">REF</text>
<wire x1="20.32" y1="-7.62" x2="19.685" y2="-6.985" width="0.127" layer="22"/>
<wire x1="19.685" y1="-6.985" x2="17.4244" y2="-6.985" width="0.127" layer="22"/>
<wire x1="21.59" y1="-8.89" x2="22.86" y2="-10.16" width="0.127" layer="22"/>
<wire x1="22.86" y1="10.16" x2="21.59" y2="8.89" width="0.127" layer="22"/>
<wire x1="20.32" y1="7.62" x2="19.685" y2="6.985" width="0.127" layer="22"/>
<wire x1="19.685" y1="6.985" x2="17.4625" y2="6.985" width="0.127" layer="22"/>
</package>
<package name="27UPIN-MICRO-SMALL-NULL">
<pad name="2" x="2.54" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="3" x="5.08" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="4" x="7.62" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="5" x="10.16" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="6" x="12.7" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="7" x="15.24" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="8" x="17.78" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="9" x="20.32" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="10" x="22.86" y="-10.16" drill="0.8" diameter="1.4224"/>
<pad name="11" x="22.86" y="-7.62" drill="0.8" diameter="1.4224"/>
<pad name="12" x="22.86" y="-5.08" drill="0.8" diameter="1.4224"/>
<pad name="13" x="22.86" y="-2.54" drill="0.8" diameter="1.4224"/>
<pad name="14" x="22.86" y="0" drill="0.8" diameter="1.4224"/>
<pad name="15" x="22.86" y="2.54" drill="0.8" diameter="1.4224"/>
<pad name="16" x="22.86" y="5.08" drill="0.8" diameter="1.4224"/>
<pad name="17" x="22.86" y="7.62" drill="0.8" diameter="1.4224"/>
<pad name="18" x="22.86" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="19" x="20.32" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="20" x="17.78" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="21" x="15.24" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="22" x="12.7" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="23" x="10.16" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="24" x="7.62" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="25" x="5.08" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="26" x="2.54" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="27" x="0" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="1" x="0" y="-10.16" drill="0.8" diameter="1.4224"/>
</package>
<package name="27UPIN-COOKIE-OLD">
<pad name="2" x="2.54" y="-10.16" drill="1.52"/>
<pad name="3" x="5.08" y="-10.16" drill="1.52"/>
<pad name="4" x="7.62" y="-10.16" drill="1.52"/>
<pad name="5" x="10.16" y="-10.16" drill="1.52"/>
<pad name="6" x="12.7" y="-10.16" drill="1.52"/>
<pad name="7" x="15.24" y="-10.16" drill="1.52"/>
<pad name="8" x="17.78" y="-10.16" drill="1.52"/>
<pad name="9" x="20.32" y="-10.16" drill="1.52"/>
<pad name="10" x="22.86" y="-10.16" drill="1.52"/>
<pad name="11" x="22.86" y="-7.62" drill="1.52"/>
<pad name="12" x="22.86" y="-5.08" drill="1.52"/>
<pad name="13" x="22.86" y="-2.54" drill="1.52"/>
<pad name="14" x="22.86" y="0" drill="1.52"/>
<pad name="15" x="22.86" y="2.54" drill="1.52"/>
<pad name="16" x="22.86" y="5.08" drill="1.52"/>
<pad name="17" x="22.86" y="7.62" drill="1.52"/>
<pad name="18" x="22.86" y="10.16" drill="1.52"/>
<pad name="19" x="20.32" y="10.16" drill="1.52"/>
<pad name="20" x="17.78" y="10.16" drill="1.52"/>
<pad name="21" x="15.24" y="10.16" drill="1.52"/>
<pad name="22" x="12.7" y="10.16" drill="1.52"/>
<pad name="23" x="10.16" y="10.16" drill="1.52"/>
<pad name="24" x="7.62" y="10.16" drill="1.52"/>
<pad name="25" x="5.08" y="10.16" drill="1.52"/>
<pad name="26" x="2.54" y="10.16" drill="1.52"/>
<pad name="27" x="0" y="10.16" drill="1.52"/>
<pad name="1" x="0" y="-10.16" drill="1.52"/>
<wire x1="24" y1="-18.5" x2="24.5" y2="-18.5" width="0.1" layer="20"/>
<wire x1="24.5" y1="-18.5" x2="25.5" y2="-17.5" width="0.1" layer="20" curve="90"/>
<wire x1="25.5" y1="-17.5" x2="25.5" y2="-12.2" width="0.1" layer="20"/>
<wire x1="25.5" y1="-12.2" x2="27.5" y2="-12.2" width="0.1" layer="20"/>
<wire x1="27.5" y1="12.2" x2="25.5" y2="12.2" width="0.1" layer="20"/>
<wire x1="-2.5" y1="12.2" x2="-4.5" y2="12.2" width="0.1" layer="20"/>
<wire x1="-4.5" y1="-12.2" x2="-2.5" y2="-12.2" width="0.1" layer="20"/>
<wire x1="-2.5" y1="-12.2" x2="-2.5" y2="-17.5" width="0.1" layer="20"/>
<wire x1="-2.5" y1="-17.5" x2="-1.5" y2="-18.5" width="0.1" layer="20" curve="90"/>
<wire x1="-1.5" y1="-18.5" x2="-1" y2="-18.5" width="0.1" layer="20"/>
<wire x1="27.5" y1="-12" x2="27.5" y2="-16" width="0.127" layer="51"/>
<wire x1="27.5" y1="-16" x2="23.5" y2="-20" width="0.127" layer="51" curve="-90"/>
<wire x1="23.5" y1="-20" x2="-0.5" y2="-20" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-20" x2="-4.5" y2="-16" width="0.127" layer="51" curve="-90"/>
<wire x1="-4.5" y1="-16" x2="-4.5" y2="-12" width="0.127" layer="51"/>
<wire x1="27.5" y1="12" x2="27.5" y2="16" width="0.127" layer="51"/>
<wire x1="27.5" y1="16" x2="23.5" y2="20" width="0.127" layer="51" curve="90"/>
<wire x1="23.5" y1="20" x2="-0.5" y2="20" width="0.127" layer="51"/>
<wire x1="-0.5" y1="20" x2="-4.5" y2="16" width="0.127" layer="51" curve="90"/>
<wire x1="-4.5" y1="16" x2="-4.5" y2="12" width="0.127" layer="51"/>
<wire x1="5" y1="-16" x2="10" y2="-16" width="0.1" layer="20" curve="-180"/>
<wire x1="10" y1="-18.5" x2="13" y2="-18.5" width="0.1" layer="20"/>
<wire x1="13" y1="-16" x2="18" y2="-16" width="0.1" layer="20" curve="-180"/>
<hole x="11.5" y="-15" drill="1.8"/>
<hole x="-2.65875" y="12.35875" drill="0.8"/>
<hole x="25.65875" y="12.35875" drill="0.8"/>
<hole x="25.65875" y="-12.35875" drill="0.8"/>
<hole x="-2.65875" y="-12.35875" drill="0.8"/>
<wire x1="16.589375" y1="5.23875" x2="6.270625" y2="5.23875" width="0.127" layer="48" style="shortdash"/>
<wire x1="6.270625" y1="5.23875" x2="6.270625" y2="-5.23875" width="0.127" layer="48" style="shortdash"/>
<dimension x1="6.35" y1="5.08" x2="6.35" y2="-5.08" x3="5.08" y3="0" textsize="0.6096" layer="48" width="0.0508" visible="yes"/>
<dimension x1="16.51" y1="5.08" x2="6.35" y2="5.08" x3="11.43" y3="6.35" textsize="0.6096" layer="48" width="0.0508" visible="yes"/>
<wire x1="16.589375" y1="5.23875" x2="16.589375" y2="-5.23875" width="0.127" layer="48" style="shortdash"/>
<wire x1="16.589375" y1="-5.23875" x2="6.270625" y2="-5.23875" width="0.127" layer="48" style="shortdash"/>
<wire x1="18.4" y1="-13.3" x2="18.4" y2="-14.1" width="0.1" layer="20"/>
<wire x1="18.4" y1="-14.1" x2="18.8" y2="-14.5" width="0.1" layer="20" curve="90"/>
<wire x1="18.8" y1="-14.5" x2="19" y2="-14.5" width="0.1" layer="20"/>
<wire x1="18.4" y1="-13.3" x2="18.8" y2="-12.9" width="0.1" layer="20" curve="-90"/>
<wire x1="18.8" y1="-12.9" x2="20.2" y2="-12.9" width="0.1" layer="20"/>
<wire x1="20.2" y1="-12.9" x2="20.7" y2="-13.4" width="0.1" layer="20" curve="-90"/>
<wire x1="19" y1="-14.5" x2="19.3" y2="-14.8" width="0.1" layer="20" curve="-90"/>
<wire x1="20.7" y1="-13.4" x2="20.9" y2="-13.6" width="0.1" layer="20" curve="90"/>
<wire x1="24" y1="-18.45" x2="24" y2="-16" width="0.1" layer="20"/>
<wire x1="24" y1="-16" x2="20.84" y2="-13.59" width="0.1" layer="20" curve="105.315451"/>
<wire x1="19" y1="-16" x2="19.33" y2="-14.76" width="0.1" layer="20" curve="-29.744881"/>
<wire x1="19" y1="-16" x2="19" y2="-17" width="0.1" layer="20" curve="0.060312"/>
<wire x1="19" y1="-17" x2="18" y2="-17" width="0.1" layer="20"/>
<wire x1="13" y1="-16" x2="13" y2="-18.5" width="0.1" layer="20"/>
<wire x1="18" y1="-16" x2="18" y2="-17" width="0.1" layer="20"/>
<wire x1="10" y1="-16" x2="10" y2="-18.5" width="0.1" layer="20"/>
<wire x1="5" y1="-16" x2="5" y2="-17" width="0.1" layer="20" curve="0.060312"/>
<wire x1="4.6" y1="-13.3" x2="4.6" y2="-14.1" width="0.1" layer="20"/>
<wire x1="4.6" y1="-14.1" x2="4.2" y2="-14.5" width="0.1" layer="20" curve="-90"/>
<wire x1="4.2" y1="-14.5" x2="4" y2="-14.5" width="0.1" layer="20"/>
<wire x1="4.6" y1="-13.3" x2="4.2" y2="-12.9" width="0.1" layer="20" curve="90"/>
<wire x1="4.2" y1="-12.9" x2="2.8" y2="-12.9" width="0.1" layer="20"/>
<wire x1="2.8" y1="-12.9" x2="2.3" y2="-13.4" width="0.1" layer="20" curve="90"/>
<wire x1="4" y1="-14.5" x2="3.7" y2="-14.8" width="0.1" layer="20" curve="90"/>
<wire x1="2.3" y1="-13.4" x2="2.1" y2="-13.6" width="0.1" layer="20" curve="-90"/>
<wire x1="-1" y1="-18.45" x2="-1" y2="-16" width="0.1" layer="20"/>
<wire x1="-1" y1="-16" x2="2.16" y2="-13.59" width="0.1" layer="20" curve="-105.315451"/>
<wire x1="4" y1="-16" x2="3.67" y2="-14.76" width="0.1" layer="20" curve="29.744881"/>
<wire x1="4" y1="-16" x2="4" y2="-17" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="4" y1="-17" x2="5" y2="-17" width="0.1" layer="20"/>
<hole x="11.5" y="-12.85" drill="0.9"/>
<hole x="11" y="-12.85" drill="0.9"/>
<hole x="12" y="-12.85" drill="0.9"/>
<hole x="11.25" y="-12.85" drill="0.9"/>
<hole x="11.75" y="-12.85" drill="0.9"/>
<hole x="11.125" y="-12.85" drill="0.9"/>
<hole x="11.375" y="-12.85" drill="0.9"/>
<hole x="11.625" y="-12.85" drill="0.9"/>
<hole x="11.875" y="-12.85" drill="0.9"/>
<wire x1="-1" y1="18.5" x2="-1.5" y2="18.5" width="0.1" layer="20"/>
<wire x1="-1.5" y1="18.5" x2="-2.5" y2="17.5" width="0.1" layer="20" curve="90"/>
<wire x1="-2.5" y1="17.5" x2="-2.5" y2="12.2" width="0.1" layer="20"/>
<wire x1="25.5" y1="12.2" x2="25.5" y2="17.5" width="0.1" layer="20"/>
<wire x1="25.5" y1="17.5" x2="24.5" y2="18.5" width="0.1" layer="20" curve="90"/>
<wire x1="24.5" y1="18.5" x2="24" y2="18.5" width="0.1" layer="20"/>
<wire x1="18" y1="16" x2="13" y2="16" width="0.1" layer="20" curve="-180"/>
<wire x1="13" y1="18.5" x2="10" y2="18.5" width="0.1" layer="20"/>
<wire x1="10" y1="16" x2="5" y2="16" width="0.1" layer="20" curve="-180"/>
<hole x="11.5" y="15" drill="1.8"/>
<wire x1="4.6" y1="13.3" x2="4.6" y2="14.1" width="0.1" layer="20"/>
<wire x1="4.6" y1="14.1" x2="4.2" y2="14.5" width="0.1" layer="20" curve="90"/>
<wire x1="4.2" y1="14.5" x2="4" y2="14.5" width="0.1" layer="20"/>
<wire x1="4.6" y1="13.3" x2="4.2" y2="12.9" width="0.1" layer="20" curve="-90"/>
<wire x1="4.2" y1="12.9" x2="2.8" y2="12.9" width="0.1" layer="20"/>
<wire x1="2.8" y1="12.9" x2="2.3" y2="13.4" width="0.1" layer="20" curve="-90"/>
<wire x1="4" y1="14.5" x2="3.7" y2="14.8" width="0.1" layer="20" curve="-90"/>
<wire x1="2.3" y1="13.4" x2="2.1" y2="13.6" width="0.1" layer="20" curve="90"/>
<wire x1="-1" y1="18.45" x2="-1" y2="16" width="0.1" layer="20"/>
<wire x1="-1" y1="16" x2="2.16" y2="13.59" width="0.1" layer="20" curve="105.315451"/>
<wire x1="4" y1="16" x2="3.67" y2="14.76" width="0.1" layer="20" curve="-29.744881"/>
<wire x1="4" y1="16" x2="4" y2="17" width="0.1" layer="20" curve="0.060312"/>
<wire x1="4" y1="17" x2="5" y2="17" width="0.1" layer="20"/>
<wire x1="10" y1="16" x2="10" y2="18.5" width="0.1" layer="20"/>
<wire x1="5" y1="16" x2="5" y2="17" width="0.1" layer="20"/>
<wire x1="13" y1="16" x2="13" y2="18.5" width="0.1" layer="20"/>
<wire x1="18" y1="16" x2="18" y2="17" width="0.1" layer="20" curve="0.060312"/>
<wire x1="18.4" y1="13.3" x2="18.4" y2="14.1" width="0.1" layer="20"/>
<wire x1="18.4" y1="14.1" x2="18.8" y2="14.5" width="0.1" layer="20" curve="-90"/>
<wire x1="18.8" y1="14.5" x2="19" y2="14.5" width="0.1" layer="20"/>
<wire x1="18.4" y1="13.3" x2="18.8" y2="12.9" width="0.1" layer="20" curve="90"/>
<wire x1="18.8" y1="12.9" x2="20.2" y2="12.9" width="0.1" layer="20"/>
<wire x1="20.2" y1="12.9" x2="20.7" y2="13.4" width="0.1" layer="20" curve="90"/>
<wire x1="19" y1="14.5" x2="19.3" y2="14.8" width="0.1" layer="20" curve="90"/>
<wire x1="20.7" y1="13.4" x2="20.9" y2="13.6" width="0.1" layer="20" curve="-90"/>
<wire x1="24" y1="18.45" x2="24" y2="16" width="0.1" layer="20"/>
<wire x1="24" y1="16" x2="20.84" y2="13.59" width="0.1" layer="20" curve="-105.315451"/>
<wire x1="19" y1="16" x2="19.33" y2="14.76" width="0.1" layer="20" curve="29.744881"/>
<wire x1="19" y1="16" x2="19" y2="17" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="19" y1="17" x2="18" y2="17" width="0.1" layer="20"/>
<hole x="11.5" y="12.85" drill="0.9"/>
<hole x="12" y="12.85" drill="0.9"/>
<hole x="11" y="12.85" drill="0.9"/>
<hole x="11.75" y="12.85" drill="0.9"/>
<hole x="11.25" y="12.85" drill="0.9"/>
<hole x="11.875" y="12.85" drill="0.9"/>
<hole x="11.625" y="12.85" drill="0.9"/>
<hole x="11.375" y="12.85" drill="0.9"/>
<hole x="11.125" y="12.85" drill="0.9"/>
<circle x="-1" y="13" radius="0.472465625" width="0.3048" layer="22"/>
<rectangle x1="25.9461" y1="3.3401" x2="27.2415" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="3.3401" x2="27.2415" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="3.3401" x2="27.2415" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="3.3401" x2="27.1653" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="26.3271" y1="-1.3843" x2="26.4033" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="26.3271" y1="-2.0701" x2="26.4033" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="26.3271" y1="-2.5273" x2="26.4033" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="26.1366" y1="3.9878" x2="26.4414" y2="4.064" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="3.5687" x2="26.4795" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="3.1115" x2="26.4795" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="26.1366" y1="2.6924" x2="26.4414" y2="2.7686" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="1.7399" x2="26.5557" y2="1.8161" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="0.6731" x2="26.4795" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="-0.5461" x2="26.4795" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="26.1747" y1="-1.3843" x2="26.4033" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="26.1747" y1="-2.0701" x2="26.4033" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="26.1747" y1="-2.5273" x2="26.4033" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="26.0604" y1="-3.4036" x2="26.5176" y2="-3.3274" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="4.0259" x2="26.3271" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="3.5687" x2="26.3271" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="3.1115" x2="26.3271" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="2.6543" x2="26.3271" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="1.7399" x2="26.5557" y2="1.8161" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="0.6731" x2="26.4795" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="-0.5461" x2="26.4795" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="-1.3843" x2="26.3271" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="-2.0701" x2="26.3271" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="-2.5273" x2="26.3271" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.908" y1="-3.4036" x2="26.5176" y2="-3.3274" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="4.0259" x2="26.2509" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="3.5687" x2="26.2509" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="3.1115" x2="26.2509" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="2.6543" x2="26.2509" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.8318" y1="1.778" x2="26.4414" y2="1.8542" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="0.6731" x2="26.4033" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-0.5461" x2="26.4033" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="-1.3843" x2="26.2509" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="-2.0701" x2="26.2509" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="-2.5273" x2="26.2509" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-3.3655" x2="26.4033" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="4.0259" x2="26.1747" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="3.5687" x2="26.1747" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="3.1115" x2="26.1747" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="2.6543" x2="26.1747" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="2.159" x2="26.1366" y2="2.2352" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="1.016" x2="26.1366" y2="1.0922" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="0.3302" x2="26.1366" y2="0.4064" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="-0.2032" x2="26.1366" y2="-0.127" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="-0.889" x2="26.1366" y2="-0.8128" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="-1.3843" x2="26.1747" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="-2.0701" x2="26.1747" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="-2.5273" x2="26.1747" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="-3.0226" x2="26.1366" y2="-2.9464" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="4.0259" x2="26.0985" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="3.5687" x2="26.0985" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="3.1115" x2="26.0985" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="2.6543" x2="26.0985" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="2.1971" x2="26.0985" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="1.0541" x2="26.0985" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="0.2921" x2="26.0985" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-0.1651" x2="26.0985" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-0.9271" x2="26.0985" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-1.3843" x2="26.0985" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.908" y1="-2.032" x2="26.0604" y2="-1.9558" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-2.5273" x2="26.0985" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-2.9845" x2="26.0985" y2="-2.9083" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="4.0259" x2="26.0223" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="3.5687" x2="26.0223" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="3.1115" x2="26.0223" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="2.6543" x2="26.0223" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="2.1971" x2="26.0223" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="1.0541" x2="26.0223" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="0.2921" x2="26.0223" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="-0.1651" x2="26.0223" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="-0.9271" x2="26.0223" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-1.6129" x2="26.2509" y2="-1.5367" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="-2.5273" x2="26.0223" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.4508" y1="-3.3274" x2="26.3652" y2="-3.2512" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="4.0259" x2="25.9461" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="3.5687" x2="25.9461" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="3.1115" x2="25.9461" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="2.6543" x2="25.9461" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="2.1971" x2="25.9461" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="1.0541" x2="25.9461" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="0.2921" x2="25.9461" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="-0.1651" x2="25.9461" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="-0.9271" x2="25.9461" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="-1.6129" x2="26.1747" y2="-1.5367" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="-2.5273" x2="25.9461" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.3365" y1="-3.3655" x2="26.3271" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="4.0259" x2="25.8699" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="3.5687" x2="25.8699" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="3.1115" x2="25.8699" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="2.6543" x2="25.8699" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="2.1971" x2="25.8699" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="1.0541" x2="25.8699" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="0.2921" x2="25.8699" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="-0.1651" x2="25.8699" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="-0.9271" x2="25.8699" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="-1.6129" x2="26.0985" y2="-1.5367" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="-2.5273" x2="25.8699" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-3.3655" x2="26.2509" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="4.0259" x2="25.7937" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="3.5687" x2="25.7937" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="3.1115" x2="25.7937" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="2.6543" x2="25.7937" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="2.1971" x2="25.7937" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="1.0541" x2="25.7937" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="0.2921" x2="25.7937" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-0.1651" x2="25.7937" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-0.9271" x2="25.7937" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-1.3843" x2="25.7937" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.6032" y1="-2.032" x2="25.7556" y2="-1.9558" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-2.5273" x2="25.7937" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-2.9845" x2="25.7937" y2="-2.9083" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-3.7465" x2="25.7937" y2="-3.6703" layer="22" rot="R270"/>
<rectangle x1="25.4508" y1="3.9878" x2="25.7556" y2="4.064" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="3.5687" x2="25.7175" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="3.1115" x2="25.7175" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.4508" y1="2.6924" x2="25.7556" y2="2.7686" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="2.159" x2="25.6794" y2="2.2352" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="1.016" x2="25.6794" y2="1.0922" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="0.3302" x2="25.6794" y2="0.4064" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="-0.2032" x2="25.6794" y2="-0.127" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="-0.889" x2="25.6794" y2="-0.8128" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="-1.3843" x2="25.7175" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="-2.0701" x2="25.7175" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="-2.5273" x2="25.7175" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="-3.0226" x2="25.6794" y2="-2.9464" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="-3.7084" x2="25.6794" y2="-3.6322" layer="22" rot="R270"/>
<rectangle x1="25.2984" y1="3.9116" x2="25.7556" y2="3.9878" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="3.3401" x2="25.6413" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.2984" y1="2.7686" x2="25.7556" y2="2.8448" layer="22" rot="R270"/>
<rectangle x1="25.2222" y1="1.778" x2="25.8318" y2="1.8542" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="0.6731" x2="25.7937" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-0.5461" x2="25.7937" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="-1.3843" x2="25.6413" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="-2.0701" x2="25.6413" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="-2.5273" x2="25.6413" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-3.3655" x2="25.7937" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.2222" y1="3.9116" x2="25.6794" y2="3.9878" layer="22" rot="R270"/>
<rectangle x1="25.3365" y1="3.3401" x2="25.5651" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.2222" y1="2.7686" x2="25.6794" y2="2.8448" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="1.7399" x2="25.7937" y2="1.8161" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="0.6731" x2="25.7175" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-0.5461" x2="25.7175" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="25.3365" y1="-1.3843" x2="25.5651" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.3365" y1="-2.0701" x2="25.5651" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-3.3655" x2="25.7175" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="3.8735" x2="25.6413" y2="3.9497" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="3.3401" x2="25.5651" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="2.8067" x2="25.6413" y2="2.8829" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="1.7399" x2="25.6413" y2="1.8161" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="0.6731" x2="25.5651" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-0.5461" x2="25.5651" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-1.3843" x2="25.4889" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-2.0701" x2="25.4889" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-3.3655" x2="25.5651" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="24.4983" y1="3.3401" x2="26.0985" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-1.3843" x2="25.4127" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-2.0701" x2="25.3365" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-2.5273" x2="25.4127" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="24.4221" y1="3.3401" x2="26.0223" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="-1.3843" x2="25.3365" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="-2.5273" x2="25.3365" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="24.4221" y1="3.3401" x2="25.8699" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.0317" y1="-1.3843" x2="25.2603" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.0317" y1="-2.5273" x2="25.2603" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="24.4221" y1="3.3401" x2="25.7175" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="24.9555" y1="-1.3843" x2="25.1841" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.0317" y1="-2.5273" x2="25.1079" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="24.8793" y1="-1.3843" x2="25.1079" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="24.8031" y1="-1.3843" x2="25.0317" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="24.8031" y1="-1.3843" x2="24.8793" y2="-1.3081" layer="22" rot="R270"/>
<wire x1="-4.5" y1="12.2" x2="-4.5" y2="-12.2" width="0.1" layer="20"/>
<wire x1="27.5" y1="12.2" x2="27.5" y2="-12.2" width="0.1" layer="20"/>
</package>
<package name="27UPIN-PAD-BUT">
<smd name="1" x="0" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="2" x="2.54" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="3" x="5.08" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="4" x="7.62" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="5" x="10.16" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="6" x="12.7" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="7" x="15.24" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="8" x="17.78" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="9" x="20.32" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="10" x="22.86" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="11" x="22.86" y="-7.62" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="12" x="22.86" y="-5.08" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="13" x="22.86" y="-2.54" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="14" x="22.86" y="0" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="15" x="22.86" y="2.54" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="16" x="22.86" y="5.08" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="17" x="22.86" y="7.62" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="18" x="22.86" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="19" x="20.32" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="20" x="17.78" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="21" x="15.24" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="22" x="12.7" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="23" x="10.16" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="24" x="7.62" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="25" x="5.08" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="26" x="2.54" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="27" x="0" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
</package>
<package name="27UPIN-PAD-TOP">
<smd name="1" x="0" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="2.54" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="3" x="5.08" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="4" x="7.62" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="5" x="10.16" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="6" x="12.7" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="7" x="15.24" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="8" x="17.78" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="9" x="20.32" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="10" x="22.86" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="11" x="22.86" y="-7.62" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="12" x="22.86" y="-5.08" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="13" x="22.86" y="-2.54" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="14" x="22.86" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="15" x="22.86" y="2.54" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="16" x="22.86" y="5.08" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="17" x="22.86" y="7.62" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="18" x="22.86" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="19" x="20.32" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="20" x="17.78" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="21" x="15.24" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="22" x="12.7" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="23" x="10.16" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="24" x="7.62" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="25" x="5.08" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="26" x="2.54" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="27" x="0" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
</package>
<package name="MICRO-USB-NJ-B">
<smd name="P1" x="-1.3" y="2.575" dx="0.4" dy="1.35" layer="1" roundness="10"/>
<smd name="P2" x="-0.65" y="2.575" dx="0.4" dy="1.35" layer="1" roundness="10"/>
<smd name="P3" x="0" y="2.575" dx="0.4" dy="1.35" layer="1" roundness="10"/>
<smd name="P4" x="0.65" y="2.575" dx="0.4" dy="1.35" layer="1" roundness="10"/>
<smd name="P5" x="1.3" y="2.575" dx="0.4" dy="1.35" layer="1" roundness="10"/>
<smd name="G9" x="-0.781" y="0" dx="1.27" dy="1.27" layer="1" roundness="10"/>
<smd name="G10" x="0.8065" y="0" dx="1.27" dy="1.27" layer="1" roundness="10" rot="R180"/>
<text x="5.08" y="0" size="0.6096" layer="25" font="vector" rot="R90" align="center">&gt;NAME</text>
<pad name="G3" x="-3.625" y="-0.2" drill="0.7" shape="octagon"/>
<pad name="G6" x="3.625" y="-0.2" drill="0.7" shape="octagon"/>
<pad name="G2" x="-3.625" y="0" drill="0.7" shape="octagon"/>
<pad name="G1" x="-3.625" y="0.2" drill="0.7" shape="octagon"/>
<pad name="G5" x="3.625" y="0" drill="0.7" shape="octagon"/>
<pad name="G4" x="3.625" y="0.2" drill="0.7" shape="octagon"/>
<pad name="G7" x="-2.425" y="2.65" drill="0.7" shape="octagon"/>
<pad name="G8" x="2.425" y="2.65" drill="0.7" shape="octagon" rot="R90"/>
<wire x1="-3.65125" y1="-2.06375" x2="3.65125" y2="-2.06375" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="2.8575" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.8575" x2="3.81" y2="2.8575" width="0.127" layer="51"/>
<wire x1="3.81" y1="2.8575" x2="3.81" y2="-1.905" width="0.127" layer="51"/>
<wire x1="-3.65125" y1="-2.06375" x2="3.65125" y2="-2.06375" width="0.127" layer="20"/>
</package>
<package name="27UPIN-COOKIE-PAD-SHIELD">
<wire x1="27.5" y1="-12" x2="27.5" y2="-16" width="0.127" layer="51"/>
<wire x1="27.5" y1="-16" x2="23.5" y2="-20" width="0.127" layer="51" curve="-90"/>
<wire x1="23.5" y1="-20" x2="-0.5" y2="-20" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-20" x2="-4.5" y2="-16" width="0.127" layer="51" curve="-90"/>
<wire x1="-4.5" y1="-16" x2="-4.5" y2="-12" width="0.127" layer="51"/>
<wire x1="27.5" y1="12" x2="27.5" y2="16" width="0.127" layer="51"/>
<wire x1="27.5" y1="16" x2="23.5" y2="20" width="0.127" layer="51" curve="90"/>
<wire x1="23.5" y1="20" x2="-0.5" y2="20" width="0.127" layer="51"/>
<wire x1="-0.5" y1="20" x2="-4.5" y2="16" width="0.127" layer="51" curve="90"/>
<wire x1="-4.5" y1="16" x2="-4.5" y2="12" width="0.127" layer="51"/>
<wire x1="16.589375" y1="5.23875" x2="6.270625" y2="5.23875" width="0.127" layer="48" style="shortdash"/>
<wire x1="6.270625" y1="5.23875" x2="6.270625" y2="-5.23875" width="0.127" layer="48" style="shortdash"/>
<wire x1="16.589375" y1="5.23875" x2="16.589375" y2="-5.23875" width="0.127" layer="48" style="shortdash"/>
<wire x1="16.589375" y1="-5.23875" x2="6.270625" y2="-5.23875" width="0.127" layer="48" style="shortdash"/>
<circle x="-1" y="13" radius="0.472465625" width="0.3048" layer="22"/>
<rectangle x1="25.9461" y1="3.3401" x2="27.2415" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="3.3401" x2="27.2415" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="3.3401" x2="27.2415" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="3.3401" x2="27.1653" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="26.3271" y1="-1.3843" x2="26.4033" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="26.3271" y1="-2.0701" x2="26.4033" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="26.3271" y1="-2.5273" x2="26.4033" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="26.1366" y1="3.9878" x2="26.4414" y2="4.064" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="3.5687" x2="26.4795" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="3.1115" x2="26.4795" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="26.1366" y1="2.6924" x2="26.4414" y2="2.7686" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="1.7399" x2="26.5557" y2="1.8161" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="0.6731" x2="26.4795" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="-0.5461" x2="26.4795" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="26.1747" y1="-1.3843" x2="26.4033" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="26.1747" y1="-2.0701" x2="26.4033" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="26.1747" y1="-2.5273" x2="26.4033" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="26.0604" y1="-3.4036" x2="26.5176" y2="-3.3274" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="4.0259" x2="26.3271" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="3.5687" x2="26.3271" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="3.1115" x2="26.3271" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="2.6543" x2="26.3271" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="1.7399" x2="26.5557" y2="1.8161" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="0.6731" x2="26.4795" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="-0.5461" x2="26.4795" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="-1.3843" x2="26.3271" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="-2.0701" x2="26.3271" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="-2.5273" x2="26.3271" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.908" y1="-3.4036" x2="26.5176" y2="-3.3274" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="4.0259" x2="26.2509" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="3.5687" x2="26.2509" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="3.1115" x2="26.2509" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="2.6543" x2="26.2509" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.8318" y1="1.778" x2="26.4414" y2="1.8542" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="0.6731" x2="26.4033" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-0.5461" x2="26.4033" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="-1.3843" x2="26.2509" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="-2.0701" x2="26.2509" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="-2.5273" x2="26.2509" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-3.3655" x2="26.4033" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="4.0259" x2="26.1747" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="3.5687" x2="26.1747" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="3.1115" x2="26.1747" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="2.6543" x2="26.1747" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="2.159" x2="26.1366" y2="2.2352" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="1.016" x2="26.1366" y2="1.0922" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="0.3302" x2="26.1366" y2="0.4064" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="-0.2032" x2="26.1366" y2="-0.127" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="-0.889" x2="26.1366" y2="-0.8128" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="-1.3843" x2="26.1747" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="-2.0701" x2="26.1747" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="-2.5273" x2="26.1747" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="-3.0226" x2="26.1366" y2="-2.9464" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="4.0259" x2="26.0985" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="3.5687" x2="26.0985" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="3.1115" x2="26.0985" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="2.6543" x2="26.0985" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="2.1971" x2="26.0985" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="1.0541" x2="26.0985" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="0.2921" x2="26.0985" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-0.1651" x2="26.0985" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-0.9271" x2="26.0985" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-1.3843" x2="26.0985" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.908" y1="-2.032" x2="26.0604" y2="-1.9558" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-2.5273" x2="26.0985" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-2.9845" x2="26.0985" y2="-2.9083" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="4.0259" x2="26.0223" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="3.5687" x2="26.0223" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="3.1115" x2="26.0223" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="2.6543" x2="26.0223" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="2.1971" x2="26.0223" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="1.0541" x2="26.0223" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="0.2921" x2="26.0223" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="-0.1651" x2="26.0223" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="-0.9271" x2="26.0223" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-1.6129" x2="26.2509" y2="-1.5367" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="-2.5273" x2="26.0223" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.4508" y1="-3.3274" x2="26.3652" y2="-3.2512" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="4.0259" x2="25.9461" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="3.5687" x2="25.9461" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="3.1115" x2="25.9461" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="2.6543" x2="25.9461" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="2.1971" x2="25.9461" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="1.0541" x2="25.9461" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="0.2921" x2="25.9461" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="-0.1651" x2="25.9461" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="-0.9271" x2="25.9461" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="-1.6129" x2="26.1747" y2="-1.5367" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="-2.5273" x2="25.9461" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.3365" y1="-3.3655" x2="26.3271" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="4.0259" x2="25.8699" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="3.5687" x2="25.8699" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="3.1115" x2="25.8699" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="2.6543" x2="25.8699" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="2.1971" x2="25.8699" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="1.0541" x2="25.8699" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="0.2921" x2="25.8699" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="-0.1651" x2="25.8699" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="-0.9271" x2="25.8699" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="-1.6129" x2="26.0985" y2="-1.5367" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="-2.5273" x2="25.8699" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-3.3655" x2="26.2509" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="4.0259" x2="25.7937" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="3.5687" x2="25.7937" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="3.1115" x2="25.7937" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="2.6543" x2="25.7937" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="2.1971" x2="25.7937" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="1.0541" x2="25.7937" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="0.2921" x2="25.7937" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-0.1651" x2="25.7937" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-0.9271" x2="25.7937" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-1.3843" x2="25.7937" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.6032" y1="-2.032" x2="25.7556" y2="-1.9558" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-2.5273" x2="25.7937" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-2.9845" x2="25.7937" y2="-2.9083" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-3.7465" x2="25.7937" y2="-3.6703" layer="22" rot="R270"/>
<rectangle x1="25.4508" y1="3.9878" x2="25.7556" y2="4.064" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="3.5687" x2="25.7175" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="3.1115" x2="25.7175" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.4508" y1="2.6924" x2="25.7556" y2="2.7686" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="2.159" x2="25.6794" y2="2.2352" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="1.016" x2="25.6794" y2="1.0922" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="0.3302" x2="25.6794" y2="0.4064" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="-0.2032" x2="25.6794" y2="-0.127" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="-0.889" x2="25.6794" y2="-0.8128" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="-1.3843" x2="25.7175" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="-2.0701" x2="25.7175" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="-2.5273" x2="25.7175" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="-3.0226" x2="25.6794" y2="-2.9464" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="-3.7084" x2="25.6794" y2="-3.6322" layer="22" rot="R270"/>
<rectangle x1="25.2984" y1="3.9116" x2="25.7556" y2="3.9878" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="3.3401" x2="25.6413" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.2984" y1="2.7686" x2="25.7556" y2="2.8448" layer="22" rot="R270"/>
<rectangle x1="25.2222" y1="1.778" x2="25.8318" y2="1.8542" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="0.6731" x2="25.7937" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-0.5461" x2="25.7937" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="-1.3843" x2="25.6413" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="-2.0701" x2="25.6413" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="-2.5273" x2="25.6413" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-3.3655" x2="25.7937" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.2222" y1="3.9116" x2="25.6794" y2="3.9878" layer="22" rot="R270"/>
<rectangle x1="25.3365" y1="3.3401" x2="25.5651" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.2222" y1="2.7686" x2="25.6794" y2="2.8448" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="1.7399" x2="25.7937" y2="1.8161" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="0.6731" x2="25.7175" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-0.5461" x2="25.7175" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="25.3365" y1="-1.3843" x2="25.5651" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.3365" y1="-2.0701" x2="25.5651" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-3.3655" x2="25.7175" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="3.8735" x2="25.6413" y2="3.9497" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="3.3401" x2="25.5651" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="2.8067" x2="25.6413" y2="2.8829" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="1.7399" x2="25.6413" y2="1.8161" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="0.6731" x2="25.5651" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-0.5461" x2="25.5651" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-1.3843" x2="25.4889" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-2.0701" x2="25.4889" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-3.3655" x2="25.5651" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="24.4983" y1="3.3401" x2="26.0985" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-1.3843" x2="25.4127" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-2.0701" x2="25.3365" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-2.5273" x2="25.4127" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="24.4221" y1="3.3401" x2="26.0223" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="-1.3843" x2="25.3365" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="-2.5273" x2="25.3365" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="24.4221" y1="3.3401" x2="25.8699" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.0317" y1="-1.3843" x2="25.2603" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.0317" y1="-2.5273" x2="25.2603" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="24.4221" y1="3.3401" x2="25.7175" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="24.9555" y1="-1.3843" x2="25.1841" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.0317" y1="-2.5273" x2="25.1079" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="24.8793" y1="-1.3843" x2="25.1079" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="24.8031" y1="-1.3843" x2="25.0317" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="24.8031" y1="-1.3843" x2="24.8793" y2="-1.3081" layer="22" rot="R270"/>
<smd name="1" x="0" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="2" x="2.54" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="3" x="5.08" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="4" x="7.62" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="5" x="10.16" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="6" x="12.7" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="7" x="15.24" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="8" x="17.78" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="9" x="20.32" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="10" x="22.86" y="-10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="11" x="22.86" y="-7.62" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="12" x="22.86" y="-5.08" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="13" x="22.86" y="-2.54" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="14" x="22.86" y="0" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="15" x="22.86" y="2.54" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="16" x="22.86" y="5.08" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="17" x="22.86" y="7.62" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="18" x="22.86" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="19" x="20.32" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="20" x="17.78" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="21" x="15.24" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="22" x="12.7" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="23" x="10.16" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="24" x="7.62" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="25" x="5.08" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="26" x="2.54" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<smd name="27" x="0" y="10.16" dx="1.9" dy="1.9" layer="16" roundness="100" rot="R180"/>
<hole x="11.5" y="-14.96" drill="1.8"/>
<hole x="11.5" y="-12.71" drill="0.9"/>
<hole x="11" y="-12.71" drill="0.9"/>
<hole x="12" y="-12.71" drill="0.9"/>
<hole x="11.25" y="-12.71" drill="0.9"/>
<hole x="11.75" y="-12.71" drill="0.9"/>
<hole x="11.125" y="-12.71" drill="0.9"/>
<hole x="11.375" y="-12.71" drill="0.9"/>
<hole x="11.625" y="-12.71" drill="0.9"/>
<hole x="11.875" y="-12.71" drill="0.9"/>
<hole x="11.5" y="14.96" drill="1.8"/>
<hole x="11.5" y="12.744159375" drill="0.9"/>
<hole x="12" y="12.744159375" drill="0.9"/>
<hole x="11" y="12.744159375" drill="0.9"/>
<hole x="11.75" y="12.744159375" drill="0.9"/>
<hole x="11.25" y="12.744159375" drill="0.9"/>
<hole x="11.875" y="12.744159375" drill="0.9"/>
<hole x="11.625" y="12.744159375" drill="0.9"/>
<hole x="11.375" y="12.744159375" drill="0.9"/>
<hole x="11.125" y="12.744159375" drill="0.9"/>
<wire x1="-4.4" y1="-12.06" x2="-2.45" y2="-12.06" width="0.1" layer="20"/>
<wire x1="-2.45" y1="-12.06" x2="-2.1" y2="-12.41" width="0.1" layer="20" curve="-90"/>
<wire x1="-2.1" y1="-12.41" x2="-2.45" y2="-12.76" width="0.1" layer="20" curve="-90"/>
<wire x1="-2.45" y1="-12.76" x2="-2.45" y2="-13.58" width="0.1" layer="20"/>
<wire x1="-2.45" y1="-13.58" x2="-2.11" y2="-13.98" width="0.1" layer="20" curve="90"/>
<wire x1="-2.11" y1="-13.98" x2="-0.82" y2="-14" width="0.1" layer="20"/>
<wire x1="5" y1="-15.96" x2="10" y2="-15.96" width="0.1" layer="20" curve="-180"/>
<wire x1="10" y1="-15.96" x2="10" y2="-18.46" width="0.1" layer="20"/>
<wire x1="5" y1="-15.96" x2="5" y2="-16.96" width="0.1" layer="20" curve="0.060312"/>
<wire x1="4.6" y1="-13.26" x2="4.6" y2="-13.96" width="0.1" layer="20"/>
<wire x1="4.6" y1="-13.96" x2="4.2" y2="-14.36" width="0.1" layer="20" curve="-90"/>
<wire x1="4.2" y1="-14.36" x2="4" y2="-14.36" width="0.1" layer="20"/>
<wire x1="4.6" y1="-13.26" x2="4.2" y2="-12.86" width="0.1" layer="20" curve="90"/>
<wire x1="4.2" y1="-12.86" x2="2.8" y2="-12.86" width="0.1" layer="20"/>
<wire x1="4" y1="-14.36" x2="3.7" y2="-14.76" width="0.1" layer="20" curve="90"/>
<wire x1="4" y1="-15.96" x2="4" y2="-16.96" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="4" y1="-16.96" x2="5" y2="-16.96" width="0.1" layer="20"/>
<wire x1="2.52" y1="-12.96" x2="1.444375" y2="-12.96" width="0.1" layer="20"/>
<wire x1="2.78" y1="-12.86" x2="2.76" y2="-12.86" width="0.1" layer="20"/>
<wire x1="2.76" y1="-12.86" x2="2.66" y2="-12.9" width="0.1" layer="20" curve="43.602819"/>
<wire x1="2.66" y1="-12.9" x2="2.52" y2="-12.96" width="0.1" layer="20" curve="-50.335242"/>
<wire x1="3.7" y1="-14.76" x2="3.76" y2="-14.91" width="0.1" layer="20"/>
<wire x1="1.5" y1="-12.96" x2="-0.803125" y2="-14.009375" width="0.1016" layer="20" curve="49.737166"/>
<wire x1="4" y1="-15.96" x2="3.76" y2="-14.9" width="0.1016" layer="20" curve="25.127824"/>
<wire x1="27.4" y1="12.06" x2="25.45" y2="12.06" width="0.1" layer="20"/>
<wire x1="25.45" y1="12.06" x2="25.1" y2="12.41" width="0.1" layer="20" curve="-90"/>
<wire x1="25.1" y1="12.41" x2="25.45" y2="12.76" width="0.1" layer="20" curve="-90"/>
<wire x1="25.45" y1="12.76" x2="25.45" y2="13.58" width="0.1" layer="20"/>
<wire x1="25.45" y1="13.58" x2="25.11" y2="13.98" width="0.1" layer="20" curve="90"/>
<wire x1="25.11" y1="13.98" x2="23.82" y2="14" width="0.1" layer="20"/>
<wire x1="18" y1="15.96" x2="13" y2="15.96" width="0.1" layer="20" curve="-180"/>
<wire x1="13" y1="15.96" x2="13" y2="18.46" width="0.1" layer="20"/>
<wire x1="18" y1="15.96" x2="18" y2="16.96" width="0.1" layer="20" curve="0.060312"/>
<wire x1="18.4" y1="13.26" x2="18.4" y2="13.96" width="0.1" layer="20"/>
<wire x1="18.4" y1="13.96" x2="18.8" y2="14.36" width="0.1" layer="20" curve="-90"/>
<wire x1="18.8" y1="14.36" x2="19" y2="14.36" width="0.1" layer="20"/>
<wire x1="18.4" y1="13.26" x2="18.8" y2="12.86" width="0.1" layer="20" curve="90"/>
<wire x1="18.8" y1="12.86" x2="20.2" y2="12.86" width="0.1" layer="20"/>
<wire x1="19" y1="14.36" x2="19.3" y2="14.76" width="0.1" layer="20" curve="90"/>
<wire x1="19" y1="15.96" x2="19" y2="16.96" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="19" y1="16.96" x2="18" y2="16.96" width="0.1" layer="20"/>
<wire x1="20.48" y1="12.96" x2="21.555625" y2="12.96" width="0.1" layer="20"/>
<wire x1="20.22" y1="12.86" x2="20.24" y2="12.86" width="0.1" layer="20"/>
<wire x1="20.24" y1="12.86" x2="20.34" y2="12.9" width="0.1" layer="20" curve="43.602819"/>
<wire x1="20.34" y1="12.9" x2="20.48" y2="12.96" width="0.1" layer="20" curve="-50.335242"/>
<wire x1="19.3" y1="14.76" x2="19.24" y2="14.91" width="0.1" layer="20"/>
<wire x1="21.5" y1="12.96" x2="23.803125" y2="14.009375" width="0.1016" layer="20" curve="49.737166"/>
<wire x1="19" y1="15.96" x2="19.24" y2="14.9" width="0.1016" layer="20" curve="25.127824"/>
<wire x1="27.4" y1="-12.06" x2="25.45" y2="-12.06" width="0.1" layer="20"/>
<wire x1="25.45" y1="-12.06" x2="25.1" y2="-12.41" width="0.1" layer="20" curve="90"/>
<wire x1="25.1" y1="-12.41" x2="25.45" y2="-12.76" width="0.1" layer="20" curve="90"/>
<wire x1="25.45" y1="-12.76" x2="25.45" y2="-13.58" width="0.1" layer="20"/>
<wire x1="25.45" y1="-13.58" x2="25.11" y2="-13.98" width="0.1" layer="20" curve="-90"/>
<wire x1="25.11" y1="-13.98" x2="23.82" y2="-14" width="0.1" layer="20"/>
<wire x1="18" y1="-15.96" x2="13" y2="-15.96" width="0.1" layer="20" curve="180"/>
<wire x1="13" y1="-15.96" x2="13" y2="-18.46" width="0.1" layer="20"/>
<wire x1="18" y1="-15.96" x2="18" y2="-16.96" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="18.4" y1="-13.26" x2="18.4" y2="-13.96" width="0.1" layer="20"/>
<wire x1="18.4" y1="-13.96" x2="18.8" y2="-14.36" width="0.1" layer="20" curve="90"/>
<wire x1="18.8" y1="-14.36" x2="19" y2="-14.36" width="0.1" layer="20"/>
<wire x1="18.4" y1="-13.26" x2="18.8" y2="-12.86" width="0.1" layer="20" curve="-90"/>
<wire x1="18.8" y1="-12.86" x2="20.2" y2="-12.86" width="0.1" layer="20"/>
<wire x1="19" y1="-14.36" x2="19.3" y2="-14.76" width="0.1" layer="20" curve="-90"/>
<wire x1="19" y1="-15.96" x2="19" y2="-16.96" width="0.1" layer="20" curve="0.060312"/>
<wire x1="19" y1="-16.96" x2="18" y2="-16.96" width="0.1" layer="20"/>
<wire x1="20.48" y1="-12.96" x2="21.555625" y2="-12.96" width="0.1" layer="20"/>
<wire x1="20.22" y1="-12.86" x2="20.24" y2="-12.86" width="0.1" layer="20"/>
<wire x1="20.24" y1="-12.86" x2="20.34" y2="-12.9" width="0.1" layer="20" curve="-43.602819"/>
<wire x1="20.34" y1="-12.9" x2="20.48" y2="-12.96" width="0.1" layer="20" curve="50.335242"/>
<wire x1="19.3" y1="-14.76" x2="19.24" y2="-14.91" width="0.1" layer="20"/>
<wire x1="10" y1="-18.46" x2="13" y2="-18.46" width="0.1" layer="20"/>
<wire x1="21.5" y1="-12.96" x2="23.803125" y2="-14.009375" width="0.1016" layer="20" curve="-49.737166"/>
<wire x1="19" y1="-15.96" x2="19.24" y2="-14.9" width="0.1016" layer="20" curve="-25.127824"/>
<wire x1="-4.4" y1="12.06" x2="-2.45" y2="12.06" width="0.1" layer="20"/>
<wire x1="-2.45" y1="12.06" x2="-2.1" y2="12.41" width="0.1" layer="20" curve="90"/>
<wire x1="-2.1" y1="12.41" x2="-2.45" y2="12.76" width="0.1" layer="20" curve="90"/>
<wire x1="-2.45" y1="12.76" x2="-2.45" y2="13.58" width="0.1" layer="20"/>
<wire x1="-2.45" y1="13.58" x2="-2.11" y2="13.98" width="0.1" layer="20" curve="-90"/>
<wire x1="-2.11" y1="13.98" x2="-0.82" y2="14" width="0.1" layer="20"/>
<wire x1="5" y1="15.96" x2="10" y2="15.96" width="0.1" layer="20" curve="180"/>
<wire x1="10" y1="15.96" x2="10" y2="18.46" width="0.1" layer="20"/>
<wire x1="5" y1="15.96" x2="5" y2="16.96" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="4.6" y1="13.26" x2="4.6" y2="13.96" width="0.1" layer="20"/>
<wire x1="4.6" y1="13.96" x2="4.2" y2="14.36" width="0.1" layer="20" curve="90"/>
<wire x1="4.2" y1="14.36" x2="4" y2="14.36" width="0.1" layer="20"/>
<wire x1="4.6" y1="13.26" x2="4.2" y2="12.86" width="0.1" layer="20" curve="-90"/>
<wire x1="4.2" y1="12.86" x2="2.8" y2="12.86" width="0.1" layer="20"/>
<wire x1="4" y1="14.36" x2="3.7" y2="14.76" width="0.1" layer="20" curve="-90"/>
<wire x1="4" y1="15.96" x2="4" y2="16.96" width="0.1" layer="20" curve="0.060312"/>
<wire x1="4" y1="16.96" x2="5" y2="16.96" width="0.1" layer="20"/>
<wire x1="2.52" y1="12.96" x2="1.444375" y2="12.96" width="0.1" layer="20"/>
<wire x1="2.78" y1="12.86" x2="2.76" y2="12.86" width="0.1" layer="20"/>
<wire x1="2.76" y1="12.86" x2="2.66" y2="12.9" width="0.1" layer="20" curve="-43.602819"/>
<wire x1="2.66" y1="12.9" x2="2.52" y2="12.96" width="0.1" layer="20" curve="50.335242"/>
<wire x1="3.7" y1="14.76" x2="3.76" y2="14.91" width="0.1" layer="20"/>
<wire x1="13" y1="18.46" x2="10" y2="18.46" width="0.1" layer="20"/>
<wire x1="1.5" y1="12.96" x2="-0.803125" y2="14.009375" width="0.1016" layer="20" curve="-49.737166"/>
<wire x1="4" y1="15.96" x2="3.76" y2="14.9" width="0.1016" layer="20" curve="-25.127824"/>
</package>
<package name="27UPIN-COOKIE-PAD-HUB">
<wire x1="27.5" y1="-12" x2="27.5" y2="-16" width="0.127" layer="51"/>
<wire x1="27.5" y1="-16" x2="23.5" y2="-20" width="0.127" layer="51" curve="-90"/>
<wire x1="23.5" y1="-20" x2="-0.5" y2="-20" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-20" x2="-4.5" y2="-16" width="0.127" layer="51" curve="-90"/>
<wire x1="-4.5" y1="-16" x2="-4.5" y2="-12" width="0.127" layer="51"/>
<wire x1="27.5" y1="12" x2="27.5" y2="16" width="0.127" layer="51"/>
<wire x1="27.5" y1="16" x2="23.5" y2="20" width="0.127" layer="51" curve="90"/>
<wire x1="23.5" y1="20" x2="-0.5" y2="20" width="0.127" layer="51"/>
<wire x1="-0.5" y1="20" x2="-4.5" y2="16" width="0.127" layer="51" curve="90"/>
<wire x1="-4.5" y1="16" x2="-4.5" y2="12" width="0.127" layer="51"/>
<wire x1="16.589375" y1="5.23875" x2="6.270625" y2="5.23875" width="0.127" layer="48" style="shortdash"/>
<wire x1="6.270625" y1="5.23875" x2="6.270625" y2="-5.23875" width="0.127" layer="48" style="shortdash"/>
<wire x1="16.589375" y1="5.23875" x2="16.589375" y2="-5.23875" width="0.127" layer="48" style="shortdash"/>
<wire x1="16.589375" y1="-5.23875" x2="6.270625" y2="-5.23875" width="0.127" layer="48" style="shortdash"/>
<circle x="-1" y="13" radius="0.472465625" width="0.3048" layer="22"/>
<smd name="1" x="0" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="2.54" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="3" x="5.08" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="4" x="7.62" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="5" x="10.16" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="6" x="12.7" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="7" x="15.24" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="8" x="17.78" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="9" x="20.32" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="10" x="22.86" y="-10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="11" x="22.86" y="-7.62" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="12" x="22.86" y="-5.08" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="13" x="22.86" y="-2.54" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="14" x="22.86" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="15" x="22.86" y="2.54" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="16" x="22.86" y="5.08" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="17" x="22.86" y="7.62" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="18" x="22.86" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="19" x="20.32" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="20" x="17.78" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="21" x="15.24" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="22" x="12.7" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="23" x="10.16" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="24" x="7.62" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="25" x="5.08" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="26" x="2.54" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="27" x="0" y="10.16" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<hole x="11.5" y="-14.96" drill="1.8"/>
<hole x="11.5" y="-12.71" drill="0.9"/>
<hole x="11" y="-12.71" drill="0.9"/>
<hole x="12" y="-12.71" drill="0.9"/>
<hole x="11.25" y="-12.71" drill="0.9"/>
<hole x="11.75" y="-12.71" drill="0.9"/>
<hole x="11.125" y="-12.71" drill="0.9"/>
<hole x="11.375" y="-12.71" drill="0.9"/>
<hole x="11.625" y="-12.71" drill="0.9"/>
<hole x="11.875" y="-12.71" drill="0.9"/>
<hole x="11.5" y="14.96" drill="1.8"/>
<hole x="11.5" y="12.744159375" drill="0.9"/>
<hole x="12" y="12.744159375" drill="0.9"/>
<hole x="11" y="12.744159375" drill="0.9"/>
<hole x="11.75" y="12.744159375" drill="0.9"/>
<hole x="11.25" y="12.744159375" drill="0.9"/>
<hole x="11.875" y="12.744159375" drill="0.9"/>
<hole x="11.625" y="12.744159375" drill="0.9"/>
<hole x="11.375" y="12.744159375" drill="0.9"/>
<hole x="11.125" y="12.744159375" drill="0.9"/>
<wire x1="-4.4" y1="-12.06" x2="-2.45" y2="-12.06" width="0.1" layer="20"/>
<wire x1="-2.45" y1="-12.06" x2="-2.1" y2="-12.41" width="0.1" layer="20" curve="-90"/>
<wire x1="-2.1" y1="-12.41" x2="-2.45" y2="-12.76" width="0.1" layer="20" curve="-90"/>
<wire x1="-2.45" y1="-12.76" x2="-2.45" y2="-13.58" width="0.1" layer="20"/>
<wire x1="-2.45" y1="-13.58" x2="-2.11" y2="-13.98" width="0.1" layer="20" curve="90"/>
<wire x1="-2.11" y1="-13.98" x2="-0.82" y2="-14" width="0.1" layer="20"/>
<wire x1="5" y1="-15.96" x2="10" y2="-15.96" width="0.1" layer="20" curve="-180"/>
<wire x1="10" y1="-15.96" x2="10" y2="-18.46" width="0.1" layer="20"/>
<wire x1="5" y1="-15.96" x2="5" y2="-16.96" width="0.1" layer="20" curve="0.060312"/>
<wire x1="4.6" y1="-13.26" x2="4.6" y2="-13.96" width="0.1" layer="20"/>
<wire x1="4.6" y1="-13.96" x2="4.2" y2="-14.36" width="0.1" layer="20" curve="-90"/>
<wire x1="4.2" y1="-14.36" x2="4" y2="-14.36" width="0.1" layer="20"/>
<wire x1="4.6" y1="-13.26" x2="4.2" y2="-12.86" width="0.1" layer="20" curve="90"/>
<wire x1="4.2" y1="-12.86" x2="2.8" y2="-12.86" width="0.1" layer="20"/>
<wire x1="4" y1="-14.36" x2="3.7" y2="-14.76" width="0.1" layer="20" curve="90"/>
<wire x1="4" y1="-15.96" x2="4" y2="-16.96" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="4" y1="-16.96" x2="5" y2="-16.96" width="0.1" layer="20"/>
<wire x1="2.52" y1="-12.96" x2="1.444375" y2="-12.96" width="0.1" layer="20"/>
<wire x1="2.78" y1="-12.86" x2="2.76" y2="-12.86" width="0.1" layer="20"/>
<wire x1="2.76" y1="-12.86" x2="2.66" y2="-12.9" width="0.1" layer="20" curve="43.602819"/>
<wire x1="2.66" y1="-12.9" x2="2.52" y2="-12.96" width="0.1" layer="20" curve="-50.335242"/>
<wire x1="3.7" y1="-14.76" x2="3.76" y2="-14.91" width="0.1" layer="20"/>
<wire x1="1.5" y1="-12.96" x2="-0.803125" y2="-14.009375" width="0.1016" layer="20" curve="49.737166"/>
<wire x1="4" y1="-15.96" x2="3.76" y2="-14.9" width="0.1016" layer="20" curve="25.127824"/>
<wire x1="27.4" y1="12.06" x2="25.45" y2="12.06" width="0.1" layer="20"/>
<wire x1="25.45" y1="12.06" x2="25.1" y2="12.41" width="0.1" layer="20" curve="-90"/>
<wire x1="25.1" y1="12.41" x2="25.45" y2="12.76" width="0.1" layer="20" curve="-90"/>
<wire x1="25.45" y1="12.76" x2="25.45" y2="13.58" width="0.1" layer="20"/>
<wire x1="25.45" y1="13.58" x2="25.11" y2="13.98" width="0.1" layer="20" curve="90"/>
<wire x1="25.11" y1="13.98" x2="23.82" y2="14" width="0.1" layer="20"/>
<wire x1="18" y1="15.96" x2="13" y2="15.96" width="0.1" layer="20" curve="-180"/>
<wire x1="13" y1="15.96" x2="13" y2="18.46" width="0.1" layer="20"/>
<wire x1="18" y1="15.96" x2="18" y2="16.96" width="0.1" layer="20" curve="0.060312"/>
<wire x1="18.4" y1="13.26" x2="18.4" y2="13.96" width="0.1" layer="20"/>
<wire x1="18.4" y1="13.96" x2="18.8" y2="14.36" width="0.1" layer="20" curve="-90"/>
<wire x1="18.8" y1="14.36" x2="19" y2="14.36" width="0.1" layer="20"/>
<wire x1="18.4" y1="13.26" x2="18.8" y2="12.86" width="0.1" layer="20" curve="90"/>
<wire x1="18.8" y1="12.86" x2="20.2" y2="12.86" width="0.1" layer="20"/>
<wire x1="19" y1="14.36" x2="19.3" y2="14.76" width="0.1" layer="20" curve="90"/>
<wire x1="19" y1="15.96" x2="19" y2="16.96" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="19" y1="16.96" x2="18" y2="16.96" width="0.1" layer="20"/>
<wire x1="20.48" y1="12.96" x2="21.555625" y2="12.96" width="0.1" layer="20"/>
<wire x1="20.22" y1="12.86" x2="20.24" y2="12.86" width="0.1" layer="20"/>
<wire x1="20.24" y1="12.86" x2="20.34" y2="12.9" width="0.1" layer="20" curve="43.602819"/>
<wire x1="20.34" y1="12.9" x2="20.48" y2="12.96" width="0.1" layer="20" curve="-50.335242"/>
<wire x1="19.3" y1="14.76" x2="19.24" y2="14.91" width="0.1" layer="20"/>
<wire x1="21.5" y1="12.96" x2="23.803125" y2="14.009375" width="0.1016" layer="20" curve="49.737166"/>
<wire x1="19" y1="15.96" x2="19.24" y2="14.9" width="0.1016" layer="20" curve="25.127824"/>
<wire x1="27.4" y1="-12.06" x2="25.45" y2="-12.06" width="0.1" layer="20"/>
<wire x1="25.45" y1="-12.06" x2="25.1" y2="-12.41" width="0.1" layer="20" curve="90"/>
<wire x1="25.1" y1="-12.41" x2="25.45" y2="-12.76" width="0.1" layer="20" curve="90"/>
<wire x1="25.45" y1="-12.76" x2="25.45" y2="-13.58" width="0.1" layer="20"/>
<wire x1="25.45" y1="-13.58" x2="25.11" y2="-13.98" width="0.1" layer="20" curve="-90"/>
<wire x1="25.11" y1="-13.98" x2="23.82" y2="-14" width="0.1" layer="20"/>
<wire x1="18" y1="-15.96" x2="13" y2="-15.96" width="0.1" layer="20" curve="180"/>
<wire x1="13" y1="-15.96" x2="13" y2="-18.46" width="0.1" layer="20"/>
<wire x1="18" y1="-15.96" x2="18" y2="-16.96" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="18.4" y1="-13.26" x2="18.4" y2="-13.96" width="0.1" layer="20"/>
<wire x1="18.4" y1="-13.96" x2="18.8" y2="-14.36" width="0.1" layer="20" curve="90"/>
<wire x1="18.8" y1="-14.36" x2="19" y2="-14.36" width="0.1" layer="20"/>
<wire x1="18.4" y1="-13.26" x2="18.8" y2="-12.86" width="0.1" layer="20" curve="-90"/>
<wire x1="18.8" y1="-12.86" x2="20.2" y2="-12.86" width="0.1" layer="20"/>
<wire x1="19" y1="-14.36" x2="19.3" y2="-14.76" width="0.1" layer="20" curve="-90"/>
<wire x1="19" y1="-15.96" x2="19" y2="-16.96" width="0.1" layer="20" curve="0.060312"/>
<wire x1="19" y1="-16.96" x2="18" y2="-16.96" width="0.1" layer="20"/>
<wire x1="20.48" y1="-12.96" x2="21.555625" y2="-12.96" width="0.1" layer="20"/>
<wire x1="20.22" y1="-12.86" x2="20.24" y2="-12.86" width="0.1" layer="20"/>
<wire x1="20.24" y1="-12.86" x2="20.34" y2="-12.9" width="0.1" layer="20" curve="-43.602819"/>
<wire x1="20.34" y1="-12.9" x2="20.48" y2="-12.96" width="0.1" layer="20" curve="50.335242"/>
<wire x1="19.3" y1="-14.76" x2="19.24" y2="-14.91" width="0.1" layer="20"/>
<wire x1="10" y1="-18.46" x2="13" y2="-18.46" width="0.1" layer="20"/>
<wire x1="27.4" y1="-12.06" x2="27.4" y2="12.06" width="0.1" layer="20"/>
<wire x1="21.5" y1="-12.96" x2="23.803125" y2="-14.009375" width="0.1016" layer="20" curve="-49.737166"/>
<wire x1="19" y1="-15.96" x2="19.24" y2="-14.9" width="0.1016" layer="20" curve="-25.127824"/>
<wire x1="-4.4" y1="12.06" x2="-2.45" y2="12.06" width="0.1" layer="20"/>
<wire x1="-2.45" y1="12.06" x2="-2.1" y2="12.41" width="0.1" layer="20" curve="90"/>
<wire x1="-2.1" y1="12.41" x2="-2.45" y2="12.76" width="0.1" layer="20" curve="90"/>
<wire x1="-2.45" y1="12.76" x2="-2.45" y2="13.58" width="0.1" layer="20"/>
<wire x1="-2.45" y1="13.58" x2="-2.11" y2="13.98" width="0.1" layer="20" curve="-90"/>
<wire x1="-2.11" y1="13.98" x2="-0.82" y2="14" width="0.1" layer="20"/>
<wire x1="5" y1="15.96" x2="10" y2="15.96" width="0.1" layer="20" curve="180"/>
<wire x1="10" y1="15.96" x2="10" y2="18.46" width="0.1" layer="20"/>
<wire x1="5" y1="15.96" x2="5" y2="16.96" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="4.6" y1="13.26" x2="4.6" y2="13.96" width="0.1" layer="20"/>
<wire x1="4.6" y1="13.96" x2="4.2" y2="14.36" width="0.1" layer="20" curve="90"/>
<wire x1="4.2" y1="14.36" x2="4" y2="14.36" width="0.1" layer="20"/>
<wire x1="4.6" y1="13.26" x2="4.2" y2="12.86" width="0.1" layer="20" curve="-90"/>
<wire x1="4.2" y1="12.86" x2="2.8" y2="12.86" width="0.1" layer="20"/>
<wire x1="4" y1="14.36" x2="3.7" y2="14.76" width="0.1" layer="20" curve="-90"/>
<wire x1="4" y1="15.96" x2="4" y2="16.96" width="0.1" layer="20" curve="0.060312"/>
<wire x1="4" y1="16.96" x2="5" y2="16.96" width="0.1" layer="20"/>
<wire x1="2.52" y1="12.96" x2="1.444375" y2="12.96" width="0.1" layer="20"/>
<wire x1="2.78" y1="12.86" x2="2.76" y2="12.86" width="0.1" layer="20"/>
<wire x1="2.76" y1="12.86" x2="2.66" y2="12.9" width="0.1" layer="20" curve="-43.602819"/>
<wire x1="2.66" y1="12.9" x2="2.52" y2="12.96" width="0.1" layer="20" curve="50.335242"/>
<wire x1="3.7" y1="14.76" x2="3.76" y2="14.91" width="0.1" layer="20"/>
<wire x1="13" y1="18.46" x2="10" y2="18.46" width="0.1" layer="20"/>
<wire x1="-4.4" y1="12.06" x2="-4.4" y2="-12.06" width="0.1" layer="20"/>
<wire x1="1.5" y1="12.96" x2="-0.803125" y2="14.009375" width="0.1016" layer="20" curve="-49.737166"/>
<wire x1="4" y1="15.96" x2="3.76" y2="14.9" width="0.1016" layer="20" curve="-25.127824"/>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="0" y="1.905" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="0" y="1.905" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="0" y="0" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="1206-CAP">
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="36"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="36"/>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<text x="0" y="1.42875" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-1.508125" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
<wire x1="0" y1="0.079375" x2="0" y2="-0.079375" width="0.8128" layer="21"/>
</package>
<package name="CRYSTAL-CT">
<wire x1="1.8" y1="0.65" x2="1.8" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-0.65" x2="-1.8" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="0.65" x2="1.6" y2="0.65" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.65" x2="1.6" y2="-0.65" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.65" x2="-1.6" y2="-0.65" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.65" x2="-1.6" y2="0.65" width="0.127" layer="51"/>
<smd name="1" x="-1.2" y="0" dx="0.7" dy="1.7" layer="1"/>
<smd name="2" x="0" y="0" dx="0.7" dy="1.7" layer="1"/>
<smd name="3" x="1.2" y="0" dx="0.7" dy="1.7" layer="1"/>
<text x="0" y="1.4605" size="0.6096" layer="25" font="vector" align="center">&gt;Name</text>
</package>
<package name="SWITCH-1P2T(SK12D07)">
<pad name="A" x="2" y="0" drill="0.6" shape="octagon"/>
<pad name="S" x="0" y="0" drill="0.6" shape="octagon"/>
<pad name="B" x="-2" y="0" drill="0.6" shape="octagon"/>
<pad name="G1" x="-4.1" y="0" drill="1.2" shape="octagon"/>
<pad name="G2" x="4.1" y="0" drill="1.2" shape="octagon"/>
<wire x1="-4.3" y1="2.25" x2="4.3" y2="2.25" width="0.127" layer="21"/>
<wire x1="4.3" y1="2.25" x2="4.3" y2="-2.25" width="0.127" layer="21"/>
<wire x1="4.3" y1="-2.25" x2="-4.3" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-4.3" y1="-2.25" x2="-4.3" y2="2.25" width="0.127" layer="21"/>
<text x="0" y="1.74625" size="0.6096" layer="21" font="vector" rot="R180" align="center">&gt;NAME</text>
</package>
<package name="SWITCH-1P2T(MSK12C01)">
<hole x="1.5" y="0" drill="0.9"/>
<hole x="-1.5" y="0" drill="0.9"/>
<smd name="P$3" x="3.95" y="1.1" dx="1" dy="0.8" layer="1" roundness="36"/>
<smd name="P$1" x="3.95" y="-1.1" dx="1" dy="0.8" layer="1" roundness="36"/>
<smd name="P$2" x="-3.95" y="-1.1" dx="1" dy="0.8" layer="1" roundness="36"/>
<smd name="P$4" x="-3.95" y="1.1" dx="1" dy="0.8" layer="1" roundness="36"/>
<smd name="S" x="-0.75" y="-2.3" dx="1.554" dy="0.9" layer="1" roundness="36" rot="R270"/>
<smd name="A" x="2.25" y="-2.3" dx="1.554" dy="0.9" layer="1" roundness="36" rot="R270"/>
<smd name="B" x="-2.25" y="-2.3" dx="1.554" dy="0.9" layer="1" roundness="36" rot="R270"/>
<text x="5.08" y="0" size="0.6096" layer="25" font="vector" rot="R90" align="center">&gt;NAME</text>
<wire x1="3.2385" y1="1.412875" x2="-3.2445" y2="1.412875" width="0.127" layer="21"/>
<wire x1="-3.2445" y1="1.412875" x2="-3.2445" y2="-1.475875" width="0.127" layer="21"/>
<wire x1="-3.2445" y1="-1.475875" x2="3.2385" y2="-1.475875" width="0.127" layer="21"/>
<wire x1="3.2385" y1="-1.475875" x2="3.2385" y2="1.412875" width="0.127" layer="21"/>
</package>
<package name="SWITCH-1P2T(MM0059022B1)">
<wire x1="-2.125" y1="6.75" x2="2.125" y2="6.75" width="0.127" layer="21"/>
<wire x1="2.125" y1="6.75" x2="2.125" y2="-6.75" width="0.127" layer="21"/>
<wire x1="2.125" y1="-6.75" x2="-2.125" y2="-6.75" width="0.127" layer="21"/>
<wire x1="-2.125" y1="-6.75" x2="-2.125" y2="6.75" width="0.127" layer="21"/>
<smd name="1_1" x="-2.8" y="5.25" dx="1" dy="0.6" layer="1"/>
<smd name="1_2" x="2.8" y="5.25" dx="1" dy="0.6" layer="1"/>
<smd name="2_1" x="-2.8" y="-5.25" dx="1" dy="0.6" layer="1"/>
<smd name="2_2" x="2.8" y="-5.25" dx="1" dy="0.6" layer="1"/>
<smd name="3" x="2.8" y="0" dx="1" dy="0.6" layer="1"/>
<text x="0" y="0" size="0.6096" layer="21" font="vector" rot="SR270" align="center">&gt;NAME</text>
<wire x1="-1.27" y1="5.08" x2="1.27" y2="5.08" width="0.127" layer="21"/>
<wire x1="1.27" y1="5.08" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="5.08" width="0.127" layer="21"/>
<text x="0" y="3.81" size="1.27" layer="21" font="vector" rot="SR270" align="center">G</text>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="2.2225" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.2225" x2="-1.27" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-5.08" x2="1.27" y2="-5.08" width="0.127" layer="21"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="-4.7625" width="0.127" layer="21"/>
<wire x1="1.27" y1="-4.7625" x2="1.27" y2="-4.683125" width="0.127" layer="21"/>
<wire x1="1.27" y1="-4.683125" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-4.7625" x2="-1.27" y2="2.2225" width="0.127" layer="21"/>
<wire x1="-1.031875" y1="2.2225" x2="1.031875" y2="2.2225" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.2225" x2="1.27" y2="-4.683125" width="0.127" layer="21"/>
<wire x1="-1.031875" y1="2.2225" x2="-1.031875" y2="-4.7625" width="0.127" layer="21"/>
<wire x1="-1.031875" y1="-4.7625" x2="1.031875" y2="-4.7625" width="0.127" layer="21"/>
<wire x1="1.031875" y1="-4.7625" x2="1.031875" y2="2.2225" width="0.127" layer="21"/>
<wire x1="1.031875" y1="2.2225" x2="-1.031875" y2="2.2225" width="0.127" layer="21"/>
</package>
<package name="SWITCH-1P2T(SS12D10)">
<pad name="A" x="4.7" y="0" drill="1" shape="octagon"/>
<pad name="S" x="0" y="0" drill="1" shape="octagon"/>
<pad name="B" x="-4.7" y="0" drill="1" shape="octagon"/>
<wire x1="-6.35" y1="3.2" x2="6.35" y2="3.2" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.2" x2="6.35" y2="-3.2" width="0.127" layer="21"/>
<wire x1="6.35" y1="-3.2" x2="-6.35" y2="-3.2" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.2" x2="-6.35" y2="3.2" width="0.127" layer="21"/>
<text x="0" y="1.74625" size="0.6096" layer="21" font="vector" rot="R180" align="center">&gt;NAME</text>
</package>
<package name="TQFP-32">
<circle x="-4.0655" y="-4.1306" radius="0.4016" width="0.2032" layer="21"/>
<wire x1="4.0371" y1="-4.0036" x2="-3.15185" y2="-4.0036" width="0.127" layer="51"/>
<wire x1="-3.15185" y1="-4.0036" x2="-4.0147" y2="-3.14075" width="0.127" layer="51"/>
<wire x1="-4.0147" y1="-3.14075" x2="-4.0147" y2="3.9974" width="0.127" layer="51"/>
<wire x1="-4.0147" y1="3.9974" x2="4.0371" y2="3.9974" width="0.127" layer="51"/>
<wire x1="4.0371" y1="3.9974" x2="4.0371" y2="-4.0036" width="0.127" layer="51"/>
<smd name="1" x="-2.7955" y="-4.0036" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="2" x="-1.9959" y="-4.0036" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="3" x="-1.1961" y="-4.0036" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="4" x="-0.3965" y="-4.0036" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="5" x="0.4031" y="-4.0036" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="6" x="1.203" y="-4.0036" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="2.0026" y="-4.0036" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="8" x="2.8022" y="-4.0036" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="4.0117" y="-2.8098" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="10" x="4.0117" y="-1.997" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="11" x="4.0117" y="-1.2096" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="12" x="4.0117" y="-0.3968" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="13" x="4.0117" y="0.3906" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="14" x="4.0117" y="1.178" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="15" x="4.0117" y="1.9908" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="16" x="4.0201" y="2.7993" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="17" x="2.7925" y="3.9974" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="18" x="2.0026" y="3.9931" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="19" x="1.203" y="3.9931" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="20" x="0.4031" y="3.9931" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="21" x="-0.3965" y="3.9931" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="22" x="-1.1961" y="3.9931" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="23" x="-1.9967" y="3.9931" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="24" x="-2.7955" y="3.9931" dx="1.778" dy="0.4318" layer="1" roundness="100" rot="R90"/>
<smd name="25" x="-3.9893" y="2.8036" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="26" x="-3.9893" y="1.9908" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="27" x="-3.9893" y="1.178" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="28" x="-3.9893" y="0.3906" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="29" x="-3.9893" y="-0.3968" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="30" x="-3.9893" y="-1.2096" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="31" x="-3.9893" y="-1.997" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<smd name="32" x="-3.9893" y="-2.8098" dx="1.778" dy="0.4318" layer="1" roundness="100"/>
<text x="0" y="0" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="MLF-32">
<description>&lt;b&gt;32M1-A&lt;/b&gt; Micro Lead Frame package (MLF)</description>
<wire x1="-2.35" y1="1.7325" x2="-1.7325" y2="2.35" width="0.254" layer="21"/>
<wire x1="-1.7325" y1="2.35" x2="2.3675" y2="2.35" width="0.254" layer="51"/>
<wire x1="2.35" y1="2.3675" x2="2.35" y2="-2.3675" width="0.254" layer="51"/>
<wire x1="2.3675" y1="-2.35" x2="-2.3675" y2="-2.35" width="0.254" layer="51"/>
<wire x1="-2.35" y1="-2.3675" x2="-2.35" y2="1.7325" width="0.254" layer="51"/>
<smd name="1" x="-2.35" y="1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="2" x="-2.35" y="1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="3" x="-2.35" y="0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="4" x="-2.35" y="0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="5" x="-2.35" y="-0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="6" x="-2.35" y="-0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="7" x="-2.35" y="-1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="8" x="-2.35" y="-1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="9" x="-1.75" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="10" x="-1.25" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="11" x="-0.75" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="12" x="-0.25" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="13" x="0.25" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="14" x="0.75" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="15" x="1.25" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="16" x="1.75" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="17" x="2.35" y="-1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="18" x="2.35" y="-1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="19" x="2.35" y="-0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="20" x="2.35" y="-0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="21" x="2.35" y="0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="22" x="2.35" y="0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="23" x="2.35" y="1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="24" x="2.35" y="1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<smd name="25" x="1.75" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="26" x="1.25" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="27" x="0.75" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="28" x="0.25" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="29" x="-0.25" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="30" x="-0.75" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="31" x="-1.25" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<smd name="32" x="-1.75" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="1.6" x2="-2.05" y2="1.9" layer="51"/>
<rectangle x1="-2.5" y1="1.1" x2="-2.05" y2="1.4" layer="51"/>
<rectangle x1="-2.5" y1="0.6" x2="-2.05" y2="0.9" layer="51"/>
<rectangle x1="-2.5" y1="0.1" x2="-2.05" y2="0.4" layer="51"/>
<rectangle x1="-2.5" y1="-0.4" x2="-2.05" y2="-0.1" layer="51"/>
<rectangle x1="-2.5" y1="-0.9" x2="-2.05" y2="-0.6" layer="51"/>
<rectangle x1="-2.5" y1="-1.4" x2="-2.05" y2="-1.1" layer="51"/>
<rectangle x1="-2.5" y1="-1.9" x2="-2.05" y2="-1.6" layer="51"/>
<rectangle x1="-1.9" y1="-2.5" x2="-1.6" y2="-2.05" layer="51"/>
<rectangle x1="-1.4" y1="-2.5" x2="-1.1" y2="-2.05" layer="51"/>
<rectangle x1="-0.9" y1="-2.5" x2="-0.6" y2="-2.05" layer="51"/>
<rectangle x1="-0.4" y1="-2.5" x2="-0.1" y2="-2.05" layer="51"/>
<rectangle x1="0.1" y1="-2.5" x2="0.4" y2="-2.05" layer="51"/>
<rectangle x1="0.6" y1="-2.5" x2="0.9" y2="-2.05" layer="51"/>
<rectangle x1="1.1" y1="-2.5" x2="1.4" y2="-2.05" layer="51"/>
<rectangle x1="1.6" y1="-2.5" x2="1.9" y2="-2.05" layer="51"/>
<rectangle x1="2.05" y1="-1.9" x2="2.5" y2="-1.6" layer="51"/>
<rectangle x1="2.05" y1="-1.4" x2="2.5" y2="-1.1" layer="51"/>
<rectangle x1="2.05" y1="-0.9" x2="2.5" y2="-0.6" layer="51"/>
<rectangle x1="2.05" y1="-0.4" x2="2.5" y2="-0.1" layer="51"/>
<rectangle x1="2.05" y1="0.1" x2="2.5" y2="0.4" layer="51"/>
<rectangle x1="2.05" y1="0.6" x2="2.5" y2="0.9" layer="51"/>
<rectangle x1="2.05" y1="1.1" x2="2.5" y2="1.4" layer="51"/>
<rectangle x1="2.05" y1="1.6" x2="2.5" y2="1.9" layer="51"/>
<rectangle x1="1.6" y1="2.05" x2="1.9" y2="2.5" layer="51"/>
<rectangle x1="1.1" y1="2.05" x2="1.4" y2="2.5" layer="51"/>
<rectangle x1="0.6" y1="2.05" x2="0.9" y2="2.5" layer="51"/>
<rectangle x1="0.1" y1="2.05" x2="0.4" y2="2.5" layer="51"/>
<rectangle x1="-0.4" y1="2.05" x2="-0.1" y2="2.5" layer="51"/>
<rectangle x1="-0.9" y1="2.05" x2="-0.6" y2="2.5" layer="51"/>
<rectangle x1="-1.4" y1="2.05" x2="-1.1" y2="2.5" layer="51"/>
<rectangle x1="-1.9" y1="2.05" x2="-1.6" y2="2.5" layer="51"/>
<circle x="-2.54" y="2.54" radius="0.3175" width="0.127" layer="21"/>
<text x="0" y="3.81" size="0.6096" layer="21" font="vector" align="center">&gt;NAME</text>
<smd name="P$1" x="0" y="0" dx="2.5" dy="2.5" layer="1"/>
</package>
<package name="SOT23-5">
<wire x1="1.422" y1="-0.781" x2="-1.423" y2="-0.781" width="0.1524" layer="51"/>
<wire x1="-1.423" y1="-0.781" x2="-1.423" y2="0.781" width="0.1524" layer="21"/>
<wire x1="-1.423" y1="0.781" x2="1.422" y2="0.781" width="0.1524" layer="51"/>
<wire x1="1.422" y1="0.781" x2="1.422" y2="-0.781" width="0.1524" layer="21"/>
<smd name="1" x="-0.95" y="-1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="2" x="0" y="-1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="3" x="0.95" y="-1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="4" x="0.95" y="1.15" dx="0.6604" dy="0.9652" layer="1"/>
<smd name="5" x="-0.95" y="1.15" dx="0.6604" dy="0.9652" layer="1"/>
<rectangle x1="-1.2" y1="-1.4" x2="-0.7" y2="-0.8" layer="51"/>
<rectangle x1="-0.25" y1="-1.4" x2="0.25" y2="-0.8" layer="51"/>
<rectangle x1="0.7" y1="-1.4" x2="1.2" y2="-0.8" layer="51"/>
<rectangle x1="0.7" y1="0.8" x2="1.2" y2="1.4" layer="51"/>
<rectangle x1="-1.2" y1="0.8" x2="-0.7" y2="1.4" layer="51"/>
<text x="-2.06375" y="0" size="0.6096" layer="25" font="vector" ratio="10" rot="R90" align="center">&gt;NAME</text>
<circle x="-0.95885" y="-0.29845" radius="0.15875" width="0.127" layer="21"/>
</package>
<package name="PAD-1.27MM">
<smd name="P" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" cream="no"/>
</package>
<package name="PINA">
<pad name="P$1" x="0" y="-1.27" drill="3" diameter="6" shape="square"/>
<pad name="P$2" x="-1.445" y="1.96" drill="0.000003125" diameter="3.1" shape="square"/>
<pad name="P$3" x="1.455" y="1.96" drill="0.000003125" diameter="3.1" shape="square"/>
</package>
<package name="PAD-1MM">
<smd name="P" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" cream="no"/>
</package>
<package name="SOT23-3">
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.8" y1="0.7" x2="-1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.1" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1.1" dx="0.9144" dy="1.016" layer="1"/>
<smd name="2" x="0.95" y="-1.079375" dx="0.8128" dy="0.9144" layer="1"/>
<smd name="1" x="-0.95" y="-1.079375" dx="0.8128" dy="0.9144" layer="1"/>
<text x="-2.06375" y="0" size="0.6096" layer="25" font="vector" ratio="10" rot="R90" align="center">&gt;NAME</text>
</package>
<package name="CAP-3216">
<wire x1="-1" y1="-1.2" x2="-2.5" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.2" x2="-2.5" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.2" x2="-1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.2" x2="2.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-1.2" x2="2.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-0.8" x2="2.5" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.8" x2="2.1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="1.2" x2="1" y2="1.2" width="0.2032" layer="21"/>
<smd name="C" x="-1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="A" x="1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<text x="-3.286" y="0" size="0.6096" layer="25" font="vector" rot="R90" align="center">&gt;NAME</text>
<text x="3.113" y="0" size="0.6096" layer="27" font="vector" rot="R90" align="center">&gt;VALUE</text>
<wire x1="0.3" y1="0.8" x2="0.3" y2="-0.8" width="0.2032" layer="21"/>
</package>
<package name="CAP-7343">
<wire x1="-5" y1="2.5" x2="-2" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="2.5" x2="-5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.5" x2="-2" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="2" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="-1.5" x2="4" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-2.5" x2="2" y2="-2.5" width="0.2032" layer="21"/>
<smd name="C" x="-3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<smd name="A" x="3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.27" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-0605">
<wire x1="2.4" y1="-3.3" x2="-3.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-1.1" x2="-3.3" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="1.1" x2="-3.3" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="2.4" y2="3.3" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2.4" x2="3.3" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.3" y1="1.1" x2="3.3" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="3.3" y1="-1.1" x2="3.3" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2.4" x2="2.4" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="2.4" y1="3.3" x2="3.3" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-0.9" x2="2.95" y2="-0.95" width="0.2032" layer="21" curve="145.181395"/>
<wire x1="-2.95" y1="-0.9" x2="-2.95" y2="0.95" width="0.2032" layer="51" curve="-34.818605"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.9" width="0.2032" layer="21" curve="-145.181395"/>
<wire x1="2.95" y1="-0.95" x2="2.95" y2="0.9" width="0.2032" layer="51" curve="34.818605"/>
<smd name="+" x="2.7" y="0" dx="3.5" dy="1.6" layer="1"/>
<smd name="-" x="-2.7" y="0" dx="3.5" dy="1.6" layer="1"/>
<text x="0" y="4.445" size="0.6096" layer="21" font="vector" align="center">&gt;NAME</text>
</package>
<package name="SOD123">
<description>&lt;b&gt;Diode&lt;/b&gt;</description>
<wire x1="-1.1" y1="0.7" x2="1.1" y2="0.7" width="0.254" layer="21"/>
<wire x1="1.1" y1="0.7" x2="1.1" y2="-0.7" width="0.254" layer="21"/>
<wire x1="1.1" y1="-0.7" x2="-1.1" y2="-0.7" width="0.254" layer="21"/>
<wire x1="-1.1" y1="-0.7" x2="-1.1" y2="0.7" width="0.254" layer="21"/>
<smd name="C" x="-1.6" y="0" dx="0.9" dy="1" layer="1" roundness="50"/>
<smd name="A" x="1.6" y="0" dx="0.9" dy="1" layer="1" roundness="50"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
<rectangle x1="-1.95" y1="-0.45" x2="-1.2" y2="0.4" layer="51"/>
<rectangle x1="1.2" y1="-0.45" x2="1.95" y2="0.4" layer="51"/>
<rectangle x1="-1.05" y1="-0.65" x2="-0.15" y2="0.7" layer="21"/>
</package>
<package name="DO214">
<description>&lt;b&gt;SURFACE MOUNT GENERAL RECTIFIER&lt;/b&gt; JEDEC DO-214AC molded platic body&lt;p&gt;
Method 2026&lt;br&gt;
Source: http://www.kingtronics.com/SMD_M7/M7_SMD_4007.pdf</description>
<wire x1="-1.8" y1="1.3" x2="1.8" y2="1.3" width="0.2032" layer="21"/>
<wire x1="1.8" y1="1.3" x2="1.8" y2="-1.3" width="0.2032" layer="21"/>
<wire x1="1.8" y1="-1.3" x2="-1.8" y2="-1.3" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-1.3" x2="-1.8" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.035" y1="1.3" x2="1.025" y2="1.3" width="0.2032" layer="21"/>
<wire x1="1.025" y1="-1.3" x2="-1.035" y2="-1.3" width="0.2032" layer="21"/>
<smd name="C" x="-1.8" y="0" dx="1.4" dy="1.6" layer="1"/>
<smd name="A" x="1.8" y="0" dx="1.4" dy="1.6" layer="1" rot="R180"/>
<text x="0" y="1.905" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<rectangle x1="-1.115" y1="-1.225" x2="-0.44" y2="1.225" layer="21"/>
<rectangle x1="-1.75" y1="-1.225" x2="-1.075" y2="1.225" layer="21"/>
</package>
<package name="SOD523">
<description>&lt;B&gt;DIODE&lt;/B&gt;</description>
<wire x1="-0.64" y1="0.425" x2="0.64" y2="0.425" width="0.1016" layer="21"/>
<wire x1="0.64" y1="0.425" x2="0.64" y2="-0.425" width="0.1016" layer="21"/>
<wire x1="0.64" y1="-0.425" x2="-0.64" y2="-0.425" width="0.1016" layer="21"/>
<wire x1="-0.64" y1="-0.425" x2="-0.64" y2="0.425" width="0.1016" layer="21"/>
<smd name="A" x="0.65" y="0" dx="0.6" dy="0.5" layer="1"/>
<smd name="C" x="-0.65" y="0" dx="0.6" dy="0.5" layer="1"/>
<text x="0" y="0.8255" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<rectangle x1="-0.59" y1="-0.4" x2="-0.3" y2="0.4" layer="21"/>
<rectangle x1="-0.336" y1="-0.4" x2="-0.046" y2="0.4" layer="21"/>
<rectangle x1="-0.2725" y1="-0.4" x2="0.0175" y2="0.4" layer="21"/>
</package>
<package name="0603-IND">
<wire x1="-0.356" y1="0" x2="0.356" y2="0" width="0.4064" layer="21"/>
<smd name="1" x="-0.7" y="0" dx="0.75" dy="1" layer="1" roundness="50"/>
<smd name="2" x="0.7" y="0" dx="0.75" dy="1" layer="1" roundness="50"/>
<text x="0" y="1.11125" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-1.11125" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
</package>
<package name="CR54">
<wire x1="2.8" y1="2.98" x2="-2.8" y2="2.98" width="0.127" layer="51"/>
<wire x1="-2.8" y1="2.98" x2="-2.8" y2="-3" width="0.127" layer="51"/>
<wire x1="-2.8" y1="-3" x2="2.8" y2="-3" width="0.127" layer="51"/>
<wire x1="2.8" y1="-3" x2="2.8" y2="2.98" width="0.127" layer="51"/>
<smd name="P$1" x="0" y="1.92" dx="5.5" dy="2.15" layer="1"/>
<smd name="P$2" x="0" y="-1.92" dx="5.5" dy="2.15" layer="1"/>
<text x="0" y="0" size="0.6096" layer="25" font="vector" align="center">&gt;Name</text>
</package>
<package name="0402-IND">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0" x2="0.245" y2="0" width="0.3048125" layer="21"/>
<smd name="1" x="-0.4984" y="0" dx="0.508" dy="0.7112" layer="1" roundness="50"/>
<smd name="2" x="0.4984" y="0" dx="0.508" dy="0.7112" layer="1" roundness="50"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<text x="0" y="0.873125" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-0.873125" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-0.8" y="0.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.7" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1007">
<description>1007 (2518 metric) package</description>
<wire x1="0.9" y1="1.25" x2="-0.9" y2="1.25" width="0.127" layer="51"/>
<wire x1="-0.9" y1="1.25" x2="-0.9" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-0.9" y1="-1.25" x2="0.9" y2="-1.25" width="0.127" layer="51"/>
<wire x1="0.9" y1="-1.25" x2="0.9" y2="1.25" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.4" x2="-1" y2="0.4" width="0.127" layer="21"/>
<wire x1="1" y1="-0.4" x2="1" y2="0.4" width="0.127" layer="21"/>
<smd name="1" x="0" y="1" dx="2" dy="0.8" layer="1"/>
<smd name="2" x="0" y="-1" dx="2" dy="0.8" layer="1"/>
<text x="-1" y="1.6" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="RWPA4018">
<smd name="P$1" x="1.5" y="0" dx="3.7" dy="1.1" layer="1" roundness="40" rot="R90"/>
<smd name="P$2" x="-1.5" y="0" dx="3.7" dy="1.1" layer="1" roundness="40" rot="R90"/>
<text x="0" y="-2.54" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.127" layer="21"/>
</package>
<package name="0805-IND">
<wire x1="-0.619125" y1="0" x2="0.619125" y2="0" width="0.4064" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1" roundness="40"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1" roundness="40"/>
<text x="0" y="1.11125" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-1.11125" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
<rectangle x1="-1.076325" y1="-0.508" x2="-0.576225" y2="0.508" layer="51"/>
<rectangle x1="0.568325" y1="-0.508" x2="1.068425" y2="0.508" layer="51"/>
</package>
<package name="0402-IND_L">
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1" roundness="50"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1" roundness="50"/>
<wire x1="-0.245" y1="0" x2="0.245" y2="0" width="0.3048125" layer="51"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<text x="0" y="0.873125" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-0.873125" size="0.6096" layer="27" font="vector" align="center">&gt;VALUE</text>
</package>
<package name="SOD923">
<smd name="A" x="0.45" y="0" dx="0.4" dy="0.5" layer="1"/>
<smd name="C" x="-0.45" y="0" dx="0.4" dy="0.5" layer="1"/>
<wire x1="-0.4" y1="0.3" x2="0.4" y2="0.3" width="0.06" layer="21"/>
<wire x1="-0.4" y1="-0.3" x2="0.4" y2="-0.3" width="0.06" layer="21"/>
<wire x1="-0.15875" y1="0.15875" x2="-0.15875" y2="-0.15875" width="0.2" layer="21"/>
<text x="0" y="0.85725" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<wire x1="-0.3175" y1="0.4445" x2="-0.5715" y2="0.4445" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="-0.4445" x2="-0.5715" y2="-0.4445" width="0.127" layer="21"/>
</package>
<package name="CRYSTAL-3215">
<smd name="1" x="-1.172075" y="0" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="1.177925" y="0" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<wire x1="-1.592075" y1="-0.75" x2="-1.592075" y2="0.75" width="0.127" layer="21"/>
<wire x1="-1.592075" y1="-0.75" x2="1.607925" y2="-0.75" width="0.127" layer="21"/>
<wire x1="1.607925" y1="-0.75" x2="1.607925" y2="0.75" width="0.127" layer="21"/>
<wire x1="-1.592075" y1="0.75" x2="1.607925" y2="0.75" width="0.127" layer="21"/>
<text x="0" y="1.5875" size="0.6096" layer="21" font="vector" align="center">&gt;NAME</text>
</package>
<package name="QFN48">
<wire x1="-3" y1="-3" x2="-3" y2="2.151471875" width="0.127" layer="21"/>
<wire x1="-3" y1="2.151471875" x2="-2.151471875" y2="3" width="0.127" layer="21"/>
<wire x1="-2.151471875" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<smd name="G" x="0" y="0" dx="4" dy="4" layer="1" roundness="24" rot="R180"/>
<circle x="-2.94" y="2.94" radius="0.3175" width="0.127" layer="21"/>
<text x="0" y="3.9925" size="0.6096" layer="21" font="vector" align="center">&gt;NAME</text>
<smd name="1" x="-3" y="2.2" dx="0.7" dy="0.2" layer="1"/>
<smd name="2" x="-3" y="1.8" dx="0.7" dy="0.2" layer="1"/>
<smd name="3" x="-3" y="1.4" dx="0.7" dy="0.2" layer="1"/>
<smd name="4" x="-3" y="1" dx="0.7" dy="0.2" layer="1"/>
<smd name="5" x="-3" y="0.6" dx="0.7" dy="0.2" layer="1"/>
<smd name="6" x="-3" y="0.2" dx="0.7" dy="0.2" layer="1"/>
<smd name="7" x="-3" y="-0.2" dx="0.7" dy="0.2" layer="1"/>
<smd name="8" x="-3" y="-0.6" dx="0.7" dy="0.2" layer="1"/>
<smd name="9" x="-3" y="-1" dx="0.7" dy="0.2" layer="1"/>
<smd name="10" x="-3" y="-1.4" dx="0.7" dy="0.2" layer="1"/>
<smd name="11" x="-3" y="-1.8" dx="0.7" dy="0.2" layer="1"/>
<smd name="12" x="-3" y="-2.2" dx="0.7" dy="0.2" layer="1"/>
<smd name="13" x="-2.2" y="-3" dx="0.7" dy="0.2" layer="1" rot="R90"/>
<smd name="14" x="-1.8" y="-3" dx="0.7" dy="0.2" layer="1" rot="R90"/>
<smd name="15" x="-1.4" y="-3" dx="0.7" dy="0.2" layer="1" rot="R90"/>
<smd name="16" x="-1" y="-3" dx="0.7" dy="0.2" layer="1" rot="R90"/>
<smd name="17" x="-0.6" y="-3" dx="0.7" dy="0.2" layer="1" rot="R90"/>
<smd name="18" x="-0.2" y="-3" dx="0.7" dy="0.2" layer="1" rot="R90"/>
<smd name="19" x="0.2" y="-3" dx="0.7" dy="0.2" layer="1" rot="R90"/>
<smd name="20" x="0.6" y="-3" dx="0.7" dy="0.2" layer="1" rot="R90"/>
<smd name="21" x="1" y="-3" dx="0.7" dy="0.2" layer="1" rot="R90"/>
<smd name="22" x="1.4" y="-3" dx="0.7" dy="0.2" layer="1" rot="R90"/>
<smd name="23" x="1.8" y="-3" dx="0.7" dy="0.2" layer="1" rot="R90"/>
<smd name="24" x="2.2" y="-3" dx="0.7" dy="0.2" layer="1" rot="R90"/>
<smd name="25" x="3" y="-2.2" dx="0.7" dy="0.2" layer="1" rot="R180"/>
<smd name="26" x="3" y="-1.8" dx="0.7" dy="0.2" layer="1" rot="R180"/>
<smd name="27" x="3" y="-1.4" dx="0.7" dy="0.2" layer="1" rot="R180"/>
<smd name="28" x="3" y="-1" dx="0.7" dy="0.2" layer="1" rot="R180"/>
<smd name="29" x="3" y="-0.6" dx="0.7" dy="0.2" layer="1" rot="R180"/>
<smd name="30" x="3" y="-0.2" dx="0.7" dy="0.2" layer="1" rot="R180"/>
<smd name="31" x="3" y="0.2" dx="0.7" dy="0.2" layer="1" rot="R180"/>
<smd name="32" x="3" y="0.6" dx="0.7" dy="0.2" layer="1" rot="R180"/>
<smd name="33" x="3" y="1" dx="0.7" dy="0.2" layer="1" rot="R180"/>
<smd name="34" x="3" y="1.4" dx="0.7" dy="0.2" layer="1" rot="R180"/>
<smd name="35" x="3" y="1.8" dx="0.7" dy="0.2" layer="1" rot="R180"/>
<smd name="36" x="3" y="2.2" dx="0.7" dy="0.2" layer="1" rot="R180"/>
<smd name="37" x="2.2" y="3" dx="0.7" dy="0.2" layer="1" rot="R270"/>
<smd name="38" x="1.8" y="3" dx="0.7" dy="0.2" layer="1" rot="R270"/>
<smd name="39" x="1.4" y="3" dx="0.7" dy="0.2" layer="1" rot="R270"/>
<smd name="40" x="1" y="3" dx="0.7" dy="0.2" layer="1" rot="R270"/>
<smd name="41" x="0.6" y="3" dx="0.7" dy="0.2" layer="1" rot="R270"/>
<smd name="42" x="0.2" y="3" dx="0.7" dy="0.2" layer="1" rot="R270"/>
<smd name="43" x="-0.2" y="3" dx="0.7" dy="0.2" layer="1" rot="R270"/>
<smd name="44" x="-0.6" y="3" dx="0.7" dy="0.2" layer="1" rot="R270"/>
<smd name="45" x="-1" y="3" dx="0.7" dy="0.2" layer="1" rot="R270"/>
<smd name="46" x="-1.4" y="3" dx="0.7" dy="0.2" layer="1" rot="R270"/>
<smd name="47" x="-1.8" y="3" dx="0.7" dy="0.2" layer="1" rot="R270"/>
<smd name="48" x="-2.2" y="3" dx="0.7" dy="0.2" layer="1" rot="R270"/>
</package>
<package name="ANT-2.4G">
<smd name="GND" x="0.4515" y="0.251" dx="0.9" dy="0.754" layer="1" stop="no"/>
<smd name="ANT" x="2.554" y="0.251" dx="0.5" dy="0.754" layer="1"/>
<polygon width="0.02" layer="1">
<vertex x="0.01" y="0.5"/>
<vertex x="0.01" y="5.39"/>
<vertex x="4.99" y="5.39"/>
<vertex x="4.99" y="2.75"/>
<vertex x="7.01" y="2.75"/>
<vertex x="7.01" y="5.39"/>
<vertex x="9.69" y="5.39"/>
<vertex x="9.69" y="2.75"/>
<vertex x="11.71" y="2.75"/>
<vertex x="11.71" y="5.39"/>
<vertex x="14.39" y="5.39"/>
<vertex x="14.39" y="0.97"/>
<vertex x="13.91" y="0.97"/>
<vertex x="13.91" y="4.91"/>
<vertex x="12.19" y="4.91"/>
<vertex x="12.19" y="2.27"/>
<vertex x="9.21" y="2.27"/>
<vertex x="9.21" y="4.91"/>
<vertex x="7.49" y="4.91"/>
<vertex x="7.49" y="2.27"/>
<vertex x="4.51" y="2.27"/>
<vertex x="4.51" y="4.91"/>
<vertex x="2.795" y="4.91"/>
<vertex x="2.795" y="0.5"/>
<vertex x="2.315" y="0.5"/>
<vertex x="2.315" y="4.91"/>
<vertex x="0.8925" y="4.91"/>
<vertex x="0.8925" y="0.5"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="0.01" y="0.5"/>
<vertex x="0.01" y="5.39"/>
<vertex x="4.99" y="5.39"/>
<vertex x="4.99" y="2.75"/>
<vertex x="7.01" y="2.75"/>
<vertex x="7.01" y="5.39"/>
<vertex x="9.69" y="5.39"/>
<vertex x="9.69" y="2.75"/>
<vertex x="11.71" y="2.75"/>
<vertex x="11.71" y="5.39"/>
<vertex x="14.39" y="5.39"/>
<vertex x="14.39" y="0.97"/>
<vertex x="13.91" y="0.97"/>
<vertex x="13.91" y="4.91"/>
<vertex x="12.19" y="4.91"/>
<vertex x="12.19" y="2.27"/>
<vertex x="9.21" y="2.27"/>
<vertex x="9.21" y="4.91"/>
<vertex x="7.49" y="4.91"/>
<vertex x="7.49" y="2.27"/>
<vertex x="4.51" y="2.27"/>
<vertex x="4.51" y="4.91"/>
<vertex x="2.795" y="4.91"/>
<vertex x="2.795" y="0.5"/>
<vertex x="2.315" y="0.5"/>
<vertex x="2.315" y="4.91"/>
<vertex x="0.8925" y="4.91"/>
<vertex x="0.8925" y="0.5"/>
</polygon>
<rectangle x1="-0.47625" y1="0.47625" x2="14.684375" y2="5.715" layer="41"/>
<rectangle x1="-0.47625" y1="0.47625" x2="14.684375" y2="5.715" layer="42"/>
</package>
<package name="ANT-2.4G-F">
<smd name="GND" x="-0.4515" y="0.251" dx="0.9" dy="0.754" layer="1" rot="R180" stop="no"/>
<smd name="ANT" x="-2.554" y="0.251" dx="0.5" dy="0.754" layer="1" rot="R180"/>
<polygon width="0.02" layer="16">
<vertex x="-0.01" y="0.5"/>
<vertex x="-0.01" y="5.39"/>
<vertex x="-4.99" y="5.39"/>
<vertex x="-4.99" y="2.75"/>
<vertex x="-7.01" y="2.75"/>
<vertex x="-7.01" y="5.39"/>
<vertex x="-9.69" y="5.39"/>
<vertex x="-9.69" y="2.75"/>
<vertex x="-11.71" y="2.75"/>
<vertex x="-11.71" y="5.39"/>
<vertex x="-14.39" y="5.39"/>
<vertex x="-14.39" y="0.97"/>
<vertex x="-13.91" y="0.97"/>
<vertex x="-13.91" y="4.91"/>
<vertex x="-12.19" y="4.91"/>
<vertex x="-12.19" y="2.27"/>
<vertex x="-9.21" y="2.27"/>
<vertex x="-9.21" y="4.91"/>
<vertex x="-7.49" y="4.91"/>
<vertex x="-7.49" y="2.27"/>
<vertex x="-4.51" y="2.27"/>
<vertex x="-4.51" y="4.91"/>
<vertex x="-2.795" y="4.91"/>
<vertex x="-2.795" y="0.5"/>
<vertex x="-2.315" y="0.5"/>
<vertex x="-2.315" y="4.91"/>
<vertex x="-0.8925" y="4.91"/>
<vertex x="-0.8925" y="0.5"/>
</polygon>
<polygon width="0.02" layer="1">
<vertex x="-0.01" y="0.5"/>
<vertex x="-0.01" y="5.39"/>
<vertex x="-4.99" y="5.39"/>
<vertex x="-4.99" y="2.75"/>
<vertex x="-7.01" y="2.75"/>
<vertex x="-7.01" y="5.39"/>
<vertex x="-9.69" y="5.39"/>
<vertex x="-9.69" y="2.75"/>
<vertex x="-11.71" y="2.75"/>
<vertex x="-11.71" y="5.39"/>
<vertex x="-14.39" y="5.39"/>
<vertex x="-14.39" y="0.97"/>
<vertex x="-13.91" y="0.97"/>
<vertex x="-13.91" y="4.91"/>
<vertex x="-12.19" y="4.91"/>
<vertex x="-12.19" y="2.27"/>
<vertex x="-9.21" y="2.27"/>
<vertex x="-9.21" y="4.91"/>
<vertex x="-7.49" y="4.91"/>
<vertex x="-7.49" y="2.27"/>
<vertex x="-4.51" y="2.27"/>
<vertex x="-4.51" y="4.91"/>
<vertex x="-2.795" y="4.91"/>
<vertex x="-2.795" y="0.5"/>
<vertex x="-2.315" y="0.5"/>
<vertex x="-2.315" y="4.91"/>
<vertex x="-0.8925" y="4.91"/>
<vertex x="-0.8925" y="0.5"/>
</polygon>
<rectangle x1="-14.684375" y1="0.47625" x2="0.47625" y2="5.715" layer="42" rot="R180"/>
<rectangle x1="-14.684375" y1="0.47625" x2="0.47625" y2="5.715" layer="41" rot="R180"/>
</package>
<package name="27UPIN-COOKIE-NEW">
<pad name="2" x="2.54" y="-10.16" drill="1.58" diameter="2.3"/>
<pad name="3" x="5.08" y="-10.16" drill="1.58" diameter="2.3"/>
<pad name="4" x="7.62" y="-10.16" drill="1.58" diameter="2.3"/>
<pad name="5" x="10.16" y="-10.16" drill="1.58" diameter="2.3"/>
<pad name="6" x="12.7" y="-10.16" drill="1.58" diameter="2.3"/>
<pad name="7" x="15.24" y="-10.16" drill="1.58" diameter="2.3"/>
<pad name="8" x="17.78" y="-10.16" drill="1.58" diameter="2.3"/>
<pad name="9" x="20.32" y="-10.16" drill="1.58" diameter="2.3"/>
<pad name="10" x="22.86" y="-10.16" drill="1.58" diameter="2.3"/>
<pad name="11" x="22.86" y="-7.62" drill="1.58" diameter="2.3"/>
<pad name="12" x="22.86" y="-5.08" drill="1.58" diameter="2.3"/>
<pad name="13" x="22.86" y="-2.54" drill="1.58" diameter="2.3"/>
<pad name="14" x="22.86" y="0" drill="1.58" diameter="2.3"/>
<pad name="15" x="22.86" y="2.54" drill="1.58" diameter="2.3"/>
<pad name="16" x="22.86" y="5.08" drill="1.58" diameter="2.3"/>
<pad name="17" x="22.86" y="7.62" drill="1.58" diameter="2.3"/>
<pad name="18" x="22.86" y="10.16" drill="1.58" diameter="2.3"/>
<pad name="19" x="20.32" y="10.16" drill="1.58" diameter="2.3"/>
<pad name="20" x="17.78" y="10.16" drill="1.58" diameter="2.3"/>
<pad name="21" x="15.24" y="10.16" drill="1.58" diameter="2.3"/>
<pad name="22" x="12.7" y="10.16" drill="1.58" diameter="2.3"/>
<pad name="23" x="10.16" y="10.16" drill="1.58" diameter="2.3"/>
<pad name="24" x="7.62" y="10.16" drill="1.58" diameter="2.3"/>
<pad name="25" x="5.08" y="10.16" drill="1.58" diameter="2.3"/>
<pad name="26" x="2.54" y="10.16" drill="1.58" diameter="2.3"/>
<pad name="27" x="0" y="10.16" drill="1.58" diameter="2.3"/>
<pad name="1" x="0" y="-10.16" drill="1.58" diameter="2.3"/>
<wire x1="27.5" y1="-12" x2="27.5" y2="-16" width="0.127" layer="51"/>
<wire x1="27.5" y1="-16" x2="23.5" y2="-20" width="0.127" layer="51" curve="-90"/>
<wire x1="23.5" y1="-20" x2="-0.5" y2="-20" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-20" x2="-4.5" y2="-16" width="0.127" layer="51" curve="-90"/>
<wire x1="-4.5" y1="-16" x2="-4.5" y2="-12" width="0.127" layer="51"/>
<wire x1="27.5" y1="12" x2="27.5" y2="16" width="0.127" layer="51"/>
<wire x1="27.5" y1="16" x2="23.5" y2="20" width="0.127" layer="51" curve="90"/>
<wire x1="23.5" y1="20" x2="-0.5" y2="20" width="0.127" layer="51"/>
<wire x1="-0.5" y1="20" x2="-4.5" y2="16" width="0.127" layer="51" curve="90"/>
<wire x1="-4.5" y1="16" x2="-4.5" y2="12" width="0.127" layer="51"/>
<wire x1="16.589375" y1="5.23875" x2="6.270625" y2="5.23875" width="0.127" layer="48" style="shortdash"/>
<wire x1="6.270625" y1="5.23875" x2="6.270625" y2="-5.23875" width="0.127" layer="48" style="shortdash"/>
<dimension x1="6.35" y1="5.08" x2="6.35" y2="-5.08" x3="5.08" y3="0" textsize="0.6096" layer="48" width="0.0508" visible="yes"/>
<dimension x1="16.51" y1="5.08" x2="6.35" y2="5.08" x3="11.43" y3="6.35" textsize="0.6096" layer="48" width="0.0508" visible="yes"/>
<wire x1="16.589375" y1="5.23875" x2="16.589375" y2="-5.23875" width="0.127" layer="48" style="shortdash"/>
<wire x1="16.589375" y1="-5.23875" x2="6.270625" y2="-5.23875" width="0.127" layer="48" style="shortdash"/>
<circle x="-1" y="13" radius="0.472465625" width="0.3048" layer="22"/>
<rectangle x1="25.9461" y1="3.3401" x2="27.2415" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="3.3401" x2="27.2415" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="3.3401" x2="27.2415" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="3.3401" x2="27.1653" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="26.3271" y1="-1.3843" x2="26.4033" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="26.3271" y1="-2.0701" x2="26.4033" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="26.3271" y1="-2.5273" x2="26.4033" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="26.1366" y1="3.9878" x2="26.4414" y2="4.064" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="3.5687" x2="26.4795" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="3.1115" x2="26.4795" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="26.1366" y1="2.6924" x2="26.4414" y2="2.7686" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="1.7399" x2="26.5557" y2="1.8161" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="0.6731" x2="26.4795" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="-0.5461" x2="26.4795" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="26.1747" y1="-1.3843" x2="26.4033" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="26.1747" y1="-2.0701" x2="26.4033" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="26.1747" y1="-2.5273" x2="26.4033" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="26.0604" y1="-3.4036" x2="26.5176" y2="-3.3274" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="4.0259" x2="26.3271" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="3.5687" x2="26.3271" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="3.1115" x2="26.3271" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="2.6543" x2="26.3271" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="1.7399" x2="26.5557" y2="1.8161" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="0.6731" x2="26.4795" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="-0.5461" x2="26.4795" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="-1.3843" x2="26.3271" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="-2.0701" x2="26.3271" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="26.0985" y1="-2.5273" x2="26.3271" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.908" y1="-3.4036" x2="26.5176" y2="-3.3274" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="4.0259" x2="26.2509" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="3.5687" x2="26.2509" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="3.1115" x2="26.2509" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="2.6543" x2="26.2509" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.8318" y1="1.778" x2="26.4414" y2="1.8542" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="0.6731" x2="26.4033" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-0.5461" x2="26.4033" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="-1.3843" x2="26.2509" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="-2.0701" x2="26.2509" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="26.0223" y1="-2.5273" x2="26.2509" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-3.3655" x2="26.4033" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="4.0259" x2="26.1747" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="3.5687" x2="26.1747" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="3.1115" x2="26.1747" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="2.6543" x2="26.1747" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="2.159" x2="26.1366" y2="2.2352" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="1.016" x2="26.1366" y2="1.0922" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="0.3302" x2="26.1366" y2="0.4064" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="-0.2032" x2="26.1366" y2="-0.127" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="-0.889" x2="26.1366" y2="-0.8128" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="-1.3843" x2="26.1747" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="-2.0701" x2="26.1747" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.9461" y1="-2.5273" x2="26.1747" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.9842" y1="-3.0226" x2="26.1366" y2="-2.9464" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="4.0259" x2="26.0985" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="3.5687" x2="26.0985" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="3.1115" x2="26.0985" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="2.6543" x2="26.0985" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="2.1971" x2="26.0985" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="1.0541" x2="26.0985" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="0.2921" x2="26.0985" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-0.1651" x2="26.0985" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-0.9271" x2="26.0985" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-1.3843" x2="26.0985" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.908" y1="-2.032" x2="26.0604" y2="-1.9558" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-2.5273" x2="26.0985" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.8699" y1="-2.9845" x2="26.0985" y2="-2.9083" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="4.0259" x2="26.0223" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="3.5687" x2="26.0223" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="3.1115" x2="26.0223" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="2.6543" x2="26.0223" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="2.1971" x2="26.0223" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="1.0541" x2="26.0223" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="0.2921" x2="26.0223" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="-0.1651" x2="26.0223" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="-0.9271" x2="26.0223" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-1.6129" x2="26.2509" y2="-1.5367" layer="22" rot="R270"/>
<rectangle x1="25.7937" y1="-2.5273" x2="26.0223" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.4508" y1="-3.3274" x2="26.3652" y2="-3.2512" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="4.0259" x2="25.9461" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="3.5687" x2="25.9461" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="3.1115" x2="25.9461" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="2.6543" x2="25.9461" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="2.1971" x2="25.9461" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="1.0541" x2="25.9461" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="0.2921" x2="25.9461" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="-0.1651" x2="25.9461" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="-0.9271" x2="25.9461" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="-1.6129" x2="26.1747" y2="-1.5367" layer="22" rot="R270"/>
<rectangle x1="25.7175" y1="-2.5273" x2="25.9461" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.3365" y1="-3.3655" x2="26.3271" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="4.0259" x2="25.8699" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="3.5687" x2="25.8699" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="3.1115" x2="25.8699" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="2.6543" x2="25.8699" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="2.1971" x2="25.8699" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="1.0541" x2="25.8699" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="0.2921" x2="25.8699" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="-0.1651" x2="25.8699" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="-0.9271" x2="25.8699" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="-1.6129" x2="26.0985" y2="-1.5367" layer="22" rot="R270"/>
<rectangle x1="25.6413" y1="-2.5273" x2="25.8699" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-3.3655" x2="26.2509" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="4.0259" x2="25.7937" y2="4.1021" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="3.5687" x2="25.7937" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="3.1115" x2="25.7937" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="2.6543" x2="25.7937" y2="2.7305" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="2.1971" x2="25.7937" y2="2.2733" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="1.0541" x2="25.7937" y2="1.1303" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="0.2921" x2="25.7937" y2="0.3683" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-0.1651" x2="25.7937" y2="-0.0889" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-0.9271" x2="25.7937" y2="-0.8509" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-1.3843" x2="25.7937" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.6032" y1="-2.032" x2="25.7556" y2="-1.9558" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-2.5273" x2="25.7937" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-2.9845" x2="25.7937" y2="-2.9083" layer="22" rot="R270"/>
<rectangle x1="25.5651" y1="-3.7465" x2="25.7937" y2="-3.6703" layer="22" rot="R270"/>
<rectangle x1="25.4508" y1="3.9878" x2="25.7556" y2="4.064" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="3.5687" x2="25.7175" y2="3.6449" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="3.1115" x2="25.7175" y2="3.1877" layer="22" rot="R270"/>
<rectangle x1="25.4508" y1="2.6924" x2="25.7556" y2="2.7686" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="2.159" x2="25.6794" y2="2.2352" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="1.016" x2="25.6794" y2="1.0922" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="0.3302" x2="25.6794" y2="0.4064" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="-0.2032" x2="25.6794" y2="-0.127" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="-0.889" x2="25.6794" y2="-0.8128" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="-1.3843" x2="25.7175" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="-2.0701" x2="25.7175" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.4889" y1="-2.5273" x2="25.7175" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="-3.0226" x2="25.6794" y2="-2.9464" layer="22" rot="R270"/>
<rectangle x1="25.527" y1="-3.7084" x2="25.6794" y2="-3.6322" layer="22" rot="R270"/>
<rectangle x1="25.2984" y1="3.9116" x2="25.7556" y2="3.9878" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="3.3401" x2="25.6413" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.2984" y1="2.7686" x2="25.7556" y2="2.8448" layer="22" rot="R270"/>
<rectangle x1="25.2222" y1="1.778" x2="25.8318" y2="1.8542" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="0.6731" x2="25.7937" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-0.5461" x2="25.7937" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="-1.3843" x2="25.6413" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="-2.0701" x2="25.6413" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.4127" y1="-2.5273" x2="25.6413" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-3.3655" x2="25.7937" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.2222" y1="3.9116" x2="25.6794" y2="3.9878" layer="22" rot="R270"/>
<rectangle x1="25.3365" y1="3.3401" x2="25.5651" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.2222" y1="2.7686" x2="25.6794" y2="2.8448" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="1.7399" x2="25.7937" y2="1.8161" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="0.6731" x2="25.7175" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-0.5461" x2="25.7175" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="25.3365" y1="-1.3843" x2="25.5651" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.3365" y1="-2.0701" x2="25.5651" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-3.3655" x2="25.7175" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="3.8735" x2="25.6413" y2="3.9497" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="3.3401" x2="25.5651" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="2.8067" x2="25.6413" y2="2.8829" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="1.7399" x2="25.6413" y2="1.8161" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="0.6731" x2="25.5651" y2="0.7493" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-0.5461" x2="25.5651" y2="-0.4699" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-1.3843" x2="25.4889" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-2.0701" x2="25.4889" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-3.3655" x2="25.5651" y2="-3.2893" layer="22" rot="R270"/>
<rectangle x1="24.4983" y1="3.3401" x2="26.0985" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-1.3843" x2="25.4127" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.2603" y1="-2.0701" x2="25.3365" y2="-1.9939" layer="22" rot="R270"/>
<rectangle x1="25.1841" y1="-2.5273" x2="25.4127" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="24.4221" y1="3.3401" x2="26.0223" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="-1.3843" x2="25.3365" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.1079" y1="-2.5273" x2="25.3365" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="24.4221" y1="3.3401" x2="25.8699" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="25.0317" y1="-1.3843" x2="25.2603" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.0317" y1="-2.5273" x2="25.2603" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="24.4221" y1="3.3401" x2="25.7175" y2="3.4163" layer="22" rot="R270"/>
<rectangle x1="24.9555" y1="-1.3843" x2="25.1841" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="25.0317" y1="-2.5273" x2="25.1079" y2="-2.4511" layer="22" rot="R270"/>
<rectangle x1="24.8793" y1="-1.3843" x2="25.1079" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="24.8031" y1="-1.3843" x2="25.0317" y2="-1.3081" layer="22" rot="R270"/>
<rectangle x1="24.8031" y1="-1.3843" x2="24.8793" y2="-1.3081" layer="22" rot="R270"/>
<hole x="11.5" y="-14.96" drill="1.9"/>
<hole x="11.5" y="-12.71" drill="0.9"/>
<hole x="11" y="-12.71" drill="0.9"/>
<hole x="12" y="-12.71" drill="0.9"/>
<hole x="11.25" y="-12.71" drill="0.9"/>
<hole x="11.75" y="-12.71" drill="0.9"/>
<hole x="11.125" y="-12.71" drill="0.9"/>
<hole x="11.375" y="-12.71" drill="0.9"/>
<hole x="11.625" y="-12.71" drill="0.9"/>
<hole x="11.875" y="-12.71" drill="0.9"/>
<hole x="11.5" y="14.96" drill="1.9"/>
<hole x="11.5" y="12.744159375" drill="0.9"/>
<hole x="12" y="12.744159375" drill="0.9"/>
<hole x="11" y="12.744159375" drill="0.9"/>
<hole x="11.75" y="12.744159375" drill="0.9"/>
<hole x="11.25" y="12.744159375" drill="0.9"/>
<hole x="11.875" y="12.744159375" drill="0.9"/>
<hole x="11.625" y="12.744159375" drill="0.9"/>
<hole x="11.375" y="12.744159375" drill="0.9"/>
<hole x="11.125" y="12.744159375" drill="0.9"/>
<wire x1="-4.4" y1="-12.06" x2="-2.45" y2="-12.06" width="0.1" layer="20"/>
<wire x1="-2.45" y1="-12.06" x2="-2.1" y2="-12.41" width="0.1" layer="20" curve="-90"/>
<wire x1="-2.1" y1="-12.41" x2="-2.45" y2="-12.76" width="0.1" layer="20" curve="-90"/>
<wire x1="-2.45" y1="-12.76" x2="-2.45" y2="-13.58" width="0.1" layer="20"/>
<wire x1="-2.45" y1="-13.58" x2="-2.11" y2="-13.98" width="0.1" layer="20" curve="90"/>
<wire x1="-2.11" y1="-13.98" x2="-0.82" y2="-14" width="0.1" layer="20"/>
<wire x1="5" y1="-15.96" x2="10" y2="-15.96" width="0.1" layer="20" curve="-180"/>
<wire x1="10" y1="-15.96" x2="10" y2="-18.46" width="0.1" layer="20"/>
<wire x1="5" y1="-15.96" x2="5" y2="-16.96" width="0.1" layer="20" curve="0.060312"/>
<wire x1="4.6" y1="-13.26" x2="4.6" y2="-13.96" width="0.1" layer="20"/>
<wire x1="4.6" y1="-13.96" x2="4.2" y2="-14.36" width="0.1" layer="20" curve="-90"/>
<wire x1="4.2" y1="-14.36" x2="4" y2="-14.36" width="0.1" layer="20"/>
<wire x1="4.6" y1="-13.26" x2="4.2" y2="-12.86" width="0.1" layer="20" curve="90"/>
<wire x1="4.2" y1="-12.86" x2="2.8" y2="-12.86" width="0.1" layer="20"/>
<wire x1="4" y1="-14.36" x2="3.7" y2="-14.76" width="0.1" layer="20" curve="90"/>
<wire x1="4" y1="-15.96" x2="4" y2="-16.96" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="4" y1="-16.96" x2="5" y2="-16.96" width="0.1" layer="20"/>
<wire x1="2.52" y1="-12.96" x2="1.444375" y2="-12.96" width="0.1" layer="20"/>
<wire x1="2.78" y1="-12.86" x2="2.76" y2="-12.86" width="0.1" layer="20"/>
<wire x1="2.76" y1="-12.86" x2="2.66" y2="-12.9" width="0.1" layer="20" curve="43.602819"/>
<wire x1="2.66" y1="-12.9" x2="2.52" y2="-12.96" width="0.1" layer="20" curve="-50.335242"/>
<wire x1="3.7" y1="-14.76" x2="3.76" y2="-14.91" width="0.1" layer="20"/>
<wire x1="1.5" y1="-12.96" x2="-0.803125" y2="-14.009375" width="0.1016" layer="20" curve="49.737166"/>
<wire x1="4" y1="-15.96" x2="3.76" y2="-14.9" width="0.1016" layer="20" curve="25.127824"/>
<wire x1="27.4" y1="12.06" x2="25.45" y2="12.06" width="0.1" layer="20"/>
<wire x1="25.45" y1="12.06" x2="25.1" y2="12.41" width="0.1" layer="20" curve="-90"/>
<wire x1="25.1" y1="12.41" x2="25.45" y2="12.76" width="0.1" layer="20" curve="-90"/>
<wire x1="25.45" y1="12.76" x2="25.45" y2="13.58" width="0.1" layer="20"/>
<wire x1="25.45" y1="13.58" x2="25.11" y2="13.98" width="0.1" layer="20" curve="90"/>
<wire x1="25.11" y1="13.98" x2="23.82" y2="14" width="0.1" layer="20"/>
<wire x1="18" y1="15.96" x2="13" y2="15.96" width="0.1" layer="20" curve="-180"/>
<wire x1="13" y1="15.96" x2="13" y2="18.46" width="0.1" layer="20"/>
<wire x1="18" y1="15.96" x2="18" y2="16.96" width="0.1" layer="20" curve="0.060312"/>
<wire x1="18.4" y1="13.26" x2="18.4" y2="13.96" width="0.1" layer="20"/>
<wire x1="18.4" y1="13.96" x2="18.8" y2="14.36" width="0.1" layer="20" curve="-90"/>
<wire x1="18.8" y1="14.36" x2="19" y2="14.36" width="0.1" layer="20"/>
<wire x1="18.4" y1="13.26" x2="18.8" y2="12.86" width="0.1" layer="20" curve="90"/>
<wire x1="18.8" y1="12.86" x2="20.2" y2="12.86" width="0.1" layer="20"/>
<wire x1="19" y1="14.36" x2="19.3" y2="14.76" width="0.1" layer="20" curve="90"/>
<wire x1="19" y1="15.96" x2="19" y2="16.96" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="19" y1="16.96" x2="18" y2="16.96" width="0.1" layer="20"/>
<wire x1="20.48" y1="12.96" x2="21.555625" y2="12.96" width="0.1" layer="20"/>
<wire x1="20.22" y1="12.86" x2="20.24" y2="12.86" width="0.1" layer="20"/>
<wire x1="20.24" y1="12.86" x2="20.34" y2="12.9" width="0.1" layer="20" curve="43.602819"/>
<wire x1="20.34" y1="12.9" x2="20.48" y2="12.96" width="0.1" layer="20" curve="-50.335242"/>
<wire x1="19.3" y1="14.76" x2="19.24" y2="14.91" width="0.1" layer="20"/>
<wire x1="21.5" y1="12.96" x2="23.803125" y2="14.009375" width="0.1016" layer="20" curve="49.737166"/>
<wire x1="19" y1="15.96" x2="19.24" y2="14.9" width="0.1016" layer="20" curve="25.127824"/>
<wire x1="27.4" y1="-12.06" x2="25.45" y2="-12.06" width="0.1" layer="20"/>
<wire x1="25.45" y1="-12.06" x2="25.1" y2="-12.41" width="0.1" layer="20" curve="90"/>
<wire x1="25.1" y1="-12.41" x2="25.45" y2="-12.76" width="0.1" layer="20" curve="90"/>
<wire x1="25.45" y1="-12.76" x2="25.45" y2="-13.58" width="0.1" layer="20"/>
<wire x1="25.45" y1="-13.58" x2="25.11" y2="-13.98" width="0.1" layer="20" curve="-90"/>
<wire x1="25.11" y1="-13.98" x2="23.82" y2="-14" width="0.1" layer="20"/>
<wire x1="18" y1="-15.96" x2="13" y2="-15.96" width="0.1" layer="20" curve="180"/>
<wire x1="13" y1="-15.96" x2="13" y2="-18.46" width="0.1" layer="20"/>
<wire x1="18" y1="-15.96" x2="18" y2="-16.96" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="18.4" y1="-13.26" x2="18.4" y2="-13.96" width="0.1" layer="20"/>
<wire x1="18.4" y1="-13.96" x2="18.8" y2="-14.36" width="0.1" layer="20" curve="90"/>
<wire x1="18.8" y1="-14.36" x2="19" y2="-14.36" width="0.1" layer="20"/>
<wire x1="18.4" y1="-13.26" x2="18.8" y2="-12.86" width="0.1" layer="20" curve="-90"/>
<wire x1="18.8" y1="-12.86" x2="20.2" y2="-12.86" width="0.1" layer="20"/>
<wire x1="19" y1="-14.36" x2="19.3" y2="-14.76" width="0.1" layer="20" curve="-90"/>
<wire x1="19" y1="-15.96" x2="19" y2="-16.96" width="0.1" layer="20" curve="0.060312"/>
<wire x1="19" y1="-16.96" x2="18" y2="-16.96" width="0.1" layer="20"/>
<wire x1="20.48" y1="-12.96" x2="21.555625" y2="-12.96" width="0.1" layer="20"/>
<wire x1="20.22" y1="-12.86" x2="20.24" y2="-12.86" width="0.1" layer="20"/>
<wire x1="20.24" y1="-12.86" x2="20.34" y2="-12.9" width="0.1" layer="20" curve="-43.602819"/>
<wire x1="20.34" y1="-12.9" x2="20.48" y2="-12.96" width="0.1" layer="20" curve="50.335242"/>
<wire x1="19.3" y1="-14.76" x2="19.24" y2="-14.91" width="0.1" layer="20"/>
<wire x1="10" y1="-18.46" x2="13" y2="-18.46" width="0.1" layer="20"/>
<wire x1="27.4" y1="-12.06" x2="27.4" y2="12.06" width="0.1" layer="20"/>
<wire x1="21.5" y1="-12.96" x2="23.803125" y2="-14.009375" width="0.1016" layer="20" curve="-49.737166"/>
<wire x1="19" y1="-15.96" x2="19.24" y2="-14.9" width="0.1016" layer="20" curve="-25.127824"/>
<wire x1="-4.4" y1="12.06" x2="-2.45" y2="12.06" width="0.1" layer="20"/>
<wire x1="-2.45" y1="12.06" x2="-2.1" y2="12.41" width="0.1" layer="20" curve="90"/>
<wire x1="-2.1" y1="12.41" x2="-2.45" y2="12.76" width="0.1" layer="20" curve="90"/>
<wire x1="-2.45" y1="12.76" x2="-2.45" y2="13.58" width="0.1" layer="20"/>
<wire x1="-2.45" y1="13.58" x2="-2.11" y2="13.98" width="0.1" layer="20" curve="-90"/>
<wire x1="-2.11" y1="13.98" x2="-0.82" y2="14" width="0.1" layer="20"/>
<wire x1="5" y1="15.96" x2="10" y2="15.96" width="0.1" layer="20" curve="180"/>
<wire x1="10" y1="15.96" x2="10" y2="18.46" width="0.1" layer="20"/>
<wire x1="5" y1="15.96" x2="5" y2="16.96" width="0.1" layer="20" curve="-0.060312"/>
<wire x1="4.6" y1="13.26" x2="4.6" y2="13.96" width="0.1" layer="20"/>
<wire x1="4.6" y1="13.96" x2="4.2" y2="14.36" width="0.1" layer="20" curve="90"/>
<wire x1="4.2" y1="14.36" x2="4" y2="14.36" width="0.1" layer="20"/>
<wire x1="4.6" y1="13.26" x2="4.2" y2="12.86" width="0.1" layer="20" curve="-90"/>
<wire x1="4.2" y1="12.86" x2="2.8" y2="12.86" width="0.1" layer="20"/>
<wire x1="4" y1="14.36" x2="3.7" y2="14.76" width="0.1" layer="20" curve="-90"/>
<wire x1="4" y1="15.96" x2="4" y2="16.96" width="0.1" layer="20" curve="0.060312"/>
<wire x1="4" y1="16.96" x2="5" y2="16.96" width="0.1" layer="20"/>
<wire x1="2.52" y1="12.96" x2="1.444375" y2="12.96" width="0.1" layer="20"/>
<wire x1="2.78" y1="12.86" x2="2.76" y2="12.86" width="0.1" layer="20"/>
<wire x1="2.76" y1="12.86" x2="2.66" y2="12.9" width="0.1" layer="20" curve="-43.602819"/>
<wire x1="2.66" y1="12.9" x2="2.52" y2="12.96" width="0.1" layer="20" curve="50.335242"/>
<wire x1="3.7" y1="14.76" x2="3.76" y2="14.91" width="0.1" layer="20"/>
<wire x1="13" y1="18.46" x2="10" y2="18.46" width="0.1" layer="20"/>
<wire x1="-4.4" y1="12.06" x2="-4.4" y2="-12.06" width="0.1" layer="20"/>
<wire x1="1.5" y1="12.96" x2="-0.803125" y2="14.009375" width="0.1016" layer="20" curve="-49.737166"/>
<wire x1="4" y1="15.96" x2="3.76" y2="14.9" width="0.1016" layer="20" curve="-25.127824"/>
</package>
<package name="0603-LED-DUAL">
<smd name="1" x="-0.75" y="0.425" dx="0.65" dy="0.8" layer="1" roundness="36" rot="R90"/>
<smd name="2" x="0.75" y="0.425" dx="0.65" dy="0.8" layer="1" roundness="36" rot="R90"/>
<smd name="3" x="-0.75" y="-0.425" dx="0.65" dy="0.8" layer="1" roundness="36" rot="R90"/>
<smd name="4" x="0.75" y="-0.425" dx="0.65" dy="0.8" layer="1" roundness="36" rot="R90"/>
<wire x1="-1.1556" y1="-0.962325" x2="-0.34935" y2="-0.962325" width="0.127" layer="21"/>
<wire x1="-1.1556" y1="0.963125" x2="-0.34935" y2="0.963125" width="0.127" layer="21"/>
<wire x1="-0.25" y1="0.3" x2="-0.25" y2="-0.3" width="0.127" layer="21"/>
<wire x1="0" y1="0.2" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0" y2="-0.2" width="0.127" layer="21"/>
<text x="0" y="1.42875" size="0.6096" layer="25" font="vector" rot="R180" align="center">&gt;NAME</text>
</package>
<package name="SOP8-S">
<wire x1="-1.8034" y1="2.3622" x2="-1.8034" y2="-2.3622" width="0.127" layer="21"/>
<wire x1="-1.8034" y1="-2.3622" x2="1.8034" y2="-2.3622" width="0.127" layer="21"/>
<wire x1="1.8034" y1="-2.3622" x2="1.8034" y2="2.3622" width="0.127" layer="21"/>
<wire x1="1.8034" y1="2.3622" x2="-1.8034" y2="2.3622" width="0.127" layer="21" style="shortdash"/>
<circle x="-2.5146" y="3.0734" radius="0.3556" width="0.127" layer="21"/>
<smd name="1" x="-2.505075" y="1.905" dx="1.143" dy="0.6096" layer="1"/>
<smd name="8" x="2.505075" y="1.905" dx="1.143" dy="0.6096" layer="1"/>
<smd name="2" x="-2.505075" y="0.635" dx="1.143" dy="0.6096" layer="1"/>
<smd name="3" x="-2.505075" y="-0.635" dx="1.143" dy="0.6096" layer="1"/>
<smd name="7" x="2.505075" y="0.635" dx="1.143" dy="0.6096" layer="1"/>
<smd name="6" x="2.505075" y="-0.635" dx="1.143" dy="0.6096" layer="1"/>
<smd name="4" x="-2.505075" y="-1.905" dx="1.143" dy="0.6096" layer="1"/>
<smd name="5" x="2.505075" y="-1.905" dx="1.143" dy="0.6096" layer="1"/>
<text x="0" y="-3.175" size="0.6096" layer="27" font="vector" ratio="10" align="center">&gt;VALUE</text>
<text x="0" y="3.175" size="0.6096" layer="25" font="vector" ratio="10" align="center">&gt;NAME</text>
<rectangle x1="-2.8702" y1="1.7272" x2="-1.8542" y2="2.0828" layer="51"/>
<rectangle x1="-2.8702" y1="0.4572" x2="-1.8542" y2="0.8128" layer="51"/>
<rectangle x1="-2.8702" y1="-0.8128" x2="-1.8542" y2="-0.4572" layer="51"/>
<rectangle x1="-2.8702" y1="-2.0828" x2="-1.8542" y2="-1.7272" layer="51"/>
<rectangle x1="1.8542" y1="1.7272" x2="2.8702" y2="2.0828" layer="51"/>
<rectangle x1="1.8542" y1="0.4572" x2="2.8702" y2="0.8128" layer="51"/>
<rectangle x1="1.8542" y1="-0.8128" x2="2.8702" y2="-0.4572" layer="51"/>
<rectangle x1="1.8542" y1="-2.0828" x2="2.8702" y2="-1.7272" layer="51"/>
<smd name="G" x="0" y="0" dx="2.286" dy="3.048" layer="1" rot="R180" thermals="no"/>
</package>
<package name="XH-1.27-2P-W">
<wire x1="-2.025" y1="-1.375" x2="-1.5" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-1.375" x2="1.5" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-1.375" x2="2.025" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="2.025" y1="-1.375" x2="2.025" y2="2.625" width="0.2032" layer="21"/>
<wire x1="2.025" y1="2.625" x2="1.875" y2="2.625" width="0.2032" layer="21"/>
<wire x1="1.875" y1="2.625" x2="-1.875" y2="2.625" width="0.2032" layer="21"/>
<wire x1="-1.875" y1="2.625" x2="-2.025" y2="2.625" width="0.2032" layer="21"/>
<wire x1="-2.025" y1="2.625" x2="-2.025" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.625" x2="-1.5" y2="1.625" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="1.625" x2="-1.25" y2="1.625" width="0.0508" layer="21"/>
<wire x1="-1.25" y1="1.625" x2="-1.25" y2="1" width="0.0508" layer="21"/>
<wire x1="-1.25" y1="1" x2="1.25" y2="1" width="0.0508" layer="21"/>
<wire x1="1.25" y1="1.625" x2="1.5" y2="1.625" width="0.0508" layer="21"/>
<wire x1="1.5" y1="1.625" x2="2" y2="1.625" width="0.0508" layer="21"/>
<wire x1="1.25" y1="1.625" x2="1.25" y2="1" width="0.0508" layer="21"/>
<wire x1="-2" y1="-0.75" x2="-1.5" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="2" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="1.625" x2="-1.5" y2="2.25" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="2.25" x2="1.5" y2="2.25" width="0.0508" layer="21"/>
<wire x1="1.5" y1="2.25" x2="1.5" y2="1.625" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="2.25" x2="-1.875" y2="2.625" width="0.0508" layer="21"/>
<wire x1="1.5" y1="2.25" x2="1.875" y2="2.625" width="0.0508" layer="21"/>
<wire x1="-0.875" y1="-1.25" x2="-0.875" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-0.875" y1="-0.75" x2="-0.375" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-0.375" y1="-0.75" x2="-0.375" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="0.375" y1="-1.25" x2="0.375" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="0.375" y1="-0.75" x2="0.875" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="0.875" y1="-0.75" x2="0.875" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="1.77375" x2="-3.71" y2="1.77375" width="0.2032" layer="51"/>
<wire x1="-3.71" y1="1.77375" x2="-3.71" y2="-0.6890375" width="0.2032" layer="51"/>
<wire x1="-3.71" y1="-0.6890375" x2="-3.4109125" y2="-0.988125" width="0.2032" layer="51"/>
<wire x1="-3.4109125" y1="-0.988125" x2="-2.125" y2="-0.988125" width="0.2032" layer="51"/>
<wire x1="2.125" y1="-0.988125" x2="3.4109125" y2="-0.988125" width="0.2032" layer="51"/>
<wire x1="3.4109125" y1="-0.988125" x2="3.71" y2="-0.6890375" width="0.2032" layer="51"/>
<wire x1="3.71" y1="-0.6890375" x2="3.71" y2="1.77375" width="0.2032" layer="51"/>
<wire x1="3.71" y1="1.77375" x2="2.125" y2="1.77375" width="0.2032" layer="51"/>
<rectangle x1="-0.875" y1="1" x2="-0.375" y2="1.875" layer="21"/>
<rectangle x1="0.375" y1="1" x2="0.875" y2="1.875" layer="21"/>
<smd name="1" x="0.625" y="-2.275" dx="0.8" dy="1.6" layer="1" roundness="40"/>
<smd name="2" x="-0.625" y="-2.275" dx="0.8" dy="1.6" layer="1" roundness="40"/>
<smd name="S1" x="2.9" y="0.78375" dx="1.6" dy="2.1" layer="1" roundness="40"/>
<smd name="S2" x="-2.9" y="0.78375" dx="1.6" dy="2.1" layer="1" roundness="40"/>
<text x="0" y="0.3175" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="XH-1.27-2P-L">
<smd name="2" x="-0.624" y="-0.0485" dx="2" dy="0.87" layer="1" rot="R90"/>
<smd name="1" x="0.626" y="-0.0485" dx="2" dy="0.87" layer="1" rot="R90"/>
<smd name="P$5" x="-3.048" y="2.203" dx="3" dy="1.592" layer="1" rot="R90"/>
<smd name="P$6" x="3.048" y="2.203" dx="3" dy="1.592" layer="1" rot="R90"/>
<wire x1="2.05" y1="3.81" x2="2.05" y2="0.145" width="0.127" layer="21"/>
<wire x1="2.05" y1="0.145" x2="1.5625" y2="0.145" width="0.127" layer="21"/>
<wire x1="-1.5625" y1="0.145" x2="-2.05" y2="0.145" width="0.127" layer="21"/>
<wire x1="-2.05" y1="0.145" x2="-2.05" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.05" y1="3.81" x2="2.05" y2="3.81" width="0.127" layer="21"/>
<wire x1="1.5625" y1="0.145" x2="1.2625" y2="0.445" width="0.127" layer="21"/>
<wire x1="-1.2625" y1="0.445" x2="-1.5625" y2="0.145" width="0.127" layer="21"/>
<wire x1="1.2625" y1="0.9975" x2="1.2625" y2="0.445" width="0.127" layer="21"/>
<wire x1="-1.2625" y1="0.9975" x2="-1.2625" y2="0.445" width="0.127" layer="21"/>
<wire x1="1.25" y1="0.9975" x2="-1.25" y2="0.9975" width="0.127" layer="21"/>
<wire x1="0.625" y1="2.2775" x2="0.625" y2="2.69" width="0.26" layer="21"/>
<wire x1="-0.625" y1="2.2775" x2="-0.625" y2="2.69" width="0.26" layer="21"/>
<text x="0" y="4.365625" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="XH-1.00-2P-W">
<wire x1="-2.025" y1="-2.175" x2="-1.5" y2="-2.175" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-2.175" x2="1.5" y2="-2.175" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-2.175" x2="2.025" y2="-2.175" width="0.2032" layer="21"/>
<wire x1="2.025" y1="-2.175" x2="2.025" y2="2.075" width="0.2032" layer="21"/>
<wire x1="2.025" y1="2.075" x2="1.875" y2="2.075" width="0.2032" layer="21"/>
<wire x1="1.875" y1="2.075" x2="-1.875" y2="2.075" width="0.2032" layer="21"/>
<wire x1="-1.875" y1="2.075" x2="-2.025" y2="2.075" width="0.2032" layer="21"/>
<wire x1="-2.025" y1="2.075" x2="-2.025" y2="-2.175" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.075" x2="-1.5" y2="1.075" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="1.075" x2="-1.25" y2="1.075" width="0.0508" layer="21"/>
<wire x1="-1.25" y1="1.075" x2="-1.25" y2="0.45" width="0.0508" layer="21"/>
<wire x1="-1.25" y1="0.45" x2="1.25" y2="0.45" width="0.0508" layer="21"/>
<wire x1="1.25" y1="1.075" x2="1.5" y2="1.075" width="0.0508" layer="21"/>
<wire x1="1.5" y1="1.075" x2="2" y2="1.075" width="0.0508" layer="21"/>
<wire x1="1.25" y1="1.075" x2="1.25" y2="0.45" width="0.0508" layer="21"/>
<wire x1="-2" y1="-1.55" x2="-1.5" y2="-1.55" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="-1.55" x2="-1.5" y2="-2.175" width="0.0508" layer="21"/>
<wire x1="1.5" y1="-1.55" x2="2" y2="-1.55" width="0.0508" layer="21"/>
<wire x1="1.5" y1="-1.55" x2="1.5" y2="-2.175" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="1.075" x2="-1.5" y2="1.7" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="1.7" x2="1.5" y2="1.7" width="0.0508" layer="21"/>
<wire x1="1.5" y1="1.7" x2="1.5" y2="1.075" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="1.7" x2="-1.875" y2="2.075" width="0.0508" layer="21"/>
<wire x1="1.5" y1="1.7" x2="1.875" y2="2.075" width="0.0508" layer="21"/>
<wire x1="-0.875" y1="-2.05" x2="-0.875" y2="-1.55" width="0.0508" layer="21"/>
<wire x1="-0.875" y1="-1.55" x2="-0.375" y2="-1.55" width="0.0508" layer="21"/>
<wire x1="-0.375" y1="-1.55" x2="-0.375" y2="-2.05" width="0.0508" layer="21"/>
<wire x1="0.375" y1="-2.05" x2="0.375" y2="-1.55" width="0.0508" layer="21"/>
<wire x1="0.375" y1="-1.55" x2="0.875" y2="-1.55" width="0.0508" layer="21"/>
<wire x1="0.875" y1="-1.55" x2="0.875" y2="-2.05" width="0.0508" layer="21"/>
<rectangle x1="-0.875" y1="0.45" x2="-0.375" y2="1.325" layer="21"/>
<rectangle x1="0.375" y1="0.45" x2="0.875" y2="1.325" layer="21"/>
<smd name="1" x="0.5" y="-2.375" dx="0.5" dy="1.55" layer="1" roundness="40"/>
<smd name="2" x="-0.5" y="-2.375" dx="0.5" dy="1.55" layer="1" roundness="40"/>
<smd name="S1" x="1.8" y="0.98375" dx="1.2" dy="2" layer="1" roundness="40"/>
<smd name="S2" x="-1.8" y="0.98375" dx="1.2" dy="2" layer="1" roundness="40"/>
<text x="0" y="2.7425" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="XH-2.54-2P-L">
<smd name="1" x="1.27" y="0" dx="2.5" dy="1.3" layer="1" rot="R270"/>
<smd name="2" x="-1.27" y="0" dx="2.5" dy="1.3" layer="1" rot="R270"/>
<smd name="GND" x="4.41" y="5.3" dx="3.9" dy="1.6" layer="1" rot="R270"/>
<smd name="GND1" x="-4.41" y="5.3" dx="3.9" dy="1.6" layer="1" rot="R270"/>
<text x="0" y="7.62" size="0.6096" layer="25" font="vector" rot="R180" align="center">&gt;NAME</text>
<wire x1="-4.96" y1="1.25" x2="4.96" y2="1.25" width="0.127" layer="21"/>
<wire x1="4.96" y1="1.25" x2="4.96" y2="6.95" width="0.127" layer="21"/>
<wire x1="4.96" y1="6.95" x2="-4.96" y2="6.95" width="0.127" layer="21"/>
<wire x1="-4.96" y1="6.95" x2="-4.96" y2="1.25" width="0.127" layer="21"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="3.81" width="1.27" layer="51"/>
<wire x1="1.27" y1="5.08" x2="1.27" y2="3.81" width="1.27" layer="51"/>
</package>
<package name="XH-1.00-2P-L">
<wire x1="-2.125" y1="-0.875" x2="-1.5" y2="-0.875" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.875" x2="1.5" y2="-0.875" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.875" x2="2.125" y2="-0.875" width="0.2032" layer="21"/>
<wire x1="2.125" y1="-0.875" x2="2.125" y2="2.075" width="0.2032" layer="21"/>
<wire x1="2.125" y1="2.075" x2="1.875" y2="2.075" width="0.2032" layer="21"/>
<wire x1="1.875" y1="2.075" x2="-1.875" y2="2.075" width="0.2032" layer="21"/>
<wire x1="-1.875" y1="2.075" x2="-2.125" y2="2.075" width="0.2032" layer="21"/>
<wire x1="-2.125" y1="2.075" x2="-2.125" y2="-0.875" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.075" x2="-1.5" y2="1.075" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="1.075" x2="-1.25" y2="1.075" width="0.0508" layer="21"/>
<wire x1="-1.25" y1="1.075" x2="-1.25" y2="0.45" width="0.0508" layer="21"/>
<wire x1="-1.25" y1="0.45" x2="1.25" y2="0.45" width="0.0508" layer="21"/>
<wire x1="1.25" y1="1.075" x2="1.5" y2="1.075" width="0.0508" layer="21"/>
<wire x1="1.5" y1="1.075" x2="2" y2="1.075" width="0.0508" layer="21"/>
<wire x1="1.25" y1="1.075" x2="1.25" y2="0.45" width="0.0508" layer="21"/>
<wire x1="-2.1" y1="-0.25" x2="-1.5" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="-0.25" x2="-1.5" y2="-0.875" width="0.0508" layer="21"/>
<wire x1="1.5" y1="-0.25" x2="2.1" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="1.5" y1="-0.25" x2="1.5" y2="-0.875" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="1.075" x2="-1.5" y2="1.7" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="1.7" x2="1.5" y2="1.7" width="0.0508" layer="21"/>
<wire x1="1.5" y1="1.7" x2="1.5" y2="1.075" width="0.0508" layer="21"/>
<wire x1="-1.5" y1="1.7" x2="-1.875" y2="2.075" width="0.0508" layer="21"/>
<wire x1="1.5" y1="1.7" x2="1.875" y2="2.075" width="0.0508" layer="21"/>
<smd name="S1" x="1.8" y="1.28375" dx="1.2" dy="1.5" layer="1" roundness="40"/>
<smd name="S2" x="-1.8" y="1.28375" dx="1.2" dy="1.5" layer="1" roundness="40"/>
<text x="0" y="2.7425" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<wire x1="-0.825" y1="-0.75" x2="-0.825" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="-0.825" y1="-0.25" x2="-0.225" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="-0.225" y1="-0.25" x2="-0.225" y2="-0.75" width="0.0508" layer="21"/>
<rectangle x1="-0.725" y1="0.45" x2="-0.225" y2="1.325" layer="21"/>
<smd name="2" x="-0.5" y="-1.375" dx="0.5" dy="1.2" layer="1" roundness="40"/>
<wire x1="0.175" y1="-0.75" x2="0.175" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="0.175" y1="-0.25" x2="0.775" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="0.775" y1="-0.25" x2="0.775" y2="-0.75" width="0.0508" layer="21"/>
<rectangle x1="0.225" y1="0.45" x2="0.725" y2="1.325" layer="21"/>
<smd name="1" x="0.5" y="-1.375" dx="0.5" dy="1.2" layer="1" roundness="40"/>
</package>
<package name="PAD-1.9MM">
<smd name="P" x="0" y="0" dx="1.905" dy="1.905" layer="1" roundness="100" cream="no"/>
</package>
<package name="PINB">
<pad name="A" x="0" y="0" drill="1.6" shape="octagon"/>
</package>
<package name="CAP-3528">
<wire x1="-1.2" y1="-1.523" x2="-2.473" y2="-1.523" width="0.2032" layer="21"/>
<wire x1="-2.473" y1="-1.523" x2="-2.473" y2="1.523" width="0.2032" layer="21"/>
<wire x1="-2.473" y1="1.523" x2="-1.2" y2="1.523" width="0.2032" layer="21"/>
<wire x1="1.2" y1="-1.523" x2="2.269" y2="-1.523" width="0.2032" layer="21"/>
<wire x1="2.269" y1="-1.523" x2="2.523" y2="-1.269" width="0.2032" layer="21"/>
<wire x1="2.523" y1="-1.269" x2="2.523" y2="1.269" width="0.2032" layer="21"/>
<wire x1="2.523" y1="1.269" x2="2.269" y2="1.523" width="0.2032" layer="21"/>
<wire x1="2.269" y1="1.523" x2="1.2" y2="1.523" width="0.2032" layer="21"/>
<wire x1="0.65" y1="1.1" x2="0.65" y2="-1.1" width="0.2032" layer="21"/>
<smd name="C" x="-1.65" y="0" dx="2.5" dy="1.2" layer="1" roundness="50" rot="R90"/>
<smd name="A" x="1.65" y="0" dx="2.5" dy="1.2" layer="1" roundness="50" rot="R90"/>
<text x="0" y="-2.2225" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="CAP-6032">
<smd name="C" x="-2.7" y="0" dx="3" dy="2.4" layer="1" rot="R270"/>
<smd name="A" x="2.7" y="0" dx="3" dy="2.4" layer="1" rot="R270"/>
<wire x1="-1.8" y1="2.1" x2="-4.5" y2="2.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.1" x2="-4.5" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.1" x2="-1.8" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="1.8" y1="-2.1" x2="3.9" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="3.9" y1="-2.1" x2="4.5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.5" x2="4.5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.5" x2="3.9" y2="2.1" width="0.2032" layer="21"/>
<wire x1="3.9" y1="2.1" x2="1.8" y2="2.1" width="0.2032" layer="21"/>
<wire x1="0.9" y1="1.5" x2="0.9" y2="-1.5" width="0.2032" layer="21"/>
<text x="-0.055" y="2.7" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="CAP-SANYO-E12">
<circle x="0" y="0" radius="3.9" width="0.2032" layer="21"/>
<pad name="-" x="-1.75" y="0" drill="0.8" diameter="1.27"/>
<pad name="+" x="1.75" y="0" drill="0.8" diameter="1.27"/>
<text x="0" y="1.905" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
<polygon width="0.2032" layer="21">
<vertex x="-2.65" y="2.7"/>
<vertex x="-2.65" y="-2.75" curve="-41.038922"/>
<vertex x="-3.8" y="0" curve="-41.675057"/>
</polygon>
<pad name="-1" x="-2.5" y="0" drill="0.8" diameter="1.27"/>
<pad name="+1" x="2.5" y="0" drill="0.8" diameter="1.27"/>
<pad name="-2" x="-1.85" y="0" drill="0.8" diameter="1.27"/>
<pad name="-3" x="-1.95" y="0" drill="0.8" diameter="1.27"/>
<pad name="-4" x="-2.05" y="0" drill="0.8" diameter="1.27"/>
<pad name="-5" x="-2.15" y="0" drill="0.8" diameter="1.27"/>
<pad name="-6" x="-2.25" y="0" drill="0.8" diameter="1.27"/>
<pad name="-7" x="-2.35" y="0" drill="0.8" diameter="1.27"/>
<pad name="-8" x="-2.45" y="0" drill="0.8" diameter="1.27"/>
<pad name="+2" x="1.85" y="0" drill="0.8" diameter="1.27"/>
<pad name="+3" x="1.95" y="0" drill="0.8" diameter="1.27"/>
<pad name="+4" x="2.05" y="0" drill="0.8" diameter="1.27"/>
<pad name="+5" x="2.15" y="0" drill="0.8" diameter="1.27"/>
<pad name="+6" x="2.25" y="0" drill="0.8" diameter="1.27"/>
<pad name="+7" x="2.35" y="0" drill="0.8" diameter="1.27"/>
<pad name="+8" x="2.45" y="0" drill="0.8" diameter="1.27"/>
<wire x1="1.74625" y1="1.905" x2="2.69875" y2="1.905" width="0.127" layer="21"/>
<wire x1="2.2225" y1="2.38125" x2="2.2225" y2="1.42875" width="0.127" layer="21"/>
</package>
<package name="CR75">
<wire x1="-4.01625" y1="3.65" x2="3.98375" y2="3.65" width="0.127" layer="21"/>
<wire x1="3.98375" y1="3.65" x2="3.98375" y2="2.55" width="0.127" layer="21"/>
<wire x1="-4.01625" y1="3.65" x2="-4.01625" y2="2.55" width="0.127" layer="21"/>
<wire x1="-4.01625" y1="-3.65" x2="3.98375" y2="-3.65" width="0.127" layer="21"/>
<wire x1="3.98375" y1="-3.65" x2="3.98375" y2="-2.55" width="0.127" layer="21"/>
<wire x1="-4.01625" y1="-3.65" x2="-4.01625" y2="-2.55" width="0.127" layer="21"/>
<smd name="P$1" x="-3.01625" y="0" dx="4.7" dy="1.75" layer="1" rot="R90"/>
<smd name="P$2" x="3.03375" y="0" dx="4.7" dy="1.75" layer="1" rot="R90"/>
<text x="0" y="0" size="0.6096" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="CR127">
<wire x1="-3.5" y1="6" x2="-6" y2="6" width="0.2032" layer="21"/>
<wire x1="-6" y1="6" x2="-6" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-6" x2="-3.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="3.5" y1="-6" x2="6" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="6" x2="3.5" y2="6" width="0.2032" layer="21"/>
<smd name="1" x="0" y="4.9" dx="5.4" dy="4" layer="1"/>
<smd name="2" x="0" y="-4.9" dx="5.4" dy="4" layer="1"/>
<text x="0" y="0" size="0.6096" layer="25" font="vector" align="center">&gt;Name</text>
</package>
<package name="ANT-2.4G-NOGOOD">
<smd name="GND" x="0.4515" y="0.251" dx="0.9" dy="0.754" layer="1" stop="no"/>
<smd name="ANT" x="2.554" y="0.251" dx="0.5" dy="0.754" layer="1"/>
<polygon width="0.02" layer="1">
<vertex x="0.01" y="0.5"/>
<vertex x="0.01" y="4.99"/>
<vertex x="4.99" y="4.99"/>
<vertex x="4.99" y="2.75"/>
<vertex x="7.01" y="2.75"/>
<vertex x="7.01" y="5.19"/>
<vertex x="9.69" y="5.19"/>
<vertex x="9.69" y="2.75"/>
<vertex x="11.71" y="2.75"/>
<vertex x="11.71" y="5.19"/>
<vertex x="14.39" y="5.19"/>
<vertex x="14.39" y="0.97"/>
<vertex x="13.91" y="0.97"/>
<vertex x="13.91" y="4.71"/>
<vertex x="12.19" y="4.71"/>
<vertex x="12.19" y="2.27"/>
<vertex x="9.21" y="2.27"/>
<vertex x="9.21" y="4.71"/>
<vertex x="7.49" y="4.71"/>
<vertex x="7.49" y="2.27"/>
<vertex x="4.51" y="2.27"/>
<vertex x="4.51" y="4.51"/>
<vertex x="2.795" y="4.51"/>
<vertex x="2.795" y="0.5"/>
<vertex x="2.315" y="0.5"/>
<vertex x="2.315" y="4.51"/>
<vertex x="0.8925" y="4.51"/>
<vertex x="0.8925" y="0.5"/>
</polygon>
<polygon width="0.02" layer="29">
<vertex x="0.01" y="0.5"/>
<vertex x="0.01" y="4.99"/>
<vertex x="4.99" y="4.99"/>
<vertex x="4.99" y="2.75"/>
<vertex x="7.01" y="2.75"/>
<vertex x="7.01" y="5.19"/>
<vertex x="9.69" y="5.19"/>
<vertex x="9.69" y="2.75"/>
<vertex x="11.71" y="2.75"/>
<vertex x="11.71" y="5.19"/>
<vertex x="14.39" y="5.19"/>
<vertex x="14.39" y="0.97"/>
<vertex x="13.91" y="0.97"/>
<vertex x="13.91" y="4.71"/>
<vertex x="12.19" y="4.71"/>
<vertex x="12.19" y="2.27"/>
<vertex x="9.21" y="2.27"/>
<vertex x="9.21" y="4.71"/>
<vertex x="7.49" y="4.71"/>
<vertex x="7.49" y="2.27"/>
<vertex x="4.51" y="2.27"/>
<vertex x="4.51" y="4.51"/>
<vertex x="2.795" y="4.51"/>
<vertex x="2.795" y="0.5"/>
<vertex x="2.315" y="0.5"/>
<vertex x="2.315" y="4.51"/>
<vertex x="0.8925" y="4.51"/>
<vertex x="0.8925" y="0.5"/>
</polygon>
<rectangle x1="-0.47625" y1="0.47625" x2="14.684375" y2="5.715" layer="41"/>
<rectangle x1="-0.47625" y1="0.47625" x2="14.684375" y2="5.715" layer="42"/>
<rectangle x1="-0.47625" y1="0.47625" x2="14.684375" y2="5.715" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="0" y="-1.524" size="1.27" layer="96" font="vector" align="center">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="2.5146" size="1.27" layer="95" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="96" font="vector" align="center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.762" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="0" size="1.27" layer="95" font="vector" rot="R90" align="center">&gt;NAME</text>
<text x="2.54" y="0" size="1.27" layer="96" font="vector" rot="R90" align="center">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.762" x2="1.27" y2="-0.254" layer="94"/>
<rectangle x1="-1.27" y1="0.254" x2="1.27" y2="0.762" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="PIN-MICRODUINO">
<pin name="D6" x="-10.16" y="-33.02" length="middle" rot="R90"/>
<pin name="D7" x="-10.16" y="33.02" length="middle" rot="R270"/>
<pin name="D8" x="-5.08" y="33.02" length="middle" rot="R270"/>
<pin name="D9" x="0" y="33.02" length="middle" rot="R270"/>
<pin name="D10" x="5.08" y="33.02" length="middle" rot="R270"/>
<pin name="D11/MOSI" x="10.16" y="33.02" length="middle" rot="R270"/>
<pin name="D12/MISO" x="15.24" y="33.02" length="middle" rot="R270"/>
<pin name="D13/SCK" x="20.32" y="33.02" length="middle" rot="R270"/>
<pin name="GND" x="-20.32" y="-33.02" length="middle" rot="R90"/>
<pin name="RESET" x="-15.24" y="-33.02" length="middle" rot="R90"/>
<pin name="A0" x="35.56" y="15.24" length="middle" rot="R180"/>
<pin name="A1" x="35.56" y="10.16" length="middle" rot="R180"/>
<pin name="A2" x="35.56" y="5.08" length="middle" rot="R180"/>
<pin name="A3" x="35.56" y="0" length="middle" rot="R180"/>
<pin name="SDA" x="35.56" y="-5.08" length="middle" rot="R180"/>
<pin name="SCL" x="35.56" y="-10.16" length="middle" rot="R180"/>
<pin name="A6" x="35.56" y="-15.24" length="middle" rot="R180"/>
<pin name="A7" x="35.56" y="-20.32" length="middle" rot="R180"/>
<pin name="AREF" x="35.56" y="20.32" length="middle" rot="R180"/>
<pin name="D5" x="-5.08" y="-33.02" length="middle" rot="R90"/>
<pin name="D4" x="0" y="-33.02" length="middle" rot="R90"/>
<pin name="D3" x="5.08" y="-33.02" length="middle" rot="R90"/>
<pin name="+5V" x="-20.32" y="33.02" length="middle" rot="R270"/>
<pin name="+3V3" x="-15.24" y="33.02" length="middle" rot="R270"/>
<pin name="D2" x="10.16" y="-33.02" length="middle" rot="R90"/>
<pin name="TX1" x="15.24" y="-33.02" length="middle" rot="R90"/>
<pin name="RX0" x="20.32" y="-33.02" length="middle" rot="R90"/>
<wire x1="-25.4" y1="27.94" x2="30.48" y2="27.94" width="0.254" layer="94"/>
<wire x1="30.48" y1="27.94" x2="30.48" y2="-27.94" width="0.254" layer="94"/>
<wire x1="30.48" y1="-27.94" x2="-25.4" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-27.94" x2="-25.4" y2="27.94" width="0.254" layer="94"/>
<text x="0" y="2.54" size="2.54" layer="95" font="vector" align="top-center">Microduino I/O</text>
</symbol>
<symbol name="+3V3">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="0" y="3.81" size="1.27" layer="96" font="vector" align="center">&gt;VALUE</text>
<pin name="+3V3" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="0" y="3.81" size="1.27" layer="96" font="vector" align="center">&gt;VALUE</text>
<pin name="+5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="MICRO_USB">
<wire x1="-7.62" y1="20.32" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="20.32" width="0.254" layer="94"/>
<wire x1="7.62" y1="20.32" x2="-7.62" y2="20.32" width="0.254" layer="94"/>
<pin name="D+" x="-10.16" y="5.08" length="short"/>
<pin name="D-" x="-10.16" y="7.62" length="short"/>
<pin name="GND" x="-10.16" y="0" length="short" direction="pas"/>
<pin name="ID" x="-10.16" y="2.54" length="short"/>
<pin name="VBUS" x="-10.16" y="10.16" length="short" direction="pwr"/>
<text x="0" y="21.59" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
<text x="0.254" y="-8.89" size="1.778" layer="96" font="vector" align="center">&gt;VALUE</text>
<pin name="G1" x="-10.16" y="-5.08" visible="pin" length="short"/>
</symbol>
<symbol name="CH340G">
<description>&lt;b&gt;CH340G&lt;/b&gt;
&lt;p&gt;CH34X USB to serial.&lt;/p&gt;</description>
<pin name="GND" x="-10.16" y="10.16" length="short"/>
<pin name="TX" x="-10.16" y="7.62" length="short"/>
<pin name="RX" x="-10.16" y="5.08" length="short"/>
<pin name="V3" x="-10.16" y="2.54" length="short"/>
<pin name="D+" x="-10.16" y="0" length="short"/>
<pin name="D-" x="-10.16" y="-2.54" length="short"/>
<pin name="XI" x="-10.16" y="-5.08" length="short"/>
<pin name="XO" x="-10.16" y="-7.62" length="short"/>
<pin name="CTS" x="10.16" y="-7.62" length="short" rot="R180"/>
<pin name="DSR" x="10.16" y="-5.08" length="short" rot="R180"/>
<pin name="RI" x="10.16" y="-2.54" length="short" rot="R180"/>
<pin name="DCD" x="10.16" y="0" length="short" rot="R180"/>
<pin name="DTR" x="10.16" y="2.54" length="short" rot="R180"/>
<pin name="RTS" x="10.16" y="5.08" length="short" rot="R180"/>
<pin name="RS232" x="10.16" y="7.62" length="short" rot="R180"/>
<pin name="VCC" x="10.16" y="10.16" length="short" rot="R180"/>
<text x="-5.08" y="12.7" size="1.778" layer="95">CH340G</text>
<wire x1="-7.62" y1="12.7" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
</symbol>
<symbol name="QSMD">
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="3" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="-3.81" width="0.254" layer="94" style="shortdash"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.254" layer="94" style="shortdash"/>
<wire x1="-2.54" y1="-3.81" x2="3.81" y2="-3.81" width="0.254" layer="94" style="shortdash"/>
<wire x1="3.81" y1="-3.81" x2="3.81" y2="3.81" width="0.254" layer="94" style="shortdash"/>
<wire x1="3.81" y1="3.81" x2="-3.81" y2="3.81" width="0.254" layer="94" style="shortdash"/>
<pin name="2" x="-2.54" y="-5.08" visible="off" length="point" rot="R90"/>
<pin name="4" x="2.54" y="-5.08" visible="off" length="point" rot="R90"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<text x="0" y="5.08" size="1.27" layer="95" font="vector" align="center">&gt;NAME</text>
<text x="0" y="7.62" size="1.27" layer="96" font="vector" align="center">&gt;VALUE</text>
</symbol>
<symbol name="CRYSTAL">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-2.032" x2="2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="1.778" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-3.048" x2="2.54" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.048" x2="1.778" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.048" x2="2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-3.048" x2="-2.54" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.048" x2="-3.302" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.048" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.032" x2="-2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.032" x2="-3.302" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.032" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="0" y="-7.62" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<pin name="3" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<text x="1.27" y="5.08" size="1.27" layer="95" font="vector" rot="R90" align="center">&gt;NAME</text>
<text x="-1.27" y="5.08" size="1.27" layer="96" font="vector" rot="R90" align="center">&gt;VALUE</text>
</symbol>
<symbol name="SWITCH_3P">
<pin name="A" x="-5.08" y="7.62" visible="off" length="middle" direction="in" function="clk" rot="R270"/>
<pin name="S" x="0" y="7.62" visible="off" length="middle" direction="in" function="clk" rot="R270"/>
<pin name="B" x="2.54" y="7.62" visible="off" length="middle" direction="out" function="clk" rot="R270"/>
<wire x1="1.905" y1="0.635" x2="3.175" y2="0.635" width="0.254" layer="94"/>
<wire x1="3.175" y1="0.635" x2="3.175" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="0.635" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-2.54" y2="0.635" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0.635" x2="-3.175" y2="0.635" width="0.254" layer="94"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-0.635" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="3.175" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.27" layer="95" font="vector" rot="R90">&gt;NAME</text>
<wire x1="-3.4925" y1="2.54" x2="-2.54" y2="0.635" width="0.127" layer="94"/>
<wire x1="-2.54" y1="0.635" x2="-1.5875" y2="2.54" width="0.127" layer="94"/>
<wire x1="-2.54" y1="0.9525" x2="-2.54" y2="5.08" width="0.127" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.127" layer="94"/>
</symbol>
<symbol name="ATMEGAX8">
<wire x1="-17.78" y1="-27.94" x2="15.24" y2="-27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="-27.94" x2="15.24" y2="27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="27.94" x2="-17.78" y2="27.94" width="0.254" layer="94"/>
<wire x1="-17.78" y1="27.94" x2="-17.78" y2="-27.94" width="0.254" layer="94"/>
<pin name="ADC6" x="-22.86" y="-10.16" length="middle" direction="pas"/>
<pin name="ADC7" x="-22.86" y="-12.7" length="middle" direction="pas"/>
<pin name="AGND" x="-22.86" y="-17.78" length="middle" direction="pas"/>
<pin name="AREF" x="-22.86" y="10.16" length="middle" direction="pas"/>
<pin name="AVCC" x="20.32" y="20.32" length="middle" direction="pas" rot="R180"/>
<pin name="GND1" x="-22.86" y="-20.32" length="middle" direction="pwr"/>
<pin name="GND2" x="-22.86" y="-22.86" length="middle" direction="pwr"/>
<pin name="PB0(ICP)" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="PB1(OC1)" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="PB2(SS)" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="PB3(MOSI)" x="20.32" y="-15.24" length="middle" rot="R180"/>
<pin name="PB4(MISO)" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="PB5(SCK)" x="20.32" y="-20.32" length="middle" rot="R180"/>
<pin name="PB6(XTAL1)" x="-22.86" y="20.32" length="middle" direction="pas"/>
<pin name="PB7(XTAL2)" x="-22.86" y="15.24" length="middle" direction="pas"/>
<pin name="PC0(ADC0)" x="-22.86" y="5.08" length="middle" direction="pas"/>
<pin name="PC1(ADC1)" x="-22.86" y="2.54" length="middle" direction="pas"/>
<pin name="PC2(ADC2)" x="-22.86" y="0" length="middle" direction="pas"/>
<pin name="PC3(ADC3)" x="-22.86" y="-2.54" length="middle" direction="pas"/>
<pin name="PC4(ADC4)" x="-22.86" y="-5.08" length="middle" direction="pas"/>
<pin name="PC5(ADC5)" x="-22.86" y="-7.62" length="middle" direction="pas"/>
<pin name="PC6(RESET)" x="-22.86" y="25.4" length="middle" function="dot"/>
<pin name="PD0(RXD)" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="PD1(TXD)" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="PD2(IND0)" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="PD3(INT1)" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="PD4(T0)" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="PD5(T1)" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="PD6(AIN0)" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="PD7(AIN1)" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="VCC1" x="20.32" y="25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="VCC2" x="20.32" y="22.86" length="middle" direction="pwr" rot="R180"/>
<text x="-12.7" y="-26.67" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-17.78" y="28.956" size="1.778" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="LP2985">
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<pin name="VIN" x="-12.7" y="5.08" length="short" direction="pwr"/>
<pin name="ON/OFF" x="-12.7" y="2.54" length="short"/>
<pin name="VOUT" x="12.7" y="5.08" length="short" direction="pwr" rot="R180"/>
<pin name="BYPASS" x="-12.7" y="0" length="short"/>
<pin name="GND" x="5.08" y="-7.62" length="short" direction="pwr" rot="R90"/>
<text x="-2.54" y="11.43" size="1.27" layer="96" font="vector" align="center">&gt;VALUE</text>
<text x="-2.54" y="-6.35" size="1.27" layer="95" font="vector" align="center">&gt;NAME</text>
</symbol>
<symbol name="LTC4054">
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<pin name="VCC" x="-12.7" y="0" length="short" direction="pwr"/>
<pin name="GND" x="12.7" y="-7.62" length="short" direction="pwr" rot="R180"/>
<pin name="CHRG(-)" x="-12.7" y="-7.62" length="short"/>
<pin name="BAT" x="-12.7" y="7.62" length="short"/>
<pin name="PROG" x="12.7" y="7.62" length="short" rot="R180"/>
</symbol>
<symbol name="PAD">
<circle x="2.54" y="0" radius="1.04726875" width="0.254" layer="94"/>
<pin name="P$1" x="0" y="0" visible="off" length="short"/>
</symbol>
<symbol name="CAP_POL">
<wire x1="-1.27" y1="0.762" x2="0" y2="0.762" width="0.254" layer="94"/>
<wire x1="0" y1="0.762" x2="1.27" y2="0.762" width="0.254" layer="94"/>
<wire x1="0" y1="-0.508" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.508" x2="1.2192" y2="-0.8382" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-1.2192" y1="-0.8382" x2="0" y2="-0.508" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="2.413" y="0" size="1.27" layer="95" font="vector" rot="R90" align="center">&gt;NAME</text>
<text x="-2.413" y="0" size="1.27" layer="96" font="vector" rot="R90" align="center">&gt;VALUE</text>
<rectangle x1="-1.491" y1="1.684" x2="-0.602" y2="1.811" layer="94"/>
<rectangle x1="-1.11" y1="1.303" x2="-0.983" y2="2.192" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="0.762" width="0.1524" layer="94"/>
</symbol>
<symbol name="P-MOS">
<wire x1="0" y1="0" x2="-1.016" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.381" x2="-1.016" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-0.381" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.381" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="1.27" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-3.048" x2="1.27" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="2.794" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.794" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="1.778" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.762" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="0.762" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="-0.889" y2="-0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="-0.127" x2="-0.889" y2="0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="0.127" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.635" x2="1.524" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.635" x2="1.27" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.016" y2="-0.635" width="0.254" layer="94"/>
<circle x="0" y="2.794" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="2.032" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-3.048" radius="0.3592" width="0" layer="94"/>
<text x="2.54" y="0" size="1.27" layer="95" font="vector" rot="R90" align="center">&gt;NAME</text>
<rectangle x1="-2.032" y1="-2.54" x2="-1.524" y2="-1.27" layer="94"/>
<rectangle x1="-2.032" y1="1.27" x2="-1.524" y2="2.54" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="0" y="2.5146" size="1.27" layer="95" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-2.5654" size="1.27" layer="96" font="vector" align="center">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="INDUCTOR">
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="2.54" y="0" size="1.27" layer="95" font="vector" rot="R90" align="center">&gt;NAME</text>
<text x="-1.778" y="0" size="1.27" layer="96" font="vector" rot="R90" align="center">&gt;VALUE</text>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="CRYSTAL-C">
<wire x1="1.016" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<text x="0" y="3.81" size="1.27" layer="95" font="vector" align="center">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94" style="shortdash"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94" style="shortdash"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="2.54" width="0.254" layer="94" style="shortdash"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94" style="shortdash"/>
<text x="0" y="6.35" size="1.27" layer="96" font="vector" align="center">&gt;VALUE</text>
</symbol>
<symbol name="NRF51822">
<pin name="P0.00/AREF" x="-27.94" y="27.94" visible="pin" length="short"/>
<pin name="P0.01/AIN2" x="-27.94" y="25.4" visible="pin" length="short"/>
<pin name="P0.02/AIN3" x="-27.94" y="22.86" visible="pin" length="short"/>
<pin name="P0.03/AIN4" x="-27.94" y="20.32" visible="pin" length="short"/>
<pin name="P0.04/AIN5" x="-27.94" y="17.78" visible="pin" length="short"/>
<pin name="P0.05/AIN6" x="-27.94" y="15.24" visible="pin" length="short"/>
<pin name="P0.06/AIN7" x="-27.94" y="12.7" visible="pin" length="short"/>
<pin name="P0.07" x="-27.94" y="10.16" visible="pin" length="short"/>
<pin name="P0.08" x="-27.94" y="7.62" visible="pin" length="short"/>
<pin name="P0.09" x="-27.94" y="5.08" visible="pin" length="short"/>
<pin name="P0.10" x="-27.94" y="2.54" visible="pin" length="short"/>
<pin name="P0.11" x="-27.94" y="0" visible="pin" length="short"/>
<pin name="P0.12" x="-27.94" y="-2.54" visible="pin" length="short"/>
<pin name="P0.13" x="-27.94" y="-5.08" visible="pin" length="short"/>
<pin name="P0.14" x="-27.94" y="-7.62" visible="pin" length="short"/>
<pin name="P0.15" x="-27.94" y="-10.16" visible="pin" length="short"/>
<pin name="P0.16" x="-27.94" y="-12.7" visible="pin" length="short"/>
<pin name="P0.17" x="-27.94" y="-15.24" visible="pin" length="short"/>
<pin name="P0.18" x="-27.94" y="-17.78" visible="pin" length="short"/>
<pin name="P0.19" x="-27.94" y="-20.32" visible="pin" length="short"/>
<pin name="P0.20" x="-27.94" y="-22.86" visible="pin" length="short"/>
<pin name="P0.21" x="-27.94" y="-25.4" visible="pin" length="short"/>
<pin name="P0.22" x="-27.94" y="-27.94" visible="pin" length="short"/>
<pin name="P0.23" x="-27.94" y="-30.48" visible="pin" length="short"/>
<pin name="P0.24" x="-27.94" y="-33.02" visible="pin" length="short"/>
<pin name="P0.25" x="-27.94" y="-35.56" visible="pin" length="short"/>
<pin name="P0.26/AIN0" x="-27.94" y="-38.1" visible="pin" length="short"/>
<pin name="P0.27/AIN1" x="-27.94" y="-40.64" visible="pin" length="short"/>
<pin name="P0.28" x="-27.94" y="-43.18" visible="pin" length="short"/>
<pin name="P0.29" x="-27.94" y="-45.72" visible="pin" length="short"/>
<pin name="P0.30" x="-27.94" y="-48.26" visible="pin" length="short"/>
<pin name="SWDIO/NRESET" x="-27.94" y="-55.88" visible="pin" length="short"/>
<pin name="SWDCLK" x="-27.94" y="-58.42" visible="pin" length="short"/>
<pin name="AVDD1" x="27.94" y="22.86" visible="pin" length="short" rot="R180"/>
<pin name="VDD1" x="27.94" y="17.78" visible="pin" length="short" rot="R180"/>
<pin name="DCC" x="27.94" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="VSS1" x="27.94" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="DEC2" x="27.94" y="-7.62" visible="pin" length="short" rot="R180"/>
<pin name="DEC1" x="27.94" y="-5.08" visible="pin" length="short" rot="R180"/>
<pin name="VDD-PA" x="27.94" y="-33.02" visible="pin" length="short" rot="R180"/>
<pin name="ANT1" x="27.94" y="-25.4" visible="pin" length="short" rot="R180"/>
<pin name="ANT2" x="27.94" y="-22.86" visible="pin" length="short" rot="R180"/>
<pin name="XC1" x="27.94" y="-50.8" visible="pin" length="short" rot="R180"/>
<pin name="XC2" x="27.94" y="-53.34" visible="pin" length="short" rot="R180"/>
<wire x1="-25.4" y1="30.48" x2="25.4" y2="30.48" width="0.254" layer="94"/>
<wire x1="25.4" y1="30.48" x2="25.4" y2="-60.96" width="0.254" layer="94"/>
<wire x1="25.4" y1="-60.96" x2="-25.4" y2="-60.96" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-60.96" x2="-25.4" y2="30.48" width="0.254" layer="94"/>
<text x="0" y="-63.5" size="1.27" layer="95" font="vector" align="center">&gt;NAME</text>
</symbol>
<symbol name="ANT-2P">
<wire x1="0" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="1" x="0" y="0" visible="pad" length="point" rot="R90"/>
<pin name="2" x="-5.08" y="5.08" visible="pad" length="point"/>
</symbol>
<symbol name="LED-DUO">
<wire x1="2.54" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.778" y1="4.572" x2="0.381" y2="5.969" width="0.1524" layer="94"/>
<wire x1="0.635" y1="4.445" x2="-0.762" y2="5.842" width="0.1524" layer="94"/>
<pin name="3" x="-2.54" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="4" x="5.08" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<polygon width="0.1524" layer="94">
<vertex x="0.381" y="5.969"/>
<vertex x="1.27" y="5.588"/>
<vertex x="0.762" y="5.08"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-0.762" y="5.842"/>
<vertex x="0.127" y="5.461"/>
<vertex x="-0.381" y="4.953"/>
</polygon>
<wire x1="2.54" y1="-3.81" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.508" x2="0.381" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="-0.762" y2="0.762" width="0.1524" layer="94"/>
<pin name="1" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="2" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<polygon width="0.1524" layer="94">
<vertex x="0.381" y="0.889"/>
<vertex x="1.27" y="0.508"/>
<vertex x="0.762" y="0"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-0.762" y="0.762"/>
<vertex x="0.127" y="0.381"/>
<vertex x="-0.381" y="-0.127"/>
</polygon>
</symbol>
<symbol name="G5177">
<pin name="LX1" x="-12.7" y="5.08" visible="pin" length="short"/>
<pin name="LX2" x="-12.7" y="2.54" visible="pin" length="short"/>
<pin name="VBAT" x="-12.7" y="-2.54" visible="pin" length="short"/>
<pin name="ILM01" x="-12.7" y="-5.08" visible="pin" length="short"/>
<pin name="GND" x="0" y="-10.16" visible="pin" length="short" rot="R90"/>
<pin name="FB" x="12.7" y="-5.08" visible="pin" length="short" rot="R180"/>
<pin name="VOUT2" x="12.7" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="VOUT1" x="12.7" y="5.08" visible="pin" length="short" rot="R180"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<pin name="G" x="2.54" y="-10.16" visible="pin" length="short" rot="R90"/>
</symbol>
<symbol name="S2B-XH">
<pin name="2" x="2.54" y="-5.08" visible="pin" length="short" rot="R90"/>
<pin name="1" x="-2.54" y="-5.08" visible="pin" length="short" rot="R90"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<pin name="GND" x="-10.16" y="2.54" visible="off" length="short"/>
<pin name="GND1" x="10.16" y="2.54" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH 1/10th watt (small) resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402_L" package="0402-RES_L">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402_L" package="0402-CAP_L">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICRDUINO-PIN">
<description>microduino引脚封装</description>
<gates>
<gate name="G$1" symbol="PIN-MICRODUINO" x="0" y="0"/>
</gates>
<devices>
<device name="-MICRO-NORMAL" package="27UPIN-MICRO-NORMAL">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MICRO-NULL+" package="27UPIN-MICRO-NULL+">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MICRO-NORMAL+" package="27UPIN-MICRO-NORMAL+">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MICRO-SMALL" package="27UPIN-MICRO-SMALL">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MICRO-NULL" package="27UPIN-MICRO-NULL">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MICRO-SMALL-NULL" package="27UPIN-MICRO-SMALL-NULL">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-COOKIE-OLD" package="27UPIN-COOKIE-OLD">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-COOKIE-PAD-SHIELD" package="27UPIN-COOKIE-PAD-SHIELD">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-COOKIE-PAD-NOF" package="27UPIN-COOKIE-PAD-NOF">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-COOKIE-PAD-HUB" package="27UPIN-COOKIE-PAD-HUB">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-COOKIE-NEW" package="27UPIN-COOKIE-NEW">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-COOKIE-PAD-LAYER-BUT" package="27UPIN-PAD-BUT">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-COOKIE-PAD-LAYER-TOP" package="27UPIN-PAD-TOP">
<connects>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="A3" pad="14"/>
<connect gate="G$1" pin="A6" pad="17"/>
<connect gate="G$1" pin="A7" pad="18"/>
<connect gate="G$1" pin="AREF" pad="10"/>
<connect gate="G$1" pin="D10" pad="6"/>
<connect gate="G$1" pin="D11/MOSI" pad="7"/>
<connect gate="G$1" pin="D12/MISO" pad="8"/>
<connect gate="G$1" pin="D13/SCK" pad="9"/>
<connect gate="G$1" pin="D2" pad="21"/>
<connect gate="G$1" pin="D3" pad="22"/>
<connect gate="G$1" pin="D4" pad="23"/>
<connect gate="G$1" pin="D5" pad="24"/>
<connect gate="G$1" pin="D6" pad="25"/>
<connect gate="G$1" pin="D7" pad="3"/>
<connect gate="G$1" pin="D8" pad="4"/>
<connect gate="G$1" pin="D9" pad="5"/>
<connect gate="G$1" pin="GND" pad="27"/>
<connect gate="G$1" pin="RESET" pad="26"/>
<connect gate="G$1" pin="RX0" pad="19"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="TX1" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V">
<gates>
<gate name="+5V" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICRO-USB" prefix="USB">
<gates>
<gate name="USB" symbol="MICRO_USB" x="0" y="0"/>
</gates>
<devices>
<device name="NJ" package="MICRO-USB-NJ">
<connects>
<connect gate="USB" pin="D+" pad="P3"/>
<connect gate="USB" pin="D-" pad="P2"/>
<connect gate="USB" pin="G1" pad="G1 G2 G3 G4 G5 G6 G7 G8 G9 G10"/>
<connect gate="USB" pin="GND" pad="P5" route="any"/>
<connect gate="USB" pin="ID" pad="P4"/>
<connect gate="USB" pin="VBUS" pad="P1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LC" package="MICRO-USB-LC">
<connects>
<connect gate="USB" pin="D+" pad="3"/>
<connect gate="USB" pin="D-" pad="2"/>
<connect gate="USB" pin="G1" pad="P$1 P$2 P$5 P$6 P$7 P$8 P$15 P$16 P$17 P$18"/>
<connect gate="USB" pin="GND" pad="5"/>
<connect gate="USB" pin="ID" pad="4"/>
<connect gate="USB" pin="VBUS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NJ-B" package="MICRO-USB-NJ-B">
<connects>
<connect gate="USB" pin="D+" pad="P3"/>
<connect gate="USB" pin="D-" pad="P2"/>
<connect gate="USB" pin="G1" pad="G1 G2 G3 G4 G5 G6 G7 G8 G9 G10"/>
<connect gate="USB" pin="GND" pad="P5"/>
<connect gate="USB" pin="ID" pad="P4"/>
<connect gate="USB" pin="VBUS" pad="P1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CH340" prefix="IC">
<gates>
<gate name="G$1" symbol="CH340G" x="22.86" y="-5.08"/>
</gates>
<devices>
<device name="G" package="SOIC16">
<connects>
<connect gate="G$1" pin="CTS" pad="9"/>
<connect gate="G$1" pin="D+" pad="5"/>
<connect gate="G$1" pin="D-" pad="6"/>
<connect gate="G$1" pin="DCD" pad="12"/>
<connect gate="G$1" pin="DSR" pad="10"/>
<connect gate="G$1" pin="DTR" pad="13"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="RI" pad="11"/>
<connect gate="G$1" pin="RS232" pad="15"/>
<connect gate="G$1" pin="RTS" pad="14"/>
<connect gate="G$1" pin="RX" pad="3"/>
<connect gate="G$1" pin="TX" pad="2"/>
<connect gate="G$1" pin="V3" pad="4"/>
<connect gate="G$1" pin="VCC" pad="16"/>
<connect gate="G$1" pin="XI" pad="7"/>
<connect gate="G$1" pin="XO" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T" package="SSOP20">
<connects>
<connect gate="G$1" pin="CTS" pad="11"/>
<connect gate="G$1" pin="D+" pad="6"/>
<connect gate="G$1" pin="D-" pad="7"/>
<connect gate="G$1" pin="DCD" pad="14"/>
<connect gate="G$1" pin="DSR" pad="12"/>
<connect gate="G$1" pin="DTR" pad="15"/>
<connect gate="G$1" pin="GND" pad="8"/>
<connect gate="G$1" pin="RI" pad="13"/>
<connect gate="G$1" pin="RS232" pad="18"/>
<connect gate="G$1" pin="RTS" pad="16"/>
<connect gate="G$1" pin="RX" pad="4"/>
<connect gate="G$1" pin="TX" pad="3"/>
<connect gate="G$1" pin="V3" pad="5"/>
<connect gate="G$1" pin="VCC" pad="19"/>
<connect gate="G$1" pin="XI" pad="9"/>
<connect gate="G$1" pin="XO" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL-3225" prefix="Y">
<gates>
<gate name="G$1" symbol="QSMD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CRYSTAL-3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL-CT-SMD" prefix="Y">
<description>&lt;b&gt;Resonator&lt;/b&gt;
Small SMD resonator. This is the itty bitty 10/20MHz resonators with built in caps. CSTCE10M0G55 and CSTCE20M0V53. Footprint has been reviewed closely but hasn't been tested yet.</description>
<gates>
<gate name="G$1" symbol="CRYSTAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CRYSTAL-CT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWITCH-1P2T" prefix="SW">
<gates>
<gate name="G$2" symbol="SWITCH_3P" x="0" y="0"/>
</gates>
<devices>
<device name="-SK12D07" package="SWITCH-1P2T(SK12D07)">
<connects>
<connect gate="G$2" pin="A" pad="A"/>
<connect gate="G$2" pin="B" pad="B"/>
<connect gate="G$2" pin="S" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MSK12C02" package="SWITCH-1P2T(MSK12C01)">
<connects>
<connect gate="G$2" pin="A" pad="A"/>
<connect gate="G$2" pin="B" pad="B"/>
<connect gate="G$2" pin="S" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MM0059022B1" package="SWITCH-1P2T(MM0059022B1)">
<connects>
<connect gate="G$2" pin="A" pad="1_1 1_2"/>
<connect gate="G$2" pin="B" pad="2_1 2_2"/>
<connect gate="G$2" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SK12D10" package="SWITCH-1P2T(SS12D10)">
<connects>
<connect gate="G$2" pin="A" pad="A"/>
<connect gate="G$2" pin="B" pad="B"/>
<connect gate="G$2" pin="S" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATMEGA328P" prefix="IC">
<gates>
<gate name="G$1" symbol="ATMEGAX8" x="0" y="0"/>
</gates>
<devices>
<device name="-AU" package="TQFP-32">
<connects>
<connect gate="G$1" pin="ADC6" pad="19"/>
<connect gate="G$1" pin="ADC7" pad="22"/>
<connect gate="G$1" pin="AGND" pad="21"/>
<connect gate="G$1" pin="AREF" pad="20"/>
<connect gate="G$1" pin="AVCC" pad="18"/>
<connect gate="G$1" pin="GND1" pad="3"/>
<connect gate="G$1" pin="GND2" pad="5"/>
<connect gate="G$1" pin="PB0(ICP)" pad="12"/>
<connect gate="G$1" pin="PB1(OC1)" pad="13"/>
<connect gate="G$1" pin="PB2(SS)" pad="14"/>
<connect gate="G$1" pin="PB3(MOSI)" pad="15"/>
<connect gate="G$1" pin="PB4(MISO)" pad="16"/>
<connect gate="G$1" pin="PB5(SCK)" pad="17"/>
<connect gate="G$1" pin="PB6(XTAL1)" pad="7"/>
<connect gate="G$1" pin="PB7(XTAL2)" pad="8"/>
<connect gate="G$1" pin="PC0(ADC0)" pad="23"/>
<connect gate="G$1" pin="PC1(ADC1)" pad="24"/>
<connect gate="G$1" pin="PC2(ADC2)" pad="25"/>
<connect gate="G$1" pin="PC3(ADC3)" pad="26"/>
<connect gate="G$1" pin="PC4(ADC4)" pad="27"/>
<connect gate="G$1" pin="PC5(ADC5)" pad="28"/>
<connect gate="G$1" pin="PC6(RESET)" pad="29"/>
<connect gate="G$1" pin="PD0(RXD)" pad="30"/>
<connect gate="G$1" pin="PD1(TXD)" pad="31"/>
<connect gate="G$1" pin="PD2(IND0)" pad="32"/>
<connect gate="G$1" pin="PD3(INT1)" pad="1"/>
<connect gate="G$1" pin="PD4(T0)" pad="2"/>
<connect gate="G$1" pin="PD5(T1)" pad="9"/>
<connect gate="G$1" pin="PD6(AIN0)" pad="10"/>
<connect gate="G$1" pin="PD7(AIN1)" pad="11"/>
<connect gate="G$1" pin="VCC1" pad="4"/>
<connect gate="G$1" pin="VCC2" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MU" package="MLF-32">
<connects>
<connect gate="G$1" pin="ADC6" pad="19"/>
<connect gate="G$1" pin="ADC7" pad="22"/>
<connect gate="G$1" pin="AGND" pad="21 P$1"/>
<connect gate="G$1" pin="AREF" pad="20"/>
<connect gate="G$1" pin="AVCC" pad="18"/>
<connect gate="G$1" pin="GND1" pad="3"/>
<connect gate="G$1" pin="GND2" pad="5"/>
<connect gate="G$1" pin="PB0(ICP)" pad="12"/>
<connect gate="G$1" pin="PB1(OC1)" pad="13"/>
<connect gate="G$1" pin="PB2(SS)" pad="14"/>
<connect gate="G$1" pin="PB3(MOSI)" pad="15"/>
<connect gate="G$1" pin="PB4(MISO)" pad="16"/>
<connect gate="G$1" pin="PB5(SCK)" pad="17"/>
<connect gate="G$1" pin="PB6(XTAL1)" pad="7"/>
<connect gate="G$1" pin="PB7(XTAL2)" pad="8"/>
<connect gate="G$1" pin="PC0(ADC0)" pad="23"/>
<connect gate="G$1" pin="PC1(ADC1)" pad="24"/>
<connect gate="G$1" pin="PC2(ADC2)" pad="25"/>
<connect gate="G$1" pin="PC3(ADC3)" pad="26"/>
<connect gate="G$1" pin="PC4(ADC4)" pad="27"/>
<connect gate="G$1" pin="PC5(ADC5)" pad="28"/>
<connect gate="G$1" pin="PC6(RESET)" pad="29"/>
<connect gate="G$1" pin="PD0(RXD)" pad="30"/>
<connect gate="G$1" pin="PD1(TXD)" pad="31"/>
<connect gate="G$1" pin="PD2(IND0)" pad="32"/>
<connect gate="G$1" pin="PD3(INT1)" pad="1"/>
<connect gate="G$1" pin="PD4(T0)" pad="2"/>
<connect gate="G$1" pin="PD5(T1)" pad="9"/>
<connect gate="G$1" pin="PD6(AIN0)" pad="10"/>
<connect gate="G$1" pin="PD7(AIN1)" pad="11"/>
<connect gate="G$1" pin="VCC1" pad="4"/>
<connect gate="G$1" pin="VCC2" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LP2985AIM5-3.3" prefix="IC">
<description>RT9193-33GB</description>
<gates>
<gate name="G$1" symbol="LP2985" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="BYPASS" pad="4"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="ON/OFF" pad="3"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTC4054ES5-4.2" prefix="IC">
<gates>
<gate name="G$1" symbol="LTC4054" x="-5.08" y="12.7"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="BAT" pad="3"/>
<connect gate="G$1" pin="CHRG(-)" pad="1"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="PROG" pad="5"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PAD">
<gates>
<gate name="G$1" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="-1.27" package="PAD-1.27MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1.9" package="PAD-1.9MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-A" package="PINA">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1 P$2 P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1" package="PAD-1MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-B" package="PINB">
<connects>
<connect gate="G$1" pin="P$1" pad="A"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_POL" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor Polarized&lt;/b&gt;
These are standard SMD and PTH capacitors. Normally 10uF, 47uF, and 100uF in electrolytic and tantalum varieties. Always verify the external diameter of the through hole cap, it varies with capacity, voltage, and manufacturer. The EIA devices should be standard.</description>
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="3216" package="CAP-3216">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3528" package="CAP-3528">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7343" package="CAP-7343">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6032" package="CAP-6032">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0605" package="CAP-0605">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E12" package="CAP-SANYO-E12">
<connects>
<connect gate="G$1" pin="+" pad="+ +1 +2 +3 +4 +5 +6 +7 +8"/>
<connect gate="G$1" pin="-" pad="- -1 -2 -3 -4 -5 -6 -7 -8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRIODE-MOS-P" prefix="T">
<description>P-MOS</description>
<gates>
<gate name="G$1" symbol="P-MOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="-AO3407">
<attribute name="A" value="5.2" constant="no"/>
<attribute name="V" value="30" constant="no"/>
</technology>
<technology name="-BSS84">
<attribute name="A" value="0.13" constant="no"/>
<attribute name="V" value="50" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="SOD123" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO214" package="DO214">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD923" package="SOD923">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD523" package="SOD523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" prefix="L" uservalue="yes">
<description>&lt;b&gt;Inductors&lt;/b&gt;
Basic Inductor/Choke - 0603 and 1206. Footprints are not proven and vary greatly between part numbers.</description>
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="0603-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CR54" package="CR54">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CR75" package="CR75">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1007" package="1007">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4018" package="RWPA4018">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402_L" package="0402-IND_L">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CR127" package="CR127">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL-32.768K-3215" prefix="Y">
<gates>
<gate name="G$1" symbol="CRYSTAL-C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CRYSTAL-3215">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NRF51822" prefix="IC">
<gates>
<gate name="G$1" symbol="NRF51822" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN48">
<connects>
<connect gate="G$1" pin="ANT1" pad="31"/>
<connect gate="G$1" pin="ANT2" pad="32"/>
<connect gate="G$1" pin="AVDD1" pad="35 36"/>
<connect gate="G$1" pin="DCC" pad="2"/>
<connect gate="G$1" pin="DEC1" pad="39"/>
<connect gate="G$1" pin="DEC2" pad="29"/>
<connect gate="G$1" pin="P0.00/AREF" pad="4"/>
<connect gate="G$1" pin="P0.01/AIN2" pad="5"/>
<connect gate="G$1" pin="P0.02/AIN3" pad="6"/>
<connect gate="G$1" pin="P0.03/AIN4" pad="7"/>
<connect gate="G$1" pin="P0.04/AIN5" pad="8"/>
<connect gate="G$1" pin="P0.05/AIN6" pad="9"/>
<connect gate="G$1" pin="P0.06/AIN7" pad="10"/>
<connect gate="G$1" pin="P0.07" pad="11"/>
<connect gate="G$1" pin="P0.08" pad="14"/>
<connect gate="G$1" pin="P0.09" pad="15"/>
<connect gate="G$1" pin="P0.10" pad="16"/>
<connect gate="G$1" pin="P0.11" pad="17"/>
<connect gate="G$1" pin="P0.12" pad="18"/>
<connect gate="G$1" pin="P0.13" pad="19"/>
<connect gate="G$1" pin="P0.14" pad="20"/>
<connect gate="G$1" pin="P0.15" pad="21"/>
<connect gate="G$1" pin="P0.16" pad="22"/>
<connect gate="G$1" pin="P0.17" pad="25"/>
<connect gate="G$1" pin="P0.18" pad="26"/>
<connect gate="G$1" pin="P0.19" pad="27"/>
<connect gate="G$1" pin="P0.20" pad="28"/>
<connect gate="G$1" pin="P0.21" pad="40"/>
<connect gate="G$1" pin="P0.22" pad="41"/>
<connect gate="G$1" pin="P0.23" pad="42"/>
<connect gate="G$1" pin="P0.24" pad="43"/>
<connect gate="G$1" pin="P0.25" pad="44"/>
<connect gate="G$1" pin="P0.26/AIN0" pad="45"/>
<connect gate="G$1" pin="P0.27/AIN1" pad="46"/>
<connect gate="G$1" pin="P0.28" pad="47"/>
<connect gate="G$1" pin="P0.29" pad="48"/>
<connect gate="G$1" pin="P0.30" pad="3"/>
<connect gate="G$1" pin="SWDCLK" pad="24"/>
<connect gate="G$1" pin="SWDIO/NRESET" pad="23"/>
<connect gate="G$1" pin="VDD-PA" pad="30"/>
<connect gate="G$1" pin="VDD1" pad="1 12"/>
<connect gate="G$1" pin="VSS1" pad="13 33 34 G"/>
<connect gate="G$1" pin="XC1" pad="37"/>
<connect gate="G$1" pin="XC2" pad="38"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ANT-2.4G" prefix="ANT">
<gates>
<gate name="G$1" symbol="ANT-2P" x="0" y="0"/>
</gates>
<devices>
<device name="-A" package="ANT-2.4G">
<connects>
<connect gate="G$1" pin="1" pad="ANT"/>
<connect gate="G$1" pin="2" pad="GND"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-B" package="ANT-2.4G-F">
<connects>
<connect gate="G$1" pin="1" pad="ANT"/>
<connect gate="G$1" pin="2" pad="GND"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-NOGOOD" package="ANT-2.4G-NOGOOD">
<connects>
<connect gate="G$1" pin="1" pad="ANT"/>
<connect gate="G$1" pin="2" pad="GND"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED-DUAL" prefix="LED">
<gates>
<gate name="LED$1" symbol="LED-DUO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603-LED-DUAL">
<connects>
<connect gate="LED$1" pin="1" pad="1"/>
<connect gate="LED$1" pin="2" pad="2"/>
<connect gate="LED$1" pin="3" pad="3"/>
<connect gate="LED$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="G5177CF11U" prefix="IC">
<gates>
<gate name="G$1" symbol="G5177" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP8-S">
<connects>
<connect gate="G$1" pin="FB" pad="6"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="ILM01" pad="4"/>
<connect gate="G$1" pin="LX1" pad="1"/>
<connect gate="G$1" pin="LX2" pad="2"/>
<connect gate="G$1" pin="VBAT" pad="3"/>
<connect gate="G$1" pin="VOUT1" pad="8"/>
<connect gate="G$1" pin="VOUT2" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XH-2P" prefix="CON">
<gates>
<gate name="G$1" symbol="S2B-XH" x="0" y="0"/>
</gates>
<devices>
<device name="-1.27-W" package="XH-1.27-2P-W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="GND" pad="S1"/>
<connect gate="G$1" pin="GND1" pad="S2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1.27-L" package="XH-1.27-2P-L">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="GND" pad="P$5"/>
<connect gate="G$1" pin="GND1" pad="P$6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1.00-W" package="XH-1.00-2P-W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="GND" pad="S1"/>
<connect gate="G$1" pin="GND1" pad="S2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.54-L" package="XH-2.54-2P-L">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND1" pad="GND1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1.00-L" package="XH-1.00-2P-L">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="GND" pad="S1"/>
<connect gate="G$1" pin="GND1" pad="S2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="#_PKJ">
<packages>
<package name="SOT23-6">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt; 6 lead</description>
<wire x1="1.422" y1="-0.781" x2="-1.423" y2="-0.781" width="0.1524" layer="51"/>
<wire x1="-1.423" y1="-0.781" x2="-1.423" y2="0.781" width="0.1524" layer="21"/>
<wire x1="-1.423" y1="0.781" x2="1.422" y2="0.781" width="0.1524" layer="51"/>
<wire x1="1.422" y1="0.781" x2="1.422" y2="-0.781" width="0.1524" layer="21"/>
<circle x="-1.785" y="-1.4525" radius="0.254" width="0" layer="21"/>
<smd name="1" x="-0.95" y="-1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="2" x="0" y="-1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="3" x="0.95" y="-1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="4" x="0.95" y="1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="5" x="0" y="1.15" dx="0.6" dy="0.9" layer="1"/>
<smd name="6" x="-0.95" y="1.15" dx="0.6" dy="0.9" layer="1"/>
<text x="0" y="2.54" size="0.6096" layer="25" font="vector" ratio="10" align="center">&gt;NAME</text>
<rectangle x1="-1.2" y1="-1.4" x2="-0.7" y2="-0.8" layer="51"/>
<rectangle x1="-0.25" y1="-1.4" x2="0.25" y2="-0.8" layer="51"/>
<rectangle x1="0.7" y1="-1.4" x2="1.2" y2="-0.8" layer="51"/>
<rectangle x1="0.7" y1="0.8" x2="1.2" y2="1.4" layer="51"/>
<rectangle x1="-0.25" y1="0.8" x2="0.25" y2="1.4" layer="51"/>
<rectangle x1="-1.2" y1="0.8" x2="-0.7" y2="1.4" layer="51"/>
<wire x1="-1.3462" y1="-0.254" x2="-0.8382" y2="-0.254" width="0.3048" layer="21"/>
<wire x1="-0.8382" y1="-0.254" x2="-0.8382" y2="-0.508" width="0.3048" layer="21"/>
<wire x1="-0.8382" y1="-0.508" x2="-0.8382" y2="-0.6604" width="0.3048" layer="21"/>
<wire x1="-0.8382" y1="-0.508" x2="-1.3462" y2="-0.508" width="0.3048" layer="21"/>
<wire x1="-0.8382" y1="-0.6604" x2="-1.3208" y2="-0.6604" width="0.3048" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="ESDA5V3SC6">
<pin name="1" x="-5.08" y="10.16" visible="off" length="short" rot="R270"/>
<pin name="2" x="-2.54" y="-7.62" visible="off" length="short" rot="R90"/>
<pin name="3" x="-2.54" y="10.16" visible="off" length="short" rot="R270"/>
<pin name="5" x="2.54" y="-7.62" visible="off" length="short" rot="R90"/>
<pin name="4" x="2.54" y="10.16" visible="off" length="short" rot="R270"/>
<pin name="6" x="5.08" y="10.16" visible="off" length="short" rot="R270"/>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESDA5V3SC6">
<gates>
<gate name="G$1" symbol="ESDA5V3SC6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="2" name="gnd" width="0.254" drill="0.3048">
<clearance class="2" value="0.254"/>
</class>
</classes>
<parts>
<part name="GND16" library="#_Microduino" deviceset="GND" device=""/>
<part name="U$1" library="#_Microduino" deviceset="MICRDUINO-PIN" device="-COOKIE-PAD-NOF" value="MICRDUINO-PIN-COOKIE-PAD-NOF"/>
<part name="U$6" library="#_Microduino" deviceset="MICRDUINO-PIN" device="-COOKIE-PAD-NOF" value="MICRDUINO-PIN-COOKIE-PAD-NOF"/>
<part name="P+6" library="#_Microduino" deviceset="+3V3" device=""/>
<part name="U$9" library="#_Microduino" deviceset="+5V" device=""/>
<part name="GND15" library="#_Microduino" deviceset="GND" device=""/>
<part name="USB1" library="#_Microduino" deviceset="MICRO-USB" device="NJ-B" value="MICRO-USBNJ-B"/>
<part name="R1" library="#_Microduino" deviceset="RES" device="0402" value="1KR"/>
<part name="R4" library="#_Microduino" deviceset="RES" device="0402" value="6.8KR"/>
<part name="C5" library="#_Microduino" deviceset="CAP" device="0805" value="10uF"/>
<part name="C8" library="#_Microduino" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="C12" library="#_Microduino" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="R8" library="#_Microduino" deviceset="RES" device="0402" value="2.2KR"/>
<part name="IC4" library="#_Microduino" deviceset="CH340" device="T" value="CH340T"/>
<part name="Y1" library="#_Microduino" deviceset="CRYSTAL-3225" device="" value="12M"/>
<part name="C15" library="#_Microduino" deviceset="CAP" device="0402" value="22pF"/>
<part name="C16" library="#_Microduino" deviceset="CAP" device="0402" value="22pF"/>
<part name="GND8" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND5" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND7" library="#_Microduino" deviceset="GND" device=""/>
<part name="IC3" library="#_PKJ" deviceset="ESDA5V3SC6" device=""/>
<part name="GND3" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND13" library="#_Microduino" deviceset="GND" device=""/>
<part name="IC5" library="#_Microduino" deviceset="ATMEGA328P" device="-AU" value="ATMEGA328P-AU"/>
<part name="Y3" library="#_Microduino" deviceset="CRYSTAL-CT-SMD" device="" value="16M"/>
<part name="R3" library="#_Microduino" deviceset="RES" device="0402" value="4.7KR"/>
<part name="C11" library="#_Microduino" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="C13" library="#_Microduino" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="GND11" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND12" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND14" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND17" library="#_Microduino" deviceset="GND" device=""/>
<part name="C26" library="#_Microduino" deviceset="CAP" device="0805" value="10uF"/>
<part name="R2" library="#_Microduino" deviceset="RES" device="0402" value="1.25K"/>
<part name="GND1" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND2" library="#_Microduino" deviceset="GND" device=""/>
<part name="C1" library="#_Microduino" deviceset="CAP" device="0402" value="1uF"/>
<part name="IC1" library="#_Microduino" deviceset="LTC4054ES5-4.2" device=""/>
<part name="R11" library="#_Microduino" deviceset="RES" device="0402" value="22KR"/>
<part name="U$3" library="#_Microduino" deviceset="PAD" device="-1" value="NULL"/>
<part name="U$4" library="#_Microduino" deviceset="PAD" device="-1" value="NULL"/>
<part name="GND9" library="#_Microduino" deviceset="GND" device=""/>
<part name="R5" library="#_Microduino" deviceset="RES" device="0805" value="0.25R"/>
<part name="R6" library="#_Microduino" deviceset="RES" device="0402" value="2KR"/>
<part name="R12" library="#_Microduino" deviceset="RES" device="0402" value="800KR"/>
<part name="R14" library="#_Microduino" deviceset="RES" device="0402" value="1MR"/>
<part name="R15" library="#_Microduino" deviceset="RES" device="0402" value="1MR"/>
<part name="GND6" library="#_Microduino" deviceset="GND" device=""/>
<part name="C7" library="#_Microduino" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="GND18" library="#_Microduino" deviceset="GND" device=""/>
<part name="IC8" library="#_Microduino" deviceset="LP2985AIM5-3.3" device="" value="HT7833 SOT23-5"/>
<part name="GND27" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND29" library="#_Microduino" deviceset="GND" device=""/>
<part name="C27" library="#_Microduino" deviceset="CAP" device="0402" value="1uF"/>
<part name="D1" library="#_Microduino" deviceset="DIODE" device="DO214" value="SS34F"/>
<part name="T2" library="#_Microduino" deviceset="TRIODE-MOS-P" device="" technology="-AO3407" value="SI2329DS-T1-GE3"/>
<part name="R16" library="#_Microduino" deviceset="RES" device="0402" value="22KR"/>
<part name="SW1" library="#_Microduino" deviceset="SWITCH-1P2T" device="-MSK12C02"/>
<part name="GND30" library="#_Microduino" deviceset="GND" device=""/>
<part name="R18" library="#_Microduino" deviceset="RES" device="0402" value="47KR"/>
<part name="R19" library="#_Microduino" deviceset="RES" device="0402" value="15KR"/>
<part name="GND32" library="#_Microduino" deviceset="GND" device=""/>
<part name="C30" library="#_Microduino" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="C31" library="#_Microduino" deviceset="CAP" device="0603" value="10uF"/>
<part name="C32" library="#_Microduino" deviceset="CAP_POL" device="3216" value="47uF"/>
<part name="GND33" library="#_Microduino" deviceset="GND" device=""/>
<part name="C33" library="#_Microduino" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="GND34" library="#_Microduino" deviceset="GND" device=""/>
<part name="C36" library="#_Microduino" deviceset="CAP_POL" device="3216" value="47uF"/>
<part name="U$5" library="#_Microduino" deviceset="+5V" device=""/>
<part name="C3" library="#_Microduino" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="C9" library="#_Microduino" deviceset="CAP" device="0402" value="1nF"/>
<part name="C42" library="#_Microduino" deviceset="CAP" device="0402" value="47nF"/>
<part name="C43" library="#_Microduino" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="GND38" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND43" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND46" library="#_Microduino" deviceset="GND" device=""/>
<part name="C44" library="#_Microduino" deviceset="CAP" device="0402" value="12pF"/>
<part name="C45" library="#_Microduino" deviceset="CAP" device="0402" value="12pF"/>
<part name="C46" library="#_Microduino" deviceset="CAP" device="0402" value="12pF"/>
<part name="C47" library="#_Microduino" deviceset="CAP" device="0402" value="12pF"/>
<part name="Y4" library="#_Microduino" deviceset="CRYSTAL-32.768K-3215" device="" value="32.768K(FC-135/NX3215SA)"/>
<part name="Y2" library="#_Microduino" deviceset="CRYSTAL-3225" device="" value="16M 3225"/>
<part name="GND47" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND48" library="#_Microduino" deviceset="GND" device=""/>
<part name="IC6" library="#_Microduino" deviceset="NRF51822" device="" value="NRF51822-QFAC"/>
<part name="L2" library="#_Microduino" deviceset="INDUCTOR" device="0402" value="4.7nH"/>
<part name="L3" library="#_Microduino" deviceset="INDUCTOR" device="0402" value="10nH"/>
<part name="L4" library="#_Microduino" deviceset="INDUCTOR" device="0402" value="3.3nH"/>
<part name="C49" library="#_Microduino" deviceset="CAP" device="0402" value="2.2pF"/>
<part name="C50" library="#_Microduino" deviceset="CAP" device="0402" value="1.0pF"/>
<part name="C51" library="#_Microduino" deviceset="CAP" device="0402" value="1.5pF"/>
<part name="C52" library="#_Microduino" deviceset="CAP" device="0402" value="2.2nF"/>
<part name="GND50" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND51" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND52" library="#_Microduino" deviceset="GND" device=""/>
<part name="ANT1" library="#_Microduino" deviceset="ANT-2.4G" device="-NOGOOD" value="NULL"/>
<part name="GND53" library="#_Microduino" deviceset="GND" device=""/>
<part name="R20" library="#_Microduino" deviceset="RES" device="0402" value="470R"/>
<part name="R21" library="#_Microduino" deviceset="RES" device="0402" value="470R"/>
<part name="GND49" library="#_Microduino" deviceset="GND" device=""/>
<part name="R25" library="#_Microduino" deviceset="RES" device="0402" value="16KR"/>
<part name="GND54" library="supply1" deviceset="GND" device="" value=""/>
<part name="R26" library="#_Microduino" deviceset="RES" device="0402" value="4.7KR"/>
<part name="GND55" library="supply1" deviceset="GND" device="" value=""/>
<part name="D3" library="#_Microduino" deviceset="DIODE" device="SOD123" value="MBR0520"/>
<part name="U$8" library="#_Microduino" deviceset="+5V" device=""/>
<part name="IC7" library="#_Microduino" deviceset="LP2985AIM5-3.3" device="" value="RT9167A-33GB"/>
<part name="C2" library="#_Microduino" deviceset="CAP" device="0402" value="22nF"/>
<part name="GND10" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND35" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND36" library="#_Microduino" deviceset="GND" device=""/>
<part name="C4" library="#_Microduino" deviceset="CAP" device="0402" value="1uF"/>
<part name="U$2" library="#_Microduino" deviceset="+5V" device=""/>
<part name="P+1" library="#_Microduino" deviceset="+3V3" device=""/>
<part name="R9" library="#_Microduino" deviceset="RES" device="0402" value="4.7KR"/>
<part name="GND20" library="supply1" deviceset="GND" device="" value=""/>
<part name="R10" library="#_Microduino" deviceset="RES" device="0402" value="16KR"/>
<part name="GND21" library="supply1" deviceset="GND" device="" value=""/>
<part name="C6" library="#_Microduino" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="R13" library="#_Microduino" deviceset="RES" device="0402" value="220R"/>
<part name="R22" library="#_Microduino" deviceset="RES" device="0402" value="1KR"/>
<part name="R24" library="#_Microduino" deviceset="RES" device="0402" value="1KR"/>
<part name="C17" library="#_Microduino" deviceset="CAP_POL" device="3216" value="47uF"/>
<part name="U$7" library="#_Microduino" deviceset="PAD" device="-1" value="NULL"/>
<part name="U$10" library="#_Microduino" deviceset="PAD" device="-1" value="NULL"/>
<part name="U$11" library="#_Microduino" deviceset="PAD" device="-1.27" value="NULL"/>
<part name="U$12" library="#_Microduino" deviceset="PAD" device="-1.27" value="NULL"/>
<part name="U$13" library="#_Microduino" deviceset="PAD" device="-1.27" value="NULL"/>
<part name="U$14" library="#_Microduino" deviceset="PAD" device="-1.27" value="NULL"/>
<part name="U$15" library="#_Microduino" deviceset="PAD" device="-1.27" value="NULL"/>
<part name="U$16" library="#_Microduino" deviceset="PAD" device="-1.27" value="NULL"/>
<part name="U$17" library="#_Microduino" deviceset="PAD" device="-1.27" value="NULL"/>
<part name="U$18" library="#_Microduino" deviceset="PAD" device="-1.27" value="NULL"/>
<part name="U$19" library="#_Microduino" deviceset="PAD" device="-1.27" value="NULL"/>
<part name="U$20" library="#_Microduino" deviceset="PAD" device="-1.27" value="NULL"/>
<part name="R7" library="#_Microduino" deviceset="RES" device="0402" value="100KR"/>
<part name="U$21" library="#_Microduino" deviceset="PAD" device="-1.27" value="NULL"/>
<part name="U$23" library="#_Microduino" deviceset="PAD" device="-1.27" value="NULL"/>
<part name="U$25" library="#_Microduino" deviceset="PAD" device="-1" value="NULL"/>
<part name="C18" library="#_Microduino" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="LED1" library="#_Microduino" deviceset="LED-DUAL" device=""/>
<part name="LED2" library="#_Microduino" deviceset="LED-DUAL" device=""/>
<part name="U$27" library="#_Microduino" deviceset="PAD" device="-1" value="NULL"/>
<part name="L5" library="#_Microduino" deviceset="INDUCTOR" device="CR54" value="2.2uH"/>
<part name="GND19" library="#_Microduino" deviceset="GND" device=""/>
<part name="IC2" library="#_Microduino" deviceset="G5177CF11U" device=""/>
<part name="C10" library="#_Microduino" deviceset="CAP" device="0402" value="10pF"/>
<part name="R17" library="#_Microduino" deviceset="RES" device="0402" value="100KR"/>
<part name="CON1" library="#_Microduino" deviceset="XH-2P" device="-1.00-L"/>
<part name="GND4" library="#_Microduino" deviceset="GND" device=""/>
<part name="GND22" library="#_Microduino" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="106.68" y="55.88" size="1.778" layer="91" font="vector" rot="R90" align="center">800MA</text>
<wire x1="0" y1="81.28" x2="0" y2="200.66" width="0.3048" layer="94" style="longdash"/>
<wire x1="137.16" y1="200.66" x2="0" y2="200.66" width="0.3048" layer="94" style="longdash"/>
<wire x1="137.16" y1="81.28" x2="0" y2="81.28" width="0.3048" layer="94" style="longdash"/>
<wire x1="137.16" y1="200.66" x2="137.16" y2="132.08" width="0.3048" layer="94" style="longdash"/>
<wire x1="137.16" y1="132.08" x2="137.16" y2="81.28" width="0.3048" layer="94" style="longdash"/>
<wire x1="0" y1="38.1" x2="55.88" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="55.88" y1="38.1" x2="73.66" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="73.66" y1="38.1" x2="73.66" y2="0" width="0.1524" layer="94" style="longdash"/>
<wire x1="73.66" y1="0" x2="0" y2="0" width="0.1524" layer="94" style="longdash"/>
<wire x1="0" y1="0" x2="0" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="73.66" y1="38.1" x2="119.38" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="119.38" y1="38.1" x2="172.72" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="172.72" y1="0" x2="73.66" y2="0" width="0.1524" layer="94" style="longdash"/>
<wire x1="172.72" y1="38.1" x2="180.34" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="38.1" x2="220.98" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="220.98" y1="38.1" x2="220.98" y2="0" width="0.1524" layer="94" style="longdash"/>
<wire x1="220.98" y1="0" x2="172.72" y2="0" width="0.1524" layer="94" style="longdash"/>
<wire x1="0" y1="38.1" x2="0" y2="78.74" width="0.1524" layer="94" style="longdash"/>
<wire x1="0" y1="78.74" x2="55.88" y2="78.74" width="0.1524" layer="94" style="longdash"/>
<wire x1="55.88" y1="78.74" x2="55.88" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="55.88" y1="78.74" x2="119.38" y2="78.74" width="0.1524" layer="94" style="longdash"/>
<wire x1="119.38" y1="78.74" x2="119.38" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="119.38" y1="78.74" x2="180.34" y2="78.74" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="78.74" x2="180.34" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="78.74" x2="220.98" y2="78.74" width="0.1524" layer="94" style="longdash"/>
<wire x1="220.98" y1="78.74" x2="220.98" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="137.16" y1="81.28" x2="220.98" y2="81.28" width="0.1524" layer="94" style="longdash"/>
<wire x1="220.98" y1="81.28" x2="220.98" y2="132.08" width="0.1524" layer="94" style="longdash"/>
<wire x1="220.98" y1="132.08" x2="220.98" y2="200.66" width="0.1524" layer="94" style="longdash"/>
<wire x1="220.98" y1="200.66" x2="137.16" y2="200.66" width="0.1524" layer="94" style="longdash"/>
<wire x1="220.98" y1="132.08" x2="137.16" y2="132.08" width="0.1524" layer="94" style="longdash"/>
<text x="116.84" y="76.2" size="2.54" layer="97" rot="R180" align="center-left">充电</text>
<text x="149.86" y="76.2" size="2.54" layer="97" rot="R180" align="center-left">锂电接入</text>
<text x="218.44" y="76.2" size="2.54" layer="97" rot="R180" align="center-left">蓝牙LDO</text>
<text x="2.54" y="76.2" size="2.54" layer="97" align="center-left">USB</text>
<text x="71.12" y="35.56" size="2.54" layer="97" rot="R180" align="center-left">USB/锂电-UPS切换</text>
<text x="170.18" y="35.56" size="2.54" layer="97" rot="R180" align="center-left">5V升压</text>
<text x="218.44" y="35.56" size="2.54" layer="97" rot="R180" align="center-left">3V3降压</text>
<text x="218.44" y="83.82" size="2.54" layer="97" rot="R180" align="center-left">USBTTL</text>
<text x="218.44" y="134.62" size="2.54" layer="97" rot="R180" align="center-left">Core</text>
<text x="134.62" y="83.82" size="2.54" layer="97" rot="R180" align="center-left">BLE Upload+PMU</text>
<wire x1="68.58" y1="60.96" x2="68.58" y2="63.5" width="0.8128" layer="98"/>
<wire x1="73.66" y1="60.96" x2="73.66" y2="63.5" width="0.8128" layer="98"/>
<wire x1="172.72" y1="58.42" x2="172.72" y2="60.96" width="0.8128" layer="98"/>
<wire x1="15.24" y1="15.24" x2="12.7" y2="15.24" width="0.8128" layer="98"/>
<wire x1="12.7" y1="7.62" x2="15.24" y2="7.62" width="0.8128" layer="98"/>
<wire x1="25.4" y1="185.42" x2="22.86" y2="185.42" width="0.8128" layer="98"/>
<wire x1="25.4" y1="182.88" x2="22.86" y2="182.88" width="0.8128" layer="98"/>
<wire x1="25.4" y1="114.3" x2="22.86" y2="114.3" width="0.8128" layer="98"/>
<wire x1="172.72" y1="38.1" x2="172.72" y2="0" width="0.1524" layer="94" style="longdash"/>
</plain>
<instances>
<instance part="GND16" gate="1" x="27.94" y="43.18" rot="MR0"/>
<instance part="USB1" gate="USB" x="12.7" y="50.8" rot="MR0"/>
<instance part="U$1" gate="G$1" x="256.54" y="142.24"/>
<instance part="U$6" gate="G$1" x="256.54" y="142.24"/>
<instance part="P+6" gate="G$1" x="241.3" y="180.34"/>
<instance part="U$9" gate="+5V" x="236.22" y="180.34"/>
<instance part="GND15" gate="1" x="236.22" y="101.6"/>
<instance part="R1" gate="G$1" x="193.04" y="93.98"/>
<instance part="R4" gate="G$1" x="193.04" y="101.6"/>
<instance part="C5" gate="G$1" x="154.94" y="96.52" rot="R180"/>
<instance part="C8" gate="G$1" x="193.04" y="109.22" rot="MR270"/>
<instance part="C12" gate="G$1" x="152.4" y="114.3"/>
<instance part="R8" gate="G$1" x="147.32" y="96.52" rot="R180"/>
<instance part="IC4" gate="G$1" x="167.64" y="111.76" rot="R180"/>
<instance part="Y1" gate="G$1" x="185.42" y="121.92" rot="MR270"/>
<instance part="C15" gate="G$1" x="193.04" y="127" rot="R90"/>
<instance part="C16" gate="G$1" x="193.04" y="116.84" rot="R90"/>
<instance part="GND8" gate="1" x="198.12" y="111.76"/>
<instance part="GND5" gate="1" x="198.12" y="104.14"/>
<instance part="GND7" gate="1" x="147.32" y="86.36"/>
<instance part="IC3" gate="G$1" x="45.72" y="53.34"/>
<instance part="GND3" gate="1" x="48.26" y="43.18"/>
<instance part="GND13" gate="1" x="43.18" y="43.18"/>
<instance part="R2" gate="G$1" x="114.3" y="55.88" rot="R90"/>
<instance part="GND1" gate="1" x="114.3" y="43.18"/>
<instance part="GND2" gate="1" x="81.28" y="43.18"/>
<instance part="C1" gate="G$1" x="81.28" y="48.26"/>
<instance part="IC1" gate="G$1" x="99.06" y="55.88"/>
<instance part="R11" gate="G$1" x="20.32" y="15.24" rot="R180"/>
<instance part="U$3" gate="G$1" x="149.86" y="58.42" rot="R180"/>
<instance part="U$4" gate="G$1" x="149.86" y="48.26" rot="R180"/>
<instance part="GND9" gate="1" x="152.4" y="43.18" rot="MR0"/>
<instance part="R5" gate="G$1" x="78.74" y="58.42" rot="R270"/>
<instance part="R6" gate="G$1" x="68.58" y="55.88" rot="R90"/>
<instance part="R12" gate="G$1" x="60.96" y="55.88" rot="R90"/>
<instance part="IC5" gate="G$1" x="177.8" y="162.56"/>
<instance part="Y3" gate="G$1" x="154.94" y="180.34" smashed="yes" rot="R270">
<attribute name="NAME" x="147.32" y="179.07" size="1.27" layer="95" font="vector" align="center"/>
<attribute name="VALUE" x="147.32" y="181.61" size="1.27" layer="96" font="vector" align="center"/>
</instance>
<instance part="R3" gate="G$1" x="177.8" y="195.58"/>
<instance part="C11" gate="G$1" x="152.4" y="172.72" rot="R90"/>
<instance part="C13" gate="G$1" x="210.82" y="167.64" rot="R270"/>
<instance part="GND11" gate="1" x="144.78" y="177.8"/>
<instance part="GND12" gate="1" x="144.78" y="167.64"/>
<instance part="GND14" gate="1" x="144.78" y="137.16"/>
<instance part="GND17" gate="1" x="215.9" y="154.94"/>
<instance part="C26" gate="G$1" x="210.82" y="175.26" rot="R270"/>
<instance part="R14" gate="G$1" x="160.02" y="50.8" rot="R90"/>
<instance part="R15" gate="G$1" x="165.1" y="58.42" rot="R180"/>
<instance part="GND6" gate="1" x="160.02" y="43.18" rot="MR0"/>
<instance part="C7" gate="G$1" x="172.72" y="50.8"/>
<instance part="GND18" gate="1" x="172.72" y="43.18" rot="MR0"/>
<instance part="IC8" gate="G$1" x="200.66" y="53.34"/>
<instance part="GND27" gate="1" x="205.74" y="43.18"/>
<instance part="GND29" gate="1" x="215.9" y="43.18"/>
<instance part="C27" gate="G$1" x="215.9" y="53.34" rot="R180"/>
<instance part="D1" gate="G$1" x="33.02" y="25.4" rot="R90"/>
<instance part="T2" gate="G$1" x="25.4" y="30.48" rot="MR90"/>
<instance part="R16" gate="G$1" x="35.56" y="15.24"/>
<instance part="SW1" gate="G$2" x="60.96" y="15.24" rot="R90"/>
<instance part="GND30" gate="1" x="45.72" y="5.08"/>
<instance part="R18" gate="G$1" x="137.16" y="22.86" rot="R90"/>
<instance part="R19" gate="G$1" x="137.16" y="12.7" rot="R90"/>
<instance part="GND32" gate="1" x="137.16" y="5.08"/>
<instance part="C30" gate="G$1" x="167.64" y="22.86"/>
<instance part="C31" gate="G$1" x="160.02" y="22.86"/>
<instance part="C32" gate="G$1" x="152.4" y="22.86"/>
<instance part="GND33" gate="1" x="152.4" y="5.08"/>
<instance part="C33" gate="G$1" x="93.98" y="22.86"/>
<instance part="GND34" gate="1" x="93.98" y="5.08"/>
<instance part="C36" gate="G$1" x="86.36" y="22.86"/>
<instance part="U$5" gate="+5V" x="152.4" y="30.48"/>
<instance part="C3" gate="G$1" x="83.82" y="172.72"/>
<instance part="C9" gate="G$1" x="91.44" y="172.72"/>
<instance part="C42" gate="G$1" x="83.82" y="147.32"/>
<instance part="C43" gate="G$1" x="91.44" y="147.32"/>
<instance part="GND38" gate="1" x="91.44" y="142.24"/>
<instance part="GND43" gate="1" x="83.82" y="142.24"/>
<instance part="GND46" gate="1" x="91.44" y="160.02"/>
<instance part="C44" gate="G$1" x="96.52" y="109.22" rot="R90"/>
<instance part="C45" gate="G$1" x="96.52" y="93.98" rot="R270"/>
<instance part="C46" gate="G$1" x="10.16" y="119.38" rot="R270"/>
<instance part="C47" gate="G$1" x="10.16" y="134.62" rot="R90"/>
<instance part="Y4" gate="G$1" x="15.24" y="127" smashed="yes" rot="R270">
<attribute name="NAME" x="19.05" y="132.08" size="1.27" layer="95" font="vector" rot="R270" align="center"/>
<attribute name="VALUE" x="21.59" y="132.08" size="1.27" layer="96" font="vector" rot="R90" align="center"/>
</instance>
<instance part="Y2" gate="G$1" x="91.44" y="101.6" rot="MR270"/>
<instance part="GND47" gate="1" x="5.08" y="114.3"/>
<instance part="GND48" gate="1" x="101.6" y="88.9"/>
<instance part="IC6" gate="G$1" x="53.34" y="160.02"/>
<instance part="L2" gate="G$1" x="101.6" y="147.32"/>
<instance part="L3" gate="G$1" x="101.6" y="132.08"/>
<instance part="L4" gate="G$1" x="119.38" y="154.94" rot="R90"/>
<instance part="C49" gate="G$1" x="106.68" y="154.94" rot="R270"/>
<instance part="C50" gate="G$1" x="111.76" y="147.32"/>
<instance part="C51" gate="G$1" x="127" y="147.32"/>
<instance part="C52" gate="G$1" x="101.6" y="119.38"/>
<instance part="GND50" gate="1" x="101.6" y="114.3"/>
<instance part="GND51" gate="1" x="111.76" y="142.24"/>
<instance part="GND52" gate="1" x="127" y="142.24"/>
<instance part="ANT1" gate="G$1" x="129.54" y="154.94" rot="MR90"/>
<instance part="GND53" gate="1" x="134.62" y="142.24"/>
<instance part="R20" gate="G$1" x="22.86" y="93.98" rot="R90"/>
<instance part="R21" gate="G$1" x="15.24" y="93.98" rot="R90"/>
<instance part="GND49" gate="1" x="22.86" y="86.36"/>
<instance part="R25" gate="G$1" x="132.08" y="104.14" rot="R270"/>
<instance part="GND54" gate="1" x="132.08" y="88.9" rot="MR0"/>
<instance part="R26" gate="G$1" x="124.46" y="104.14" rot="R270"/>
<instance part="GND55" gate="1" x="127" y="88.9" rot="MR0"/>
<instance part="D3" gate="G$1" x="210.82" y="190.5" rot="R180"/>
<instance part="U$8" gate="+5V" x="215.9" y="193.04"/>
<instance part="IC7" gate="G$1" x="193.04" y="15.24"/>
<instance part="C2" gate="G$1" x="177.8" y="10.16"/>
<instance part="GND10" gate="1" x="198.12" y="5.08"/>
<instance part="GND35" gate="1" x="177.8" y="5.08"/>
<instance part="GND36" gate="1" x="208.28" y="5.08"/>
<instance part="C4" gate="G$1" x="208.28" y="15.24" rot="R180"/>
<instance part="U$2" gate="+5V" x="177.8" y="22.86"/>
<instance part="P+1" gate="G$1" x="208.28" y="22.86"/>
<instance part="R9" gate="G$1" x="116.84" y="104.14" rot="R270"/>
<instance part="GND20" gate="1" x="116.84" y="88.9" rot="MR0"/>
<instance part="R10" gate="G$1" x="109.22" y="104.14" rot="R270"/>
<instance part="GND21" gate="1" x="111.76" y="88.9" rot="MR0"/>
<instance part="C6" gate="G$1" x="210.82" y="160.02" rot="R270"/>
<instance part="R13" gate="G$1" x="17.78" y="165.1"/>
<instance part="R22" gate="G$1" x="12.7" y="160.02"/>
<instance part="R24" gate="G$1" x="12.7" y="170.18"/>
<instance part="C17" gate="G$1" x="210.82" y="182.88" rot="R90"/>
<instance part="U$7" gate="G$1" x="22.86" y="101.6" rot="R90"/>
<instance part="U$10" gate="G$1" x="15.24" y="104.14" rot="R90"/>
<instance part="U$11" gate="G$1" x="205.74" y="142.24"/>
<instance part="U$12" gate="G$1" x="205.74" y="144.78"/>
<instance part="U$13" gate="G$1" x="205.74" y="147.32"/>
<instance part="U$14" gate="G$1" x="149.86" y="195.58" rot="R180"/>
<instance part="U$15" gate="G$1" x="200.66" y="190.5" rot="R90"/>
<instance part="U$16" gate="G$1" x="149.86" y="139.7" rot="R270"/>
<instance part="U$17" gate="G$1" x="200.66" y="177.8"/>
<instance part="U$18" gate="G$1" x="200.66" y="175.26"/>
<instance part="U$19" gate="G$1" x="17.78" y="20.32" rot="R90"/>
<instance part="U$20" gate="G$1" x="17.78" y="30.48" rot="R90"/>
<instance part="R7" gate="G$1" x="20.32" y="7.62" rot="R180"/>
<instance part="U$21" gate="G$1" x="137.16" y="27.94" rot="R90"/>
<instance part="U$23" gate="G$1" x="215.9" y="20.32" rot="R90"/>
<instance part="U$25" gate="G$1" x="68.58" y="48.26" rot="R270"/>
<instance part="C18" gate="G$1" x="215.9" y="15.24" rot="R180"/>
<instance part="LED1" gate="LED$1" x="129.54" y="93.98" rot="MR90"/>
<instance part="LED2" gate="LED$1" x="114.3" y="93.98" rot="R90"/>
<instance part="U$27" gate="G$1" x="172.72" y="58.42"/>
<instance part="L5" gate="G$1" x="101.6" y="27.94" rot="R90"/>
<instance part="GND19" gate="1" x="119.38" y="5.08"/>
<instance part="IC2" gate="G$1" x="119.38" y="22.86"/>
<instance part="C10" gate="G$1" x="144.78" y="22.86"/>
<instance part="R17" gate="G$1" x="35.56" y="7.62" rot="R180"/>
<instance part="CON1" gate="G$1" x="132.08" y="55.88" rot="R90"/>
<instance part="GND4" gate="1" x="139.7" y="43.18"/>
<instance part="GND22" gate="1" x="50.8" y="5.08"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="USB1" gate="USB" pin="GND"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="25.4" y1="50.8" x2="22.86" y2="50.8" width="0.1524" layer="91"/>
<wire x1="25.4" y1="45.72" x2="25.4" y2="50.8" width="0.1524" layer="91"/>
<pinref part="USB1" gate="USB" pin="G1"/>
<wire x1="22.86" y1="45.72" x2="25.4" y2="45.72" width="0.1524" layer="91"/>
<junction x="25.4" y="45.72"/>
<wire x1="27.94" y1="45.72" x2="25.4" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="236.22" y1="109.22" x2="236.22" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="GND"/>
<junction x="236.22" y="109.22"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<pinref part="Y1" gate="G$1" pin="4"/>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="195.58" y1="127" x2="198.12" y2="127" width="0.1524" layer="91"/>
<wire x1="198.12" y1="127" x2="198.12" y2="124.46" width="0.1524" layer="91"/>
<wire x1="198.12" y1="124.46" x2="198.12" y2="119.38" width="0.1524" layer="91"/>
<wire x1="198.12" y1="119.38" x2="198.12" y2="116.84" width="0.1524" layer="91"/>
<wire x1="198.12" y1="116.84" x2="198.12" y2="114.3" width="0.1524" layer="91"/>
<wire x1="195.58" y1="116.84" x2="198.12" y2="116.84" width="0.1524" layer="91"/>
<junction x="198.12" y="116.84"/>
<wire x1="190.5" y1="119.38" x2="198.12" y2="119.38" width="0.1524" layer="91"/>
<junction x="198.12" y="119.38"/>
<wire x1="190.5" y1="124.46" x2="198.12" y2="124.46" width="0.1524" layer="91"/>
<junction x="198.12" y="124.46"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="198.12" y1="106.68" x2="198.12" y2="109.22" width="0.1524" layer="91"/>
<wire x1="198.12" y1="109.22" x2="195.58" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="147.32" y1="88.9" x2="147.32" y2="91.44" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="IC4" gate="G$1" pin="GND"/>
<wire x1="147.32" y1="91.44" x2="152.4" y2="96.52" width="0.1524" layer="91"/>
<wire x1="177.8" y1="101.6" x2="180.34" y2="101.6" width="0.1524" layer="91"/>
<wire x1="180.34" y1="101.6" x2="180.34" y2="91.44" width="0.1524" layer="91"/>
<wire x1="147.32" y1="91.44" x2="154.94" y2="91.44" width="0.1524" layer="91"/>
<wire x1="154.94" y1="91.44" x2="154.94" y2="93.98" width="0.1524" layer="91"/>
<junction x="147.32" y="91.44"/>
<wire x1="154.94" y1="91.44" x2="180.34" y2="91.44" width="0.1524" layer="91"/>
<junction x="154.94" y="91.44"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="5"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="2"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="114.3" y1="50.8" x2="114.3" y2="48.26" width="0.1524" layer="91"/>
<wire x1="114.3" y1="48.26" x2="111.76" y2="48.26" width="0.1524" layer="91"/>
<wire x1="114.3" y1="48.26" x2="114.3" y2="45.72" width="0.1524" layer="91"/>
<junction x="114.3" y="48.26"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="Y3" gate="G$1" pin="2"/>
<wire x1="147.32" y1="180.34" x2="144.78" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="1"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="149.86" y1="172.72" x2="144.78" y2="172.72" width="0.1524" layer="91"/>
<wire x1="144.78" y1="172.72" x2="144.78" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="154.94" y1="142.24" x2="144.78" y2="142.24" width="0.1524" layer="91"/>
<wire x1="154.94" y1="139.7" x2="149.86" y2="139.7" width="0.1524" layer="91"/>
<wire x1="144.78" y1="139.7" x2="149.86" y2="139.7" width="0.1524" layer="91"/>
<junction x="144.78" y="142.24"/>
<junction x="149.86" y="139.7"/>
<pinref part="IC5" gate="G$1" pin="AGND"/>
<pinref part="IC5" gate="G$1" pin="GND1"/>
<pinref part="IC5" gate="G$1" pin="GND2"/>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="U$16" gate="G$1" pin="P$1"/>
<junction x="149.86" y="139.7"/>
<wire x1="144.78" y1="142.24" x2="144.78" y2="139.7" width="0.1524" layer="91"/>
<junction x="144.78" y="139.7"/>
<wire x1="154.94" y1="144.78" x2="144.78" y2="144.78" width="0.1524" layer="91"/>
<wire x1="144.78" y1="144.78" x2="144.78" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="213.36" y1="175.26" x2="215.9" y2="175.26" width="0.1524" layer="91"/>
<wire x1="215.9" y1="175.26" x2="215.9" y2="167.64" width="0.1524" layer="91"/>
<junction x="215.9" y="167.64"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="215.9" y1="167.64" x2="213.36" y2="167.64" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="C26" gate="G$1" pin="1"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="215.9" y1="157.48" x2="215.9" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="-"/>
<wire x1="215.9" y1="160.02" x2="215.9" y2="167.64" width="0.1524" layer="91"/>
<wire x1="213.36" y1="182.88" x2="215.9" y2="182.88" width="0.1524" layer="91"/>
<wire x1="215.9" y1="182.88" x2="215.9" y2="175.26" width="0.1524" layer="91"/>
<junction x="215.9" y="175.26"/>
<wire x1="213.36" y1="160.02" x2="215.9" y2="160.02" width="0.1524" layer="91" style="longdash"/>
<junction x="215.9" y="160.02"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="172.72" y1="45.72" x2="172.72" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC8" gate="G$1" pin="GND"/>
<pinref part="GND27" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="215.9" y1="50.8" x2="215.9" y2="45.72" width="0.1524" layer="91"/>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="C27" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="GND32" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="-"/>
<wire x1="152.4" y1="20.32" x2="152.4" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="152.4" y1="15.24" x2="152.4" y2="7.62" width="0.1524" layer="91"/>
<wire x1="152.4" y1="15.24" x2="160.02" y2="15.24" width="0.1524" layer="91"/>
<wire x1="160.02" y1="15.24" x2="160.02" y2="20.32" width="0.1524" layer="91"/>
<junction x="152.4" y="15.24"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="160.02" y1="15.24" x2="167.64" y2="15.24" width="0.1524" layer="91"/>
<wire x1="167.64" y1="15.24" x2="167.64" y2="20.32" width="0.1524" layer="91"/>
<junction x="160.02" y="15.24"/>
<pinref part="GND33" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="93.98" y1="7.62" x2="93.98" y2="15.24" width="0.1524" layer="91"/>
<wire x1="93.98" y1="15.24" x2="93.98" y2="20.32" width="0.1524" layer="91"/>
<junction x="93.98" y="15.24"/>
<wire x1="86.36" y1="20.32" x2="86.36" y2="15.24" width="0.1524" layer="91"/>
<wire x1="86.36" y1="15.24" x2="93.98" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="P$1"/>
<wire x1="149.86" y1="48.26" x2="152.4" y2="48.26" width="0.1524" layer="91"/>
<wire x1="152.4" y1="48.26" x2="152.4" y2="45.72" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C43" gate="G$1" pin="2"/>
<pinref part="GND38" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C42" gate="G$1" pin="2"/>
<pinref part="GND43" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="83.82" y1="165.1" x2="83.82" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="83.82" y1="165.1" x2="91.44" y2="165.1" width="0.1524" layer="91"/>
<wire x1="91.44" y1="165.1" x2="91.44" y2="170.18" width="0.1524" layer="91"/>
<junction x="91.44" y="165.1"/>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="91.44" y1="162.56" x2="91.44" y2="165.1" width="0.1524" layer="91"/>
<wire x1="81.28" y1="165.1" x2="83.82" y2="165.1" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="VSS1"/>
<junction x="83.82" y="165.1"/>
</segment>
<segment>
<pinref part="C47" gate="G$1" pin="1"/>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="7.62" y1="134.62" x2="5.08" y2="134.62" width="0.1524" layer="91"/>
<wire x1="5.08" y1="134.62" x2="5.08" y2="119.38" width="0.1524" layer="91"/>
<wire x1="5.08" y1="119.38" x2="7.62" y2="119.38" width="0.1524" layer="91"/>
<wire x1="5.08" y1="119.38" x2="5.08" y2="116.84" width="0.1524" layer="91"/>
<junction x="5.08" y="119.38"/>
<pinref part="GND47" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C44" gate="G$1" pin="2"/>
<pinref part="C45" gate="G$1" pin="1"/>
<pinref part="GND48" gate="1" pin="GND"/>
<pinref part="Y2" gate="G$1" pin="2"/>
<pinref part="Y2" gate="G$1" pin="4"/>
<wire x1="99.06" y1="93.98" x2="101.6" y2="93.98" width="0.1524" layer="91"/>
<wire x1="101.6" y1="93.98" x2="101.6" y2="91.44" width="0.1524" layer="91"/>
<wire x1="96.52" y1="99.06" x2="101.6" y2="99.06" width="0.1524" layer="91"/>
<wire x1="101.6" y1="99.06" x2="101.6" y2="93.98" width="0.1524" layer="91"/>
<junction x="101.6" y="93.98"/>
<wire x1="96.52" y1="104.14" x2="101.6" y2="104.14" width="0.1524" layer="91"/>
<wire x1="101.6" y1="104.14" x2="101.6" y2="99.06" width="0.1524" layer="91"/>
<junction x="101.6" y="99.06"/>
<wire x1="99.06" y1="109.22" x2="101.6" y2="109.22" width="0.1524" layer="91"/>
<wire x1="101.6" y1="109.22" x2="101.6" y2="104.14" width="0.1524" layer="91"/>
<junction x="101.6" y="104.14"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="2"/>
<pinref part="GND50" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="2"/>
<pinref part="GND51" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C51" gate="G$1" pin="2"/>
<pinref part="GND52" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND53" gate="1" pin="GND"/>
<pinref part="ANT1" gate="G$1" pin="2"/>
<wire x1="134.62" y1="144.78" x2="134.62" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="GND49" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="GND"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="GND35" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="208.28" y1="12.7" x2="208.28" y2="10.16" width="0.1524" layer="91"/>
<pinref part="GND36" gate="1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="208.28" y1="10.16" x2="208.28" y2="7.62" width="0.1524" layer="91"/>
<wire x1="215.9" y1="12.7" x2="215.9" y2="10.16" width="0.1524" layer="91"/>
<wire x1="215.9" y1="10.16" x2="208.28" y2="10.16" width="0.1524" layer="91"/>
<junction x="208.28" y="10.16"/>
</segment>
<segment>
<pinref part="GND54" gate="1" pin="GND"/>
<pinref part="LED1" gate="LED$1" pin="3"/>
</segment>
<segment>
<pinref part="GND55" gate="1" pin="GND"/>
<pinref part="LED1" gate="LED$1" pin="1"/>
</segment>
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="LED2" gate="LED$1" pin="1"/>
</segment>
<segment>
<pinref part="GND21" gate="1" pin="GND"/>
<pinref part="LED2" gate="LED$1" pin="3"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="IC2" gate="G$1" pin="GND"/>
<wire x1="119.38" y1="12.7" x2="119.38" y2="10.16" width="0.1524" layer="91"/>
<wire x1="119.38" y1="10.16" x2="119.38" y2="7.62" width="0.1524" layer="91"/>
<junction x="119.38" y="10.16"/>
<pinref part="IC2" gate="G$1" pin="G"/>
<wire x1="121.92" y1="12.7" x2="121.92" y2="10.16" width="0.1524" layer="91"/>
<wire x1="121.92" y1="10.16" x2="119.38" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="40.64" y1="7.62" x2="43.18" y2="7.62" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="43.18" y1="7.62" x2="45.72" y2="7.62" width="0.1524" layer="91"/>
<wire x1="40.64" y1="15.24" x2="43.18" y2="15.24" width="0.1524" layer="91"/>
<wire x1="43.18" y1="15.24" x2="43.18" y2="7.62" width="0.1524" layer="91"/>
<junction x="43.18" y="7.62"/>
<pinref part="GND30" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="1"/>
<wire x1="137.16" y1="53.34" x2="139.7" y2="53.34" width="0.1524" layer="91"/>
<wire x1="139.7" y1="53.34" x2="139.7" y2="48.26" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="CON1" gate="G$1" pin="GND1"/>
<wire x1="139.7" y1="48.26" x2="139.7" y2="45.72" width="0.1524" layer="91"/>
<wire x1="129.54" y1="66.04" x2="129.54" y2="68.58" width="0.1524" layer="91"/>
<wire x1="129.54" y1="68.58" x2="124.46" y2="68.58" width="0.1524" layer="91"/>
<wire x1="124.46" y1="68.58" x2="124.46" y2="43.18" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="GND"/>
<wire x1="124.46" y1="43.18" x2="129.54" y2="43.18" width="0.1524" layer="91"/>
<wire x1="129.54" y1="43.18" x2="129.54" y2="45.72" width="0.1524" layer="91"/>
<wire x1="129.54" y1="43.18" x2="137.16" y2="43.18" width="0.1524" layer="91"/>
<wire x1="137.16" y1="43.18" x2="137.16" y2="48.26" width="0.1524" layer="91"/>
<junction x="129.54" y="43.18"/>
<wire x1="137.16" y1="48.26" x2="139.7" y2="48.26" width="0.1524" layer="91"/>
<junction x="139.7" y="48.26"/>
</segment>
<segment>
<pinref part="SW1" gate="G$2" pin="A"/>
<wire x1="53.34" y1="10.16" x2="50.8" y2="10.16" width="0.1524" layer="91"/>
<wire x1="50.8" y1="10.16" x2="50.8" y2="7.62" width="0.1524" layer="91"/>
<pinref part="GND22" gate="1" pin="GND"/>
</segment>
</net>
<net name="VBUS" class="0">
<segment>
<pinref part="USB1" gate="USB" pin="VBUS"/>
<wire x1="25.4" y1="60.96" x2="22.86" y2="60.96" width="0.1524" layer="91"/>
<wire x1="25.4" y1="60.96" x2="25.4" y2="66.04" width="0.1524" layer="91"/>
<label x="25.4" y="66.04" size="1.27" layer="95" font="vector" rot="MR90" xref="yes"/>
</segment>
<segment>
<label x="147.32" y="104.14" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<pinref part="IC4" gate="G$1" pin="VCC"/>
<wire x1="157.48" y1="101.6" x2="154.94" y2="101.6" width="0.1524" layer="91"/>
<wire x1="154.94" y1="101.6" x2="147.32" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="142.24" y1="96.52" x2="147.32" y2="101.6" width="0.1524" layer="91"/>
<junction x="147.32" y="101.6"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="154.94" y1="99.06" x2="154.94" y2="101.6" width="0.1524" layer="91"/>
<junction x="154.94" y="101.6"/>
<wire x1="147.32" y1="104.14" x2="147.32" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="3"/>
<wire x1="43.18" y1="63.5" x2="43.18" y2="66.04" width="0.1524" layer="91"/>
<label x="43.18" y="66.04" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="78.74" y1="63.5" x2="78.74" y2="66.04" width="0.1524" layer="91"/>
<label x="78.74" y="66.04" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="T2" gate="G$1" pin="G"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="27.94" y1="20.32" x2="27.94" y2="25.4" width="0.1524" layer="91"/>
<wire x1="27.94" y1="20.32" x2="17.78" y2="20.32" width="0.1524" layer="91"/>
<junction x="27.94" y="20.32"/>
<label x="12.7" y="20.32" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<wire x1="17.78" y1="20.32" x2="12.7" y2="20.32" width="0.1524" layer="91"/>
<wire x1="27.94" y1="15.24" x2="27.94" y2="20.32" width="0.1524" layer="91"/>
<junction x="27.94" y="20.32"/>
<wire x1="33.02" y1="22.86" x2="33.02" y2="20.32" width="0.1524" layer="91"/>
<wire x1="33.02" y1="20.32" x2="27.94" y2="20.32" width="0.1524" layer="91"/>
<junction x="27.94" y="20.32"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="25.4" y1="15.24" x2="27.94" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$19" gate="G$1" pin="P$1"/>
<junction x="17.78" y="20.32"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="+5V"/>
<wire x1="236.22" y1="175.26" x2="236.22" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="+5V"/>
<junction x="236.22" y="175.26"/>
<pinref part="U$9" gate="+5V" pin="+5V"/>
</segment>
<segment>
<wire x1="132.08" y1="27.94" x2="134.62" y2="27.94" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="134.62" y1="27.94" x2="137.16" y2="27.94" width="0.1524" layer="91"/>
<wire x1="137.16" y1="27.94" x2="144.78" y2="27.94" width="0.1524" layer="91"/>
<wire x1="144.78" y1="27.94" x2="152.4" y2="27.94" width="0.1524" layer="91"/>
<wire x1="152.4" y1="27.94" x2="160.02" y2="27.94" width="0.1524" layer="91"/>
<wire x1="160.02" y1="27.94" x2="167.64" y2="27.94" width="0.1524" layer="91"/>
<junction x="137.16" y="27.94"/>
<pinref part="C32" gate="G$1" pin="+"/>
<wire x1="152.4" y1="25.4" x2="152.4" y2="27.94" width="0.1524" layer="91"/>
<junction x="152.4" y="27.94"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="160.02" y1="25.4" x2="160.02" y2="27.94" width="0.1524" layer="91"/>
<junction x="160.02" y="27.94"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="167.64" y1="25.4" x2="167.64" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U$5" gate="+5V" pin="+5V"/>
<pinref part="U$21" gate="G$1" pin="P$1"/>
<wire x1="152.4" y1="30.48" x2="152.4" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="144.78" y1="25.4" x2="144.78" y2="27.94" width="0.1524" layer="91"/>
<junction x="144.78" y="27.94"/>
<pinref part="IC2" gate="G$1" pin="VOUT1"/>
<pinref part="IC2" gate="G$1" pin="VOUT2"/>
<wire x1="132.08" y1="25.4" x2="134.62" y2="25.4" width="0.1524" layer="91"/>
<wire x1="134.62" y1="25.4" x2="134.62" y2="27.94" width="0.1524" layer="91"/>
<junction x="134.62" y="27.94"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="213.36" y1="190.5" x2="215.9" y2="190.5" width="0.1524" layer="91"/>
<wire x1="215.9" y1="190.5" x2="215.9" y2="193.04" width="0.1524" layer="91"/>
<pinref part="U$8" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="ON/OFF"/>
<pinref part="IC7" gate="G$1" pin="VIN"/>
<wire x1="177.8" y1="17.78" x2="180.34" y2="17.78" width="0.1524" layer="91"/>
<wire x1="180.34" y1="20.32" x2="177.8" y2="20.32" width="0.1524" layer="91"/>
<wire x1="177.8" y1="17.78" x2="177.8" y2="20.32" width="0.1524" layer="91"/>
<junction x="177.8" y="20.32"/>
<pinref part="U$2" gate="+5V" pin="+5V"/>
<wire x1="177.8" y1="20.32" x2="177.8" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="+3V3"/>
<wire x1="241.3" y1="175.26" x2="241.3" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="+3V3"/>
<junction x="241.3" y="175.26"/>
<pinref part="P+6" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="VOUT"/>
<wire x1="205.74" y1="20.32" x2="208.28" y2="20.32" width="0.1524" layer="91"/>
<wire x1="208.28" y1="20.32" x2="215.9" y2="20.32" width="0.1524" layer="91"/>
<junction x="215.9" y="20.32"/>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="P+1" gate="G$1" pin="+3V3"/>
<pinref part="U$23" gate="G$1" pin="P$1"/>
<wire x1="208.28" y1="22.86" x2="208.28" y2="20.32" width="0.1524" layer="91"/>
<junction x="208.28" y="20.32"/>
<wire x1="208.28" y1="20.32" x2="208.28" y2="17.78" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="215.9" y1="20.32" x2="215.9" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB_D_P" class="0">
<segment>
<pinref part="USB1" gate="USB" pin="D+"/>
<wire x1="22.86" y1="55.88" x2="25.4" y2="55.88" width="0.1524" layer="91"/>
<label x="25.4" y="55.88" size="1.27" layer="95" font="vector" rot="MR180" xref="yes"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="D+"/>
<wire x1="180.34" y1="111.76" x2="177.8" y2="111.76" width="0.1524" layer="91"/>
<label x="180.34" y="111.76" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="6"/>
<wire x1="50.8" y1="63.5" x2="50.8" y2="66.04" width="0.1524" layer="91"/>
<label x="50.8" y="66.04" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="USB_D_N" class="0">
<segment>
<pinref part="USB1" gate="USB" pin="D-"/>
<wire x1="22.86" y1="58.42" x2="25.4" y2="58.42" width="0.1524" layer="91"/>
<label x="25.4" y="58.42" size="1.27" layer="95" font="vector" rot="MR180" xref="yes"/>
</segment>
<segment>
<wire x1="177.8" y1="114.3" x2="180.34" y2="114.3" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="D-"/>
<label x="180.34" y="114.3" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="1"/>
<wire x1="40.64" y1="63.5" x2="40.64" y2="66.04" width="0.1524" layer="91"/>
<label x="40.64" y="66.04" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="D13" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D13/SCK"/>
<wire x1="276.86" y1="175.26" x2="276.86" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="D13/SCK"/>
<junction x="276.86" y="175.26"/>
<label x="276.86" y="180.34" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="205.74" y1="142.24" x2="198.12" y2="142.24" width="0.1524" layer="91"/>
<label x="200.66" y="142.24" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PB5(SCK)"/>
<pinref part="U$11" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="D12" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D12/MISO"/>
<wire x1="271.78" y1="175.26" x2="271.78" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="D12/MISO"/>
<junction x="271.78" y="175.26"/>
<label x="271.78" y="180.34" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="205.74" y1="144.78" x2="198.12" y2="144.78" width="0.1524" layer="91"/>
<label x="200.66" y="144.78" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PB4(MISO)"/>
<pinref part="U$12" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="D11" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D11/MOSI"/>
<wire x1="266.7" y1="175.26" x2="266.7" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="D11/MOSI"/>
<junction x="266.7" y="175.26"/>
<label x="266.7" y="180.34" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="205.74" y1="147.32" x2="198.12" y2="147.32" width="0.1524" layer="91"/>
<label x="200.66" y="147.32" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PB3(MOSI)"/>
<pinref part="U$13" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D3"/>
<wire x1="261.62" y1="109.22" x2="261.62" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="D3"/>
<junction x="261.62" y="109.22"/>
<label x="261.62" y="104.14" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="200.66" y1="170.18" x2="198.12" y2="170.18" width="0.1524" layer="91"/>
<label x="200.66" y="170.18" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PD3(INT1)"/>
</segment>
</net>
<net name="D8" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D8"/>
<wire x1="251.46" y1="175.26" x2="251.46" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="D8"/>
<junction x="251.46" y="175.26"/>
<label x="251.46" y="180.34" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="200.66" y1="154.94" x2="198.12" y2="154.94" width="0.1524" layer="91"/>
<label x="200.66" y="154.94" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PB0(ICP)"/>
</segment>
</net>
<net name="D7" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D7"/>
<wire x1="246.38" y1="175.26" x2="246.38" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="D7"/>
<junction x="246.38" y="175.26"/>
<label x="246.38" y="180.34" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="200.66" y1="160.02" x2="198.12" y2="160.02" width="0.1524" layer="91"/>
<label x="200.66" y="160.02" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PD7(AIN1)"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D6"/>
<wire x1="246.38" y1="109.22" x2="246.38" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="D6"/>
<junction x="246.38" y="109.22"/>
<label x="246.38" y="104.14" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="200.66" y1="162.56" x2="198.12" y2="162.56" width="0.1524" layer="91"/>
<label x="200.66" y="162.56" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PD6(AIN0)"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D5"/>
<wire x1="251.46" y1="109.22" x2="251.46" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="D5"/>
<junction x="251.46" y="109.22"/>
<label x="251.46" y="104.14" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="200.66" y1="165.1" x2="198.12" y2="165.1" width="0.1524" layer="91"/>
<label x="200.66" y="165.1" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PD5(T1)"/>
</segment>
</net>
<net name="D4" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D4"/>
<wire x1="256.54" y1="109.22" x2="256.54" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="D4"/>
<junction x="256.54" y="109.22"/>
<label x="256.54" y="104.14" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="200.66" y1="167.64" x2="198.12" y2="167.64" width="0.1524" layer="91"/>
<label x="200.66" y="167.64" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PD4(T0)"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="RESET"/>
<wire x1="241.3" y1="109.22" x2="241.3" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="RESET"/>
<junction x="241.3" y="109.22"/>
<label x="241.3" y="104.14" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<label x="152.4" y="119.38" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<wire x1="152.4" y1="116.84" x2="152.4" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="PC6(RESET)"/>
<wire x1="152.4" y1="187.96" x2="154.94" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="172.72" y1="195.58" x2="152.4" y2="195.58" width="0.1524" layer="91"/>
<junction x="152.4" y="187.96"/>
<pinref part="U$14" gate="G$1" pin="P$1"/>
<wire x1="152.4" y1="195.58" x2="152.4" y2="187.96" width="0.1524" layer="91"/>
<label x="149.86" y="187.96" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<wire x1="152.4" y1="187.96" x2="149.86" y2="187.96" width="0.1524" layer="91"/>
<wire x1="149.86" y1="195.58" x2="152.4" y2="195.58" width="0.1524" layer="91"/>
<junction x="152.4" y="195.58"/>
</segment>
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="7.62" y1="170.18" x2="5.08" y2="170.18" width="0.1524" layer="91"/>
<label x="5.08" y="170.18" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="A0"/>
<wire x1="292.1" y1="157.48" x2="294.64" y2="157.48" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="A0"/>
<junction x="292.1" y="157.48"/>
<label x="294.64" y="157.48" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<wire x1="149.86" y1="167.64" x2="154.94" y2="167.64" width="0.1524" layer="91"/>
<label x="149.86" y="167.64" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PC0(ADC0)"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="A1"/>
<wire x1="292.1" y1="152.4" x2="294.64" y2="152.4" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="A1"/>
<junction x="292.1" y="152.4"/>
<label x="294.64" y="152.4" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<wire x1="149.86" y1="165.1" x2="154.94" y2="165.1" width="0.1524" layer="91"/>
<label x="149.86" y="165.1" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PC1(ADC1)"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="A2"/>
<wire x1="292.1" y1="147.32" x2="294.64" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="A2"/>
<junction x="292.1" y="147.32"/>
<label x="294.64" y="147.32" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<wire x1="149.86" y1="162.56" x2="154.94" y2="162.56" width="0.1524" layer="91"/>
<label x="149.86" y="162.56" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PC2(ADC2)"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="A3"/>
<wire x1="292.1" y1="142.24" x2="294.64" y2="142.24" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="A3"/>
<junction x="292.1" y="142.24"/>
<label x="294.64" y="142.24" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<wire x1="149.86" y1="160.02" x2="154.94" y2="160.02" width="0.1524" layer="91"/>
<label x="149.86" y="160.02" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PC3(ADC3)"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="A6"/>
<wire x1="292.1" y1="127" x2="294.64" y2="127" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="A6"/>
<junction x="292.1" y="127"/>
<label x="294.64" y="127" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<wire x1="149.86" y1="152.4" x2="154.94" y2="152.4" width="0.1524" layer="91"/>
<label x="149.86" y="152.4" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="ADC6"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="A7"/>
<wire x1="292.1" y1="121.92" x2="294.64" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="A7"/>
<junction x="292.1" y="121.92"/>
<label x="294.64" y="121.92" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<wire x1="149.86" y1="149.86" x2="154.94" y2="149.86" width="0.1524" layer="91"/>
<label x="149.86" y="149.86" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="ADC7"/>
</segment>
</net>
<net name="D0" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="RX0"/>
<wire x1="276.86" y1="109.22" x2="276.86" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="RX0"/>
<junction x="276.86" y="109.22"/>
<label x="276.86" y="104.14" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="198.12" y1="93.98" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<label x="200.66" y="93.98" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<wire x1="200.66" y1="177.8" x2="198.12" y2="177.8" width="0.1524" layer="91"/>
<label x="200.66" y="177.8" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PD0(RXD)"/>
<pinref part="U$17" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="12.7" y1="165.1" x2="5.08" y2="165.1" width="0.1524" layer="91"/>
<label x="5.08" y="165.1" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="D1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="TX1"/>
<wire x1="271.78" y1="109.22" x2="271.78" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="TX1"/>
<junction x="271.78" y="109.22"/>
<label x="271.78" y="104.14" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="198.12" y1="101.6" x2="200.66" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<label x="200.66" y="101.6" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<wire x1="200.66" y1="175.26" x2="198.12" y2="175.26" width="0.1524" layer="91"/>
<label x="200.66" y="175.26" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PD1(TXD)"/>
<pinref part="U$18" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="7.62" y1="160.02" x2="5.08" y2="160.02" width="0.1524" layer="91"/>
<label x="5.08" y="160.02" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D2"/>
<wire x1="266.7" y1="109.22" x2="266.7" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="D2"/>
<junction x="266.7" y="109.22"/>
<label x="266.7" y="104.14" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="200.66" y1="172.72" x2="198.12" y2="172.72" width="0.1524" layer="91"/>
<label x="200.66" y="172.72" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PD2(IND0)"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="AREF"/>
<wire x1="292.1" y1="162.56" x2="294.64" y2="162.56" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="AREF"/>
<junction x="292.1" y="162.56"/>
<label x="294.64" y="162.56" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="P$1"/>
<wire x1="149.86" y1="58.42" x2="152.4" y2="58.42" width="0.1524" layer="91"/>
<wire x1="152.4" y1="58.42" x2="152.4" y2="60.96" width="0.1524" layer="91"/>
<label x="152.4" y="60.96" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<pinref part="R15" gate="G$1" pin="2"/>
<junction x="152.4" y="58.42"/>
<wire x1="152.4" y1="58.42" x2="160.02" y2="58.42" width="0.1524" layer="91"/>
<junction x="152.4" y="58.42"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="BAT"/>
<wire x1="86.36" y1="63.5" x2="83.82" y2="63.5" width="0.1524" layer="91"/>
<wire x1="83.82" y1="63.5" x2="83.82" y2="66.04" width="0.1524" layer="91"/>
<label x="83.82" y="66.04" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IC8" gate="G$1" pin="ON/OFF"/>
<pinref part="IC8" gate="G$1" pin="VIN"/>
<wire x1="185.42" y1="55.88" x2="187.96" y2="55.88" width="0.1524" layer="91"/>
<wire x1="187.96" y1="58.42" x2="185.42" y2="58.42" width="0.1524" layer="91"/>
<label x="185.42" y="60.96" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<wire x1="185.42" y1="55.88" x2="185.42" y2="58.42" width="0.1524" layer="91"/>
<junction x="185.42" y="58.42"/>
<wire x1="185.42" y1="58.42" x2="185.42" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T2" gate="G$1" pin="D"/>
<wire x1="20.32" y1="30.48" x2="17.78" y2="30.48" width="0.1524" layer="91"/>
<label x="12.7" y="30.48" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="U$20" gate="G$1" pin="P$1"/>
<wire x1="17.78" y1="30.48" x2="12.7" y2="30.48" width="0.1524" layer="91"/>
<junction x="17.78" y="30.48"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="2"/>
<wire x1="137.16" y1="58.42" x2="139.7" y2="58.42" width="0.1524" layer="91"/>
<label x="139.7" y="60.96" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<wire x1="139.7" y1="58.42" x2="139.7" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D9" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D9"/>
<wire x1="256.54" y1="175.26" x2="256.54" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="D9"/>
<junction x="256.54" y="175.26"/>
<label x="256.54" y="180.34" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="200.66" y1="152.4" x2="198.12" y2="152.4" width="0.1524" layer="91"/>
<label x="200.66" y="152.4" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PB1(OC1)"/>
</segment>
</net>
<net name="D10" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D10"/>
<wire x1="261.62" y1="175.26" x2="261.62" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="D10"/>
<junction x="261.62" y="175.26"/>
<label x="261.62" y="180.34" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="200.66" y1="149.86" x2="198.12" y2="149.86" width="0.1524" layer="91"/>
<label x="200.66" y="149.86" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PB2(SS)"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="IC4" gate="G$1" pin="DTR"/>
<wire x1="152.4" y1="111.76" x2="152.4" y2="109.22" width="0.1524" layer="91"/>
<wire x1="152.4" y1="109.22" x2="157.48" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="187.96" y1="93.98" x2="182.88" y2="93.98" width="0.1524" layer="91"/>
<wire x1="182.88" y1="93.98" x2="182.88" y2="104.14" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="TX"/>
<wire x1="182.88" y1="104.14" x2="177.8" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="187.96" y1="101.6" x2="185.42" y2="101.6" width="0.1524" layer="91"/>
<wire x1="185.42" y1="101.6" x2="185.42" y2="106.68" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="RX"/>
<wire x1="185.42" y1="106.68" x2="177.8" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="XO"/>
<wire x1="180.34" y1="119.38" x2="177.8" y2="119.38" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="1"/>
<wire x1="185.42" y1="127" x2="180.34" y2="127" width="0.1524" layer="91"/>
<wire x1="180.34" y1="127" x2="180.34" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="190.5" y1="127" x2="185.42" y2="127" width="0.1524" layer="91"/>
<junction x="185.42" y="127"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="XI"/>
<wire x1="185.42" y1="116.84" x2="177.8" y2="116.84" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="3"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="190.5" y1="116.84" x2="185.42" y2="116.84" width="0.1524" layer="91"/>
<junction x="185.42" y="116.84"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="IC4" gate="G$1" pin="V3"/>
<wire x1="190.5" y1="109.22" x2="177.8" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PROG"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="111.76" y1="63.5" x2="114.3" y2="63.5" width="0.1524" layer="91"/>
<wire x1="114.3" y1="63.5" x2="114.3" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO-VBUS" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="15.24" y1="15.24" x2="12.7" y2="15.24" width="0.1524" layer="91"/>
<label x="12.7" y="15.24" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="30.48" y1="15.24" x2="30.48" y2="12.7" width="0.1524" layer="91"/>
<wire x1="30.48" y1="12.7" x2="15.24" y2="12.7" width="0.1524" layer="91"/>
<wire x1="15.24" y1="12.7" x2="15.24" y2="15.24" width="0.1524" layer="91"/>
<junction x="15.24" y="15.24"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="P0.04/AIN5"/>
<wire x1="25.4" y1="177.8" x2="22.86" y2="177.8" width="0.1524" layer="91"/>
<label x="22.86" y="177.8" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="IO-CHRG" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CHRG(-)"/>
<label x="73.66" y="63.5" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="68.58" y1="50.8" x2="68.58" y2="48.26" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="60.96" y1="50.8" x2="60.96" y2="48.26" width="0.1524" layer="91"/>
<wire x1="60.96" y1="48.26" x2="68.58" y2="48.26" width="0.1524" layer="91"/>
<wire x1="68.58" y1="48.26" x2="73.66" y2="48.26" width="0.1524" layer="91"/>
<junction x="68.58" y="48.26"/>
<pinref part="U$25" gate="G$1" pin="P$1"/>
<wire x1="73.66" y1="48.26" x2="86.36" y2="48.26" width="0.1524" layer="91"/>
<wire x1="73.66" y1="48.26" x2="73.66" y2="63.5" width="0.1524" layer="91"/>
<junction x="73.66" y="48.26"/>
</segment>
<segment>
<wire x1="25.4" y1="185.42" x2="22.86" y2="185.42" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="P0.01/AIN2"/>
<label x="22.86" y="185.42" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="IC1" gate="G$1" pin="VCC"/>
<wire x1="78.74" y1="53.34" x2="81.28" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="81.28" y1="53.34" x2="83.82" y2="53.34" width="0.1524" layer="91"/>
<wire x1="83.82" y1="53.34" x2="83.82" y2="55.88" width="0.1524" layer="91"/>
<wire x1="83.82" y1="55.88" x2="86.36" y2="55.88" width="0.1524" layer="91"/>
<wire x1="81.28" y1="53.34" x2="81.28" y2="50.8" width="0.1524" layer="91"/>
<junction x="81.28" y="53.34"/>
</segment>
</net>
<net name="VDD-BLE" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="60.96" y1="60.96" x2="60.96" y2="63.5" width="0.1524" layer="91"/>
<label x="60.96" y="63.5" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IC8" gate="G$1" pin="VOUT"/>
<wire x1="213.36" y1="58.42" x2="215.9" y2="58.42" width="0.1524" layer="91"/>
<wire x1="215.9" y1="58.42" x2="215.9" y2="60.96" width="0.1524" layer="91"/>
<wire x1="215.9" y1="55.88" x2="215.9" y2="58.42" width="0.1524" layer="91"/>
<junction x="215.9" y="58.42"/>
<pinref part="C27" gate="G$1" pin="2"/>
<label x="215.9" y="60.96" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="83.82" y1="177.8" x2="91.44" y2="177.8" width="0.1524" layer="91"/>
<wire x1="91.44" y1="177.8" x2="91.44" y2="175.26" width="0.1524" layer="91"/>
<wire x1="81.28" y1="177.8" x2="83.82" y2="177.8" width="0.1524" layer="91"/>
<junction x="83.82" y="177.8"/>
<wire x1="83.82" y1="177.8" x2="83.82" y2="182.88" width="0.1524" layer="91"/>
<wire x1="83.82" y1="182.88" x2="83.82" y2="185.42" width="0.1524" layer="91"/>
<wire x1="81.28" y1="182.88" x2="83.82" y2="182.88" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="83.82" y1="175.26" x2="83.82" y2="177.8" width="0.1524" layer="91"/>
<junction x="83.82" y="177.8"/>
<junction x="83.82" y="182.88"/>
<pinref part="IC6" gate="G$1" pin="AVDD1"/>
<pinref part="IC6" gate="G$1" pin="VDD1"/>
<label x="83.82" y="185.42" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="15.24" y1="88.9" x2="15.24" y2="86.36" width="0.1524" layer="91"/>
<label x="15.24" y="86.36" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="IO-PULLUP" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="68.58" y1="60.96" x2="68.58" y2="63.5" width="0.1524" layer="91"/>
<label x="68.58" y="63.5" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="25.4" y1="114.3" x2="22.86" y2="114.3" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="P0.29"/>
<label x="22.86" y="114.3" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<wire x1="149.86" y1="157.48" x2="154.94" y2="157.48" width="0.1524" layer="91"/>
<label x="149.86" y="157.48" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PC4(ADC4)"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="SDA"/>
<wire x1="292.1" y1="137.16" x2="294.64" y2="137.16" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="SDA"/>
<junction x="292.1" y="137.16"/>
<label x="294.64" y="137.16" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<wire x1="149.86" y1="154.94" x2="154.94" y2="154.94" width="0.1524" layer="91"/>
<label x="149.86" y="154.94" size="1.27" layer="95" font="vector"/>
<pinref part="IC5" gate="G$1" pin="PC5(ADC5)"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="SCL"/>
<wire x1="292.1" y1="132.08" x2="294.64" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="SCL"/>
<junction x="292.1" y="132.08"/>
<label x="294.64" y="132.08" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="AREF" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="AREF"/>
<pinref part="C11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="IO-VBAT" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="170.18" y1="58.42" x2="172.72" y2="58.42" width="0.1524" layer="91"/>
<wire x1="172.72" y1="58.42" x2="172.72" y2="60.96" width="0.1524" layer="91"/>
<label x="172.72" y="60.96" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="172.72" y1="53.34" x2="172.72" y2="55.88" width="0.1524" layer="91"/>
<junction x="172.72" y="58.42"/>
<pinref part="U$27" gate="G$1" pin="P$1"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="172.72" y1="55.88" x2="172.72" y2="58.42" width="0.1524" layer="91"/>
<wire x1="160.02" y1="55.88" x2="172.72" y2="55.88" width="0.1524" layer="91"/>
<junction x="172.72" y="55.88"/>
</segment>
<segment>
<wire x1="25.4" y1="182.88" x2="22.86" y2="182.88" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="P0.02/AIN3"/>
<label x="22.86" y="182.88" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VOUT" class="0">
<segment>
<wire x1="96.52" y1="27.94" x2="93.98" y2="27.94" width="0.1524" layer="91"/>
<wire x1="93.98" y1="27.94" x2="86.36" y2="27.94" width="0.1524" layer="91"/>
<wire x1="86.36" y1="27.94" x2="83.82" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="93.98" y1="25.4" x2="93.98" y2="27.94" width="0.1524" layer="91"/>
<junction x="93.98" y="27.94"/>
<label x="83.82" y="27.94" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="C36" gate="G$1" pin="+"/>
<wire x1="86.36" y1="25.4" x2="86.36" y2="27.94" width="0.1524" layer="91"/>
<junction x="86.36" y="27.94"/>
<pinref part="L5" gate="G$1" pin="1"/>
<wire x1="96.52" y1="25.4" x2="96.52" y2="27.94" width="0.1524" layer="91"/>
<wire x1="104.14" y1="20.32" x2="104.14" y2="25.4" width="0.1524" layer="91"/>
<wire x1="104.14" y1="25.4" x2="96.52" y2="25.4" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="VBAT"/>
<wire x1="106.68" y1="20.32" x2="104.14" y2="20.32" width="0.1524" layer="91"/>
<junction x="96.52" y="27.94"/>
</segment>
<segment>
<pinref part="T2" gate="G$1" pin="S"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="30.48" y1="30.48" x2="33.02" y2="30.48" width="0.1524" layer="91"/>
<junction x="33.02" y="30.48"/>
<wire x1="33.02" y1="27.94" x2="33.02" y2="30.48" width="0.1524" layer="91"/>
<junction x="33.02" y="30.48"/>
<wire x1="40.64" y1="30.48" x2="38.1" y2="30.48" width="0.1524" layer="91"/>
<label x="40.64" y="30.48" size="1.27" layer="95" font="vector" xref="yes"/>
<wire x1="38.1" y1="30.48" x2="33.02" y2="30.48" width="0.1524" layer="91"/>
<wire x1="48.26" y1="25.4" x2="38.1" y2="25.4" width="0.1524" layer="91"/>
<wire x1="38.1" y1="25.4" x2="38.1" y2="30.48" width="0.1524" layer="91"/>
<junction x="38.1" y="30.48"/>
<wire x1="48.26" y1="25.4" x2="48.26" y2="17.78" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$2" pin="B"/>
<wire x1="48.26" y1="17.78" x2="53.34" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="81.28" y1="152.4" x2="83.82" y2="152.4" width="0.1524" layer="91"/>
<wire x1="83.82" y1="152.4" x2="83.82" y2="149.86" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="DEC2"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="81.28" y1="154.94" x2="91.44" y2="154.94" width="0.1524" layer="91"/>
<wire x1="91.44" y1="154.94" x2="91.44" y2="149.86" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="DEC1"/>
</segment>
</net>
<net name="XTAL1" class="0">
<segment>
<wire x1="81.28" y1="109.22" x2="91.44" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C44" gate="G$1" pin="1"/>
<pinref part="IC6" gate="G$1" pin="XC1"/>
<pinref part="Y2" gate="G$1" pin="1"/>
<wire x1="91.44" y1="109.22" x2="93.98" y2="109.22" width="0.1524" layer="91"/>
<wire x1="91.44" y1="106.68" x2="91.44" y2="109.22" width="0.1524" layer="91"/>
<junction x="91.44" y="109.22"/>
</segment>
</net>
<net name="XTAL2" class="0">
<segment>
<wire x1="81.28" y1="106.68" x2="83.82" y2="106.68" width="0.1524" layer="91"/>
<wire x1="83.82" y1="106.68" x2="83.82" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="83.82" y1="93.98" x2="91.44" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="XC2"/>
<pinref part="Y2" gate="G$1" pin="3"/>
<wire x1="91.44" y1="93.98" x2="93.98" y2="93.98" width="0.1524" layer="91"/>
<wire x1="91.44" y1="96.52" x2="91.44" y2="93.98" width="0.1524" layer="91"/>
<junction x="91.44" y="93.98"/>
</segment>
</net>
<net name="SWDCLK" class="0">
<segment>
<wire x1="25.4" y1="101.6" x2="22.86" y2="101.6" width="0.1524" layer="91"/>
<wire x1="22.86" y1="101.6" x2="22.86" y2="99.06" width="0.1524" layer="91"/>
<junction x="22.86" y="101.6"/>
<pinref part="IC6" gate="G$1" pin="SWDCLK"/>
<pinref part="R20" gate="G$1" pin="2"/>
<label x="12.7" y="101.6" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="U$7" gate="G$1" pin="P$1"/>
<wire x1="22.86" y1="101.6" x2="12.7" y2="101.6" width="0.1524" layer="91"/>
<junction x="22.86" y="101.6"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<wire x1="25.4" y1="104.14" x2="15.24" y2="104.14" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="SWDIO/NRESET"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="15.24" y1="104.14" x2="12.7" y2="104.14" width="0.1524" layer="91"/>
<wire x1="15.24" y1="99.06" x2="15.24" y2="104.14" width="0.1524" layer="91"/>
<junction x="15.24" y="104.14"/>
<label x="12.7" y="104.14" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="U$10" gate="G$1" pin="P$1"/>
<junction x="15.24" y="104.14"/>
</segment>
</net>
<net name="XL2" class="0">
<segment>
<wire x1="25.4" y1="121.92" x2="22.86" y2="121.92" width="0.1524" layer="91"/>
<wire x1="22.86" y1="121.92" x2="22.86" y2="134.62" width="0.1524" layer="91"/>
<wire x1="22.86" y1="134.62" x2="15.24" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C47" gate="G$1" pin="2"/>
<wire x1="15.24" y1="132.08" x2="15.24" y2="134.62" width="0.1524" layer="91"/>
<wire x1="15.24" y1="134.62" x2="12.7" y2="134.62" width="0.1524" layer="91"/>
<junction x="15.24" y="134.62"/>
<pinref part="Y4" gate="G$1" pin="1"/>
<pinref part="IC6" gate="G$1" pin="P0.26/AIN0"/>
</segment>
</net>
<net name="XL1" class="0">
<segment>
<wire x1="25.4" y1="119.38" x2="15.24" y2="119.38" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="P0.27/AIN1"/>
<pinref part="C46" gate="G$1" pin="1"/>
<pinref part="Y4" gate="G$1" pin="2"/>
<wire x1="12.7" y1="119.38" x2="15.24" y2="119.38" width="0.1524" layer="91"/>
<wire x1="15.24" y1="119.38" x2="15.24" y2="121.92" width="0.1524" layer="91"/>
<junction x="15.24" y="119.38"/>
</segment>
</net>
<net name="VDD_PA" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="VDD-PA"/>
<wire x1="81.28" y1="127" x2="99.06" y2="127" width="0.1524" layer="91"/>
<wire x1="99.06" y1="127" x2="99.06" y2="124.46" width="0.1524" layer="91"/>
<wire x1="99.06" y1="124.46" x2="101.6" y2="124.46" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="101.6" y1="124.46" x2="101.6" y2="127" width="0.1524" layer="91"/>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="101.6" y1="121.92" x2="101.6" y2="124.46" width="0.1524" layer="91"/>
<junction x="101.6" y="124.46"/>
</segment>
</net>
<net name="ANT1" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="ANT2"/>
<wire x1="81.28" y1="137.16" x2="96.52" y2="137.16" width="0.1524" layer="91"/>
<wire x1="96.52" y1="137.16" x2="96.52" y2="154.94" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="96.52" y1="154.94" x2="101.6" y2="154.94" width="0.1524" layer="91"/>
<wire x1="101.6" y1="154.94" x2="101.6" y2="152.4" width="0.1524" layer="91"/>
<wire x1="101.6" y1="154.94" x2="104.14" y2="154.94" width="0.1524" layer="91"/>
<junction x="101.6" y="154.94"/>
<pinref part="C49" gate="G$1" pin="2"/>
</segment>
</net>
<net name="ANT2" class="0">
<segment>
<wire x1="81.28" y1="134.62" x2="99.06" y2="134.62" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="ANT1"/>
<wire x1="99.06" y1="134.62" x2="99.06" y2="139.7" width="0.1524" layer="91"/>
<wire x1="99.06" y1="139.7" x2="101.6" y2="139.7" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="101.6" y1="139.7" x2="101.6" y2="142.24" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="101.6" y1="137.16" x2="101.6" y2="139.7" width="0.1524" layer="91"/>
<junction x="101.6" y="139.7"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="C49" gate="G$1" pin="1"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="109.22" y1="154.94" x2="111.76" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C50" gate="G$1" pin="1"/>
<wire x1="111.76" y1="154.94" x2="114.3" y2="154.94" width="0.1524" layer="91"/>
<wire x1="111.76" y1="149.86" x2="111.76" y2="154.94" width="0.1524" layer="91"/>
<junction x="111.76" y="154.94"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="124.46" y1="154.94" x2="127" y2="154.94" width="0.1524" layer="91"/>
<wire x1="129.54" y1="154.94" x2="127" y2="154.94" width="0.1524" layer="91"/>
<junction x="127" y="154.94"/>
<pinref part="C51" gate="G$1" pin="1"/>
<wire x1="127" y1="149.86" x2="127" y2="154.94" width="0.1524" layer="91"/>
<pinref part="ANT1" gate="G$1" pin="1"/>
<junction x="129.54" y="154.94"/>
</segment>
</net>
<net name="NRF-LED1" class="0">
<segment>
<wire x1="132.08" y1="111.76" x2="132.08" y2="109.22" width="0.1524" layer="91"/>
<label x="132.08" y="111.76" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<pinref part="R25" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="P0.15"/>
<wire x1="25.4" y1="149.86" x2="22.86" y2="149.86" width="0.1524" layer="91"/>
<wire x1="22.86" y1="149.86" x2="17.78" y2="144.78" width="0.1524" layer="91"/>
<wire x1="17.78" y1="144.78" x2="15.24" y2="144.78" width="0.1524" layer="91"/>
<label x="15.24" y="144.78" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NRF-LED2" class="0">
<segment>
<wire x1="124.46" y1="111.76" x2="124.46" y2="109.22" width="0.1524" layer="91"/>
<label x="124.46" y="111.76" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<pinref part="R26" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="P0.14"/>
<wire x1="25.4" y1="152.4" x2="22.86" y2="152.4" width="0.1524" layer="91"/>
<wire x1="22.86" y1="152.4" x2="17.78" y2="147.32" width="0.1524" layer="91"/>
<wire x1="17.78" y1="147.32" x2="15.24" y2="147.32" width="0.1524" layer="91"/>
<label x="15.24" y="147.32" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VDD-CORE" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="182.88" y1="195.58" x2="198.12" y2="195.58" width="0.1524" layer="91"/>
<wire x1="198.12" y1="195.58" x2="198.12" y2="190.5" width="0.1524" layer="91"/>
<wire x1="200.66" y1="187.96" x2="200.66" y2="190.5" width="0.1524" layer="91"/>
<wire x1="200.66" y1="190.5" x2="208.28" y2="190.5" width="0.1524" layer="91"/>
<wire x1="198.12" y1="187.96" x2="200.66" y2="187.96" width="0.1524" layer="91"/>
<wire x1="200.66" y1="185.42" x2="200.66" y2="187.96" width="0.1524" layer="91"/>
<wire x1="198.12" y1="185.42" x2="200.66" y2="185.42" width="0.1524" layer="91"/>
<wire x1="200.66" y1="182.88" x2="200.66" y2="185.42" width="0.1524" layer="91"/>
<wire x1="198.12" y1="182.88" x2="200.66" y2="182.88" width="0.1524" layer="91"/>
<junction x="200.66" y="187.96"/>
<junction x="200.66" y="185.42"/>
<junction x="200.66" y="187.96"/>
<pinref part="IC5" gate="G$1" pin="VCC1"/>
<pinref part="IC5" gate="G$1" pin="VCC2"/>
<pinref part="IC5" gate="G$1" pin="AVCC"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="205.74" y1="160.02" x2="205.74" y2="167.64" width="0.1524" layer="91"/>
<wire x1="205.74" y1="167.64" x2="205.74" y2="175.26" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="208.28" y1="167.64" x2="205.74" y2="167.64" width="0.1524" layer="91"/>
<junction x="205.74" y="167.64"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="208.28" y1="175.26" x2="205.74" y2="175.26" width="0.1524" layer="91"/>
<junction x="200.66" y="190.5"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="208.28" y1="160.02" x2="205.74" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="+"/>
<wire x1="205.74" y1="175.26" x2="205.74" y2="182.88" width="0.1524" layer="91"/>
<wire x1="205.74" y1="182.88" x2="208.28" y2="182.88" width="0.1524" layer="91"/>
<junction x="205.74" y="175.26"/>
<pinref part="U$15" gate="G$1" pin="P$1"/>
<junction x="205.74" y="182.88"/>
<wire x1="205.74" y1="182.88" x2="200.66" y2="182.88" width="0.1524" layer="91" style="longdash"/>
<junction x="200.66" y="182.88"/>
<wire x1="198.12" y1="190.5" x2="200.66" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="NRF-LED3" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="P0.13"/>
<wire x1="25.4" y1="154.94" x2="22.86" y2="154.94" width="0.1524" layer="91"/>
<wire x1="22.86" y1="154.94" x2="17.78" y2="149.86" width="0.1524" layer="91"/>
<wire x1="17.78" y1="149.86" x2="15.24" y2="149.86" width="0.1524" layer="91"/>
<label x="15.24" y="149.86" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="109.22" y1="111.76" x2="109.22" y2="109.22" width="0.1524" layer="91"/>
<label x="109.22" y="111.76" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<pinref part="R10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="NRF-LED4" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="P0.12"/>
<wire x1="25.4" y1="157.48" x2="22.86" y2="157.48" width="0.1524" layer="91"/>
<wire x1="22.86" y1="157.48" x2="17.78" y2="152.4" width="0.1524" layer="91"/>
<wire x1="17.78" y1="152.4" x2="15.24" y2="152.4" width="0.1524" layer="91"/>
<label x="15.24" y="152.4" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="116.84" y1="111.76" x2="116.84" y2="109.22" width="0.1524" layer="91"/>
<label x="116.84" y="111.76" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<pinref part="R9" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="IC7" gate="G$1" pin="BYPASS"/>
<wire x1="180.34" y1="15.24" x2="177.8" y2="15.24" width="0.1524" layer="91"/>
<wire x1="177.8" y1="15.24" x2="177.8" y2="12.7" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="PB7(XTAL2)"/>
<pinref part="Y3" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="PB6(XTAL1)"/>
<pinref part="Y3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N-D0" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="IC6" gate="G$1" pin="P0.09"/>
<wire x1="25.4" y1="165.1" x2="22.86" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N-D1" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="IC6" gate="G$1" pin="P0.10"/>
<wire x1="20.32" y1="162.56" x2="20.32" y2="160.02" width="0.1524" layer="91"/>
<wire x1="20.32" y1="160.02" x2="17.78" y2="160.02" width="0.1524" layer="91"/>
<wire x1="25.4" y1="162.56" x2="20.32" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N-RST" class="0">
<segment>
<wire x1="20.32" y1="170.18" x2="17.78" y2="170.18" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="IC6" gate="G$1" pin="P0.08"/>
<wire x1="25.4" y1="167.64" x2="20.32" y2="167.64" width="0.1524" layer="91"/>
<wire x1="20.32" y1="167.64" x2="20.32" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO-VDD" class="0">
<segment>
<label x="22.86" y="172.72" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="IC6" gate="G$1" pin="P0.06/AIN7"/>
<wire x1="25.4" y1="172.72" x2="22.86" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<label x="12.7" y="7.62" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<wire x1="15.24" y1="7.62" x2="12.7" y2="7.62" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="30.48" y1="7.62" x2="30.48" y2="5.08" width="0.1524" layer="91"/>
<wire x1="30.48" y1="5.08" x2="15.24" y2="5.08" width="0.1524" layer="91"/>
<wire x1="15.24" y1="5.08" x2="15.24" y2="7.62" width="0.1524" layer="91"/>
<junction x="15.24" y="7.62"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="127" y1="99.06" x2="124.46" y2="99.06" width="0.1524" layer="91"/>
<pinref part="LED1" gate="LED$1" pin="2"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<pinref part="LED1" gate="LED$1" pin="4"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="LED2" gate="LED$1" pin="2"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="LED2" gate="LED$1" pin="4"/>
<wire x1="111.76" y1="99.06" x2="109.22" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="L5" gate="G$1" pin="2"/>
<pinref part="IC2" gate="G$1" pin="LX1"/>
<pinref part="IC2" gate="G$1" pin="LX2"/>
<wire x1="106.68" y1="25.4" x2="106.68" y2="27.94" width="0.1524" layer="91"/>
<junction x="106.68" y="27.94"/>
</segment>
</net>
<net name="SW" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="ILM01"/>
<wire x1="106.68" y1="17.78" x2="101.6" y2="17.78" width="0.1524" layer="91"/>
<label x="101.6" y="17.78" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="25.4" y1="7.62" x2="27.94" y2="7.62" width="0.1524" layer="91"/>
<wire x1="48.26" y1="10.16" x2="27.94" y2="10.16" width="0.1524" layer="91"/>
<wire x1="27.94" y1="10.16" x2="27.94" y2="7.62" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$2" pin="S"/>
<wire x1="53.34" y1="15.24" x2="48.26" y2="15.24" width="0.1524" layer="91"/>
<wire x1="48.26" y1="15.24" x2="48.26" y2="10.16" width="0.1524" layer="91"/>
<label x="48.26" y="15.24" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="144.78" y1="20.32" x2="144.78" y2="17.78" width="0.1524" layer="91"/>
<wire x1="137.16" y1="17.78" x2="137.16" y2="15.24" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="R19" gate="G$1" pin="2"/>
<junction x="137.16" y="17.78"/>
<pinref part="IC2" gate="G$1" pin="FB"/>
<wire x1="132.08" y1="17.78" x2="137.16" y2="17.78" width="0.1524" layer="91"/>
<wire x1="144.78" y1="17.78" x2="137.16" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="P0.03/AIN4"/>
<wire x1="25.4" y1="180.34" x2="22.86" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
