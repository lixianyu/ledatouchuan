#include "app_timer.h"
#include "mimas_bsp.h"
#include "config.h"
#include "leda_communicate_protocol.h"
#include "nrf_error.h"
#include <app_scheduler.h>
#include "app_uart.h"
#include "ble_tps.h"
#include "tethys_led.h"
#include "leda_file.h"
#include "leda_avr.h"
#include "leda_queue.h"
#include "leda_pmu.h"
#include "mimas_run_in_ram.h"

extern bool leda_if_connected(void);
extern void leda_disconnect_now(void);
extern void getMacASCII(uint8_t *serialNumber);
extern tethys_ble_nus_t m_tethys_nus;
extern leda_ble_nus_t m_leda_nus;
extern uint8_t g_pstorage_buffer[32];
extern int8_t g_device_tx_power;
extern ble_tps_t                        m_tps;


static bool Leda_AT_CMD_Handle(uint8_t *pBuffer, uint16_t length);
uint8_t gOTA = 1;
uint32_t gBaudrate = UART_BAUDRATE_BAUDRATE_Baud115200;
bool gEndByApp = false;
bool gLedaIsTransmittingBin = false;
APP_TIMER_DEF(m_uart_timer_id);
bool gHaveUpdateCoonParamToMin = false;

#define BUILD_UINT32(Byte0, Byte1, Byte2, Byte3) \
          ((uint32_t)((uint32_t)((Byte0) & 0x00FF) \
          + ((uint32_t)((Byte1) & 0x00FF) << 8) \
          + ((uint32_t)((Byte2) & 0x00FF) << 16) \
          + ((uint32_t)((Byte3) & 0x00FF) << 24)))

#define MD_BLOCK_SIZE         16

static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
static uint8_t index = 0;
static void uart_timeout_handler(void *p_context)
{
    #if 0
    gUartIfTimeOut = true;
    #else
    if (leda_if_connected())
    {
        tethys_ble_nus_string_send(&m_tethys_nus, data_array, index);
    }
    else
    {
        Leda_AT_CMD_Handle(data_array, index);
    }
    index = 0;
    #endif
}

void leda_init_communicate(void)
{
    app_timer_create(&m_uart_timer_id,
                      APP_TIMER_MODE_SINGLE_SHOT,
                      uart_timeout_handler);
    
}

bool is_all_number(char *pBuf, uint8_t len)
{
    for (int i = 0; i < len; i++)
    {
        if (*pBuf < '1' || *pBuf > '9')
        {
            return false;
        }
    }
    return true;
}

extern uint8_t m_beacon_info[];
/*
 * 1个tick是1/32768秒(30.517578125 us)
 */
#define TICK_OF_20_MS    656
//#define TICK_OF_20_MS    556
/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to 
 *          a string. The string will be be sent over BLE when the last character received was a 
 *          'new line' i.e '\n' (hex 0x0D) or if the string has reached a length of 
 *          @ref NUS_MAX_DATA_LENGTH.
 */
/**@snippet [Handling the data received over UART] */
static void uart_event_handle(app_uart_evt_t * p_event)
{    
    //static uint32_t old_tick = 0;
    //uint32_t new_tick = 0;
    //uint32_t tick_diff;
    //uint32_t       err_code;

    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            if (index == 0)
            {
                app_timer_start(m_uart_timer_id, APP_TIMER_TICKS(12, 0), NULL);
            }
            app_uart_get(&data_array[index]);
            index++;

            if (index >= (BLE_NUS_MAX_DATA_LEN))
            {
                app_timer_stop(m_uart_timer_id);
                if (leda_if_connected())
                {
                    tethys_ble_nus_string_send(&m_tethys_nus, data_array, index);
                }
                else
                {
                    Leda_AT_CMD_Handle(data_array, index);
                }
                index = 0;
            }
            break;

        case APP_UART_COMMUNICATION_ERROR:
            //APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            //APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}

static inline void Leda_uart_send(uint8_t *data, uint16_t length)
{
    for (uint8_t i = 0; i < length; i++)
    {
        while(app_uart_put(data[i]) != NRF_SUCCESS);
    }
}

// 字符串对比
static inline uint8_t str_cmp(uint8_t *p1, uint8_t *p2, uint8_t len)
{
    uint8_t i = 0;
    while(i < len)
    {
        if(p1[i] != p2[i])
            return 0;
        i++;
    }
    return 1;
}

static bool Leda_AT_CMD_Handle(uint8_t *pBuffer, uint16_t length)
{
    bool ret = true;
    char strTemp[64] = {0};
    //uint8_t i;
    //uint8_t temp8;
    //bool restart = false;
    //open_LED(1);
    // 1、测试
    if((length == 4) && str_cmp(pBuffer, "AT\r\n", 4))//AT
    {
        //open_LED(2);
        strcpy(strTemp, "OK\r\n");
        //sprintf(strTemp, "OK\r\n");
        //HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, strlen(strTemp));
        Leda_uart_send((uint8_t*)strTemp, strlen(strTemp));
    }
    else if (str_cmp(pBuffer, "AT", 2))
    {
        sprintf(strTemp, "OK : %s\r\n", SW_REV_STR);
        Leda_uart_send((uint8_t*)strTemp, strlen(strTemp));
    }
    else
    {
        ret = false;
    }
    return ret;
}

#define LEDA_UART_RX_BUF_SIZE    128
#define LEDA_UART_TX_BUF_SIZE    128
void leda_uart_init(void)
{
    //avr_uart_rx_timeout_start();
    #if 1
    uint32_t err_code = NRF_SUCCESS;
    const app_uart_comm_params_t comm_params =
    {
        LEDA_RX_PIN,
        LEDA_TX_PIN,
        0xFF,
        0xFF,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        gBaudrate
    };

    APP_UART_FIFO_INIT( &comm_params,
                        LEDA_UART_RX_BUF_SIZE,
                        LEDA_UART_TX_BUF_SIZE,
                        uart_event_handle,
                        APP_IRQ_PRIORITY_LOW,
                        err_code);
    APP_ERROR_CHECK(err_code);
    #endif
}

void leda_uart_uninit(void)
{
#if 1
    app_uart_close();

    NRF_UART0->TASKS_STOPTX = 1;
    NRF_UART0->TASKS_STOPRX = 1;
    NRF_UART0->ENABLE = 0;
#endif
    //avr_uart_rx_timeout_stop();
}

//收到东西了，传给UART
void L1_receive_data_uart(tethys_ble_nus_t *p_nus, uint8_t *data, uint16_t length)
{
    if (p_nus->is_notification_enabled)
    {
        for (uint8_t i = 0; i < length; i++)
        {
            while(app_uart_put(data[i]) != NRF_SUCCESS);
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////
// return 1, can OTA.
uint8_t tethys_get_OTA(void)
{
    return gOTA;
}

static void command_error(void)
{
    strcpy((char*)g_pstorage_buffer, "Error.");
    leda_ble_nus_string_send(&m_leda_nus, g_pstorage_buffer, 14);
}

static void command_ok(void)
{
    strcpy((char*)g_pstorage_buffer, "OK.");
    leda_ble_nus_string_send(&m_leda_nus, g_pstorage_buffer, 11);
}

static void resolve_OTA_Tethys(uint8_t *pBuf)
{
    switch (pBuf[2]) //key value
    {
        case 0x01://打开 OTA
        {
            gOTA = 1;
            strcpy((char*)g_pstorage_buffer, "OTA opened.");
            leda_ble_nus_string_send(&m_leda_nus, g_pstorage_buffer, 11);
        }
        break;
        
        case 0x02://关闭 OTA
        {
            gOTA = 0;
            strcpy((char*)g_pstorage_buffer, "OTA closed.");
            leda_ble_nus_string_send(&m_leda_nus, g_pstorage_buffer, 11);
        }
        break;
        
        default:
            command_error();
            break;
    }
}

static bool resolve_baudrate_command(uint8_t *data, uint16_t length)
{
    bool baudsetflag = true;
    uint8_t command_id = data[1];
    uint32_t baudrate = UART_BAUDRATE_BAUDRATE_Baud115200;
    if (length != 2)
    {
        strcpy((char*)g_pstorage_buffer, "Set baudrate Error.");
        leda_ble_nus_string_send(&m_leda_nus, g_pstorage_buffer, 19);
        return false;
    }
    switch (command_id)
    {
    case 0x00:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud9600;
        break;
    case 0x01:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud19200;
        break;
    case 0x02:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud38400;
        break;
    case 0x03:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud57600;
        break;
    case 0x04:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud115200;
        break;
    default:
        baudsetflag = false;
        break;
    }
    if (baudsetflag)
    {
        leda_uart_uninit();
        strcpy((char*)g_pstorage_buffer, "Set baudrate sucess.");
        leda_ble_nus_string_send(&m_leda_nus, g_pstorage_buffer, 20);
        if (gBaudrate != baudrate)
        {
            leda_storage_update_baudrate();
            if (m_tethys_nus.is_notification_enabled)
            {
                app_sched_event_put(NULL, 0, (app_sched_event_handler_t)leda_uart_init);
            }
        }
    }
    return false;
}

void set_baudrate(void * p_event_data, uint16_t event_size)
{
    bool baudsetflag = true;
    uint32_t baudrate = UART_BAUDRATE_BAUDRATE_Baud115200;
    uint8_t bd = *((uint8_t *)p_event_data);
    switch (bd)
    {
    case 0x00:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud1200;
        break;
    case 0x01:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud2400;
        break;
    case 0x02:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud4800;
        break;
    case 0x03:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud9600;
        break;
    case 0x04:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud14400;
        break;
    case 0x05:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud19200;
        break;
    case 0x06:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud28800;
        break;
    case 0x07:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud38400;
        break;
    case 0x08:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud57600;
        break;
    case 0x09:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud76800;
        break;
    case 0x0A:
        //open_LED(1);
        baudrate = UART_BAUDRATE_BAUDRATE_Baud115200;
        break;
    case 0x0B:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud230400;
        break;
    case 0x0C:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud250000;
        break;
    case 0x0D:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud460800;
        break;
    case 0x0E:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud921600;
        break;
    case 0x0F:
        baudrate = UART_BAUDRATE_BAUDRATE_Baud1M;
        break;
    default:
        baudsetflag = false;
        break;
    }
    if (baudsetflag)
    {
        leda_uart_uninit();
        if (gBaudrate != baudrate)
        {
            leda_storage_update_baudrate();
            if (m_tethys_nus.is_notification_enabled)
            {
                app_sched_event_put(NULL, 0, (app_sched_event_handler_t)leda_uart_init);
            }
        }
    }
}

static void resolve_temperature_command(uint8_t *pBuf)
{
    switch (pBuf[2])
    {
        /*
        读取nRF51822温度，单位是摄氏度，误差± 4°C（Stated temperature accuracy is valid in the range 0 to 60°C.
	   Temperature accuracy outside the 0 to 60°C range is ± 8°C.）
        */
        case 0x01:
        {
            int32_t temp;
            int32_t offset = 0;
            sd_temp_get(&temp);
            temp = temp / 4;
            offset = leda_storage_get_temp_offset(temp);
            temp += offset;
            leda_ble_nus_string_send(&m_leda_nus, (uint8_t *)&temp, 4);
        }
        break;

        /*
        设置温度补偿
	     举例：AB0A0201，设置当前实际温度为1°C；
	           AB0A02FF，设置当前实际温度为-1°C；
	           AB0A021A，设置当前实际温度为26°C；
        */
        case 0x02:
        {
            int32_t temp = 100;
            sd_temp_get(&temp);
            temp = temp / 4;
            int8_t curTemp = (int8_t)pBuf[3];
            leda_storage_update_temp_offset(temp, curTemp);
            command_ok();
        }
        break;
        
        default:
            command_error();
            break;
    }
}

extern uint8_t g_percentage_batt_lvl;
extern uint16_t g_battery_voltage_mv;
static void resolve_battery_command(uint8_t *pBuf)
{
    if (pmu_is_charging())
    {
        strcpy((char*)g_pstorage_buffer, "Charging");
        leda_ble_nus_string_send(&m_leda_nus, g_pstorage_buffer, 8);
    }
    else
    {
        switch (pBuf[2])
        {
            case 0x01://读取当前电量电压
                leda_ble_nus_string_send(&m_leda_nus, (uint8_t *)&g_battery_voltage_mv, 2);
                break;
            case 0x02://读取当前电量百分比
                leda_ble_nus_string_send(&m_leda_nus, (uint8_t *)&g_percentage_batt_lvl, 1);
                break;
            default:
                break;
        }
    }
}

static void resolve_UART_command(uint8_t *pBuf)
{
    switch (pBuf[2])
    {
        case 0x01: // 打开串口透传
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)leda_uart_init);
            command_ok();
            break;
        case 0x02: // 关闭串口透传
            app_sched_event_put(NULL, 0, (app_sched_event_handler_t)leda_uart_uninit);
            command_ok();
            break;
        default:
            command_error();
            break;
    }
}

static void resolve_tx_power_command(uint8_t *pBuf)
{
    g_device_tx_power = 0x76;
    switch (pBuf[2])
    {
    case 0:
        g_device_tx_power = RADIO_TXPOWER_TXPOWER_0dBm;
        break;
    case 1:
        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Pos4dBm;
        break;
    case 2:
        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg4dBm;
        break;
    case 3:
        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg8dBm;
        break;
    case 4:
        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg12dBm;
        break;
    case 5:
        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg16dBm;
        break;
    case 6:
        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg20dBm;
        break;
    case 7:
        g_device_tx_power = RADIO_TXPOWER_TXPOWER_Neg30dBm;
        break;
    default:
        command_error();
        break;
    }
    if (g_device_tx_power != 0x76)
    {
        sd_ble_gap_tx_power_set(g_device_tx_power);
        ble_tps_tx_power_level_set(&m_tps, g_device_tx_power);
        command_ok();
    }
}

void L1_leda_receive_data(leda_ble_nus_t *p_nus, uint8_t *data, uint16_t length)
{
    if (data[0] != L1_HEADER_MAGIC)
    {
        command_error();
        return;
    }
    switch(data[1])//Command ID
    {
        case 0x00:
        case 0x01:
        case 0x02:
        case 0x03:
        case 0x04:
            resolve_baudrate_command(data, length);
            break;
        case 0x05:
            strcpy((char*)g_pstorage_buffer, SW_REV_STR);
            leda_ble_nus_string_send(&m_leda_nus, g_pstorage_buffer, 11);
            break;
        case 0x06:
            resolve_OTA_Tethys(data);
            break;
        case 0x07:
            NVIC_SystemReset();
            break;
        case 0x08: // Get mac address
            {
            memset(g_pstorage_buffer, 0, sizeof(g_pstorage_buffer));
            getMacASCII(g_pstorage_buffer);
            leda_ble_nus_string_send(&m_leda_nus, g_pstorage_buffer, strlen((char *)g_pstorage_buffer));
            }
            break;
        
        case 0x0A:
            resolve_temperature_command(data);
            break;
        case 0x0B:
            resolve_battery_command(data);
            break;
        case 0x0D:
            resolve_UART_command(data);
            break;
        case 0x0E:
            resolve_tx_power_command(data);
            break;
        default:
            strcpy((char*)g_pstorage_buffer, "Who are you??");
            leda_ble_nus_string_send(&m_leda_nus, g_pstorage_buffer, 13);
            break;
    }
}

